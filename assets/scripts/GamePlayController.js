var firebase = require('firebase');
window.VELOCITY = 250; // pixel/second
cc.Class({
    extends: cc.Component, 

    properties: {
        planetPrefab: cc.Prefab,
        planetWithGlowPrefab: cc.Prefab,
        planetWithLightPrefab: cc.Prefab,
        linePrefab: cc.Prefab,
        tailPrefab: cc.Prefab,
        lineBreakPrefab: cc.Prefab,
        circlePrefab: cc.Prefab,
        labelPrefab: cc.Prefab,
        loopBackgroundPrefab: cc.Prefab,

        planets: null,
        lightCircleTimestamps: cc.Array,

        planetParentNode: cc.Node,

        glowFrame: cc.SpriteFrame,

        soundPath: cc.AudioClip,
        handNode: cc.Node,

        isGameReady: false,
        levelEnded: false,
        levelStarted: false,

        songId: null,

        currentIdx: 0,
        currentScore: 0,
        timeLeftToStart: 0,

        time: 0,

        basePlanet: cc.Node,

        countdownLabel: cc.Label,
        scoreLabel: cc.Label,
        startNodes: cc.Node,
        backgroundParentNode: cc.Node,
        instructionText: cc.Label,


        _activatedNodes: cc.Array,
        _isTheFirstMatchDone: false,
        _winSize: null,

        _targetPlanet: null,

        _isFtuePlayed: false,

        _isFtueStarted: false,

        animation: cc.Prefab,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._winSize = cc.director.getWinSize();

        // for (var i = 0; i < 9; i++) {
            // cc.loader.loadRes("Textures/lighting-animation/" + (i+1) + ".png", cc.SpriteFrame, function(err, frame) {
            //     cc.spriteFrameCache.addSpriteFrame(frame);
            //     cc.log(frame);
            // });
        // }
    },

    start () {
        // if (!cc.sys.isNative) {
        //     firebase.initializeApp(Constants.FIRE_BASE_CONFIG);
        // }
        this.lightCircleTimestamps = [];
        this._activatedNodes = [];
        this.planets = [];
        this.prevPlanet = this.basePlanet;
        
        this.createPlanets();
        this.createBackgrounds();
        this.playStartNodeActions();

        this.node.on(cc.Node.EventType.TOUCH_START, function() {
            // in the 1st time, lightedPlanet will be Base-Planet
            if (!this.levelStarted) {
                this.startGame();
                return;
            }

            if (this.levelEnded) {
                this.resetGame();
                return;
            }

            if (this._isFtueStarted && !this._isFtuePlayed) {
                this._isFtuePlayed = true;
                this.isGameReady = true;
                this.stopFtue();
            }

            if (!this.isGameReady || !this._isFtuePlayed) {
                return;
            }

            let targetPlanet = this._activatedNodes[0];

            if (!targetPlanet) {
                return;
            }

            let circleNode = targetPlanet.getChildByTag(CIRLE_NODE_TAG);

            if (circleNode) {
                circleNode.runAction(cc.fadeTo(0.5, 0));
            }

            this.setTargetPlanet(targetPlanet);
            this._activatedNodes.splice(0, 1);

            // add light line to nearest planet
            let lightProperties = this.calculateLightProperties(targetPlanet);

            this.createLightLine(this.prevPlanet, lightProperties.angleInDegree, lightProperties.length);

            // glow planet
            let pl = cc.instantiate(this.planetWithLightPrefab);
            pl.setPosition(targetPlanet.getPosition());
            pl.runAction(cc.fadeTo(FADE_TIME, 0));
            this.planetParentNode.addChild(pl, -1);

            targetPlanet.getComponent(cc.Sprite).spriteFrame = this.glowFrame;
            // check condition and show result text
            let triggerRange = TRIGGER_TIME * VELOCITY;
            if (targetPlanet.y < (PERFECT_Y + triggerRange * 0.1) && targetPlanet.y > (PERFECT_Y - triggerRange * 0.1)) {
                this.showResult("Perfect");
                this.currentScore += 3;
            } else if (targetPlanet.y < (PERFECT_Y + triggerRange * 0.35) && targetPlanet.y > (PERFECT_Y - triggerRange * 0.35)){
                this.showResult("Great");
                this.currentScore++;
            } else {
                this.createBreakLine(this.prevPlanet, lightProperties.angleInDegree, lightProperties.length);
                this.gameOver();
            }

            // update score
            this.scoreLabel.string = this.currentScore;

            this.prevPlanet = targetPlanet;

            if (!this._isTheFirstMatchDone) {
                this._isTheFirstMatchDone = true;
            }
        }.bind(this), this.node);

    },

    createLightLine(sourcePlanet, angle, length) {
        // cc.log("planet name " + sourcePlanet.name);
        // let line = cc.instantiate(this.linePrefab);
        // let scaleX = length/line.width;
        // line.anchorX = 0;
        // line.scaleX = 0;
        // line.rotation = angle;
        // line.setPosition(sourcePlanet.getPosition());
        // sourcePlanet.parent.addChild(line, -2);

        let lineAnim = cc.instantiate(this.animation);
        lineAnim.scaleX = 0;
        let scaleX = length/lineAnim.width;
        // cc.log(length, lineAnim.scaleX);
        lineAnim.setPosition(sourcePlanet.getPosition());
        lineAnim.rotation = angle;
        // lineAnim.opacity = 0;
        sourcePlanet.parent.addChild(lineAnim, -2);

        let scaleTime = 0.15 * scaleX / 12;

        lineAnim.runAction(cc.sequence(
            cc.scaleTo(scaleTime, scaleX, 1),
            // cc.delayTime(1),
            cc.callFunc(function() {
                lineAnim.opacity = 255;
                lineAnim.getComponent(cc.Animation).play('real-lighting');
            }.bind(this))
        ));
    },

    createBreakLine(parent, angle, length) {
        let line = cc.instantiate(this.lineBreakPrefab);
        let scaleX = length/line.width;
        line.anchorX = 0;
        line.scaleX = scaleX;
        line.rotation = angle;
        parent.addChild(line, -2);
        // cc.log(scaleX);
        // let scaleTime = 0.2 / scaleX;

        // line.runAction(cc.fadeTo(FADE_TIME, 0));
    },

    createLineTail: function(tailSpeed, sourcePlanet, targetPlanet) {
        let tail = cc.instantiate(this.tailPrefab);
        tail.setPosition(sourcePlanet.getPosition());
        this.planetParentNode.addChild(tail);

        // tail.runAction(cc.sequence(
        //     cc.moveTo(tailSpeed, targetPlanet),
        //     cc.callFunc(function(obj) {
        //         obj.removeFromParent();
        //     }, tail)
        // ));
        tail.runAction(cc.moveTo(tailSpeed, targetPlanet.getPosition()))
    },

    createPlanets() {
        for (let i = 0; i < TIME_STAMP.length; i++) {
            let timestamp = TIME_STAMP[i];

            let rdmX = Math.random() * 320 + 100;
            let y = PERFECT_Y + VELOCITY * timestamp;
            

            let planet = cc.instantiate(this.planetPrefab);
            planet.setPosition(rdmX, y);
            planet.setTag(timestamp);
            this.planetParentNode.addChild(planet);
            this.planets.push(planet);
        }
    },

    createBackgrounds () {
        let loopTime = Math.ceil((PERFECT_Y + VELOCITY * TIME_STAMP[TIME_STAMP.length - 1]) / this._winSize.height);

        let lastBg = this.backgroundParentNode.getChildren()[0];

        for (let i = 0; i <= loopTime; i++) {
            let node = cc.instantiate(this.loopBackgroundPrefab);
            node.y = lastBg.y + node.height;
            this.backgroundParentNode.addChild(node);
            lastBg = node;
            // if (i % 2 !== 0) {
            //     node.scaleY = -1;
            // }
            // cc.log("createBackgrounds LOGGING i : %s, node.y : %s", i, node.y);
        }
    },

    update (dt) {
        let planets = this.planetParentNode.getChildren();
        let bgs = this.backgroundParentNode.getChildren();
        if (this.isGameReady) {
            let triggerY = PERFECT_Y + TRIGGER_TIME * VELOCITY;
            if (this._isFtuePlayed) {
                this.checkGameOver();
            }

            for (var i = 0; i < planets.length; i++) {
                if (i > 0 || this._isTheFirstMatchDone) {
                    planets[i].y -= VELOCITY * dt;
                }

                if (bgs[i]) {
                    bgs[i].y -= VELOCITY * dt / 5;
                }

                let pl = this.planets[i];

                if (pl && (pl.y <= triggerY)) {
                    this.planets.splice(i, 1);
                    this.activateNode(pl);

                    if (!this._isFtuePlayed) {
                        this.node.runAction(cc.sequence(
                            cc.delayTime(TRIGGER_TIME - 0.05),
                            cc.callFunc(function() {
                                cc.audioEngine.pause(this.songId);
                                this.isGameReady = false;
                                this.playFtue();
                            }.bind(this)),
                            cc.delayTime(0.5),
                            cc.callFunc(function() {
                                this._isFtueStarted = true;
                            }.bind(this))
                        ))
                    }
                }
            }

            if (this.planets.length == 0 && this._activatedNodes.length == 0) {
                this.clearLevel();
            }
        }

    },

    resetGame() {
        cc.audioEngine.stopAll();
        cc.director.loadScene("GamePlayScene");
    },

    gameOver() {
        cc.audioEngine.stopAll();
        this.countdownLabel.string = "YOU LOSE!\nTRY AGAIN!";
        this.isGameReady = false;
        this.levelEnded = true;
        this.countdownLabel.unscheduleAllCallbacks();
    },

    clearLevel() {
        this.isGameReady = false;
        this.countdownLabel.string = "YOU WIN!\nTAP TO REPLAY";
        this.levelEnded = true;
    },

    startCountdownLabel() {
        this.timeLeftToStart = 3;
        this.countdownLabel.schedule(this.setCountdownLabelText.bind(this), 1, 2);
    },

    setCountdownLabelText() {
        // cc.log("setCountdownLabelText()");
        if (this.timeLeftToStart <= 1) {
            this.countdownLabel.string = "";    
        } else {
            this.countdownLabel.string = --this.timeLeftToStart;
        }
    },

    activateNode(node) {
        // let glow = cc.instantiate(this.planetWithGlowPrefab);
        // glow.opacity = 0;
        // glow.setTag(GLOW_NODE_TAG);
        // node.addChild(glow, -2);

        let circle = cc.instantiate(this.circlePrefab);
        circle.scale = 0;
        circle.setTag(CIRLE_NODE_TAG);
        node.addChild(circle, -2);

        circle.runAction(cc.sequence(
            cc.scaleTo(TRIGGER_TIME, 1),
            cc.scaleTo(TRIGGER_TIME, 0)
        ));

        // glow.runAction(cc.sequence(
        //     cc.fadeTo(TRIGGER_TIME, 255),
        //     cc.fadeTo(TRIGGER_TIME, 0)
        // ));
        this._activatedNodes.push(node);
    },

    showResult (result) {
        let text = cc.instantiate(this.labelPrefab);
        text.setPosition(0, -200);
        text.getComponent(cc.Label).string = result;
        this.node.addChild(text);
        text.scale = 0;

        let scaleTo = 1;
        if (result.indexOf("Perfect") > -1) {
            scaleTo = 2;
        } else {
            text.color = cc.color(255, 255, 255, 255);
        }
        text.runAction(
            cc.spawn(
                cc.sequence(
                    cc.scaleTo(0.25, scaleTo),
                    cc.fadeTo(0.25, 0)
                ),
                cc.moveBy(0.5, 0, 30)
            )
        )
    },

    startGame () {
        this.stopStartNodeActions();
        this.levelStarted = true;
        this.startNodes.active = false;
        this.handNode.active = false;
        this.instructionText.node.active = false;

        cc.loader.loadRes("Sounds/swan-lake", cc.AudioClip, function(error, audio) {
            this.countdownLabel.string = 3;
            this.startCountdownLabel();

            this.node.runAction(
                cc.sequence(
                    cc.delayTime(3),
                    cc.callFunc(function() {
                        this.isGameReady = true;
                        this.songId = cc.audioEngine.play(audio);
                    }.bind(this))
                )
            );
        }.bind(this))
    },

    playStartNodeActions() {
        this.handNode.runAction(cc.repeatForever(
            cc.sequence(
                cc.scaleTo(0.5, 0.95),
                cc.scaleTo(0.5, 1.05)
            )
        ));

        this.basePlanet.runAction(cc.repeatForever(
            cc.sequence(
                cc.fadeTo(0.5, 180),
                cc.fadeTo(0.5, 255)
            )
        ));
    },

    stopStartNodeActions() {
        this.handNode.stopAllActions();
        this.basePlanet.stopAllActions();
        this.basePlanet.opacity = 255;
    },

    checkGameOver () {
        let targetPlanet = this._activatedNodes[0];
        if (targetPlanet) {
            let circle = targetPlanet.getChildByTag(CIRLE_NODE_TAG);
            if (circle.getNumberOfRunningActions() == 0) {
                this.gameOver();
            }
        }
    },

    calculateLightProperties (targetPlanet) {
        let vector = cc.v2(targetPlanet.x - this.prevPlanet.x, targetPlanet.y - this.prevPlanet.y);
        let angleInDegree = -cc.pToAngle(vector) * 180 / Math.PI;
        let length = cc.pLength(vector);

        return {
            "angleInDegree": angleInDegree, 
            "length": length
        }
    },

    getTargetPlanet: function() {
        return this._targetPlanet;
    },

    setTargetPlanet: function(planet) {
        this._targetPlanet = planet;
    },

    playFtue: function() {
        this.handNode.active = true;
        this.instructionText.node.active = true;

        this.handNode.runAction(cc.repeatForever(
            cc.sequence(
                cc.scaleTo(0.5, 0.95),
                cc.scaleTo(0.5, 1.05)
            )
        ));

        this.instructionText.string = "TAP NOW";

        let firstNode = this._activatedNodes[0];
        if (firstNode) {
            let circleNode = firstNode.getChildByTag(CIRLE_NODE_TAG);
            circleNode && circleNode.stopAllActions();
        }
    },

    stopFtue: function() {
        this.handNode.active = false;
        this.instructionText.node.active = false;

        cc.audioEngine.resume(this.songId);
    },
});

var TIME_STAMP = [3.286, 4.723, 5.078, 5.44, 5.794, 6.148, 7.222, 7.589, 8.651, 9.004, 
10.081, 10.432, 10.793, 11.136, 11.52, 11.903, 13.687, 14.037, 14.414, 14.737, 16.148, 
16.5, 16.863, 17.24, 17.59, 18.64, 19.003, 20.075, 20.452, 21.515, 21.851, 22.228, 
22.578, 22.928, 23.305, 25.445, 26.155, 26.863, 27.579, 28.29, 28.647, 29.005, 30.083, 
30.445, 31.161, 31.507, 31.869, 32.94, 33.294, 34.018, 34.364, 34.719, 35.789, 36.159, 
36.505, 36.876, 37.238, 37.576, 38.301, 39.014, 39.718, 40.081, 40.437, 41.507, 41.867, 
42.577, 42.937, 43.296, 44.364, 44.72, 45.433, 45.79, 46.146, 47.223, 47.583, 48.298, 
48.644, 49.004, 50.074, 50.43, 51.51, 51.86, 53.296, 53.653, 54.006, 54.369, 54.719, 
55.792, 56.146, 57.219, 57.585, 58.656, 59.009, 59.366, 59.719, 60.075, 60.439]

var PERFECT_Y = 480;
var TRIGGER_TIME = 0.5;
var CIRLE_NODE_TAG = 100001;
var GLOW_NODE_TAG = 100002;
var BUILD_VERSION = "08.01.18.00";
var FADE_TIME = 2; // time to fade as long as PERFECT_Y point 
console.log(BUILD_VERSION);
console.log(PERFECT_Y);