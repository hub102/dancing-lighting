require = function e(t, n, r) {
function i(a, s) {
if (!n[a]) {
if (!t[a]) {
var u = "function" == typeof require && require;
if (!s && u) return u(a, !0);
if (o) return o(a, !0);
var c = new Error("Cannot find module '" + a + "'");
throw c.code = "MODULE_NOT_FOUND", c;
}
var l = n[a] = {
exports: {}
};
t[a][0].call(l.exports, function(e) {
var n = t[a][1][e];
return i(n || e);
}, l, l.exports, e, t, n, r);
}
return n[a].exports;
}
for (var o = "function" == typeof require && require, a = 0; a < r.length; a++) i(r[a]);
return i;
}({
1: [ function(e, t, n) {
function r() {
throw new Error("setTimeout has not been defined");
}
function i() {
throw new Error("clearTimeout has not been defined");
}
function o(e) {
if (h === setTimeout) return setTimeout(e, 0);
if ((h === r || !h) && setTimeout) {
h = setTimeout;
return setTimeout(e, 0);
}
try {
return h(e, 0);
} catch (t) {
try {
return h.call(null, e, 0);
} catch (t) {
return h.call(this, e, 0);
}
}
}
function a(e) {
if (f === clearTimeout) return clearTimeout(e);
if ((f === i || !f) && clearTimeout) {
f = clearTimeout;
return clearTimeout(e);
}
try {
return f(e);
} catch (t) {
try {
return f.call(null, e);
} catch (t) {
return f.call(this, e);
}
}
}
function s() {
if (_ && d) {
_ = !1;
d.length ? v = d.concat(v) : y = -1;
v.length && u();
}
}
function u() {
if (!_) {
var e = o(s);
_ = !0;
for (var t = v.length; t; ) {
d = v;
v = [];
for (;++y < t; ) d && d[y].run();
y = -1;
t = v.length;
}
d = null;
_ = !1;
a(e);
}
}
function c(e, t) {
this.fun = e;
this.array = t;
}
function l() {}
var h, f, p = t.exports = {};
!function() {
try {
h = "function" == typeof setTimeout ? setTimeout : r;
} catch (e) {
h = r;
}
try {
f = "function" == typeof clearTimeout ? clearTimeout : i;
} catch (e) {
f = i;
}
}();
var d, v = [], _ = !1, y = -1;
p.nextTick = function(e) {
var t = new Array(arguments.length - 1);
if (arguments.length > 1) for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
v.push(new c(e, t));
1 !== v.length || _ || o(u);
};
c.prototype.run = function() {
this.fun.apply(null, this.array);
};
p.title = "browser";
p.browser = !0;
p.env = {};
p.argv = [];
p.version = "";
p.versions = {};
p.on = l;
p.addListener = l;
p.once = l;
p.off = l;
p.removeListener = l;
p.removeAllListeners = l;
p.emit = l;
p.prependListener = l;
p.prependOnceListener = l;
p.listeners = function(e) {
return [];
};
p.binding = function(e) {
throw new Error("process.binding is not supported");
};
p.cwd = function() {
return "/";
};
p.chdir = function(e) {
throw new Error("process.chdir is not supported");
};
p.umask = function() {
return 0;
};
}, {} ],
2: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./src/firebaseApp");
n.firebase = r.createFirebaseNamespace();
n.default = n.firebase;
}, {
"./src/firebaseApp": 3
} ],
3: [ function(e, t, n) {
"use strict";
function r() {
function e(e) {
a(l, e = e || s) || i("no-app", {
name: e
});
return l[e];
}
function t() {
return Object.keys(l).map(function(e) {
return l[e];
});
}
function n(e, t) {
Object.keys(h).forEach(function(n) {
var r = u(e, n);
null !== r && f[r] && f[r](t, e);
});
}
function u(e, t) {
if ("serverAuth" === t) return null;
var n = t;
e.options;
return n;
}
var l = {}, h = {}, f = {}, p = {
__esModule: !0,
initializeApp: function(e, t) {
void 0 === t ? t = s : "string" == typeof t && "" !== t || i("bad-app-name", {
name: t + ""
});
a(l, t) && i("duplicate-app", {
name: t
});
var r = new c(e, t, p);
l[t] = r;
n(r, "create");
return r;
},
app: e,
apps: null,
Promise: Promise,
SDK_VERSION: "4.8.1",
INTERNAL: {
registerService: function(n, r, a, s, u) {
h[n] && i("duplicate-service", {
name: n
});
h[n] = r;
if (s) {
f[n] = s;
t().forEach(function(e) {
s("create", e);
});
}
var l = function(t) {
void 0 === t && (t = e());
"function" != typeof t[n] && i("invalid-app-argument", {
name: n
});
return t[n]();
};
void 0 !== a && o.deepExtend(l, a);
p[n] = l;
c.prototype[n] = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
return this._getService.bind(this, n).apply(this, u ? e : []);
};
return l;
},
createFirebaseNamespace: r,
extendNamespace: function(e) {
o.deepExtend(p, e);
},
createSubscribe: o.createSubscribe,
ErrorFactory: o.ErrorFactory,
removeApp: function(e) {
n(l[e], "delete");
delete l[e];
},
factories: h,
useAsService: u,
Promise: Promise,
deepExtend: o.deepExtend
}
};
o.patchProperty(p, "default", p);
Object.defineProperty(p, "apps", {
get: t
});
o.patchProperty(e, "App", c);
return p;
}
function i(e, t) {
throw h.create(e, t);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var o = e("@firebase/util"), a = function(e, t) {
return Object.prototype.hasOwnProperty.call(e, t);
}, s = "[DEFAULT]", u = [], c = function() {
function e(e, t, n) {
this.firebase_ = n;
this.isDeleted_ = !1;
this.services_ = {};
this.name_ = t;
this.options_ = o.deepCopy(e);
this.INTERNAL = {
getUid: function() {
return null;
},
getToken: function() {
return Promise.resolve(null);
},
addAuthTokenListener: function(e) {
u.push(e);
setTimeout(function() {
return e(null);
}, 0);
},
removeAuthTokenListener: function(e) {
u = u.filter(function(t) {
return t !== e;
});
}
};
}
Object.defineProperty(e.prototype, "name", {
get: function() {
this.checkDestroyed_();
return this.name_;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "options", {
get: function() {
this.checkDestroyed_();
return this.options_;
},
enumerable: !0,
configurable: !0
});
e.prototype.delete = function() {
var e = this;
return new Promise(function(t) {
e.checkDestroyed_();
t();
}).then(function() {
e.firebase_.INTERNAL.removeApp(e.name_);
var t = [];
Object.keys(e.services_).forEach(function(n) {
Object.keys(e.services_[n]).forEach(function(r) {
t.push(e.services_[n][r]);
});
});
return Promise.all(t.map(function(e) {
return e.INTERNAL.delete();
}));
}).then(function() {
e.isDeleted_ = !0;
e.services_ = {};
});
};
e.prototype._getService = function(e, t) {
void 0 === t && (t = s);
this.checkDestroyed_();
this.services_[e] || (this.services_[e] = {});
if (!this.services_[e][t]) {
var n = t !== s ? t : void 0, r = this.firebase_.INTERNAL.factories[e](this, this.extendApp.bind(this), n);
this.services_[e][t] = r;
}
return this.services_[e][t];
};
e.prototype.extendApp = function(e) {
var t = this;
o.deepExtend(this, e);
if (e.INTERNAL && e.INTERNAL.addAuthTokenListener) {
u.forEach(function(e) {
t.INTERNAL.addAuthTokenListener(e);
});
u = [];
}
};
e.prototype.checkDestroyed_ = function() {
this.isDeleted_ && i("app-deleted", {
name: this.name_
});
};
return e;
}();
c.prototype.name && c.prototype.options || c.prototype.delete || console.log("dc");
n.createFirebaseNamespace = r;
var l = {
"no-app": "No Firebase App '{$name}' has been created - call Firebase App.initializeApp()",
"bad-app-name": "Illegal App name: '{$name}",
"duplicate-app": "Firebase App named '{$name}' already exists",
"app-deleted": "Firebase App named '{$name}' already deleted",
"duplicate-service": "Firebase service named '{$name}' already registered",
"sa-not-supported": "Initializing the Firebase SDK with a service account is only allowed in a Node.js environment. On client devices, you should instead initialize the SDK with an api key and auth domain",
"invalid-app-argument": "firebase.{$name}() takes either no argument or a Firebase App instance."
}, h = new o.ErrorFactory("app", "Firebase", l);
}, {
"@firebase/util": 135
} ],
4: [ function(e, t, n) {
(function(t) {
(function() {
function t(e) {
return "string" == typeof e;
}
function n(e) {
return "boolean" == typeof e;
}
function r() {}
function i(e) {
var t = typeof e;
if ("object" == t) {
if (!e) return "null";
if (e instanceof Array) return "array";
if (e instanceof Object) return t;
var n = Object.prototype.toString.call(e);
if ("[object Window]" == n) return "object";
if ("[object Array]" == n || "number" == typeof e.length && "undefined" != typeof e.splice && "undefined" != typeof e.propertyIsEnumerable && !e.propertyIsEnumerable("splice")) return "array";
if ("[object Function]" == n || "undefined" != typeof e.call && "undefined" != typeof e.propertyIsEnumerable && !e.propertyIsEnumerable("call")) return "function";
} else if ("function" == t && "undefined" == typeof e.call) return "object";
return t;
}
function o(e) {
return null === e;
}
function a(e) {
return "array" == i(e);
}
function s(e) {
var t = i(e);
return "array" == t || "object" == t && "number" == typeof e.length;
}
function u(e) {
return "function" == i(e);
}
function c(e) {
var t = typeof e;
return "object" == t && null != e || "function" == t;
}
function l(e, t, n) {
return e.call.apply(e.bind, arguments);
}
function h(e, t, n) {
if (!e) throw Error();
if (2 < arguments.length) {
var r = Array.prototype.slice.call(arguments, 2);
return function() {
var n = Array.prototype.slice.call(arguments);
Array.prototype.unshift.apply(n, r);
return e.apply(t, n);
};
}
return function() {
return e.apply(t, arguments);
};
}
function f(e, t, n) {
return (f = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? l : h).apply(null, arguments);
}
function p(e, t) {
var n = Array.prototype.slice.call(arguments, 1);
return function() {
var t = n.slice();
t.push.apply(t, arguments);
return e.apply(this, t);
};
}
function d(e, t) {
function n() {}
n.prototype = t.prototype;
e.ib = t.prototype;
e.prototype = new n();
e.prototype.constructor = e;
e.Rc = function(e, n, r) {
for (var i = Array(arguments.length - 2), o = 2; o < arguments.length; o++) i[o - 2] = arguments[o];
return t.prototype[n].apply(e, i);
};
}
function v(e) {
if (Error.captureStackTrace) Error.captureStackTrace(this, v); else {
var t = Error().stack;
t && (this.stack = t);
}
e && (this.message = String(e));
}
function _(e, t) {
for (var n = e.split("%s"), r = "", i = Array.prototype.slice.call(arguments, 1); i.length && 1 < n.length; ) r += n.shift() + i.shift();
return r + n.join("%s");
}
function y(e) {
if (!gu.test(e)) return e;
-1 != e.indexOf("&") && (e = e.replace(fu, "&amp;"));
-1 != e.indexOf("<") && (e = e.replace(pu, "&lt;"));
-1 != e.indexOf(">") && (e = e.replace(du, "&gt;"));
-1 != e.indexOf('"') && (e = e.replace(vu, "&quot;"));
-1 != e.indexOf("'") && (e = e.replace(_u, "&#39;"));
-1 != e.indexOf("\0") && (e = e.replace(yu, "&#0;"));
return e;
}
function g(e, t) {
return -1 != e.indexOf(t);
}
function m(e, t) {
return e < t ? -1 : e > t ? 1 : 0;
}
function b(e, t) {
t.unshift(e);
v.call(this, _.apply(null, t));
t.shift();
}
function E(e, t) {
throw new b("Failure" + (e ? ": " + e : ""), Array.prototype.slice.call(arguments, 1));
}
function w(e, n) {
var r = e.length, i = t(e) ? e.split("") : e;
for (--r; 0 <= r; --r) r in i && n.call(void 0, i[r], r, e);
}
function C(e) {
e: {
for (var n = wn, r = e.length, i = t(e) ? e.split("") : e, o = 0; o < r; o++) if (o in i && n.call(void 0, i[o], o, e)) {
n = o;
break e;
}
n = -1;
}
return 0 > n ? null : t(e) ? e.charAt(n) : e[n];
}
function S(e, t) {
return 0 <= bu(e, t);
}
function T(e, t) {
var n;
(n = 0 <= (t = bu(e, t))) && Array.prototype.splice.call(e, t, 1);
return n;
}
function N(e, t) {
var n = 0;
w(e, function(r, i) {
t.call(void 0, r, i, e) && 1 == Array.prototype.splice.call(e, i, 1).length && n++;
});
}
function I(e) {
return Array.prototype.concat.apply([], arguments);
}
function P(e) {
var t = e.length;
if (0 < t) {
for (var n = Array(t), r = 0; r < t; r++) n[r] = e[r];
return n;
}
return [];
}
function O(e) {
return g(mu, e);
}
function R(e, t) {
for (var n in e) t.call(void 0, e[n], n, e);
}
function A(e) {
var t, n = [], r = 0;
for (t in e) n[r++] = e[t];
return n;
}
function k(e) {
var t, n = [], r = 0;
for (t in e) n[r++] = t;
return n;
}
function D(e) {
for (var t in e) return !1;
return !0;
}
function L(e, t) {
for (var n in e) if (!(n in t) || e[n] !== t[n]) return !1;
for (n in t) if (!(n in e)) return !1;
return !0;
}
function M(e) {
var t, n = {};
for (t in e) n[t] = e[t];
return n;
}
function x(e, t) {
for (var n, r, i = 1; i < arguments.length; i++) {
r = arguments[i];
for (n in r) e[n] = r[n];
for (var o = 0; o < Nu.length; o++) n = Nu[o], Object.prototype.hasOwnProperty.call(r, n) && (e[n] = r[n]);
}
}
function F(e) {
F[" "](e);
return e;
}
function U(e, t) {
var n = Uu;
return Object.prototype.hasOwnProperty.call(n, e) ? n[e] : n[e] = t(e);
}
function j() {
var e = su.document;
return e ? e.documentMode : void 0;
}
function W(e) {
return U(e, function() {
for (var t = 0, n = hu(String(Iu)).split("."), r = hu(String(e)).split("."), i = Math.max(n.length, r.length), o = 0; 0 == t && o < i; o++) {
var a = n[o] || "", s = r[o] || "";
do {
a = /(\d*)(\D*)(.*)/.exec(a) || [ "", "", "", "" ];
s = /(\d*)(\D*)(.*)/.exec(s) || [ "", "", "", "" ];
if (0 == a[0].length && 0 == s[0].length) break;
t = m(0 == a[1].length ? 0 : parseInt(a[1], 10), 0 == s[1].length ? 0 : parseInt(s[1], 10)) || m(0 == a[2].length, 0 == s[2].length) || m(a[2], s[2]);
a = a[3];
s = s[3];
} while (0 == t);
}
return 0 <= t;
});
}
function B(e) {
e.prototype.then = e.prototype.then;
e.prototype.$goog_Thenable = !0;
}
function V(e) {
if (!e) return !1;
try {
return !!e.$goog_Thenable;
} catch (e) {
return !1;
}
}
function q(e, t, n) {
this.f = n;
this.c = e;
this.g = t;
this.b = 0;
this.a = null;
}
function K(e, t) {
e.g(t);
e.b < e.f && (e.b++, t.next = e.a, e.a = t);
}
function G() {
var e = Ku, t = null;
e.a && (t = e.a, e.a = e.a.next, e.a || (e.b = null), t.next = null);
return t;
}
function H() {
this.next = this.b = this.a = null;
}
function Q(e) {
su.setTimeout(function() {
throw e;
}, 0);
}
function X() {
var e = su.MessageChannel;
"undefined" == typeof e && "undefined" != typeof window && window.postMessage && window.addEventListener && !O("Presto") && (e = function() {
var e = document.createElement("IFRAME");
e.style.display = "none";
e.src = "";
document.documentElement.appendChild(e);
var t = e.contentWindow;
(e = t.document).open();
e.write("");
e.close();
var n = "callImmediate" + Math.random(), r = "file:" == t.location.protocol ? "*" : t.location.protocol + "//" + t.location.host;
e = f(function(e) {
"*" != r && e.origin != r || e.data != n || this.port1.onmessage();
}, this);
t.addEventListener("message", e, !1);
this.port1 = {};
this.port2 = {
postMessage: function() {
t.postMessage(n, r);
}
};
});
if ("undefined" != typeof e && !O("Trident") && !O("MSIE")) {
var t = new e(), n = {}, r = n;
t.port1.onmessage = function() {
if (void 0 !== n.next) {
var e = (n = n.next).pb;
n.pb = null;
e();
}
};
return function(e) {
r.next = {
pb: e
};
r = r.next;
t.port2.postMessage(0);
};
}
return "undefined" != typeof document && "onreadystatechange" in document.createElement("SCRIPT") ? function(e) {
var t = document.createElement("SCRIPT");
t.onreadystatechange = function() {
t.onreadystatechange = null;
t.parentNode.removeChild(t);
t = null;
e();
e = null;
};
document.documentElement.appendChild(t);
} : function(e) {
su.setTimeout(e, 0);
};
}
function Y(e, t) {
Vu || z();
qu || (Vu(), qu = !0);
var n = Ku, r = Wu.get();
r.set(e, t);
n.b ? n.b.next = r : n.a = r;
n.b = r;
}
function z() {
if (-1 != String(su.Promise).indexOf("[native code]")) {
var e = su.Promise.resolve(void 0);
Vu = function() {
e.then(J);
};
} else Vu = function() {
var e = J;
!u(su.setImmediate) || su.Window && su.Window.prototype && !O("Edge") && su.Window.prototype.setImmediate == su.setImmediate ? (Bu || (Bu = X()), 
Bu(e)) : su.setImmediate(e);
};
}
function J() {
for (var e; e = G(); ) {
try {
e.a.call(e.b);
} catch (e) {
Q(e);
}
K(Wu, e);
}
qu = !1;
}
function $(e, t) {
this.a = Gu;
this.i = void 0;
this.f = this.b = this.c = null;
this.g = this.h = !1;
if (e != r) try {
var n = this;
e.call(t, function(e) {
le(n, Hu, e);
}, function(e) {
if (!(e instanceof ge)) try {
if (e instanceof Error) throw e;
throw Error("Promise rejected.");
} catch (e) {}
le(n, Qu, e);
});
} catch (e) {
le(this, Qu, e);
}
}
function Z() {
this.next = this.f = this.b = this.g = this.a = null;
this.c = !1;
}
function ee(e, t, n) {
var r = Xu.get();
r.g = e;
r.b = t;
r.f = n;
return r;
}
function te(e) {
if (e instanceof $) return e;
var t = new $(r);
le(t, Hu, e);
return t;
}
function ne(e) {
return new $(function(t, n) {
n(e);
});
}
function re(e, t, n) {
he(e, t, n, null) || Y(p(t, e));
}
function ie(e) {
return new $(function(t, n) {
var r = e.length, i = [];
if (r) for (var o, a = 0; a < e.length; a++) o = e[a], re(o, p(function(e, n) {
r--;
i[e] = n;
0 == r && t(i);
}, a), function(e) {
n(e);
}); else t(i);
});
}
function oe(e) {
return new $(function(t) {
var n = e.length, r = [];
if (n) for (var i, o = function(e, i, o) {
n--;
r[e] = i ? {
Ub: !0,
value: o
} : {
Ub: !1,
reason: o
};
0 == n && t(r);
}, a = 0; a < e.length; a++) i = e[a], re(i, p(o, a, !0), p(o, a, !1)); else t(r);
});
}
function ae(e, t) {
(t = ee(t, t, void 0)).c = !0;
ue(e, t);
return e;
}
function se(e, t) {
if (e.a == Gu) if (e.c) {
var n = e.c;
if (n.b) {
for (var r = 0, i = null, o = null, a = n.b; a && (a.c || (r++, a.a == e && (i = a), 
!(i && 1 < r))); a = a.next) i || (o = a);
i && (n.a == Gu && 1 == r ? se(n, t) : (o ? ((r = o).next == n.f && (n.f = r), r.next = r.next.next) : de(n), 
ve(n, i, Qu, t)));
}
e.c = null;
} else le(e, Qu, t);
}
function ue(e, t) {
e.b || e.a != Hu && e.a != Qu || pe(e);
e.f ? e.f.next = t : e.b = t;
e.f = t;
}
function ce(e, t, n, r) {
var i = ee(null, null, null);
i.a = new $(function(e, o) {
i.g = t ? function(n) {
try {
var i = t.call(r, n);
e(i);
} catch (e) {
o(e);
}
} : e;
i.b = n ? function(t) {
try {
var i = n.call(r, t);
void 0 === i && t instanceof ge ? o(t) : e(i);
} catch (e) {
o(e);
}
} : o;
});
i.a.c = e;
ue(e, i);
return i.a;
}
function le(e, t, n) {
e.a == Gu && (e === n && (t = Qu, n = new TypeError("Promise cannot resolve to itself")), 
e.a = 1, he(n, e.Ac, e.Bc, e) || (e.i = n, e.a = t, e.c = null, pe(e), t != Qu || n instanceof ge || ye(e, n)));
}
function he(e, t, n, i) {
if (e instanceof $) return ue(e, ee(t || r, n || null, i)), !0;
if (V(e)) return e.then(t, n, i), !0;
if (c(e)) try {
var o = e.then;
if (u(o)) return fe(e, o, t, n, i), !0;
} catch (e) {
return n.call(i, e), !0;
}
return !1;
}
function fe(e, t, n, r, i) {
function o(e) {
a || (a = !0, r.call(i, e));
}
var a = !1;
try {
t.call(e, function(e) {
a || (a = !0, n.call(i, e));
}, o);
} catch (e) {
o(e);
}
}
function pe(e) {
e.h || (e.h = !0, Y(e.Qb, e));
}
function de(e) {
var t = null;
e.b && (t = e.b, e.b = t.next, t.next = null);
e.b || (e.f = null);
return t;
}
function ve(e, t, n, r) {
if (n == Qu && t.b && !t.c) for (;e && e.g; e = e.c) e.g = !1;
if (t.a) t.a.c = null, _e(t, n, r); else try {
t.c ? t.g.call(t.f) : _e(t, n, r);
} catch (e) {
Yu.call(null, e);
}
K(Xu, t);
}
function _e(e, t, n) {
t == Hu ? e.g.call(e.f, n) : e.b && e.b.call(e.f, n);
}
function ye(e, t) {
e.g = !0;
Y(function() {
e.g && Yu.call(null, t);
});
}
function ge(e) {
v.call(this, e);
}
function me() {
this.a = "";
this.b = Ju;
}
function be(e) {
if (e instanceof me && e.constructor === me && e.b === Ju) return e.a;
E("expected object of type Const, got '" + e + "'");
return "type_error:Const";
}
function Ee(e) {
var t = new me();
t.a = e;
return t;
}
function we() {
this.a = "";
this.b = ec;
}
function Ce(e) {
if (e instanceof we && e.constructor === we && e.b === ec) return e.a;
E("expected object of type TrustedResourceUrl, got '" + e + "' of type " + i(e));
return "type_error:TrustedResourceUrl";
}
function Se(e, t) {
e = Te(e, t);
(t = new we()).a = e;
return t;
}
function Te(e, t) {
var n = be(e);
if (!Zu.test(n)) throw Error("Invalid TrustedResourceUrl format: " + n);
return n.replace($u, function(e, r) {
if (!Object.prototype.hasOwnProperty.call(t, r)) throw Error('Found marker, "' + r + '", in format string, "' + n + '", but no valid label mapping found in args: ' + JSON.stringify(t));
return (e = t[r]) instanceof me ? be(e) : encodeURIComponent(String(e));
});
}
function Ne() {
this.a = "";
this.b = nc;
}
function Ie(e) {
if (e instanceof Ne && e.constructor === Ne && e.b === nc) return e.a;
E("expected object of type SafeUrl, got '" + e + "' of type " + i(e));
return "type_error:SafeUrl";
}
function Pe(e) {
if (e instanceof Ne) return e;
e = e.la ? e.ja() : String(e);
tc.test(e) || (e = "about:invalid#zClosurez");
return Oe(e);
}
function Oe(e) {
var t = new Ne();
t.a = e;
return t;
}
function Re() {
this.a = "";
this.b = rc;
}
function Ae(e) {
if (e instanceof Re && e.constructor === Re && e.b === rc) return e.a;
E("expected object of type SafeHtml, got '" + e + "' of type " + i(e));
return "type_error:SafeHtml";
}
function ke(e) {
var t = new Re();
t.a = e;
return t;
}
function De(e) {
var n = document;
return t(e) ? n.getElementById(e) : e;
}
function Le(e, t) {
R(t, function(t, n) {
t && t.la && (t = t.ja());
"style" == n ? e.style.cssText = t : "class" == n ? e.className = t : "for" == n ? e.htmlFor = t : ic.hasOwnProperty(n) ? e.setAttribute(ic[n], t) : 0 == n.lastIndexOf("aria-", 0) || 0 == n.lastIndexOf("data-", 0) ? e.setAttribute(n, t) : e[n] = t;
});
}
function Me(e, n, r) {
var i = arguments, o = document, s = String(i[0]), u = i[1];
if (!zu && u && (u.name || u.type)) {
s = [ "<", s ];
u.name && s.push(' name="', y(u.name), '"');
if (u.type) {
s.push(' type="', y(u.type), '"');
var c = {};
x(c, u);
delete c.type;
u = c;
}
s.push(">");
s = s.join("");
}
s = o.createElement(s);
u && (t(u) ? s.className = u : a(u) ? s.className = u.join(" ") : Le(s, u));
2 < i.length && xe(o, s, i);
return s;
}
function xe(e, n, r) {
function i(r) {
r && n.appendChild(t(r) ? e.createTextNode(r) : r);
}
for (var o = 2; o < r.length; o++) {
var a = r[o];
!s(a) || c(a) && 0 < a.nodeType ? i(a) : Eu(Fe(a) ? P(a) : a, i);
}
}
function Fe(e) {
if (e && "number" == typeof e.length) {
if (c(e)) return "function" == typeof e.item || "string" == typeof e.item;
if (u(e)) return "function" == typeof e.item;
}
return !1;
}
function Ue(e) {
var t = [];
We(new je(), e, t);
return t.join("");
}
function je() {}
function We(e, t, n) {
if (null == t) n.push("null"); else {
if ("object" == typeof t) {
if (a(t)) {
var r = t;
t = r.length;
n.push("[");
for (var i = "", o = 0; o < t; o++) n.push(i), We(e, r[o], n), i = ",";
n.push("]");
return;
}
if (!(t instanceof String || t instanceof Number || t instanceof Boolean)) {
n.push("{");
i = "";
for (r in t) Object.prototype.hasOwnProperty.call(t, r) && "function" != typeof (o = t[r]) && (n.push(i), 
Be(r, n), n.push(":"), We(e, o, n), i = ",");
n.push("}");
return;
}
t = t.valueOf();
}
switch (typeof t) {
case "string":
Be(t, n);
break;

case "number":
n.push(isFinite(t) && !isNaN(t) ? String(t) : "null");
break;

case "boolean":
n.push(String(t));
break;

case "function":
n.push("null");
break;

default:
throw Error("Unknown type: " + typeof t);
}
}
}
function Be(e, t) {
t.push('"', e.replace(ac, function(e) {
var t = oc[e];
t || (t = "\\u" + (65536 | e.charCodeAt(0)).toString(16).substr(1), oc[e] = t);
return t;
}), '"');
}
function Ve() {
0 != sc && (uc[this[uu] || (this[uu] = ++cu)] = this);
this.oa = this.oa;
this.Fa = this.Fa;
}
function qe(e) {
e.oa || (e.oa = !0, e.ta(), 0 != sc && (e = e[uu] || (e[uu] = ++cu), delete uc[e]));
}
function Ke(e, t) {
this.type = e;
this.b = this.target = t;
this.Bb = !0;
}
function Ge(e, n) {
Ke.call(this, e ? e.type : "");
this.relatedTarget = this.b = this.target = null;
this.button = this.screenY = this.screenX = this.clientY = this.clientX = 0;
this.key = "";
this.metaKey = this.shiftKey = this.altKey = this.ctrlKey = !1;
this.pointerId = 0;
this.pointerType = "";
this.a = null;
if (e) {
var r = this.type = e.type, i = e.changedTouches ? e.changedTouches[0] : null;
this.target = e.target || e.srcElement;
this.b = n;
if (n = e.relatedTarget) {
if (ku) {
e: {
try {
F(n.nodeName);
var o = !0;
break e;
} catch (e) {}
o = !1;
}
o || (n = null);
}
} else "mouseover" == r ? n = e.fromElement : "mouseout" == r && (n = e.toElement);
this.relatedTarget = n;
null === i ? (this.clientX = void 0 !== e.clientX ? e.clientX : e.pageX, this.clientY = void 0 !== e.clientY ? e.clientY : e.pageY, 
this.screenX = e.screenX || 0, this.screenY = e.screenY || 0) : (this.clientX = void 0 !== i.clientX ? i.clientX : i.pageX, 
this.clientY = void 0 !== i.clientY ? i.clientY : i.pageY, this.screenX = i.screenX || 0, 
this.screenY = i.screenY || 0);
this.button = e.button;
this.key = e.key || "";
this.ctrlKey = e.ctrlKey;
this.altKey = e.altKey;
this.shiftKey = e.shiftKey;
this.metaKey = e.metaKey;
this.pointerId = e.pointerId || 0;
this.pointerType = t(e.pointerType) ? e.pointerType : pc[e.pointerType] || "";
this.a = e;
e.defaultPrevented && this.c();
}
}
function He(e, t, n, r, i) {
this.listener = e;
this.a = null;
this.src = t;
this.type = n;
this.capture = !!r;
this.La = i;
this.key = ++vc;
this.ma = this.Ha = !1;
}
function Qe(e) {
e.ma = !0;
e.listener = null;
e.a = null;
e.src = null;
e.La = null;
}
function Xe(e) {
this.src = e;
this.a = {};
this.b = 0;
}
function Ye(e, t, n, r, i, o) {
var a = t.toString();
(t = e.a[a]) || (t = e.a[a] = [], e.b++);
var s = Je(t, n, i, o);
-1 < s ? (e = t[s], r || (e.Ha = !1)) : (e = new He(n, e.src, a, !!i, o), e.Ha = r, 
t.push(e));
return e;
}
function ze(e, t) {
var n = t.type;
n in e.a && T(e.a[n], t) && (Qe(t), 0 == e.a[n].length && (delete e.a[n], e.b--));
}
function Je(e, t, n, r) {
for (var i = 0; i < e.length; ++i) {
var o = e[i];
if (!o.ma && o.listener == t && o.capture == !!n && o.La == r) return i;
}
return -1;
}
function $e(e, t, n, r, i) {
if (r && r.once) tt(e, t, n, r, i); else if (a(t)) for (var o = 0; o < t.length; o++) $e(e, t[o], n, r, i); else n = ct(n), 
e && e[dc] ? ft(e, t, n, c(r) ? !!r.capture : !!r, i) : Ze(e, t, n, !1, r, i);
}
function Ze(e, t, n, r, i, o) {
if (!t) throw Error("Invalid event type");
var a = c(i) ? !!i.capture : !!i, s = ut(e);
s || (e[_c] = s = new Xe(e));
if (!(n = Ye(s, t, n, r, a, o)).a) {
r = et();
n.a = r;
r.src = e;
r.listener = n;
if (e.addEventListener) fc || (i = a), void 0 === i && (i = !1), e.addEventListener(t.toString(), r, i); else {
if (!e.attachEvent) throw Error("addEventListener and attachEvent are unavailable.");
e.attachEvent(it(t.toString()), r);
}
gc++;
}
}
function et() {
var e = st, t = lc ? function(n) {
return e.call(t.src, t.listener, n);
} : function(n) {
if (!(n = e.call(t.src, t.listener, n))) return n;
};
return t;
}
function tt(e, t, n, r, i) {
if (a(t)) for (var o = 0; o < t.length; o++) tt(e, t[o], n, r, i); else n = ct(n), 
e && e[dc] ? pt(e, t, n, c(r) ? !!r.capture : !!r, i) : Ze(e, t, n, !0, r, i);
}
function nt(e, t, n, r, i) {
if (a(t)) for (var o = 0; o < t.length; o++) nt(e, t[o], n, r, i); else r = c(r) ? !!r.capture : !!r, 
n = ct(n), e && e[dc] ? (e = e.u, (t = String(t).toString()) in e.a && (o = e.a[t], 
-1 < (n = Je(o, n, r, i)) && (Qe(o[n]), Array.prototype.splice.call(o, n, 1), 0 == o.length && (delete e.a[t], 
e.b--)))) : e && (e = ut(e)) && (t = e.a[t.toString()], e = -1, t && (e = Je(t, n, r, i)), 
(n = -1 < e ? t[e] : null) && rt(n));
}
function rt(e) {
if ("number" != typeof e && e && !e.ma) {
var t = e.src;
if (t && t[dc]) ze(t.u, e); else {
var n = e.type, r = e.a;
t.removeEventListener ? t.removeEventListener(n, r, e.capture) : t.detachEvent && t.detachEvent(it(n), r);
gc--;
(n = ut(t)) ? (ze(n, e), 0 == n.b && (n.src = null, t[_c] = null)) : Qe(e);
}
}
}
function it(e) {
return e in yc ? yc[e] : yc[e] = "on" + e;
}
function ot(e, t, n, r) {
var i = !0;
if ((e = ut(e)) && (t = e.a[t.toString()])) for (t = t.concat(), e = 0; e < t.length; e++) {
var o = t[e];
o && o.capture == n && !o.ma && (o = at(o, r), i = i && !1 !== o);
}
return i;
}
function at(e, t) {
var n = e.listener, r = e.La || e.src;
e.Ha && rt(e);
return n.call(r, t);
}
function st(e, t) {
if (e.ma) return !0;
if (!lc) {
if (!t) e: {
t = [ "window", "event" ];
for (var n = su, r = 0; r < t.length; r++) if (null == (n = n[t[r]])) {
t = null;
break e;
}
t = n;
}
t = new Ge(r = t, this);
n = !0;
if (!(0 > r.keyCode || void 0 != r.returnValue)) {
e: {
var i = !1;
if (0 == r.keyCode) try {
r.keyCode = -1;
break e;
} catch (e) {
i = !0;
}
(i || void 0 == r.returnValue) && (r.returnValue = !0);
}
r = [];
for (i = t.b; i; i = i.parentNode) r.push(i);
e = e.type;
for (i = r.length - 1; 0 <= i; i--) {
t.b = r[i];
var o = ot(r[i], e, !0, t);
n = n && o;
}
for (i = 0; i < r.length; i++) t.b = r[i], o = ot(r[i], e, !1, t), n = n && o;
}
return n;
}
return at(e, new Ge(t, this));
}
function ut(e) {
return (e = e[_c]) instanceof Xe ? e : null;
}
function ct(e) {
if (u(e)) return e;
e[mc] || (e[mc] = function(t) {
return e.handleEvent(t);
});
return e[mc];
}
function lt() {
Ve.call(this);
this.u = new Xe(this);
this.Ib = this;
this.Ra = null;
}
function ht(e, n) {
var r, i = e.Ra;
if (i) for (r = []; i; i = i.Ra) r.push(i);
e = e.Ib;
i = n.type || n;
if (t(n)) n = new Ke(n, e); else if (n instanceof Ke) n.target = n.target || e; else {
var o = n;
x(n = new Ke(i, e), o);
}
o = !0;
if (r) for (var a = r.length - 1; 0 <= a; a--) {
var s = n.b = r[a];
o = dt(s, i, !0, n) && o;
}
o = dt(s = n.b = e, i, !0, n) && o;
o = dt(s, i, !1, n) && o;
if (r) for (a = 0; a < r.length; a++) s = n.b = r[a], o = dt(s, i, !1, n) && o;
}
function ft(e, t, n, r, i) {
Ye(e.u, String(t), n, !1, r, i);
}
function pt(e, t, n, r, i) {
Ye(e.u, String(t), n, !0, r, i);
}
function dt(e, t, n, r) {
if (!(t = e.u.a[String(t)])) return !0;
t = t.concat();
for (var i = !0, o = 0; o < t.length; ++o) {
var a = t[o];
if (a && !a.ma && a.capture == n) {
var s = a.listener, u = a.La || a.src;
a.Ha && ze(e.u, a);
i = !1 !== s.call(u, r) && i;
}
}
return i && 0 != r.Bb;
}
function vt(e, t, n) {
if (u(e)) n && (e = f(e, n)); else {
if (!e || "function" != typeof e.handleEvent) throw Error("Invalid listener argument");
e = f(e.handleEvent, e);
}
return 2147483647 < Number(t) ? -1 : su.setTimeout(e, t || 0);
}
function _t(e) {
var t = null;
return new $(function(n, r) {
-1 == (t = vt(function() {
n(void 0);
}, e)) && r(Error("Failed to schedule timer."));
}).s(function(e) {
su.clearTimeout(t);
throw e;
});
}
function yt(e, t, n, r, i) {
this.reset(e, t, n, r, i);
}
function gt(e) {
this.f = e;
this.b = this.c = this.a = null;
}
function mt(e, t) {
this.name = e;
this.value = t;
}
function bt(e) {
if (e.c) return e.c;
if (e.a) return bt(e.a);
E("Root logger has no level set.");
return null;
}
function Et(e) {
Tc || (Tc = new gt(""), Sc[""] = Tc, Tc.c = wc);
var t;
if (!(t = Sc[e])) {
t = new gt(e);
var n = e.lastIndexOf("."), r = e.substr(n + 1);
(n = Et(e.substr(0, n))).b || (n.b = {});
n.b[r] = t;
t.a = n;
Sc[e] = t;
}
return t;
}
function wt(e, t) {
this.b = {};
this.a = [];
this.c = 0;
var n = arguments.length;
if (1 < n) {
if (n % 2) throw Error("Uneven number of arguments");
for (var r = 0; r < n; r += 2) this.set(arguments[r], arguments[r + 1]);
} else if (e) {
e instanceof wt ? (n = e.S(), r = e.P()) : (n = k(e), r = A(e));
for (var i = 0; i < n.length; i++) this.set(n[i], r[i]);
}
}
function Ct(e) {
if (e.c != e.a.length) {
for (var t = 0, n = 0; t < e.a.length; ) {
var r = e.a[t];
St(e.b, r) && (e.a[n++] = r);
t++;
}
e.a.length = n;
}
if (e.c != e.a.length) {
var i = {};
for (n = t = 0; t < e.a.length; ) r = e.a[t], St(i, r) || (e.a[n++] = r, i[r] = 1), 
t++;
e.a.length = n;
}
}
function St(e, t) {
return Object.prototype.hasOwnProperty.call(e, t);
}
function Tt(e, t) {
e && e.log(Cc, t, void 0);
}
function Nt(e) {
return wu(e, function(e) {
return 1 < (e = e.toString(16)).length ? e : "0" + e;
}).join("");
}
function It(e) {
var t = "";
Pt(e, function(e) {
t += String.fromCharCode(e);
});
return t;
}
function Pt(e, t) {
function n(t) {
for (;r < e.length; ) {
var n = e.charAt(r++), i = Ic[n];
if (null != i) return i;
if (!/^[\s\xa0]*$/.test(n)) throw Error("Unknown base64 encoding at char: " + n);
}
return t;
}
Ot();
for (var r = 0; ;) {
var i = n(-1), o = n(0), a = n(64), s = n(64);
if (64 === s && -1 === i) break;
t(i << 2 | o >> 4);
64 != a && (t(o << 4 & 240 | a >> 2), 64 != s && t(a << 6 & 192 | s));
}
}
function Ot() {
if (!Nc) {
Nc = {};
Ic = {};
for (var e = 0; 65 > e; e++) Nc[e] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(e), 
Ic[Nc[e]] = e, 62 <= e && (Ic["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.".charAt(e)] = e);
}
}
function Rt(e, t) {
this.g = [];
this.v = e;
this.o = t || null;
this.f = this.a = !1;
this.c = void 0;
this.u = this.w = this.i = !1;
this.h = 0;
this.b = null;
this.l = 0;
}
function At(e, t, n) {
e.a = !0;
e.c = n;
e.f = !t;
xt(e);
}
function kt(e) {
if (e.a) {
if (!e.u) throw new Ft();
e.u = !1;
}
}
function Dt(e, t) {
Lt(e, null, t, void 0);
}
function Lt(e, t, n, r) {
e.g.push([ t, n, r ]);
e.a && xt(e);
}
function Mt(e) {
return Cu(e.g, function(e) {
return u(e[1]);
});
}
function xt(e) {
if (e.h && e.a && Mt(e)) {
var t = e.h, n = Oc[t];
n && (su.clearTimeout(n.a), delete Oc[t]);
e.h = 0;
}
e.b && (e.b.l--, delete e.b);
t = e.c;
for (var r = n = !1; e.g.length && !e.i; ) {
var i = e.g.shift(), o = i[0], a = i[1];
i = i[2];
if (o = e.f ? a : o) try {
var s = o.call(i || e.o, t);
void 0 !== s && (e.f = e.f && (s == t || s instanceof Error), e.c = t = s);
(V(t) || "function" == typeof su.Promise && t instanceof su.Promise) && (r = !0, 
e.i = !0);
} catch (r) {
t = r, e.f = !0, Mt(e) || (n = !0);
}
}
e.c = t;
r && (s = f(e.m, e, !0), r = f(e.m, e, !1), t instanceof Rt ? (Lt(t, s, r), t.w = !0) : t.then(s, r));
n && (t = new jt(t), Oc[t.a] = t, e.h = t.a);
}
function Ft() {
v.call(this);
}
function Ut() {
v.call(this);
}
function jt(e) {
this.a = su.setTimeout(f(this.c, this), 0);
this.b = e;
}
function Wt(e, t) {
this.b = -1;
this.b = Rc;
this.f = su.Uint8Array ? new Uint8Array(this.b) : Array(this.b);
this.g = this.c = 0;
this.a = [];
this.i = e;
this.h = t;
this.l = su.Int32Array ? new Int32Array(64) : Array(64);
Pc || (Pc = su.Int32Array ? new Int32Array(Mc) : Mc);
this.reset();
}
function Bt(e) {
for (var t = e.f, n = e.l, r = 0, i = 0; i < t.length; ) n[r++] = t[i] << 24 | t[i + 1] << 16 | t[i + 2] << 8 | t[i + 3], 
i = 4 * r;
for (t = 16; 64 > t; t++) {
i = 0 | n[t - 15];
r = 0 | n[t - 2];
var o = (0 | n[t - 16]) + ((i >>> 7 | i << 25) ^ (i >>> 18 | i << 14) ^ i >>> 3) | 0, a = (0 | n[t - 7]) + ((r >>> 17 | r << 15) ^ (r >>> 19 | r << 13) ^ r >>> 10) | 0;
n[t] = o + a | 0;
}
r = 0 | e.a[0];
i = 0 | e.a[1];
var s = 0 | e.a[2], u = 0 | e.a[3], c = 0 | e.a[4], l = 0 | e.a[5], h = 0 | e.a[6];
o = 0 | e.a[7];
for (t = 0; 64 > t; t++) {
var f = ((r >>> 2 | r << 30) ^ (r >>> 13 | r << 19) ^ (r >>> 22 | r << 10)) + (r & i ^ r & s ^ i & s) | 0;
a = (o = o + ((c >>> 6 | c << 26) ^ (c >>> 11 | c << 21) ^ (c >>> 25 | c << 7)) | 0) + ((a = (a = c & l ^ ~c & h) + (0 | Pc[t]) | 0) + (0 | n[t]) | 0) | 0;
o = h;
h = l;
l = c;
c = u + a | 0;
u = s;
s = i;
i = r;
r = a + f | 0;
}
e.a[0] = e.a[0] + r | 0;
e.a[1] = e.a[1] + i | 0;
e.a[2] = e.a[2] + s | 0;
e.a[3] = e.a[3] + u | 0;
e.a[4] = e.a[4] + c | 0;
e.a[5] = e.a[5] + l | 0;
e.a[6] = e.a[6] + h | 0;
e.a[7] = e.a[7] + o | 0;
}
function Vt(e, n, r) {
void 0 === r && (r = n.length);
var i = 0, o = e.c;
if (t(n)) for (;i < r; ) e.f[o++] = n.charCodeAt(i++), o == e.b && (Bt(e), o = 0); else {
if (!s(n)) throw Error("message must be string or array");
for (;i < r; ) {
var a = n[i++];
if (!("number" == typeof a && 0 <= a && 255 >= a && a == (0 | a))) throw Error("message must be a byte array");
e.f[o++] = a;
o == e.b && (Bt(e), o = 0);
}
}
e.c = o;
e.g += r;
}
function qt() {
Wt.call(this, 8, xc);
}
function Kt(e) {
if (e.P && "function" == typeof e.P) return e.P();
if (t(e)) return e.split("");
if (s(e)) {
for (var n = [], r = e.length, i = 0; i < r; i++) n.push(e[i]);
return n;
}
return A(e);
}
function Gt(e) {
if (e.S && "function" == typeof e.S) return e.S();
if (!e.P || "function" != typeof e.P) {
if (s(e) || t(e)) {
var n = [];
e = e.length;
for (var r = 0; r < e; r++) n.push(r);
return n;
}
return k(e);
}
}
function Ht(e, n) {
if (e.forEach && "function" == typeof e.forEach) e.forEach(n, void 0); else if (s(e) || t(e)) Eu(e, n, void 0); else for (var r = Gt(e), i = Kt(e), o = i.length, a = 0; a < o; a++) n.call(void 0, i[a], r && r[a], e);
}
function Qt(e, t) {
if (e) {
e = e.split("&");
for (var n = 0; n < e.length; n++) {
var r = e[n].indexOf("="), i = null;
if (0 <= r) {
var o = e[n].substring(0, r);
i = e[n].substring(r + 1);
} else o = e[n];
t(o, i ? decodeURIComponent(i.replace(/\+/g, " ")) : "");
}
}
}
function Xt(e, t) {
this.b = this.l = this.c = "";
this.i = null;
this.h = this.g = "";
this.f = !1;
if (e instanceof Xt) {
this.f = void 0 !== t ? t : e.f;
Yt(this, e.c);
this.l = e.l;
this.b = e.b;
zt(this, e.i);
this.g = e.g;
t = e.a;
var n = new an();
n.c = t.c;
t.a && (n.a = new wt(t.a), n.b = t.b);
Jt(this, n);
this.h = e.h;
} else e && (n = String(e).match(Fc)) ? (this.f = !!t, Yt(this, n[1] || "", !0), 
this.l = nn(n[2] || ""), this.b = nn(n[3] || "", !0), zt(this, n[4]), this.g = nn(n[5] || "", !0), 
Jt(this, n[6] || "", !0), this.h = nn(n[7] || "")) : (this.f = !!t, this.a = new an(null, 0, this.f));
}
function Yt(e, t, n) {
e.c = n ? nn(t, !0) : t;
e.c && (e.c = e.c.replace(/:$/, ""));
}
function zt(e, t) {
if (t) {
t = Number(t);
if (isNaN(t) || 0 > t) throw Error("Bad port number " + t);
e.i = t;
} else e.i = null;
}
function Jt(e, t, n) {
t instanceof an ? (e.a = t, dn(e.a, e.f)) : (n || (t = rn(t, Bc)), e.a = new an(t, 0, e.f));
}
function $t(e, t, n) {
e.a.set(t, n);
}
function Zt(e, t) {
return e.a.get(t);
}
function en(e) {
return e instanceof Xt ? new Xt(e) : new Xt(e, void 0);
}
function tn(e, t) {
var n = new Xt(null, void 0);
Yt(n, "https");
e && (n.b = e);
t && (n.g = t);
return n;
}
function nn(e, t) {
return e ? t ? decodeURI(e.replace(/%25/g, "%2525")) : decodeURIComponent(e) : "";
}
function rn(e, n, r) {
return t(e) ? (e = encodeURI(e).replace(n, on), r && (e = e.replace(/%25([0-9a-fA-F]{2})/g, "%$1")), 
e) : null;
}
function on(e) {
return "%" + ((e = e.charCodeAt(0)) >> 4 & 15).toString(16) + (15 & e).toString(16);
}
function an(e, t, n) {
this.b = this.a = null;
this.c = e || null;
this.f = !!n;
}
function sn(e) {
e.a || (e.a = new wt(), e.b = 0, e.c && Qt(e.c, function(t, n) {
cn(e, decodeURIComponent(t.replace(/\+/g, " ")), n);
}));
}
function un(e) {
var t = Gt(e);
if ("undefined" == typeof t) throw Error("Keys are undefined");
var n = new an(null, 0, void 0);
e = Kt(e);
for (var r = 0; r < t.length; r++) {
var i = t[r], o = e[r];
a(o) ? fn(n, i, o) : cn(n, i, o);
}
return n;
}
function cn(e, t, n) {
sn(e);
e.c = null;
t = pn(e, t);
var r = e.a.get(t);
r || e.a.set(t, r = []);
r.push(n);
e.b += 1;
}
function ln(e, t) {
sn(e);
t = pn(e, t);
St(e.a.b, t) && (e.c = null, e.b -= e.a.get(t).length, e = e.a, St(e.b, t) && (delete e.b[t], 
e.c--, e.a.length > 2 * e.c && Ct(e)));
}
function hn(e, t) {
sn(e);
t = pn(e, t);
return St(e.a.b, t);
}
function fn(e, t, n) {
ln(e, t);
0 < n.length && (e.c = null, e.a.set(pn(e, t), P(n)), e.b += n.length);
}
function pn(e, t) {
t = String(t);
e.f && (t = t.toLowerCase());
return t;
}
function dn(e, t) {
t && !e.f && (sn(e), e.c = null, e.a.forEach(function(e, t) {
var n = t.toLowerCase();
t != n && (ln(this, t), fn(this, n, e));
}, e));
e.f = t;
}
function vn() {}
function _n(e) {
return e.c || (e.c = e.b());
}
function yn() {}
function gn(e) {
if (!e.f && "undefined" == typeof XMLHttpRequest && "undefined" != typeof ActiveXObject) {
for (var t = [ "MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP" ], n = 0; n < t.length; n++) {
var r = t[n];
try {
return new ActiveXObject(r), e.f = r;
} catch (e) {}
}
throw Error("Could not create ActiveXObject. ActiveX might be disabled, or MSXML might not be installed");
}
return e.f;
}
function mn(e) {
lt.call(this);
this.headers = new wt();
this.w = e || null;
this.b = !1;
this.v = this.a = null;
this.g = this.I = this.i = "";
this.c = this.G = this.h = this.A = !1;
this.f = 0;
this.m = null;
this.l = Kc;
this.o = this.N = !1;
}
function bn(e, t, n, r, i) {
if (e.a) throw Error("[goog.net.XhrIo] Object is active with another request=" + e.i + "; newUri=" + t);
n = n ? n.toUpperCase() : "GET";
e.i = t;
e.g = "";
e.I = n;
e.A = !1;
e.b = !0;
e.a = e.w ? e.w.a() : qc.a();
e.v = _n(e.w ? e.w : qc);
e.a.onreadystatechange = f(e.Ab, e);
try {
Tt(e.J, An(e, "Opening Xhr")), e.G = !0, e.a.open(n, String(t), !0), e.G = !1;
} catch (t) {
Tt(e.J, An(e, "Error opening Xhr: " + t.message));
Cn(e, t);
return;
}
t = r || "";
var o = new wt(e.headers);
i && Ht(i, function(e, t) {
o.set(t, e);
});
i = C(o.S());
r = su.FormData && t instanceof su.FormData;
!S(Xc, n) || i || r || o.set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
o.forEach(function(e, t) {
this.a.setRequestHeader(t, e);
}, e);
e.l && (e.a.responseType = e.l);
"withCredentials" in e.a && e.a.withCredentials !== e.N && (e.a.withCredentials = e.N);
try {
In(e), 0 < e.f && (e.o = En(e.a), Tt(e.J, An(e, "Will abort after " + e.f + "ms if incomplete, xhr2 " + e.o)), 
e.o ? (e.a.timeout = e.f, e.a.ontimeout = f(e.Ea, e)) : e.m = vt(e.Ea, e.f, e)), 
Tt(e.J, An(e, "Sending request")), e.h = !0, e.a.send(t), e.h = !1;
} catch (t) {
Tt(e.J, An(e, "Send error: " + t.message)), Cn(e, t);
}
}
function En(e) {
return Ou && W(9) && "number" == typeof e.timeout && void 0 !== e.ontimeout;
}
function wn(e) {
return "content-type" == e.toLowerCase();
}
function Cn(e, t) {
e.b = !1;
e.a && (e.c = !0, e.a.abort(), e.c = !1);
e.g = t;
Sn(e);
Nn(e);
}
function Sn(e) {
e.A || (e.A = !0, ht(e, "complete"), ht(e, "error"));
}
function Tn(e) {
if (e.b && "undefined" != typeof au) if (e.v[1] && 4 == Pn(e) && 2 == On(e)) Tt(e.J, An(e, "Local request error detected and ignored")); else if (e.h && 4 == Pn(e)) vt(e.Ab, 0, e); else if (ht(e, "readystatechange"), 
4 == Pn(e)) {
Tt(e.J, An(e, "Request complete"));
e.b = !1;
try {
var t = On(e);
e: switch (t) {
case 200:
case 201:
case 202:
case 204:
case 206:
case 304:
case 1223:
var n = !0;
break e;

default:
n = !1;
}
var r;
if (!(r = n)) {
var i;
if (i = 0 === t) {
var o = String(e.i).match(Fc)[1] || null;
if (!o && su.self && su.self.location) {
var a = su.self.location.protocol;
o = a.substr(0, a.length - 1);
}
i = !Qc.test(o ? o.toLowerCase() : "");
}
r = i;
}
if (r) ht(e, "complete"), ht(e, "success"); else {
try {
var s = 2 < Pn(e) ? e.a.statusText : "";
} catch (t) {
Tt(e.J, "Can not get status: " + t.message), s = "";
}
e.g = s + " [" + On(e) + "]";
Sn(e);
}
} finally {
Nn(e);
}
}
}
function Nn(e, t) {
if (e.a) {
In(e);
var n = e.a, i = e.v[0] ? r : null;
e.a = null;
e.v = null;
t || ht(e, "ready");
try {
n.onreadystatechange = i;
} catch (t) {
(e = e.J) && e.log(Ec, "Problem encountered resetting onreadystatechange: " + t.message, void 0);
}
}
}
function In(e) {
e.a && e.o && (e.a.ontimeout = null);
"number" == typeof e.m && (su.clearTimeout(e.m), e.m = null);
}
function Pn(e) {
return e.a ? e.a.readyState : 0;
}
function On(e) {
try {
return 2 < Pn(e) ? e.a.status : -1;
} catch (e) {
return -1;
}
}
function Rn(e) {
try {
return e.a ? e.a.responseText : "";
} catch (t) {
return Tt(e.J, "Can not get responseText: " + t.message), "";
}
}
function An(e, t) {
return t + " [" + e.I + " " + e.i + " " + On(e) + "]";
}
function kn(e) {
var t = {}, n = t.document || document, r = Ce(e), i = document.createElement("SCRIPT"), o = {
Cb: i,
Ea: void 0
}, a = new Rt(Ln, o), s = null, u = null != t.timeout ? t.timeout : 5e3;
0 < u && (s = window.setTimeout(function() {
Mn(i, !0);
var e = new xn(Jc, "Timeout reached for loading script " + r);
kt(a);
At(a, !1, e);
}, u), o.Ea = s);
i.onload = i.onreadystatechange = function() {
i.readyState && "loaded" != i.readyState && "complete" != i.readyState || (Mn(i, t.Sc || !1, s), 
a.A(null));
};
i.onerror = function() {
Mn(i, !0, s);
var e = new xn(zc, "Error while loading script " + r);
kt(a);
At(a, !1, e);
};
x(o = t.attributes || {}, {
type: "text/javascript",
charset: "UTF-8"
});
Le(i, o);
i.src = Ce(e);
Dn(n).appendChild(i);
return a;
}
function Dn(e) {
var t;
return (t = (e || document).getElementsByTagName("HEAD")) && 0 != t.length ? t[0] : e.documentElement;
}
function Ln() {
if (this && this.Cb) {
var e = this.Cb;
e && "SCRIPT" == e.tagName && Mn(e, !0, this.Ea);
}
}
function Mn(e, t, n) {
null != n && su.clearTimeout(n);
e.onload = r;
e.onerror = r;
e.onreadystatechange = r;
t && window.setTimeout(function() {
e && e.parentNode && e.parentNode.removeChild(e);
}, 0);
}
function xn(e, t) {
var n = "Jsloader error (code #" + e + ")";
t && (n += ": " + t);
v.call(this, n);
this.code = e;
}
function Fn() {}
function Un() {
this.a = new XDomainRequest();
this.readyState = 0;
this.onreadystatechange = null;
this.responseText = "";
this.status = -1;
this.statusText = this.responseXML = null;
this.a.onload = f(this.Wb, this);
this.a.onerror = f(this.xb, this);
this.a.onprogress = f(this.Xb, this);
this.a.ontimeout = f(this.Yb, this);
}
function jn(e, t) {
e.readyState = t;
e.onreadystatechange && e.onreadystatechange();
}
function Wn() {
var e = rr();
return Ou && !!Fu && 11 == Fu || /Edge\/\d+/.test(e);
}
function Bn() {
return su.window && su.window.location.href || "";
}
function Vn(e, t) {
t = t || su.window;
var n = "about:blank";
e && (n = Ie(Pe(e)));
t.location.href = n;
}
function qn(e, t) {
var n, r = [];
for (n in e) n in t ? typeof e[n] != typeof t[n] ? r.push(n) : a(e[n]) ? L(e[n], t[n]) || r.push(n) : "object" == typeof e[n] && null != e[n] && null != t[n] ? 0 < qn(e[n], t[n]).length && r.push(n) : e[n] !== t[n] && r.push(n) : r.push(n);
for (n in t) n in e || r.push(n);
return r;
}
function Kn() {
var e = rr();
return !((e = tr(e) != el ? null : (e = e.match(/\sChrome\/(\d+)/i)) && 2 == e.length ? parseInt(e[1], 10) : null) && 30 > e) && (!Ou || !Fu || 9 < Fu);
}
function Gn(e) {
return !!((e = (e || rr()).toLowerCase()).match(/android/) || e.match(/webos/) || e.match(/iphone|ipad|ipod/) || e.match(/blackberry/) || e.match(/windows phone/) || e.match(/iemobile/));
}
function Hn(e) {
e = e || su.window;
try {
e.close();
} catch (e) {}
}
function Qn(e, t, n) {
var r = Math.floor(1e9 * Math.random()).toString();
t = t || 500;
n = n || 600;
var i = (window.screen.availHeight - n) / 2, o = (window.screen.availWidth - t) / 2;
t = {
width: t,
height: n,
top: 0 < i ? i : 0,
left: 0 < o ? o : 0,
location: !0,
resizable: !0,
statusbar: !0,
toolbar: !1
};
n = rr().toLowerCase();
r && (t.target = r, g(n, "crios/") && (t.target = "_blank"));
tr(rr()) == Zc && (e = e || "http://localhost", t.scrollbars = !0);
n = e || "";
(r = t) || (r = {});
e = window;
t = n instanceof Ne ? n : Pe("undefined" != typeof n.href ? n.href : String(n));
n = r.target || n.target;
i = [];
for (a in r) switch (a) {
case "width":
case "height":
case "top":
case "left":
i.push(a + "=" + r[a]);
break;

case "target":
case "noreferrer":
break;

default:
i.push(a + "=" + (r[a] ? 1 : 0));
}
var a = i.join(",");
(O("iPhone") && !O("iPod") && !O("iPad") || O("iPad") || O("iPod")) && e.navigator && e.navigator.standalone && n && "_self" != n ? (a = e.document.createElement("A"), 
t instanceof Ne || t instanceof Ne || (t = t.la ? t.ja() : String(t), tc.test(t) || (t = "about:invalid#zClosurez"), 
t = Oe(t)), a.href = Ie(t), a.setAttribute("target", n), r.noreferrer && a.setAttribute("rel", "noreferrer"), 
(r = document.createEvent("MouseEvent")).initMouseEvent("click", !0, !0, e, 1), 
a.dispatchEvent(r), a = {}) : r.noreferrer ? (a = e.open("", n, a), e = Ie(t), a && (Au && g(e, ";") && (e = "'" + e.replace(/'/g, "%27") + "'"), 
a.opener = null, Ee("b/12014412, meta tag with sanitized URL"), e = '<META HTTP-EQUIV="refresh" content="0; url=' + y(e) + '">', 
e = ke(e), a.document.write(Ae(e)), a.document.close())) : a = e.open(Ie(t), n, a);
if (a) try {
a.focus();
} catch (e) {}
return a;
}
function Xn(e) {
return new $(function(t) {
function n() {
_t(2e3).then(function() {
if (e && !e.closed) return n();
t();
});
}
return n();
});
}
function Yn() {
var e = null;
return new $(function(t) {
"complete" == su.document.readyState ? t() : (e = function() {
t();
}, tt(window, "load", e));
}).s(function(t) {
nt(window, "load", e);
throw t;
});
}
function zn() {
return Jn(void 0) ? Yn().then(function() {
return new $(function(e, t) {
var n = su.document, r = setTimeout(function() {
t(Error("Cordova framework is not ready."));
}, 1e3);
n.addEventListener("deviceready", function() {
clearTimeout(r);
e();
}, !1);
});
}) : ne(Error("Cordova must run in an Android or iOS file scheme."));
}
function Jn(e) {
e = e || rr();
return !("file:" !== ur() || !e.toLowerCase().match(/iphone|ipad|ipod|android/));
}
function $n() {
var e = su.window;
try {
return !(!e || e == e.top);
} catch (e) {
return !1;
}
}
function Zn() {
return ou.INTERNAL.hasOwnProperty("reactNative") ? "ReactNative" : ou.INTERNAL.hasOwnProperty("node") ? "Node" : "Browser";
}
function er() {
var e = Zn();
return "ReactNative" === e || "Node" === e;
}
function tr(e) {
var t = e.toLowerCase();
return g(t, "opera/") || g(t, "opr/") || g(t, "opios/") ? "Opera" : g(t, "iemobile") ? "IEMobile" : g(t, "msie") || g(t, "trident/") ? "IE" : g(t, "edge/") ? "Edge" : g(t, "firefox/") ? Zc : g(t, "silk/") ? "Silk" : g(t, "blackberry") ? "Blackberry" : g(t, "webos") ? "Webos" : !g(t, "safari/") || g(t, "chrome/") || g(t, "crios/") || g(t, "android") ? !g(t, "chrome/") && !g(t, "crios/") || g(t, "edge/") ? g(t, "android") ? "Android" : (e = e.match(/([a-zA-Z\d\.]+)\/[a-zA-Z\d\.]*$/)) && 2 == e.length ? e[1] : "Other" : el : "Safari";
}
function nr(e, t) {
t = t || [];
var n, r = [], i = {};
for (n in tl) i[tl[n]] = !0;
for (n = 0; n < t.length; n++) "undefined" != typeof i[t[n]] && (delete i[t[n]], 
r.push(t[n]));
r.sort();
(t = r).length || (t = [ "FirebaseCore-web" ]);
i = "";
return (i = "Browser" === (r = Zn()) ? tr(rr()) : r) + "/JsCore/" + e + "/" + t.join(",");
}
function rr() {
return su.navigator && su.navigator.userAgent || "";
}
function ir(e, t) {
e = e.split(".");
t = t || su;
for (var n = 0; n < e.length && "object" == typeof t && null != t; n++) t = t[e[n]];
n != e.length && (t = void 0);
return t;
}
function or() {
try {
var e = su.localStorage, t = pr();
if (e) return e.setItem(t, "1"), e.removeItem(t), !Wn() || !!su.indexedDB;
} catch (e) {}
return !1;
}
function ar() {
return (sr() || "chrome-extension:" === ur() || Jn()) && !er() && or();
}
function sr() {
return "http:" === ur() || "https:" === ur();
}
function ur() {
return su.location && su.location.protocol || null;
}
function cr(e) {
return !Gn(e = e || rr()) && tr(e) != Zc;
}
function lr(e) {
return "undefined" == typeof e ? null : Ue(e);
}
function hr(e) {
var t, n = {};
for (t in e) e.hasOwnProperty(t) && null !== e[t] && void 0 !== e[t] && (n[t] = e[t]);
return n;
}
function fr(e) {
if (null !== e) return JSON.parse(e);
}
function pr(e) {
return e || "" + Math.floor(1e9 * Math.random()).toString();
}
function dr(e) {
return "Safari" != tr(e = e || rr()) && !e.toLowerCase().match(/iphone|ipad|ipod/);
}
function vr() {
var e = su.___jsl;
if (e && e.H) for (var t in e.H) if (e.H[t].r = e.H[t].r || [], e.H[t].L = e.H[t].L || [], 
e.H[t].r = e.H[t].L.concat(), e.CP) for (var n = 0; n < e.CP.length; n++) e.CP[n] = null;
}
function _r() {
var e = su.navigator;
return !e || "boolean" != typeof e.onLine || !sr() && "chrome-extension:" !== ur() && "undefined" == typeof e.connection || e.onLine;
}
function yr(e, t, n, r) {
if (e > t) throw Error("Short delay should be less than long delay!");
this.c = e;
this.b = t;
e = n || rr();
r = r || Zn();
this.a = Gn(e) || "ReactNative" === r;
}
function gr() {
var e = su.document;
return !e || "undefined" == typeof e.visibilityState || "visible" == e.visibilityState;
}
function mr() {
var e = su.document, t = null;
return gr() || !e ? te() : new $(function(n) {
t = function() {
gr() && (e.removeEventListener("visibilitychange", t, !1), n());
};
e.addEventListener("visibilitychange", t, !1);
}).s(function(n) {
e.removeEventListener("visibilitychange", t, !1);
throw n;
});
}
function br(e) {
try {
var t = new Date(parseInt(e, 10));
if (!isNaN(t.getTime()) && !/[^0-9]/.test(e)) return t.toUTCString();
} catch (e) {}
return null;
}
function Er(e, t, n) {
nl ? Object.defineProperty(e, t, {
configurable: !0,
enumerable: !0,
value: n
}) : e[t] = n;
}
function wr(e, t) {
if (t) for (var n in t) t.hasOwnProperty(n) && Er(e, n, t[n]);
}
function Cr(e) {
var t = {};
wr(t, e);
return t;
}
function Sr(e) {
var t, n = {};
for (t in e) e.hasOwnProperty(t) && (n[t] = e[t]);
return n;
}
function Tr(e, t) {
if (!t || !t.length) return !0;
if (!e) return !1;
for (var n = 0; n < t.length; n++) {
var r = e[t[n]];
if (void 0 === r || null === r || "" === r) return !1;
}
return !0;
}
function Nr(e) {
var t = e;
if ("object" == typeof e && null != e) {
t = "length" in e ? [] : {};
for (var n in e) Er(t, n, Nr(e[n]));
}
return t;
}
function Ir(e) {
var t = {}, n = e[ol], r = e[al];
e = e[sl];
if (!n || !e) throw Error("Invalid provider user info!");
t[cl] = r || null;
t[ul] = n;
Er(this, hl, e);
Er(this, ll, Nr(t));
}
function Pr(e, t) {
this.code = fl + e;
this.message = t || pl[e] || "";
}
function Or(e) {
var t = e && e.code;
return t ? new Pr(t.substring(fl.length), e.message) : null;
}
function Rr(e) {
var t = e[yl];
if ("undefined" == typeof t) throw new Pr("missing-continue-uri");
if ("string" != typeof t || "string" == typeof t && !t.length) throw new Pr("invalid-continue-uri");
this.h = t;
this.c = this.a = null;
this.g = !1;
var n = e[dl];
if (n && "object" == typeof n) {
t = n[bl];
var r = n[gl];
n = n[ml];
if ("string" == typeof t && t.length) {
this.a = t;
if ("undefined" != typeof r && "boolean" != typeof r) throw new Pr("argument-error", gl + " property must be a boolean when specified.");
this.g = !!r;
if ("undefined" != typeof n && ("string" != typeof n || "string" == typeof n && !n.length)) throw new Pr("argument-error", ml + " property must be a non empty string when specified.");
this.c = n || null;
} else {
if ("undefined" != typeof t) throw new Pr("argument-error", bl + " property must be a non empty string when specified.");
if ("undefined" != typeof r || "undefined" != typeof n) throw new Pr("missing-android-pkg-name");
}
} else if ("undefined" != typeof n) throw new Pr("argument-error", dl + " property must be a non null object when specified.");
this.b = null;
if ((t = e[_l]) && "object" == typeof t) {
if ("string" == typeof (t = t[El]) && t.length) this.b = t; else if ("undefined" != typeof t) throw new Pr("argument-error", El + " property must be a non empty string when specified.");
} else if ("undefined" != typeof t) throw new Pr("argument-error", _l + " property must be a non null object when specified.");
if ("undefined" != typeof (e = e[vl]) && "boolean" != typeof e) throw new Pr("argument-error", vl + " property must be a boolean when specified.");
if ((this.f = !!e) && !this.b && !this.a) throw new Pr("argument-error", vl + " property can't be true when no mobile application is provided.");
}
function Ar(e) {
var t = {};
t.continueUrl = e.h;
t.canHandleCodeInApp = e.f;
(t.androidPackageName = e.a) && (t.androidMinimumVersion = e.c, t.androidInstallApp = e.g);
t.iOSBundleId = e.b;
for (var n in t) null === t[n] && delete t[n];
return t;
}
function kr(e) {
this.b = e.sub;
lu();
this.a = e.provider_id || e.firebase && e.firebase.sign_in_provider || null;
}
function Dr(e) {
if (3 != (e = e.split(".")).length) return null;
for (var t = (4 - (e = e[1]).length % 4) % 4, n = 0; n < t; n++) e += ".";
try {
var r = JSON.parse(It(e));
if (r.sub && r.iss && r.aud && r.exp) return new kr(r);
} catch (e) {}
return null;
}
function Lr(e) {
for (var t in Sl) if (Sl[t].Na == e) return Sl[t];
return null;
}
function Mr(e) {
var t = {};
t["facebook.com"] = Ur;
t["google.com"] = Wr;
t["github.com"] = jr;
t["twitter.com"] = Br;
var n = e && e[Nl];
try {
if (n) return t[n] ? new t[n](e) : new Fr(e);
if ("undefined" != typeof e[Tl]) return new xr(e);
} catch (e) {}
return null;
}
function xr(e) {
var t = e[Nl];
if (!t && e[Tl]) {
var n = Dr(e[Tl]);
n && n.a && (t = n.a);
}
if (!t) throw Error("Invalid additional user info!");
"anonymous" != t && "custom" != t || (t = null);
n = !1;
"undefined" != typeof e.isNewUser ? n = !!e.isNewUser : "identitytoolkit#SignupNewUserResponse" === e.kind && (n = !0);
Er(this, "providerId", t);
Er(this, "isNewUser", n);
}
function Fr(e) {
xr.call(this, e);
Er(this, "profile", Nr((e = fr(e.rawUserInfo || "{}")) || {}));
}
function Ur(e) {
Fr.call(this, e);
if ("facebook.com" != this.providerId) throw Error("Invalid provider ID!");
}
function jr(e) {
Fr.call(this, e);
if ("github.com" != this.providerId) throw Error("Invalid provider ID!");
Er(this, "username", this.profile && this.profile.login || null);
}
function Wr(e) {
Fr.call(this, e);
if ("google.com" != this.providerId) throw Error("Invalid provider ID!");
}
function Br(e) {
Fr.call(this, e);
if ("twitter.com" != this.providerId) throw Error("Invalid provider ID!");
Er(this, "username", e.screenName || null);
}
function Vr(e, t) {
return e.then(function(e) {
if (e[Pl]) {
var n = Dr(e[Pl]);
if (!n || t != n.b) throw new Pr("user-mismatch");
return e;
}
throw new Pr("user-mismatch");
}).s(function(e) {
throw e && e.code && e.code == fl + "user-not-found" ? new Pr("user-mismatch") : e;
});
}
function qr(e, t) {
if (t.idToken || t.accessToken) t.idToken && Er(this, "idToken", t.idToken), t.accessToken && Er(this, "accessToken", t.accessToken); else {
if (!t.oauthToken || !t.oauthTokenSecret) throw new Pr("internal-error", "failed to construct a credential");
Er(this, "accessToken", t.oauthToken), Er(this, "secret", t.oauthTokenSecret);
}
Er(this, "providerId", e);
}
function Kr(e) {
var t = {};
e.idToken && (t.id_token = e.idToken);
e.accessToken && (t.access_token = e.accessToken);
e.secret && (t.oauth_token_secret = e.secret);
t.providerId = e.providerId;
return {
postBody: un(t).toString(),
requestUri: "http://localhost"
};
}
function Gr(e, t) {
this.rc = t || [];
wr(this, {
providerId: e,
isOAuthProvider: !0
});
this.rb = {};
this.Wa = (Lr(e) || {}).Ma || null;
this.Ua = null;
}
function Hr(e) {
Gr.call(this, e, Cl);
this.a = [];
}
function Qr() {
Hr.call(this, "facebook.com");
}
function Xr(e) {
if (!e) throw new Pr("argument-error", "credential failed: expected 1 argument (the OAuth access token).");
var t = e;
c(e) && (t = e.accessToken);
return new Qr().credential(null, t);
}
function Yr() {
Hr.call(this, "github.com");
}
function zr(e) {
if (!e) throw new Pr("argument-error", "credential failed: expected 1 argument (the OAuth access token).");
var t = e;
c(e) && (t = e.accessToken);
return new Yr().credential(null, t);
}
function Jr() {
Hr.call(this, "google.com");
this.sa("profile");
}
function $r(e, t) {
var n = e;
c(e) && (n = e.idToken, t = e.accessToken);
return new Jr().credential(n, t);
}
function Zr() {
Gr.call(this, "twitter.com", wl);
}
function ei(e, t) {
var n = e;
c(n) || (n = {
oauthToken: e,
oauthTokenSecret: t
});
if (!n.oauthToken || !n.oauthTokenSecret) throw new Pr("argument-error", "credential failed: expected 2 arguments (the OAuth access token and secret).");
return new qr("twitter.com", n);
}
function ti(e, t) {
this.a = e;
this.f = t;
Er(this, "providerId", "password");
}
function ni() {
wr(this, {
providerId: "password",
isOAuthProvider: !1
});
}
function ri(e) {
if (!(e.Pa && e.Oa || e.Da && e.Y)) throw new Pr("internal-error");
this.a = e;
Er(this, "providerId", "phone");
}
function ii(e) {
return e.a.Da && e.a.Y ? {
temporaryProof: e.a.Da,
phoneNumber: e.a.Y
} : {
sessionInfo: e.a.Pa,
code: e.a.Oa
};
}
function oi(e) {
try {
this.a = e || ou.auth();
} catch (e) {
throw new Pr("argument-error", "Either an instance of firebase.auth.Auth must be passed as an argument to the firebase.auth.PhoneAuthProvider constructor, or the default firebase App instance must be initialized via firebase.initializeApp().");
}
wr(this, {
providerId: "phone",
isOAuthProvider: !1
});
}
function ai(e, t) {
if (!e) throw new Pr("missing-verification-id");
if (!t) throw new Pr("missing-verification-code");
return new ri({
Pa: e,
Oa: t
});
}
function si(e) {
if (e.temporaryProof && e.phoneNumber) return new ri({
Da: e.temporaryProof,
Y: e.phoneNumber
});
var t = e && e.providerId;
if (!t || "password" === t) return null;
var n = e && e.oauthAccessToken, r = e && e.oauthTokenSecret;
e = e && e.oauthIdToken;
try {
switch (t) {
case "google.com":
return $r(e, n);

case "facebook.com":
return Xr(n);

case "github.com":
return zr(n);

case "twitter.com":
return ei(n, r);

default:
return new Hr(t).credential(e, n);
}
} catch (e) {
return null;
}
}
function ui(e) {
if (!e.isOAuthProvider) throw new Pr("invalid-oauth-provider");
}
function ci(e, t, n, r, i) {
this.b = e;
this.c = t || null;
this.f = n || null;
this.g = r || null;
this.a = i || null;
if (!this.f && !this.a) throw new Pr("invalid-auth-event");
if (this.f && this.a) throw new Pr("invalid-auth-event");
if (this.f && !this.g) throw new Pr("invalid-auth-event");
}
function li(e) {
return (e = e || {}).type ? new ci(e.type, e.eventId, e.urlResponse, e.sessionId, e.error && Or(e.error)) : null;
}
function hi(e) {
var t = "unauthorized-domain", n = void 0, r = en(e);
e = r.b;
"chrome-extension" == (r = r.c) ? n = _("This chrome extension ID (chrome-extension://%s) is not authorized to run this operation. Add it to the OAuth redirect domains list in the Firebase console -> Auth section -> Sign in method tab.", e) : "http" == r || "https" == r ? n = _("This domain (%s) is not authorized to run this operation. Add it to the OAuth redirect domains list in the Firebase console -> Auth section -> Sign in method tab.", e) : t = "operation-not-supported-in-this-environment";
Pr.call(this, t, n);
}
function fi(e, t, n) {
Pr.call(this, e, n);
(e = t || {}).sb && Er(this, "email", e.sb);
e.Y && Er(this, "phoneNumber", e.Y);
e.credential && Er(this, "credential", e.credential);
}
function pi(e) {
if (e.code) {
var t = e.code || "";
0 == t.indexOf(fl) && (t = t.substring(fl.length));
var n = {
credential: si(e)
};
if (e.email) n.sb = e.email; else {
if (!e.phoneNumber) return new Pr(t, e.message || void 0);
n.Y = e.phoneNumber;
}
return new fi(t, n, e.message);
}
return null;
}
function di(e) {
this.f = e;
}
function vi(e, t, n) {
var r = "Node" == Zn();
if (!(r = su.XMLHttpRequest || r && ou.INTERNAL.node && ou.INTERNAL.node.XMLHttpRequest)) throw new Pr("internal-error", "The XMLHttpRequest compatibility library was not found.");
this.b = e;
e = t || {};
this.i = e.secureTokenEndpoint || "https://securetoken.googleapis.com/v1/token";
this.l = e.secureTokenTimeout || Ol;
this.c = M(e.secureTokenHeaders || Rl);
this.g = e.firebaseEndpoint || "https://www.googleapis.com/identitytoolkit/v3/relyingparty/";
this.h = e.firebaseTimeout || Al;
this.a = M(e.firebaseHeaders || kl);
n && (this.a["X-Client-Version"] = n, this.c["X-Client-Version"] = n);
this.f = new Fn();
this.o = new di(r);
}
function _i(e, t) {
t ? e.a["X-Firebase-Locale"] = t : delete e.a["X-Firebase-Locale"];
}
function yi(e, t) {
t ? (e.a["X-Client-Version"] = t, e.c["X-Client-Version"] = t) : (delete e.a["X-Client-Version"], 
delete e.c["X-Client-Version"]);
}
function gi(e, t, n, r, i, o, a) {
_r() ? (Kn() ? e = f(e.m, e) : (Il || (Il = new $(function(e, t) {
mi(e, t);
})), e = f(e.u, e)), e(t, n, r, i, o, a)) : n && n(null);
}
function mi(e, t) {
if (((window.gapi || {}).client || {}).request) e(); else {
su[Ll] = function() {
((window.gapi || {}).client || {}).request ? e() : t(Error("CORS_UNSUPPORTED"));
};
Dt(kn(Se(Dl, {
onload: Ll
})), function() {
t(Error("CORS_UNSUPPORTED"));
});
}
}
function bi(e, t) {
return new $(function(n, r) {
"refresh_token" == t.grant_type && t.refresh_token || "authorization_code" == t.grant_type && t.code ? gi(e, e.i + "?key=" + encodeURIComponent(e.b), function(e) {
e ? e.error ? r(Ui(e)) : e.access_token && e.refresh_token ? n(e) : r(new Pr("internal-error")) : r(new Pr("network-request-failed"));
}, "POST", un(t).toString(), e.c, e.l.get()) : r(new Pr("internal-error"));
});
}
function Ei(e, t, n, r, i, o) {
var a = en(e.g + t);
$t(a, "key", e.b);
o && $t(a, "cb", lu().toString());
var s = "GET" == n;
if (s) for (var u in r) r.hasOwnProperty(u) && $t(a, u, r[u]);
return new $(function(t, o) {
gi(e, a.toString(), function(e) {
e ? e.error ? o(Ui(e, i || {})) : t(e) : o(new Pr("network-request-failed"));
}, n, s ? void 0 : Ue(hr(r)), e.a, e.h.get());
});
}
function wi(e) {
if (!Yc.test(e.email)) throw new Pr("invalid-email");
}
function Ci(e) {
"email" in e && wi(e);
}
function Si(e, t) {
return xi(e, Wl, {
identifier: t,
continueUri: sr() ? Bn() : "http://localhost"
}).then(function(e) {
return e.allProviders || [];
});
}
function Ti(e) {
return xi(e, Hl, {}).then(function(e) {
return e.authorizedDomains || [];
});
}
function Ni(e) {
if (!e[Pl]) throw new Pr("internal-error");
}
function Ii(e) {
if (e.phoneNumber || e.temporaryProof) {
if (!e.phoneNumber || !e.temporaryProof) throw new Pr("internal-error");
} else {
if (!e.sessionInfo) throw new Pr("missing-verification-id");
if (!e.code) throw new Pr("missing-verification-code");
}
}
function Pi(e, t) {
return xi(e, Yl, t);
}
function Oi(e, t, n) {
return xi(e, Vl, {
idToken: t,
deleteProvider: n
});
}
function Ri(e) {
if (!e.requestUri || !e.sessionId && !e.postBody) throw new Pr("internal-error");
}
function Ai(e) {
var t = null;
e.needConfirmation ? (e.code = "account-exists-with-different-credential", t = pi(e)) : "FEDERATED_USER_ID_ALREADY_LINKED" == e.errorMessage ? (e.code = "credential-already-in-use", 
t = pi(e)) : "EMAIL_EXISTS" == e.errorMessage ? (e.code = "email-already-in-use", 
t = pi(e)) : e.errorMessage && (t = Fi(e.errorMessage));
if (t) throw t;
if (!e[Pl]) throw new Pr("internal-error");
}
function ki(e, t) {
t.returnIdpCredential = !0;
return xi(e, Zl, t);
}
function Di(e, t) {
t.returnIdpCredential = !0;
return xi(e, th, t);
}
function Li(e, t) {
t.returnIdpCredential = !0;
t.autoCreate = !1;
return xi(e, eh, t);
}
function Mi(e) {
if (!e.oobCode) throw new Pr("invalid-action-code");
}
function xi(e, t, n) {
if (!Tr(n, t.ea)) return ne(new Pr("internal-error"));
var r, i = t.zb || "POST";
return te(n).then(t.D).then(function() {
t.T && (n.returnSecureToken = !0);
return Ei(e, t.endpoint, i, n, t.Pb, t.nb || !1);
}).then(function(e) {
return r = e;
}).then(t.O).then(function() {
if (!t.ga) return r;
if (!(t.ga in r)) throw new Pr("internal-error");
return r[t.ga];
});
}
function Fi(e) {
return Ui({
error: {
errors: [ {
message: e
} ],
code: 400,
message: e
}
});
}
function Ui(e, t) {
var n = (e.error && e.error.errors && e.error.errors[0] || {}).reason || "", r = {
keyInvalid: "invalid-api-key",
ipRefererBlocked: "app-not-authorized"
};
if (n = r[n] ? new Pr(r[n]) : null) return n;
n = e.error && e.error.message || "";
x(r = {
INVALID_CUSTOM_TOKEN: "invalid-custom-token",
CREDENTIAL_MISMATCH: "custom-token-mismatch",
MISSING_CUSTOM_TOKEN: "internal-error",
INVALID_IDENTIFIER: "invalid-email",
MISSING_CONTINUE_URI: "internal-error",
INVALID_EMAIL: "invalid-email",
INVALID_PASSWORD: "wrong-password",
USER_DISABLED: "user-disabled",
MISSING_PASSWORD: "internal-error",
EMAIL_EXISTS: "email-already-in-use",
PASSWORD_LOGIN_DISABLED: "operation-not-allowed",
INVALID_IDP_RESPONSE: "invalid-credential",
FEDERATED_USER_ID_ALREADY_LINKED: "credential-already-in-use",
INVALID_MESSAGE_PAYLOAD: "invalid-message-payload",
INVALID_RECIPIENT_EMAIL: "invalid-recipient-email",
INVALID_SENDER: "invalid-sender",
EMAIL_NOT_FOUND: "user-not-found",
EXPIRED_OOB_CODE: "expired-action-code",
INVALID_OOB_CODE: "invalid-action-code",
MISSING_OOB_CODE: "internal-error",
CREDENTIAL_TOO_OLD_LOGIN_AGAIN: "requires-recent-login",
INVALID_ID_TOKEN: "invalid-user-token",
TOKEN_EXPIRED: "user-token-expired",
USER_NOT_FOUND: "user-token-expired",
CORS_UNSUPPORTED: "cors-unsupported",
DYNAMIC_LINK_NOT_ACTIVATED: "dynamic-link-not-activated",
INVALID_APP_ID: "invalid-app-id",
TOO_MANY_ATTEMPTS_TRY_LATER: "too-many-requests",
WEAK_PASSWORD: "weak-password",
OPERATION_NOT_ALLOWED: "operation-not-allowed",
USER_CANCELLED: "user-cancelled",
CAPTCHA_CHECK_FAILED: "captcha-check-failed",
INVALID_APP_CREDENTIAL: "invalid-app-credential",
INVALID_CODE: "invalid-verification-code",
INVALID_PHONE_NUMBER: "invalid-phone-number",
INVALID_SESSION_INFO: "invalid-verification-id",
INVALID_TEMPORARY_PROOF: "invalid-credential",
MISSING_APP_CREDENTIAL: "missing-app-credential",
MISSING_CODE: "missing-verification-code",
MISSING_PHONE_NUMBER: "missing-phone-number",
MISSING_SESSION_INFO: "missing-verification-id",
QUOTA_EXCEEDED: "quota-exceeded",
SESSION_EXPIRED: "code-expired",
INVALID_CONTINUE_URI: "invalid-continue-uri",
MISSING_ANDROID_PACKAGE_NAME: "missing-android-pkg-name",
MISSING_IOS_BUNDLE_ID: "missing-ios-bundle-id",
UNAUTHORIZED_DOMAIN: "unauthorized-continue-uri",
INVALID_OAUTH_CLIENT_ID: "invalid-oauth-client-id",
INVALID_CERT_HASH: "invalid-cert-hash"
}, t || {});
t = (t = n.match(/^[^\s]+\s*:\s*(.*)$/)) && 1 < t.length ? t[1] : void 0;
for (var i in r) if (0 === n.indexOf(i)) return new Pr(r[i], t);
!t && e && (t = lr(e));
return new Pr("internal-error", t);
}
function ji(e) {
for (var t in sh) if (sh[t].id === e) return e = sh[t], {
firebaseEndpoint: e.Va,
secureTokenEndpoint: e.ab
};
return null;
}
function Wi(e) {
this.b = e;
this.a = null;
this.Ya = Bi(this);
}
function Bi(e) {
return Ki().then(function() {
return new $(function(t, n) {
ir("gapi.iframes.getContext")().open({
where: document.body,
url: e.b,
messageHandlersFilter: ir("gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER"),
attributes: {
style: {
position: "absolute",
top: "-100px",
width: "1px",
height: "1px"
}
},
dontclear: !0
}, function(r) {
function i() {
clearTimeout(o);
t();
}
e.a = r;
e.a.restyle({
setHideOnLeave: !1
});
var o = setTimeout(function() {
n(Error("Network Error"));
}, lh.get());
r.ping(i).then(i, function() {
n(Error("Network Error"));
});
});
});
});
}
function Vi(e, t) {
return e.Ya.then(function() {
return new $(function(n) {
e.a.send(t.type, t, n, ir("gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER"));
});
});
}
function qi(e, t) {
e.Ya.then(function() {
e.a.register("authEvent", t, ir("gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER"));
});
}
function Ki() {
return hh || (hh = new $(function(e, t) {
if (_r()) {
var n = function() {
vr();
ir("gapi.load")("gapi.iframes", {
callback: e,
ontimeout: function() {
vr();
t(Error("Network Error"));
},
timeout: ch.get()
});
};
if (ir("gapi.iframes.Iframe")) e(); else if (ir("gapi.load")) n(); else {
var r = "__iframefcb" + Math.floor(1e6 * Math.random()).toString();
su[r] = function() {
ir("gapi.load") ? n() : t(Error("Network Error"));
};
te(kn(r = Se(uh, {
onload: r
}))).s(function() {
t(Error("Network Error"));
});
}
} else t(Error("Network Error"));
}).s(function(e) {
hh = null;
throw e;
}));
}
function Gi(e, t, n) {
this.i = e;
this.g = t;
this.h = n;
this.f = null;
this.a = tn(this.i, "/__/auth/iframe");
$t(this.a, "apiKey", this.g);
$t(this.a, "appName", this.h);
this.b = null;
this.c = [];
}
function Hi(e, t, n, r, i) {
this.m = e;
this.u = t;
this.c = n;
this.l = r;
this.i = this.g = this.h = null;
this.a = i;
this.f = null;
}
function Qi(e) {
try {
return ou.app(e).auth().Ka();
} catch (e) {
return [];
}
}
function Xi(e, t, n, r, i) {
this.u = e;
this.f = t;
this.b = n;
this.c = r || null;
this.h = i || null;
this.m = this.o = this.v = null;
this.g = [];
this.l = this.a = null;
}
function Yi(e) {
var t = Bn();
return Ti(e).then(function(e) {
e: {
var n = en(t), r = n.c;
n = n.b;
for (var i = 0; i < e.length; i++) {
var o = e[i], a = n, s = r;
0 == o.indexOf("chrome-extension://") ? a = en(o).b == a && "chrome-extension" == s : "http" != s && "https" != s ? a = !1 : $c.test(o) ? a = a == o : (o = o.split(".").join("\\."), 
a = new RegExp("^(.+\\." + o + "|" + o + ")$", "i").test(a));
if (a) {
e = !0;
break e;
}
}
e = !1;
}
if (!e) throw new hi(Bn());
});
}
function zi(e) {
if (e.l) return e.l;
e.l = Yn().then(function() {
if (!e.o) {
var t = e.c, n = e.h, r = Qi(e.b), i = new Gi(e.u, e.f, e.b);
i.f = t;
i.b = n;
i.c = P(r || []);
e.o = i.toString();
}
e.i = new Wi(e.o);
Zi(e);
});
return e.l;
}
function Ji(e) {
e.m || (e.v = e.c ? nr(e.c, Qi(e.b)) : null, e.m = new vi(e.f, ji(e.h), e.v));
return e.m;
}
function $i(e, t, n, r, i, o, a, s, u, c) {
(e = new Hi(e, t, n, r, i)).h = o;
e.g = a;
e.i = s;
e.b = M(u || null);
e.f = c;
return e.toString();
}
function Zi(e) {
if (!e.i) throw Error("IfcHandler must be initialized!");
qi(e.i, function(t) {
var n = {};
if (t && t.authEvent) {
var r = !1;
t = li(t.authEvent);
for (n = 0; n < e.g.length; n++) r = e.g[n](t) || r;
(n = {}).status = r ? "ACK" : "ERROR";
return te(n);
}
n.status = "ERROR";
return te(n);
});
}
function eo(e) {
var t = {
type: "webStorageSupport"
};
return zi(e).then(function() {
return Vi(e.i, t);
}).then(function(e) {
if (e && e.length && "undefined" != typeof e[0].webStorageSupport) return e[0].webStorageSupport;
throw Error();
});
}
function to(e) {
this.a = e || ou.INTERNAL.reactNative && ou.INTERNAL.reactNative.AsyncStorage;
if (!this.a) throw new Pr("internal-error", "The React Native compatibility library was not found.");
}
function no() {
this.a = {};
}
function ro(e, t, n, r, i, o) {
try {
var a = !!su.indexedDB;
} catch (e) {
a = !1;
}
if (!a) throw new Pr("web-storage-unsupported");
this.u = e;
this.h = t;
this.g = n;
this.l = r;
this.m = i;
this.f = {};
this.c = [];
this.a = 0;
this.o = o || su.indexedDB;
}
function io(e) {
return new $(function(t, n) {
var r = e.o.open(e.u, e.m);
r.onerror = function(e) {
n(Error(e.target.errorCode));
};
r.onupgradeneeded = function(t) {
t = t.target.result;
try {
t.createObjectStore(e.h, {
keyPath: e.g
});
} catch (e) {
n(e);
}
};
r.onsuccess = function(e) {
t(e.target.result);
};
});
}
function oo(e) {
e.i || (e.i = io(e));
return e.i;
}
function ao(e, t) {
return t.objectStore(e.h);
}
function so(e, t, n) {
return t.transaction([ e.h ], n ? "readwrite" : "readonly");
}
function uo(e) {
return new $(function(t, n) {
e.onsuccess = function(e) {
e && e.target ? t(e.target.result) : t();
};
e.onerror = function(e) {
n(Error(e.target.errorCode));
};
});
}
function co(e) {
function t() {
e.b = _t(800).then(f(e.zc, e)).then(function(t) {
0 < t.length && Eu(e.c, function(e) {
e(t);
});
}).then(t).s(function(e) {
"STOP_EVENT" != e.message && t();
});
return e.b;
}
e.b && e.b.cancel("STOP_EVENT");
t();
}
function lo() {
if (!fo()) {
if ("Node" == Zn()) throw new Pr("internal-error", "The LocalStorage compatibility library was not found.");
throw new Pr("web-storage-unsupported");
}
this.a = ho() || ou.INTERNAL.node.localStorage;
}
function ho() {
try {
var e = su.localStorage, t = pr();
e && (e.setItem(t, "1"), e.removeItem(t));
return e;
} catch (e) {
return null;
}
}
function fo() {
var e = "Node" == Zn();
if (!(e = ho() || e && ou.INTERNAL.node && ou.INTERNAL.node.localStorage)) return !1;
try {
return e.setItem("__sak", "1"), e.removeItem("__sak"), !0;
} catch (e) {
return !1;
}
}
function po() {}
function vo() {
if (!yo()) {
if ("Node" == Zn()) throw new Pr("internal-error", "The SessionStorage compatibility library was not found.");
throw new Pr("web-storage-unsupported");
}
this.a = _o() || ou.INTERNAL.node.sessionStorage;
}
function _o() {
try {
var e = su.sessionStorage, t = pr();
e && (e.setItem(t, "1"), e.removeItem(t));
return e;
} catch (e) {
return null;
}
}
function yo() {
var e = "Node" == Zn();
if (!(e = _o() || e && ou.INTERNAL.node && ou.INTERNAL.node.sessionStorage)) return !1;
try {
return e.setItem("__sak", "1"), e.removeItem("__sak"), !0;
} catch (e) {
return !1;
}
}
function go() {
var e = {};
e.Browser = vh;
e.Node = _h;
e.ReactNative = yh;
this.a = e[Zn()];
}
function mo(e) {
var t = new Pr("invalid-persistence-type"), n = new Pr("unsupported-persistence-type");
e: {
for (r in gh) if (gh[r] == e) {
var r = !0;
break e;
}
r = !1;
}
if (!r || "string" != typeof e) throw t;
switch (Zn()) {
case "ReactNative":
if ("session" === e) throw n;
break;

case "Node":
if ("none" !== e) throw n;
break;

default:
if (!or() && "none" !== e) throw n;
}
}
function bo(e, t, n, r, i) {
this.i = e;
this.g = t;
this.A = n;
this.u = r;
this.v = i;
this.a = {};
ph || (ph = new go());
e = ph;
try {
if (Wn()) {
fh || (fh = new ro("firebaseLocalStorageDb", "firebaseLocalStorage", "fbase_key", "value", 1));
var o = fh;
} else o = new e.a.C();
this.l = o;
} catch (e) {
this.l = new no(), this.u = !0;
}
try {
this.o = new e.a.jb();
} catch (e) {
this.o = new no();
}
this.w = new no();
this.h = f(this.m, this);
this.b = {};
}
function Eo() {
dh || (dh = new bo("firebase", ":", !(dr(rr()) || !$n()), cr(), or()));
return dh;
}
function wo(e, t) {
switch (t) {
case "session":
return e.o;

case "none":
return e.w;

default:
return e.l;
}
}
function Co(e, t, n) {
return e.i + e.g + t.name + (n ? e.g + n : "");
}
function So(e, t, n) {
n = Co(e, t, n);
"local" == t.C && (e.b[n] = null);
return wo(e, t.C).X(n);
}
function To(e, t, n, r) {
t = Co(e, t, n);
e.v && (e.b[t] = su.localStorage.getItem(t));
D(e.a) && (wo(e, "local").ia(e.h), e.u || Wn() || !e.v || Io(e));
e.a[t] || (e.a[t] = []);
e.a[t].push(r);
}
function No(e, t, n) {
t = Co(e, us("local"), t);
e.a[t] && (N(e.a[t], function(e) {
return e == n;
}), 0 == e.a[t].length && delete e.a[t]);
D(e.a) && Oo(e);
}
function Io(e) {
Po(e);
e.f = setInterval(function() {
for (var t in e.a) {
var n = su.localStorage.getItem(t), r = e.b[t];
n != r && (e.b[t] = n, n = new Ge({
type: "storage",
key: t,
target: window,
oldValue: r,
newValue: n,
a: !0
}), e.m(n));
}
}, 1e3);
}
function Po(e) {
e.f && (clearInterval(e.f), e.f = null);
}
function Oo(e) {
wo(e, "local").da(e.h);
Po(e);
}
function Ro(e) {
this.a = e;
this.b = Eo();
}
function Ao(e) {
return e.b.get(mh, e.a).then(function(e) {
return li(e);
});
}
function ko() {
this.a = Eo();
}
function Do(e, t, n, r, i, o, a) {
this.u = e;
this.i = t;
this.l = n;
this.m = r || null;
this.o = a || null;
this.h = t + ":" + n;
this.A = new ko();
this.g = new Ro(this.h);
this.f = null;
this.b = [];
this.v = i || 500;
this.w = o || 2e3;
this.a = this.c = null;
}
function Lo(e) {
return new Pr("invalid-cordova-configuration", e);
}
function Mo() {
for (var e = 20, t = []; 0 < e; ) t.push("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(Math.floor(62 * Math.random()))), 
e--;
return t.join("");
}
function xo(e) {
var t = new qt();
Vt(t, e);
e = [];
var n = 8 * t.g;
56 > t.c ? Vt(t, Lc, 56 - t.c) : Vt(t, Lc, t.b - (t.c - 56));
for (var r = 63; 56 <= r; r--) t.f[r] = 255 & n, n /= 256;
Bt(t);
for (r = n = 0; r < t.i; r++) for (var i = 24; 0 <= i; i -= 8) e[n++] = t.a[r] >> i & 255;
return Nt(e);
}
function Fo(e, t, n, r) {
var i = Mo(), o = new ci(t, r, null, i, new Pr("no-auth-event")), a = ir("BuildInfo.packageName", su);
if ("string" != typeof a) throw new Pr("invalid-cordova-configuration");
var s = ir("BuildInfo.displayName", su), u = {};
if (rr().toLowerCase().match(/iphone|ipad|ipod/)) u.ibi = a; else {
if (!rr().toLowerCase().match(/android/)) return ne(new Pr("operation-not-supported-in-this-environment"));
u.apn = a;
}
s && (u.appDisplayName = s);
i = xo(i);
u.sessionId = i;
var c = $i(e.u, e.i, e.l, t, n, null, r, e.m, u, e.o);
return e.ba().then(function() {
var t = e.h;
return e.A.a.set(mh, o.B(), t);
}).then(function() {
var t = ir("cordova.plugins.browsertab.isAvailable", su);
if ("function" != typeof t) throw new Pr("invalid-cordova-configuration");
var n = null;
t(function(t) {
if (t) {
if ("function" != typeof (n = ir("cordova.plugins.browsertab.openUrl", su))) throw new Pr("invalid-cordova-configuration");
n(c);
} else {
if ("function" != typeof (n = ir("cordova.InAppBrowser.open", su))) throw new Pr("invalid-cordova-configuration");
t = !(!(t = rr()).match(/(iPad|iPhone|iPod).*OS 7_\d/i) && !t.match(/(iPad|iPhone|iPod).*OS 8_\d/i));
e.a = n(c, t ? "_blank" : "_system", "location=yes");
}
});
});
}
function Uo(e, t) {
for (var n = 0; n < e.b.length; n++) try {
e.b[n](t);
} catch (e) {}
}
function jo(e) {
e.f || (e.f = e.ba().then(function() {
return new $(function(t) {
function n(r) {
t(r);
e.Ja(n);
return !1;
}
e.ua(n);
Bo(e);
});
}));
return e.f;
}
function Wo(e) {
var t = null;
return Ao(e.g).then(function(n) {
t = n;
return So((n = e.g).b, mh, n.a);
}).then(function() {
return t;
});
}
function Bo(e) {
function t(t) {
i = !0;
o && o.cancel();
Wo(e).then(function(n) {
var i = r;
if (n && t && t.url) {
i = null;
var o = t.url, a = en(o), s = Zt(a, "link"), u = Zt(en(s), "link");
-1 != (o = Zt(en(a = Zt(a, "deep_link_id")), "link") || a || u || s || o).indexOf("/__/auth/callback") && (i = en(o), 
i = fr(Zt(i, "firebaseError") || null), i = (i = "object" == typeof i ? Or(i) : null) ? new ci(n.b, n.c, null, null, i) : new ci(n.b, n.c, o, n.g));
i = i || r;
}
Uo(e, i);
});
}
var n = ir("universalLinks.subscribe", su);
if ("function" != typeof n) throw new Pr("invalid-cordova-configuration");
var r = new ci("unknown", null, null, null, new Pr("no-auth-event")), i = !1, o = _t(e.v).then(function() {
return Wo(e).then(function() {
i || Uo(e, r);
});
}), a = su.handleOpenURL;
su.handleOpenURL = function(e) {
0 == e.toLowerCase().indexOf(ir("BuildInfo.packageName", su).toLowerCase() + "://") && t({
url: e
});
if ("function" == typeof a) try {
a(e);
} catch (e) {
console.error(e);
}
};
n(null, t);
}
function Vo(e) {
this.a = e;
this.b = Eo();
}
function qo(e) {
return e.b.set(bh, "pending", e.a);
}
function Ko(e) {
return So(e.b, bh, e.a);
}
function Go(e) {
return e.b.get(bh, e.a).then(function(e) {
return "pending" == e;
});
}
function Ho(e, t, n) {
this.v = e;
this.l = t;
this.u = n;
this.h = [];
this.f = !1;
this.i = f(this.m, this);
this.c = new Zo();
this.o = new aa();
this.g = new Vo(this.l + ":" + this.u);
this.b = {};
this.b.unknown = this.c;
this.b.signInViaRedirect = this.c;
this.b.linkViaRedirect = this.c;
this.b.reauthViaRedirect = this.c;
this.b.signInViaPopup = this.o;
this.b.linkViaPopup = this.o;
this.b.reauthViaPopup = this.o;
this.a = Qo(this.v, this.l, this.u, xl);
}
function Qo(e, t, n, r) {
var i = ou.SDK_VERSION || null;
return Jn() ? new Do(e, t, n, i, void 0, void 0, r) : new Xi(e, t, n, i, r);
}
function Xo(e) {
e.f || (e.f = !0, e.a.ua(e.i));
var t = e.a;
return e.a.ba().s(function(n) {
e.a == t && e.reset();
throw n;
});
}
function Yo(e) {
e.a.Db() && Xo(e).s(function(t) {
var n = new ci("unknown", null, null, null, new Pr("operation-not-supported-in-this-environment"));
Jo(t) && e.m(n);
});
e.a.yb() || ea(e.c);
}
function zo(e, t, n, r, i, o) {
return e.a.vb(t, n, r, function() {
e.f || (e.f = !0, e.a.ua(e.i));
}, function() {
e.reset();
}, i, o);
}
function Jo(e) {
return !(!e || "auth/cordova-not-ready" != e.code);
}
function $o(e, t, n) {
var r = t + ":" + n;
Ch[r] || (Ch[r] = new Ho(e, t, n));
return Ch[r];
}
function Zo() {
this.b = null;
this.f = [];
this.c = [];
this.a = null;
this.g = !1;
}
function ea(e) {
e.g || (e.g = !0, ia(e, !1, null, null));
}
function ta(e, t, n) {
n = n.va(t.b, t.c);
var r = t.f, i = t.g, o = !!t.b.match(/Redirect$/);
return n(r, i).then(function(t) {
ia(e, o, t, null);
}).s(function(t) {
ia(e, o, null, t);
});
}
function na(e, t) {
e.b = function() {
return ne(t);
};
if (e.c.length) for (var n = 0; n < e.c.length; n++) e.c[n](t);
}
function ra(e, t) {
e.b = function() {
return te(t);
};
if (e.f.length) for (var n = 0; n < e.f.length; n++) e.f[n](t);
}
function ia(e, t, n, r) {
t ? r ? na(e, r) : ra(e, n) : ra(e, {
user: null
});
e.f = [];
e.c = [];
}
function oa(e) {
var t = new Pr("timeout");
e.a && e.a.cancel();
e.a = _t(wh.get()).then(function() {
e.b || ia(e, !0, null, t);
});
}
function aa() {}
function sa(e, t) {
var n = e.c, r = e.b;
return t.va(r, n)(e.f, e.g).then(function(e) {
t.fa(r, e, null, n);
}).s(function(e) {
t.fa(r, null, e, n);
});
}
function ua(e, t) {
this.a = t;
Er(this, "verificationId", e);
}
function ca(e, t, n, r) {
return new oi(e).Qa(t, n).then(function(e) {
return new ua(e, r);
});
}
function la(e, t, n, r, i, o) {
this.h = e;
this.i = t;
this.g = n;
this.c = r;
this.f = i;
this.l = !!o;
this.b = null;
this.a = this.c;
if (this.f < this.c) throw Error("Proactive refresh lower bound greater than upper bound!");
}
function ha(e, t) {
if (t) return e.a = e.c, e.g();
t = e.a;
e.a *= 2;
e.a > e.f && (e.a = e.f);
return t;
}
function fa(e, t) {
pa(e);
e.b = _t(ha(e, t)).then(function() {
return e.l ? te() : mr();
}).then(function() {
return e.h();
}).then(function() {
fa(e, !0);
}).s(function(t) {
e.i(t) && fa(e, !1);
});
}
function pa(e) {
e.b && (e.b.cancel(), e.b = null);
}
function da(e) {
this.f = e;
this.b = this.a = null;
this.c = 0;
}
function va(e, t) {
var n = t[Pl], r = t.refreshToken;
t = _a(t.expiresIn);
e.b = n;
e.c = t;
e.a = r;
}
function _a(e) {
return lu() + 1e3 * parseInt(e, 10);
}
function ya(e, t) {
return bi(e.f, t).then(function(t) {
e.b = t.access_token;
e.c = _a(t.expires_in);
e.a = t.refresh_token;
return {
accessToken: e.b,
expirationTime: e.c,
refreshToken: e.a
};
}).s(function(t) {
"auth/user-token-expired" == t.code && (e.a = null);
throw t;
});
}
function ga(e, t) {
this.a = e || null;
this.b = t || null;
wr(this, {
lastSignInTime: br(t || null),
creationTime: br(e || null)
});
}
function ma(e) {
return new ga(e.a, e.b);
}
function ba(e, t, n, r, i, o) {
wr(this, {
uid: e,
displayName: r || null,
photoURL: i || null,
email: n || null,
phoneNumber: o || null,
providerId: t
});
}
function Ea(e, t) {
Ke.call(this, e);
for (var n in t) this[n] = t[n];
}
function wa(e, t, n) {
this.A = [];
this.G = e.apiKey;
this.o = e.appName;
this.w = e.authDomain || null;
e = ou.SDK_VERSION ? nr(ou.SDK_VERSION) : null;
this.c = new vi(this.G, ji(xl), e);
this.h = new da(this.c);
Ra(this, t[Pl]);
va(this.h, t);
Er(this, "refreshToken", this.h.a);
La(this, n || {});
lt.call(this);
this.I = !1;
this.w && ar() && (this.a = $o(this.w, this.G, this.o));
this.N = [];
this.i = null;
this.l = Ia(this);
this.U = f(this.Ga, this);
var r = this;
this.ha = null;
this.ra = function(e) {
r.na(e.h);
};
this.W = null;
this.R = [];
this.qa = function(e) {
Sa(r, e.f);
};
this.V = null;
}
function Ca(e, t) {
e.W && nt(e.W, "languageCodeChanged", e.ra);
(e.W = t) && $e(t, "languageCodeChanged", e.ra);
}
function Sa(e, t) {
e.R = t;
yi(e.c, ou.SDK_VERSION ? nr(ou.SDK_VERSION, e.R) : null);
}
function Ta(e, t) {
e.V && nt(e.V, "frameworkChanged", e.qa);
(e.V = t) && $e(t, "frameworkChanged", e.qa);
}
function Na(e) {
try {
return ou.app(e.o).auth();
} catch (t) {
throw new Pr("internal-error", "No firebase.auth.Auth instance is available for the Firebase App '" + e.o + "'!");
}
}
function Ia(e) {
return new la(function() {
return e.F(!0);
}, function(e) {
return !(!e || "auth/network-request-failed" != e.code);
}, function() {
var t = e.h.c - lu() - 3e5;
return 0 < t ? t : 0;
}, 3e4, 96e4, !1);
}
function Pa(e) {
e.m || e.l.b || (e.l.start(), nt(e, "tokenChanged", e.U), $e(e, "tokenChanged", e.U));
}
function Oa(e) {
nt(e, "tokenChanged", e.U);
pa(e.l);
}
function Ra(e, t) {
e.pa = t;
Er(e, "_lat", t);
}
function Aa(e, t) {
N(e.N, function(e) {
return e == t;
});
}
function ka(e) {
for (var t = [], n = 0; n < e.N.length; n++) t.push(e.N[n](e));
return oe(t).then(function() {
return e;
});
}
function Da(e) {
e.a && !e.I && (e.I = !0, e.a.subscribe(e));
}
function La(e, t) {
wr(e, {
uid: t.uid,
displayName: t.displayName || null,
photoURL: t.photoURL || null,
email: t.email || null,
emailVerified: t.emailVerified || !1,
phoneNumber: t.phoneNumber || null,
isAnonymous: t.isAnonymous || !1,
metadata: new ga(t.createdAt, t.lastLoginAt),
providerData: []
});
}
function Ma() {}
function xa(e) {
return te().then(function() {
if (e.m) throw new Pr("app-deleted");
});
}
function Fa(e) {
return wu(e.providerData, function(e) {
return e.providerId;
});
}
function Ua(e, t) {
t && (ja(e, t.providerId), e.providerData.push(t));
}
function ja(e, t) {
N(e.providerData, function(e) {
return e.providerId == t;
});
}
function Wa(e, t, n) {
("uid" != t || n) && e.hasOwnProperty(t) && Er(e, t, n);
}
function Ba(e, t) {
e != t && (wr(e, {
uid: t.uid,
displayName: t.displayName,
photoURL: t.photoURL,
email: t.email,
emailVerified: t.emailVerified,
phoneNumber: t.phoneNumber,
isAnonymous: t.isAnonymous,
providerData: []
}), t.metadata ? Er(e, "metadata", ma(t.metadata)) : Er(e, "metadata", new ga()), 
Eu(t.providerData, function(t) {
Ua(e, t);
}), e.h = t.h, Er(e, "refreshToken", e.h.a));
}
function Va(e) {
return e.F().then(function(t) {
var n = e.isAnonymous;
return Ka(e, t).then(function() {
n || Wa(e, "isAnonymous", !1);
return t;
});
});
}
function qa(e, t) {
t[Pl] && e.pa != t[Pl] && (va(e.h, t), ht(e, new Ea("tokenChanged")), Ra(e, t[Pl]), 
Wa(e, "refreshToken", e.h.a));
}
function Ka(e, t) {
return xi(e.c, ql, {
idToken: t
}).then(f(e.kc, e));
}
function Ga(e) {
return (e = e.providerUserInfo) && e.length ? wu(e, function(e) {
return new ba(e.rawId, e.providerId, e.email, e.displayName, e.photoUrl, e.phoneNumber);
}) : [];
}
function Ha(e, t) {
return Va(e).then(function() {
if (S(Fa(e), t)) return ka(e).then(function() {
throw new Pr("provider-already-linked");
});
});
}
function Qa(e, t, n) {
return Cr({
user: e,
credential: si(t),
additionalUserInfo: t = Mr(t),
operationType: n
});
}
function Xa(e, t) {
qa(e, t);
return e.reload().then(function() {
return e;
});
}
function Ya(e, t, n, r, i) {
if (!ar()) return ne(new Pr("operation-not-supported-in-this-environment"));
if (e.i && !i) return ne(e.i);
var o = Lr(n.providerId), a = pr(e.uid + ":::"), s = null;
(!cr() || $n()) && e.w && n.isOAuthProvider && (s = $i(e.w, e.G, e.o, t, n, null, a, ou.SDK_VERSION || null));
var u = Qn(s, o && o.za, o && o.ya);
r = r().then(function() {
Ja(e);
if (!i) return e.F().then(function() {});
}).then(function() {
return zo(e.a, u, t, n, a, !!s);
}).then(function() {
return new $(function(n, r) {
e.fa(t, null, new Pr("cancelled-popup-request"), e.g || null);
e.f = n;
e.v = r;
e.g = a;
e.b = e.a.Ca(e, t, u, a);
});
}).then(function(e) {
u && Hn(u);
return e ? Cr(e) : null;
}).s(function(e) {
u && Hn(u);
throw e;
});
return $a(e, r, i);
}
function za(e, t, n, r, i) {
if (!ar()) return ne(new Pr("operation-not-supported-in-this-environment"));
if (e.i && !i) return ne(e.i);
var o = null, a = pr(e.uid + ":::");
r = r().then(function() {
Ja(e);
if (!i) return e.F().then(function() {});
}).then(function() {
e.Z = a;
return ka(e);
}).then(function(t) {
e.ca && (t = e.ca, t = t.b.set(Sh, e.B(), t.a));
return t;
}).then(function() {
return e.a.Aa(t, n, a);
}).s(function(t) {
o = t;
if (e.ca) return rs(e.ca);
throw o;
}).then(function() {
if (o) throw o;
});
return $a(e, r, i);
}
function Ja(e) {
if (!e.a || !e.I) {
if (e.a && !e.I) throw new Pr("internal-error");
throw new Pr("auth-domain-config-required");
}
}
function $a(e, t, n) {
var r = Za(e, t, n);
e.A.push(r);
ae(r, function() {
T(e.A, r);
});
return r;
}
function Za(e, t, n) {
return e.i && !n ? (t.cancel(), ne(e.i)) : t.s(function(t) {
!t || "auth/user-disabled" != t.code && "auth/user-token-expired" != t.code || (e.i || ht(e, new Ea("userInvalidated")), 
e.i = t);
throw t;
});
}
function es(e) {
if (!e.apiKey) return null;
var t = {
apiKey: e.apiKey,
authDomain: e.authDomain,
appName: e.appName
}, n = {};
if (!(e.stsTokenManager && e.stsTokenManager.accessToken && e.stsTokenManager.expirationTime)) return null;
n[Pl] = e.stsTokenManager.accessToken, n.refreshToken = e.stsTokenManager.refreshToken || null, 
n.expiresIn = (e.stsTokenManager.expirationTime - lu()) / 1e3;
var r = new wa(t, n, e);
e.providerData && Eu(e.providerData, function(e) {
e && Ua(r, Cr(e));
});
e.redirectEventId && (r.Z = e.redirectEventId);
return r;
}
function ts(e, t, n, r) {
var i = new wa(e, t);
n && (i.ca = n);
r && Sa(i, r);
return i.reload().then(function() {
return i;
});
}
function ns(e) {
this.a = e;
this.b = Eo();
}
function rs(e) {
return So(e.b, Sh, e.a);
}
function is(e, t) {
return e.b.get(Sh, e.a).then(function(e) {
e && t && (e.authDomain = t);
return es(e || {});
});
}
function os(e, t) {
this.a = e;
this.b = t || Eo();
this.c = null;
this.f = ss(this);
To(this.b, us("local"), this.a, f(this.g, this));
}
function as(e, t) {
var n, r = [];
for (n in gh) gh[n] !== t && r.push(So(e.b, us(gh[n]), e.a));
r.push(So(e.b, Th, e.a));
return ie(r);
}
function ss(e) {
var t = us("local"), n = us("session"), r = us("none");
return e.b.get(n, e.a).then(function(i) {
return i ? n : e.b.get(r, e.a).then(function(n) {
return n ? r : e.b.get(t, e.a).then(function(n) {
return n ? t : e.b.get(Th, e.a).then(function(e) {
return e ? us(e) : t;
});
});
});
}).then(function(t) {
e.c = t;
return as(e, t.C);
}).s(function() {
e.c || (e.c = t);
});
}
function us(e) {
return {
name: "authUser",
C: e
};
}
function cs(e) {
return ps(e, function() {
return e.b.set(Th, e.c.C, e.a);
});
}
function ls(e, t) {
return ps(e, function() {
return e.b.set(e.c, t.B(), e.a);
});
}
function hs(e) {
return ps(e, function() {
return So(e.b, e.c, e.a);
});
}
function fs(e, t) {
return ps(e, function() {
return e.b.get(e.c, e.a).then(function(e) {
e && t && (e.authDomain = t);
return es(e || {});
});
});
}
function ps(e, t) {
e.f = e.f.then(t, t);
return e.f;
}
function ds(e) {
this.l = !1;
Er(this, "app", e);
if (!Ns(this).options || !Ns(this).options.apiKey) throw new Pr("invalid-api-key");
e = ou.SDK_VERSION ? nr(ou.SDK_VERSION) : null, this.c = new vi(Ns(this).options && Ns(this).options.apiKey, ji(xl), e);
this.N = [];
this.m = [];
this.I = [];
this.Kb = ou.INTERNAL.createSubscribe(f(this.ac, this));
this.R = void 0;
this.Lb = ou.INTERNAL.createSubscribe(f(this.bc, this));
Es(this, null);
this.h = new os(Ns(this).options.apiKey + ":" + Ns(this).name);
this.G = new ns(Ns(this).options.apiKey + ":" + Ns(this).name);
this.U = As(this, Cs(this));
this.i = As(this, Ss(this));
this.W = !1;
this.ha = f(this.yc, this);
this.Ga = f(this.ka, this);
this.pa = f(this.Tb, this);
this.qa = f(this.Zb, this);
this.ra = f(this.$b, this);
ms(this);
this.INTERNAL = {};
this.INTERNAL.delete = f(this.delete, this);
this.INTERNAL.logFramework = f(this.gc, this);
this.o = 0;
lt.call(this);
ys(this);
this.A = [];
}
function vs(e) {
Ke.call(this, "languageCodeChanged");
this.h = e;
}
function _s(e) {
Ke.call(this, "frameworkChanged");
this.f = e;
}
function ys(e) {
Object.defineProperty(e, "lc", {
get: function() {
return this.$();
},
set: function(e) {
this.na(e);
},
enumerable: !1
});
e.V = null;
}
function gs(e) {
return e.Jb || ne(new Pr("auth-domain-config-required"));
}
function ms(e) {
var t = Ns(e).options.authDomain, n = Ns(e).options.apiKey;
t && ar() && (e.Jb = e.U.then(function() {
if (!e.l) {
e.a = $o(t, n, Ns(e).name);
e.a.subscribe(e);
Is(e) && Da(Is(e));
if (e.w) {
Da(e.w);
var r = e.w;
r.na(e.$());
Ca(r, e);
Sa(r = e.w, e.A);
Ta(r, e);
e.w = null;
}
return e.a;
}
}));
}
function bs(e, t) {
var n = {};
n.apiKey = Ns(e).options.apiKey;
n.authDomain = Ns(e).options.authDomain;
n.appName = Ns(e).name;
return e.U.then(function() {
return ts(n, t, e.G, e.Ka());
}).then(function(t) {
if (Is(e) && t.uid == Is(e).uid) return Ba(Is(e), t), e.ka(t);
Es(e, t);
Da(t);
return e.ka(t);
}).then(function() {
Os(e);
});
}
function Es(e, t) {
Is(e) && (Aa(Is(e), e.Ga), nt(Is(e), "tokenChanged", e.pa), nt(Is(e), "userDeleted", e.qa), 
nt(Is(e), "userInvalidated", e.ra), Oa(Is(e)));
t && (t.N.push(e.Ga), $e(t, "tokenChanged", e.pa), $e(t, "userDeleted", e.qa), $e(t, "userInvalidated", e.ra), 
0 < e.o && Pa(t));
Er(e, "currentUser", t);
t && (t.na(e.$()), Ca(t, e), Sa(t, e.A), Ta(t, e));
}
function ws(e) {
var t = is(e.G, Ns(e).options.authDomain).then(function(t) {
(e.w = t) && (t.ca = e.G);
return rs(e.G);
});
return As(e, t);
}
function Cs(e) {
var t = Ns(e).options.authDomain, n = ws(e).then(function() {
return fs(e.h, t);
}).then(function(t) {
return t ? (t.ca = e.G, e.w && (e.w.Z || null) == (t.Z || null) ? t : t.reload().then(function() {
return ls(e.h, t).then(function() {
return t;
});
}).s(function(n) {
return "auth/network-request-failed" == n.code ? t : hs(e.h);
})) : null;
}).then(function(t) {
Es(e, t || null);
});
return As(e, n);
}
function Ss(e) {
return e.U.then(function() {
return e.aa();
}).s(function() {}).then(function() {
if (!e.l) return e.ha();
}).s(function() {}).then(function() {
if (!e.l) {
e.W = !0;
var t = e.h;
To(t.b, us("local"), t.a, e.ha);
}
});
}
function Ts(e, t) {
var n = null, r = null;
return As(e, t.then(function(t) {
n = si(t);
r = Mr(t);
return bs(e, t);
}).then(function() {
return Cr({
user: Is(e),
credential: n,
additionalUserInfo: r,
operationType: "signIn"
});
}));
}
function Ns(e) {
return e.app;
}
function Is(e) {
return e.currentUser;
}
function Ps(e) {
return Is(e) && Is(e)._lat || null;
}
function Os(e) {
if (e.W) {
for (var t = 0; t < e.m.length; t++) e.m[t] && e.m[t](Ps(e));
if (e.R !== e.getUid() && e.I.length) for (e.R = e.getUid(), t = 0; t < e.I.length; t++) e.I[t] && e.I[t](Ps(e));
}
}
function Rs(e, t) {
e.I.push(t);
As(e, e.i.then(function() {
!e.l && S(e.I, t) && e.R !== e.getUid() && (e.R = e.getUid(), t(Ps(e)));
}));
}
function As(e, t) {
e.N.push(t);
ae(t, function() {
T(e.N, t);
});
return t;
}
function ks(e, t, n, r) {
e: {
n = Array.prototype.slice.call(n);
for (var i = 0, o = !1, a = 0; a < t.length; a++) if (t[a].optional) o = !0; else {
if (o) throw new Pr("internal-error", "Argument validator encountered a required argument after an optional argument.");
i++;
}
o = t.length;
if (n.length < i || o < n.length) r = "Expected " + (i == o ? 1 == i ? "1 argument" : i + " arguments" : i + "-" + o + " arguments") + " but got " + n.length + "."; else {
for (i = 0; i < n.length; i++) if (o = t[i].optional && void 0 === n[i], !t[i].M(n[i]) && !o) {
t = t[i];
if (0 > i || i >= Nh.length) throw new Pr("internal-error", "Argument validator received an unsupported number of arguments.");
n = Nh[i];
r = (r ? "" : n + " argument ") + (t.name ? '"' + t.name + '" ' : "") + "must be " + t.K + ".";
break e;
}
r = null;
}
}
if (r) throw new Pr("argument-error", e + " failed: " + r);
}
function Ds(e, n) {
return {
name: e || "",
K: "a valid string",
optional: !!n,
M: t
};
}
function Ls() {
return {
name: "opt_forceRefresh",
K: "a boolean",
optional: !0,
M: n
};
}
function Ms(e, t) {
return {
name: e || "",
K: "a valid object",
optional: !!t,
M: c
};
}
function xs(e, t) {
return {
name: e || "",
K: "a function",
optional: !!t,
M: u
};
}
function Fs(e, t) {
return {
name: e || "",
K: "null",
optional: !!t,
M: o
};
}
function Us() {
return {
name: "",
K: "an HTML element",
optional: !1,
M: function(e) {
return !!(e && e instanceof Element);
}
};
}
function js() {
return {
name: "auth",
K: "an instance of Firebase Auth",
optional: !0,
M: function(e) {
return !!(e && e instanceof ds);
}
};
}
function Ws() {
return {
name: "app",
K: "an instance of Firebase App",
optional: !0,
M: function(e) {
return !!(e && e instanceof ou.app.App);
}
};
}
function Bs(e) {
return {
name: e ? e + "Credential" : "credential",
K: e ? "a valid " + e + " credential" : "a valid credential",
optional: !1,
M: function(t) {
if (!t) return !1;
var n = !e || t.providerId === e;
return !(!t.wa || !n);
}
};
}
function Vs() {
return {
name: "authProvider",
K: "a valid Auth provider",
optional: !1,
M: function(e) {
return !!(e && e.providerId && e.hasOwnProperty && e.hasOwnProperty("isOAuthProvider"));
}
};
}
function qs() {
return {
name: "applicationVerifier",
K: "an implementation of firebase.auth.ApplicationVerifier",
optional: !1,
M: function(e) {
return !!(e && t(e.type) && u(e.verify));
}
};
}
function Ks(e, t, n, r) {
return {
name: n || "",
K: e.K + " or " + t.K,
optional: !!r,
M: function(n) {
return e.M(n) || t.M(n);
}
};
}
function Gs(e, t, n, r, i, o) {
Er(this, "type", "recaptcha");
this.b = this.c = null;
this.m = !1;
this.l = t;
this.a = n || {
theme: "light",
type: "image"
};
this.g = [];
if (this.a[Oh]) throw new Pr("argument-error", "sitekey should not be provided for reCAPTCHA as one is automatically provisioned for the current project.");
this.h = "invisible" === this.a[Rh];
if (!De(t) || !this.h && De(t).hasChildNodes()) throw new Pr("argument-error", "reCAPTCHA container is either not found or already contains inner elements!");
this.u = new vi(e, o || null, i || null);
this.o = r || function() {
return null;
};
var a = this;
this.i = [];
var s = this.a[Ih];
this.a[Ih] = function(e) {
Hs(a, e);
if ("function" == typeof s) s(e); else if ("string" == typeof s) {
var t = ir(s, su);
"function" == typeof t && t(e);
}
};
var u = this.a[Ph];
this.a[Ph] = function() {
Hs(a, null);
if ("function" == typeof u) u(); else if ("string" == typeof u) {
var e = ir(u, su);
"function" == typeof e && e();
}
};
}
function Hs(e, t) {
for (var n = 0; n < e.i.length; n++) try {
e.i[n](t);
} catch (e) {}
}
function Qs(e, t) {
N(e.i, function(e) {
return e == t;
});
}
function Xs(e, t) {
e.g.push(t);
ae(t, function() {
T(e.g, t);
});
return t;
}
function Ys(e) {
if (e.m) throw new Pr("internal-error", "RecaptchaVerifier instance has been destroyed.");
}
function zs() {
this.b = su.grecaptcha ? Infinity : 0;
this.c = null;
this.a = "__rcb" + Math.floor(1e6 * Math.random()).toString();
}
function Js(e, t) {
return new $(function(n, r) {
if (_r()) if (!su.grecaptcha || t !== e.c && !e.b) {
su[e.a] = function() {
if (su.grecaptcha) {
e.c = t;
var i = su.grecaptcha.render;
su.grecaptcha.render = function(t, n) {
t = i(t, n);
e.b++;
return t;
};
n();
} else r(new Pr("internal-error"));
delete su[e.a];
};
te(kn(Se(Ah, {
onload: e.a,
hl: t || ""
}))).s(function() {
r(new Pr("internal-error", "Unable to load external reCAPTCHA dependencies!"));
});
} else n(); else r(new Pr("network-request-failed"));
});
}
function $s() {
kh || (kh = new zs());
return kh;
}
function Zs(e, t, n) {
try {
this.f = n || ou.app();
} catch (e) {
throw new Pr("argument-error", "No firebase.app.App instance is currently initialized.");
}
if (!this.f.options || !this.f.options.apiKey) throw new Pr("invalid-api-key");
n = this.f.options.apiKey;
var r = this, i = null;
try {
i = this.f.auth().Ka();
} catch (e) {}
i = ou.SDK_VERSION ? nr(ou.SDK_VERSION, i) : null;
Gs.call(this, n, e, t, function() {
try {
var e = r.f.auth().$();
} catch (t) {
e = null;
}
return e;
}, i, ji(xl));
}
function eu(e, t) {
for (var n in t) {
var r = t[n].name;
e[r] = nu(r, e[n], t[n].j);
}
}
function tu(e, t, n, r) {
e[t] = nu(t, n, r);
}
function nu(e, t, n) {
function r() {
var e = Array.prototype.slice.call(arguments);
ks(o, n, e);
return t.apply(this, e);
}
if (!n) return t;
var i, o = ru(e);
for (i in t) r[i] = t[i];
for (i in t.prototype) r.prototype[i] = t.prototype[i];
return r;
}
function ru(e) {
return (e = e.split("."))[e.length - 1];
}
var iu, ou = e("@firebase/app").default, au = au || {}, su = this, uu = "closure_uid_" + (1e9 * Math.random() >>> 0), cu = 0, lu = Date.now || function() {
return +new Date();
};
d(v, Error);
v.prototype.name = "CustomError";
var hu = String.prototype.trim ? function(e) {
return e.trim();
} : function(e) {
return e.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "");
}, fu = /&/g, pu = /</g, du = />/g, vu = /"/g, _u = /'/g, yu = /\x00/g, gu = /[\x00&<>"']/;
d(b, v);
b.prototype.name = "AssertionError";
var mu, bu = Array.prototype.indexOf ? function(e, t, n) {
return Array.prototype.indexOf.call(e, t, n);
} : function(e, n, r) {
r = null == r ? 0 : 0 > r ? Math.max(0, e.length + r) : r;
if (t(e)) return t(n) && 1 == n.length ? e.indexOf(n, r) : -1;
for (;r < e.length; r++) if (r in e && e[r] === n) return r;
return -1;
}, Eu = Array.prototype.forEach ? function(e, t, n) {
Array.prototype.forEach.call(e, t, n);
} : function(e, n, r) {
for (var i = e.length, o = t(e) ? e.split("") : e, a = 0; a < i; a++) a in o && n.call(r, o[a], a, e);
}, wu = Array.prototype.map ? function(e, t, n) {
return Array.prototype.map.call(e, t, n);
} : function(e, n, r) {
for (var i = e.length, o = Array(i), a = t(e) ? e.split("") : e, s = 0; s < i; s++) s in a && (o[s] = n.call(r, a[s], s, e));
return o;
}, Cu = Array.prototype.some ? function(e, t, n) {
return Array.prototype.some.call(e, t, n);
} : function(e, n, r) {
for (var i = e.length, o = t(e) ? e.split("") : e, a = 0; a < i; a++) if (a in o && n.call(r, o[a], a, e)) return !0;
return !1;
};
e: {
var Su = su.navigator;
if (Su) {
var Tu = Su.userAgent;
if (Tu) {
mu = Tu;
break e;
}
}
mu = "";
}
var Nu = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
F[" "] = r;
var Iu, Pu = O("Opera"), Ou = O("Trident") || O("MSIE"), Ru = O("Edge"), Au = Ru || Ou, ku = O("Gecko") && !(g(mu.toLowerCase(), "webkit") && !O("Edge")) && !(O("Trident") || O("MSIE")) && !O("Edge"), Du = g(mu.toLowerCase(), "webkit") && !O("Edge");
e: {
var Lu = "", Mu = function() {
var e = mu;
return ku ? /rv\:([^\);]+)(\)|;)/.exec(e) : Ru ? /Edge\/([\d\.]+)/.exec(e) : Ou ? /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(e) : Du ? /WebKit\/(\S+)/.exec(e) : Pu ? /(?:Version)[ \/]?(\S+)/.exec(e) : void 0;
}();
Mu && (Lu = Mu ? Mu[1] : "");
if (Ou) {
var xu = j();
if (null != xu && xu > parseFloat(Lu)) {
Iu = String(xu);
break e;
}
}
Iu = Lu;
}
var Fu, Uu = {}, ju = su.document;
Fu = ju && Ou ? j() || ("CSS1Compat" == ju.compatMode ? parseInt(Iu, 10) : 5) : void 0;
q.prototype.get = function() {
if (0 < this.b) {
this.b--;
var e = this.a;
this.a = e.next;
e.next = null;
} else e = this.c();
return e;
};
var Wu = new q(function() {
return new H();
}, function(e) {
e.reset();
}, 100);
H.prototype.set = function(e, t) {
this.a = e;
this.b = t;
this.next = null;
};
H.prototype.reset = function() {
this.next = this.b = this.a = null;
};
var Bu, Vu, qu = !1, Ku = new function() {
this.b = this.a = null;
}(), Gu = 0, Hu = 2, Qu = 3;
Z.prototype.reset = function() {
this.f = this.b = this.g = this.a = null;
this.c = !1;
};
var Xu = new q(function() {
return new Z();
}, function(e) {
e.reset();
}, 100);
$.prototype.then = function(e, t, n) {
return ce(this, u(e) ? e : null, u(t) ? t : null, n);
};
B($);
(iu = $.prototype).s = function(e, t) {
return ce(this, null, e, t);
};
iu.cancel = function(e) {
this.a == Gu && Y(function() {
se(this, new ge(e));
}, this);
};
iu.Ac = function(e) {
this.a = Gu;
le(this, Hu, e);
};
iu.Bc = function(e) {
this.a = Gu;
le(this, Qu, e);
};
iu.Qb = function() {
for (var e; e = de(this); ) ve(this, e, this.a, this.i);
this.h = !1;
};
var Yu = Q;
d(ge, v);
ge.prototype.name = "cancel";
var zu = !Ou || 9 <= Number(Fu);
me.prototype.la = !0;
me.prototype.ja = function() {
return this.a;
};
me.prototype.toString = function() {
return "Const{" + this.a + "}";
};
var Ju = {};
Ee("");
we.prototype.la = !0;
we.prototype.ja = function() {
return this.a;
};
we.prototype.toString = function() {
return "TrustedResourceUrl{" + this.a + "}";
};
var $u = /%{(\w+)}/g, Zu = /^(?:https:)?\/\/[0-9a-z.:[\]-]+\/|^\/[^\/\\]|^about:blank(#|$)/i, ec = {};
Ne.prototype.la = !0;
Ne.prototype.ja = function() {
return this.a;
};
Ne.prototype.toString = function() {
return "SafeUrl{" + this.a + "}";
};
var tc = /^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i, nc = {};
Oe("about:blank");
Re.prototype.la = !0;
Re.prototype.ja = function() {
return this.a;
};
Re.prototype.toString = function() {
return "SafeHtml{" + this.a + "}";
};
var rc = {};
ke("<!DOCTYPE html>");
ke("");
ke("<br>");
var ic = {
cellpadding: "cellPadding",
cellspacing: "cellSpacing",
colspan: "colSpan",
frameborder: "frameBorder",
height: "height",
maxlength: "maxLength",
nonce: "nonce",
role: "role",
rowspan: "rowSpan",
type: "type",
usemap: "useMap",
valign: "vAlign",
width: "width"
}, oc = {
'"': '\\"',
"\\": "\\\\",
"/": "\\/",
"\b": "\\b",
"\f": "\\f",
"\n": "\\n",
"\r": "\\r",
"\t": "\\t",
"\v": "\\u000b"
}, ac = /\uffff/.test("￿") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g, sc = 0, uc = {};
Ve.prototype.oa = !1;
Ve.prototype.ta = function() {
if (this.Fa) for (;this.Fa.length; ) this.Fa.shift()();
};
var cc = Object.freeze || function(e) {
return e;
}, lc = !Ou || 9 <= Number(Fu), hc = Ou && !W("9"), fc = function() {
if (!su.addEventListener || !Object.defineProperty) return !1;
var e = !1, t = Object.defineProperty({}, "passive", {
get: function() {
e = !0;
}
});
su.addEventListener("test", r, t);
su.removeEventListener("test", r, t);
return e;
}();
Ke.prototype.c = function() {
this.Bb = !1;
};
d(Ge, Ke);
var pc = cc({
2: "touch",
3: "pen",
4: "mouse"
});
Ge.prototype.c = function() {
Ge.ib.c.call(this);
var e = this.a;
if (e.preventDefault) e.preventDefault(); else if (e.returnValue = !1, hc) try {
(e.ctrlKey || 112 <= e.keyCode && 123 >= e.keyCode) && (e.keyCode = -1);
} catch (e) {}
};
Ge.prototype.g = function() {
return this.a;
};
var dc = "closure_listenable_" + (1e6 * Math.random() | 0), vc = 0, _c = "closure_lm_" + (1e6 * Math.random() | 0), yc = {}, gc = 0, mc = "__closure_events_fn_" + (1e9 * Math.random() >>> 0);
d(lt, Ve);
lt.prototype[dc] = !0;
lt.prototype.removeEventListener = function(e, t, n, r) {
nt(this, e, t, n, r);
};
lt.prototype.ta = function() {
lt.ib.ta.call(this);
if (this.u) {
var e, t = this.u;
for (e in t.a) {
for (var n = t.a[e], r = 0; r < n.length; r++) 0, Qe(n[r]);
delete t.a[e];
t.b--;
}
}
this.Ra = null;
};
yt.prototype.a = null;
var bc = 0;
yt.prototype.reset = function(e, t, n, r, i) {
"number" == typeof i || bc++;
r || lu();
this.b = t;
delete this.a;
};
mt.prototype.toString = function() {
return this.name;
};
var Ec = new mt("SEVERE", 1e3), wc = new mt("CONFIG", 700), Cc = new mt("FINE", 500);
gt.prototype.log = function(e, t, n) {
if (e.value >= bt(this).value) for (u(t) && (t = t()), e = new yt(e, String(t), this.f), 
n && (e.a = n), n = "log:" + e.b, (e = su.console) && e.timeStamp && e.timeStamp(n), 
(e = su.msWriteProfilerMark) && e(n), n = this; n; ) n = n.a;
};
var Sc = {}, Tc = null;
(iu = wt.prototype).P = function() {
Ct(this);
for (var e = [], t = 0; t < this.a.length; t++) e.push(this.b[this.a[t]]);
return e;
};
iu.S = function() {
Ct(this);
return this.a.concat();
};
iu.clear = function() {
this.b = {};
this.c = this.a.length = 0;
};
iu.get = function(e, t) {
return St(this.b, e) ? this.b[e] : t;
};
iu.set = function(e, t) {
St(this.b, e) || (this.c++, this.a.push(e));
this.b[e] = t;
};
iu.forEach = function(e, t) {
for (var n = this.S(), r = 0; r < n.length; r++) {
var i = n[r], o = this.get(i);
e.call(t, o, i, this);
}
};
var Nc = null, Ic = null;
Rt.prototype.cancel = function(e) {
if (this.a) this.c instanceof Rt && this.c.cancel(); else {
if (this.b) {
var t = this.b;
delete this.b;
e ? t.cancel(e) : (t.l--, 0 >= t.l && t.cancel());
}
this.v ? this.v.call(this.o, this) : this.u = !0;
this.a || (e = new Ut(), kt(this), At(this, !1, e));
}
};
Rt.prototype.m = function(e, t) {
this.i = !1;
At(this, e, t);
};
Rt.prototype.A = function(e) {
kt(this);
At(this, !0, e);
};
Rt.prototype.then = function(e, t, n) {
var r, i, o = new $(function(e, t) {
r = e;
i = t;
});
Lt(this, r, function(e) {
e instanceof Ut ? o.cancel() : i(e);
});
return o.then(e, t, n);
};
B(Rt);
d(Ft, v);
Ft.prototype.message = "Deferred has already fired";
Ft.prototype.name = "AlreadyCalledError";
d(Ut, v);
Ut.prototype.message = "Deferred was canceled";
Ut.prototype.name = "CanceledError";
jt.prototype.c = function() {
delete Oc[this.a];
throw this.b;
};
var Pc, Oc = {};
d(Wt, function() {
this.b = -1;
});
for (var Rc = 64, Ac = Rc - 1, kc = [], Dc = 0; Dc < Ac; Dc++) kc[Dc] = 0;
var Lc = I(128, kc);
Wt.prototype.reset = function() {
this.g = this.c = 0;
this.a = su.Int32Array ? new Int32Array(this.h) : P(this.h);
};
var Mc = [ 1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298 ];
d(qt, Wt);
var xc = [ 1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225 ], Fc = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;
Xt.prototype.toString = function() {
var e = [], t = this.c;
t && e.push(rn(t, Uc, !0), ":");
var n = this.b;
(n || "file" == t) && (e.push("//"), (t = this.l) && e.push(rn(t, Uc, !0), "@"), 
e.push(encodeURIComponent(String(n)).replace(/%25([0-9a-fA-F]{2})/g, "%$1")), null != (n = this.i) && e.push(":", String(n)));
(n = this.g) && (this.b && "/" != n.charAt(0) && e.push("/"), e.push(rn(n, "/" == n.charAt(0) ? Wc : jc, !0)));
(n = this.a.toString()) && e.push("?", n);
(n = this.h) && e.push("#", rn(n, Vc));
return e.join("");
};
var Uc = /[#\/\?@]/g, jc = /[\#\?:]/g, Wc = /[\#\?]/g, Bc = /[\#\?@]/g, Vc = /#/g;
(iu = an.prototype).clear = function() {
this.a = this.c = null;
this.b = 0;
};
iu.forEach = function(e, t) {
sn(this);
this.a.forEach(function(n, r) {
Eu(n, function(n) {
e.call(t, n, r, this);
}, this);
}, this);
};
iu.S = function() {
sn(this);
for (var e = this.a.P(), t = this.a.S(), n = [], r = 0; r < t.length; r++) for (var i = e[r], o = 0; o < i.length; o++) n.push(t[r]);
return n;
};
iu.P = function(e) {
sn(this);
var n = [];
if (t(e)) hn(this, e) && (n = I(n, this.a.get(pn(this, e)))); else {
e = this.a.P();
for (var r = 0; r < e.length; r++) n = I(n, e[r]);
}
return n;
};
iu.set = function(e, t) {
sn(this);
this.c = null;
hn(this, e = pn(this, e)) && (this.b -= this.a.get(e).length);
this.a.set(e, [ t ]);
this.b += 1;
return this;
};
iu.get = function(e, t) {
return 0 < (e = e ? this.P(e) : []).length ? String(e[0]) : t;
};
iu.toString = function() {
if (this.c) return this.c;
if (!this.a) return "";
for (var e = [], t = this.a.S(), n = 0; n < t.length; n++) {
var r = t[n], i = encodeURIComponent(String(r));
r = this.P(r);
for (var o = 0; o < r.length; o++) {
var a = i;
"" !== r[o] && (a += "=" + encodeURIComponent(String(r[o])));
e.push(a);
}
}
return this.c = e.join("&");
};
vn.prototype.c = null;
var qc;
d(yn, vn);
yn.prototype.a = function() {
var e = gn(this);
return e ? new ActiveXObject(e) : new XMLHttpRequest();
};
yn.prototype.b = function() {
var e = {};
gn(this) && (e[0] = !0, e[1] = !0);
return e;
};
qc = new yn();
d(mn, lt);
var Kc = "", Gc = mn.prototype, Hc = Et("goog.net.XhrIo");
Gc.J = Hc;
var Qc = /^https?$/i, Xc = [ "POST", "PUT" ];
(iu = mn.prototype).Ea = function() {
"undefined" != typeof au && this.a && (this.g = "Timed out after " + this.f + "ms, aborting", 
Tt(this.J, An(this, this.g)), ht(this, "timeout"), this.abort(8));
};
iu.abort = function() {
this.a && this.b && (Tt(this.J, An(this, "Aborting")), this.b = !1, this.c = !0, 
this.a.abort(), this.c = !1, ht(this, "complete"), ht(this, "abort"), Nn(this));
};
iu.ta = function() {
this.a && (this.b && (this.b = !1, this.c = !0, this.a.abort(), this.c = !1), Nn(this, !0));
mn.ib.ta.call(this);
};
iu.Ab = function() {
this.oa || (this.G || this.h || this.c ? Tn(this) : this.jc());
};
iu.jc = function() {
Tn(this);
};
iu.getResponse = function() {
try {
if (!this.a) return null;
if ("response" in this.a) return this.a.response;
switch (this.l) {
case Kc:
case "text":
return this.a.responseText;

case "arraybuffer":
if ("mozResponseArrayBuffer" in this.a) return this.a.mozResponseArrayBuffer;
}
var e = this.J;
e && e.log(Ec, "Response type " + this.l + " is not supported on this browser", void 0);
return null;
} catch (e) {
return Tt(this.J, "Can not get response: " + e.message), null;
}
};
var Yc = /^[+a-zA-Z0-9_.!#$%&'*\/=?^`{|}~-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,63}$/, zc = 0, Jc = 1;
d(xn, v);
d(Fn, vn);
Fn.prototype.a = function() {
var e = new XMLHttpRequest();
if ("withCredentials" in e) return e;
if ("undefined" != typeof XDomainRequest) return new Un();
throw Error("Unsupported browser");
};
Fn.prototype.b = function() {
return {};
};
(iu = Un.prototype).open = function(e, t, n) {
if (null != n && !n) throw Error("Only async requests are supported.");
this.a.open(e, t);
};
iu.send = function(e) {
if (e) {
if ("string" != typeof e) throw Error("Only string data is supported");
this.a.send(e);
} else this.a.send();
};
iu.abort = function() {
this.a.abort();
};
iu.setRequestHeader = function() {};
iu.getResponseHeader = function(e) {
return "content-type" == e.toLowerCase() ? this.a.contentType : "";
};
iu.Wb = function() {
this.status = 200;
this.responseText = this.a.responseText;
jn(this, 4);
};
iu.xb = function() {
this.status = 500;
this.responseText = "";
jn(this, 4);
};
iu.Yb = function() {
this.xb();
};
iu.Xb = function() {
this.status = 200;
jn(this, 1);
};
iu.getAllResponseHeaders = function() {
return "content-type: " + this.a.contentType;
};
var $c = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/, Zc = "Firefox", el = "Chrome", tl = {
Gc: "FirebaseCore-web",
Ic: "FirebaseUI-web"
};
yr.prototype.get = function() {
return this.a ? this.b : this.c;
};
var nl, rl = {};
try {
var il = {};
Object.defineProperty(il, "abcd", {
configurable: !0,
enumerable: !0,
value: 1
});
Object.defineProperty(il, "abcd", {
configurable: !0,
enumerable: !0,
value: 2
});
nl = 2 == il.abcd;
} catch (e) {
nl = !1;
}
var ol = "email", al = "newEmail", sl = "requestType", ul = "email", cl = "fromEmail", ll = "data", hl = "operation";
d(Pr, Error);
Pr.prototype.B = function() {
return {
code: this.code,
message: this.message
};
};
Pr.prototype.toJSON = function() {
return this.B();
};
var fl = "auth/", pl = {
"argument-error": "",
"app-not-authorized": "This app, identified by the domain where it's hosted, is not authorized to use Firebase Authentication with the provided API key. Review your key configuration in the Google API console.",
"app-not-installed": "The requested mobile application corresponding to the identifier (Android package name or iOS bundle ID) provided is not installed on this device.",
"captcha-check-failed": "The reCAPTCHA response token provided is either invalid, expired, already used or the domain associated with it does not match the list of whitelisted domains.",
"code-expired": "The SMS code has expired. Please re-send the verification code to try again.",
"cordova-not-ready": "Cordova framework is not ready.",
"cors-unsupported": "This browser is not supported.",
"credential-already-in-use": "This credential is already associated with a different user account.",
"custom-token-mismatch": "The custom token corresponds to a different audience.",
"requires-recent-login": "This operation is sensitive and requires recent authentication. Log in again before retrying this request.",
"dynamic-link-not-activated": "Please activate Dynamic Links in the Firebase Console and agree to the terms and conditions.",
"email-already-in-use": "The email address is already in use by another account.",
"expired-action-code": "The action code has expired. ",
"cancelled-popup-request": "This operation has been cancelled due to another conflicting popup being opened.",
"internal-error": "An internal error has occurred.",
"invalid-app-credential": "The phone verification request contains an invalid application verifier. The reCAPTCHA token response is either invalid or expired.",
"invalid-app-id": "The mobile app identifier is not registed for the current project.",
"invalid-user-token": "The user's credential is no longer valid. The user must sign in again.",
"invalid-auth-event": "An internal error has occurred.",
"invalid-verification-code": "The SMS verification code used to create the phone auth credential is invalid. Please resend the verification code sms and be sure use the verification code provided by the user.",
"invalid-continue-uri": "The continue URL provided in the request is invalid.",
"invalid-cordova-configuration": "The following Cordova plugins must be installed to enable OAuth sign-in: cordova-plugin-buildinfo, cordova-universal-links-plugin, cordova-plugin-browsertab, cordova-plugin-inappbrowser and cordova-plugin-customurlscheme.",
"invalid-custom-token": "The custom token format is incorrect. Please check the documentation.",
"invalid-email": "The email address is badly formatted.",
"invalid-api-key": "Your API key is invalid, please check you have copied it correctly.",
"invalid-cert-hash": "The SHA-1 certificate hash provided is invalid.",
"invalid-credential": "The supplied auth credential is malformed or has expired.",
"invalid-persistence-type": "The specified persistence type is invalid. It can only be local, session or none.",
"invalid-message-payload": "The email template corresponding to this action contains invalid characters in its message. Please fix by going to the Auth email templates section in the Firebase Console.",
"invalid-oauth-provider": "EmailAuthProvider is not supported for this operation. This operation only supports OAuth providers.",
"invalid-oauth-client-id": "The OAuth client ID provided is either invalid or does not match the specified API key.",
"unauthorized-domain": "This domain is not authorized for OAuth operations for your Firebase project. Edit the list of authorized domains from the Firebase console.",
"invalid-action-code": "The action code is invalid. This can happen if the code is malformed, expired, or has already been used.",
"wrong-password": "The password is invalid or the user does not have a password.",
"invalid-phone-number": "The format of the phone number provided is incorrect. Please enter the phone number in a format that can be parsed into E.164 format. E.164 phone numbers are written in the format [+][country code][subscriber number including area code].",
"invalid-recipient-email": "The email corresponding to this action failed to send as the provided recipient email address is invalid.",
"invalid-sender": "The email template corresponding to this action contains an invalid sender email or name. Please fix by going to the Auth email templates section in the Firebase Console.",
"invalid-verification-id": "The verification ID used to create the phone auth credential is invalid.",
"missing-android-pkg-name": "An Android Package Name must be provided if the Android App is required to be installed.",
"auth-domain-config-required": "Be sure to include authDomain when calling firebase.initializeApp(), by following the instructions in the Firebase console.",
"missing-app-credential": "The phone verification request is missing an application verifier assertion. A reCAPTCHA response token needs to be provided.",
"missing-verification-code": "The phone auth credential was created with an empty SMS verification code.",
"missing-continue-uri": "A continue URL must be provided in the request.",
"missing-iframe-start": "An internal error has occurred.",
"missing-ios-bundle-id": "An iOS Bundle ID must be provided if an App Store ID is provided.",
"missing-phone-number": "To send verification codes, provide a phone number for the recipient.",
"missing-verification-id": "The phone auth credential was created with an empty verification ID.",
"app-deleted": "This instance of FirebaseApp has been deleted.",
"account-exists-with-different-credential": "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.",
"network-request-failed": "A network error (such as timeout, interrupted connection or unreachable host) has occurred.",
"no-auth-event": "An internal error has occurred.",
"no-such-provider": "User was not linked to an account with the given provider.",
"operation-not-allowed": "The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.",
"operation-not-supported-in-this-environment": 'This operation is not supported in the environment this application is running on. "location.protocol" must be http, https or chrome-extension and web storage must be enabled.',
"popup-blocked": "Unable to establish a connection with the popup. It may have been blocked by the browser.",
"popup-closed-by-user": "The popup has been closed by the user before finalizing the operation.",
"provider-already-linked": "User can only be linked to one identity for the given provider.",
"quota-exceeded": "The project's quota for this operation has been exceeded.",
"redirect-cancelled-by-user": "The redirect operation has been cancelled by the user before finalizing.",
"redirect-operation-pending": "A redirect sign-in operation is already pending.",
timeout: "The operation has timed out.",
"user-token-expired": "The user's credential is no longer valid. The user must sign in again.",
"too-many-requests": "We have blocked all requests from this device due to unusual activity. Try again later.",
"unauthorized-continue-uri": "The domain of the continue URL is not whitelisted.  Please whitelist the domain in the Firebase console.",
"unsupported-persistence-type": "The current environment does not support the specified persistence type.",
"user-cancelled": "User did not grant your application the permissions it requested.",
"user-not-found": "There is no user record corresponding to this identifier. The user may have been deleted.",
"user-disabled": "The user account has been disabled by an administrator.",
"user-mismatch": "The supplied credentials do not correspond to the previously signed in user.",
"user-signed-out": "",
"weak-password": "The password must be 6 characters long or more.",
"web-storage-unsupported": "This browser is not supported or 3rd party cookies and data may be disabled."
}, dl = "android", vl = "handleCodeInApp", _l = "iOS", yl = "url", gl = "installApp", ml = "minimumVersion", bl = "packageName", El = "bundleId", wl = "oauth_consumer_key oauth_nonce oauth_signature oauth_signature_method oauth_timestamp oauth_token oauth_version".split(" "), Cl = [ "client_id", "response_type", "scope", "redirect_uri", "state" ], Sl = {
Hc: {
Ma: "locale",
za: 500,
ya: 600,
Na: "facebook.com",
$a: Cl
},
Jc: {
Ma: null,
za: 500,
ya: 620,
Na: "github.com",
$a: Cl
},
Kc: {
Ma: "hl",
za: 515,
ya: 680,
Na: "google.com",
$a: Cl
},
Qc: {
Ma: "lang",
za: 485,
ya: 705,
Na: "twitter.com",
$a: wl
}
}, Tl = "idToken", Nl = "providerId";
d(Fr, xr);
d(Ur, Fr);
d(jr, Fr);
d(Wr, Fr);
d(Br, Fr);
qr.prototype.wa = function(e) {
return ki(e, Kr(this));
};
qr.prototype.b = function(e, t) {
var n = Kr(this);
n.idToken = t;
return Di(e, n);
};
qr.prototype.c = function(e, t) {
return Vr(Li(e, Kr(this)), t);
};
qr.prototype.B = function() {
var e = {
providerId: this.providerId
};
this.idToken && (e.oauthIdToken = this.idToken);
this.accessToken && (e.oauthAccessToken = this.accessToken);
this.secret && (e.oauthTokenSecret = this.secret);
return e;
};
Gr.prototype.Ba = function(e) {
this.rb = M(e);
return this;
};
d(Hr, Gr);
Hr.prototype.sa = function(e) {
S(this.a, e) || this.a.push(e);
return this;
};
Hr.prototype.wb = function() {
return P(this.a);
};
Hr.prototype.credential = function(e, t) {
if (!e && !t) throw new Pr("argument-error", "credential failed: must provide the ID token and/or the access token.");
return new qr(this.providerId, {
idToken: e || null,
accessToken: t || null
});
};
d(Qr, Hr);
Er(Qr, "PROVIDER_ID", "facebook.com");
d(Yr, Hr);
Er(Yr, "PROVIDER_ID", "github.com");
d(Jr, Hr);
Er(Jr, "PROVIDER_ID", "google.com");
d(Zr, Gr);
Er(Zr, "PROVIDER_ID", "twitter.com");
ti.prototype.wa = function(e) {
return xi(e, rh, {
email: this.a,
password: this.f
});
};
ti.prototype.b = function(e, t) {
return xi(e, Jl, {
idToken: t,
email: this.a,
password: this.f
});
};
ti.prototype.c = function(e, t) {
return Vr(this.wa(e), t);
};
ti.prototype.B = function() {
return {
email: this.a,
password: this.f
};
};
wr(ni, {
PROVIDER_ID: "password"
});
ri.prototype.wa = function(e) {
return e.Qa(ii(this));
};
ri.prototype.b = function(e, t) {
var n = ii(this);
n.idToken = t;
return xi(e, oh, n);
};
ri.prototype.c = function(e, t) {
var n = ii(this);
n.operation = "REAUTH";
return Vr(e = xi(e, ah, n), t);
};
ri.prototype.B = function() {
var e = {
providerId: "phone"
};
this.a.Pa && (e.verificationId = this.a.Pa);
this.a.Oa && (e.verificationCode = this.a.Oa);
this.a.Da && (e.temporaryProof = this.a.Da);
this.a.Y && (e.phoneNumber = this.a.Y);
return e;
};
oi.prototype.Qa = function(e, n) {
var r = this.a.c;
return te(n.verify()).then(function(i) {
if (!t(i)) throw new Pr("argument-error", "An implementation of firebase.auth.ApplicationVerifier.prototype.verify() must return a firebase.Promise that resolves with a string.");
switch (n.type) {
case "recaptcha":
return Pi(r, {
phoneNumber: e,
recaptchaToken: i
}).then(function(e) {
"function" == typeof n.reset && n.reset();
return e;
}, function(e) {
"function" == typeof n.reset && n.reset();
throw e;
});

default:
throw new Pr("argument-error", 'Only firebase.auth.ApplicationVerifiers with type="recaptcha" are currently supported.');
}
});
};
wr(oi, {
PROVIDER_ID: "phone"
});
ci.prototype.B = function() {
return {
type: this.b,
eventId: this.c,
urlResponse: this.f,
sessionId: this.g,
error: this.a && this.a.B()
};
};
d(hi, Pr);
d(fi, Pr);
fi.prototype.B = function() {
var e = {
code: this.code,
message: this.message
};
this.email && (e.email = this.email);
this.phoneNumber && (e.phoneNumber = this.phoneNumber);
var t = this.credential && this.credential.B();
t && x(e, t);
return e;
};
fi.prototype.toJSON = function() {
return this.B();
};
d(di, vn);
di.prototype.a = function() {
return new this.f();
};
di.prototype.b = function() {
return {};
};
var Il, Pl = "idToken", Ol = new yr(3e4, 6e4), Rl = {
"Content-Type": "application/x-www-form-urlencoded"
}, Al = new yr(3e4, 6e4), kl = {
"Content-Type": "application/json"
};
vi.prototype.m = function(e, t, n, r, i, o) {
var a = "Node" == Zn(), s = er() ? a ? new mn(this.o) : new mn() : new mn(this.f);
if (o) {
s.f = Math.max(0, o);
var u = setTimeout(function() {
ht(s, "timeout");
}, o);
}
ft(s, "complete", function() {
u && clearTimeout(u);
var e = null;
try {
e = JSON.parse(Rn(this)) || null;
} catch (t) {
e = null;
}
t && t(e);
});
pt(s, "ready", function() {
u && clearTimeout(u);
qe(this);
});
pt(s, "timeout", function() {
u && clearTimeout(u);
qe(this);
t && t(null);
});
bn(s, e, n, r, i);
};
var Dl = Ee("https://apis.google.com/js/client.js?onload=%{onload}"), Ll = "__fcb" + Math.floor(1e6 * Math.random()).toString();
vi.prototype.u = function(e, t, n, r, i) {
var o = this;
Il.then(function() {
window.gapi.client.setApiKey(o.b);
var a = window.gapi.auth.getToken();
window.gapi.auth.setToken(null);
window.gapi.client.request({
path: e,
method: n,
body: r,
headers: i,
authType: "none",
callback: function(e) {
window.gapi.auth.setToken(a);
t && t(e);
}
});
}).s(function(e) {
t && t({
error: {
message: e && e.message || "CORS_UNSUPPORTED"
}
});
});
};
vi.prototype.gb = function() {
return xi(this, $l, {});
};
vi.prototype.kb = function(e, t) {
return xi(this, zl, {
idToken: e,
email: t
});
};
vi.prototype.lb = function(e, t) {
return xi(this, Jl, {
idToken: e,
password: t
});
};
var Ml = {
displayName: "DISPLAY_NAME",
photoUrl: "PHOTO_URL"
};
(iu = vi.prototype).mb = function(e, t) {
var n = {
idToken: e
}, r = [];
R(Ml, function(e, i) {
var o = t[i];
null === o ? r.push(e) : i in t && (n[i] = o);
});
r.length && (n.deleteAttribute = r);
return xi(this, zl, n);
};
iu.cb = function(e, t) {
x(e = {
requestType: "PASSWORD_RESET",
email: e
}, t);
return xi(this, Gl, e);
};
iu.bb = function(e, t) {
x(e = {
requestType: "VERIFY_EMAIL",
idToken: e
}, t);
return xi(this, Kl, e);
};
iu.Qa = function(e) {
return xi(this, ih, e);
};
iu.Ta = function(e, t) {
return xi(this, Xl, {
oobCode: e,
newPassword: t
});
};
iu.Ia = function(e) {
return xi(this, Ul, {
oobCode: e
});
};
iu.Sa = function(e) {
return xi(this, Fl, {
oobCode: e
});
};
var xl, Fl = {
endpoint: "setAccountInfo",
D: Mi,
ga: "email"
}, Ul = {
endpoint: "resetPassword",
D: Mi,
O: function(e) {
if (!e.email || !e.requestType) throw new Pr("internal-error");
}
}, jl = {
endpoint: "signupNewUser",
D: function(e) {
wi(e);
if (!e.password) throw new Pr("weak-password");
},
O: Ni,
T: !0
}, Wl = {
endpoint: "createAuthUri"
}, Bl = {
endpoint: "deleteAccount",
ea: [ "idToken" ]
}, Vl = {
endpoint: "setAccountInfo",
ea: [ "idToken", "deleteProvider" ],
D: function(e) {
if (!a(e.deleteProvider)) throw new Pr("internal-error");
}
}, ql = {
endpoint: "getAccountInfo"
}, Kl = {
endpoint: "getOobConfirmationCode",
ea: [ "idToken", "requestType" ],
D: function(e) {
if ("VERIFY_EMAIL" != e.requestType) throw new Pr("internal-error");
},
ga: "email"
}, Gl = {
endpoint: "getOobConfirmationCode",
ea: [ "requestType" ],
D: function(e) {
if ("PASSWORD_RESET" != e.requestType) throw new Pr("internal-error");
wi(e);
},
ga: "email"
}, Hl = {
nb: !0,
endpoint: "getProjectConfig",
zb: "GET"
}, Ql = {
nb: !0,
endpoint: "getRecaptchaParam",
zb: "GET",
O: function(e) {
if (!e.recaptchaSiteKey) throw new Pr("internal-error");
}
}, Xl = {
endpoint: "resetPassword",
D: Mi,
ga: "email"
}, Yl = {
endpoint: "sendVerificationCode",
ea: [ "phoneNumber", "recaptchaToken" ],
ga: "sessionInfo"
}, zl = {
endpoint: "setAccountInfo",
ea: [ "idToken" ],
D: Ci,
T: !0
}, Jl = {
endpoint: "setAccountInfo",
ea: [ "idToken" ],
D: function(e) {
Ci(e);
if (!e.password) throw new Pr("weak-password");
},
O: Ni,
T: !0
}, $l = {
endpoint: "signupNewUser",
O: Ni,
T: !0
}, Zl = {
endpoint: "verifyAssertion",
D: Ri,
O: Ai,
T: !0
}, eh = {
endpoint: "verifyAssertion",
D: Ri,
O: function(e) {
if (e.errorMessage && "USER_NOT_FOUND" == e.errorMessage) throw new Pr("user-not-found");
if (e.errorMessage) throw Fi(e.errorMessage);
if (!e[Pl]) throw new Pr("internal-error");
},
T: !0
}, th = {
endpoint: "verifyAssertion",
D: function(e) {
Ri(e);
if (!e.idToken) throw new Pr("internal-error");
},
O: Ai,
T: !0
}, nh = {
endpoint: "verifyCustomToken",
D: function(e) {
if (!e.token) throw new Pr("invalid-custom-token");
},
O: Ni,
T: !0
}, rh = {
endpoint: "verifyPassword",
D: function(e) {
wi(e);
if (!e.password) throw new Pr("wrong-password");
},
O: Ni,
T: !0
}, ih = {
endpoint: "verifyPhoneNumber",
D: Ii,
O: Ni
}, oh = {
endpoint: "verifyPhoneNumber",
D: function(e) {
if (!e.idToken) throw new Pr("internal-error");
Ii(e);
},
O: function(e) {
if (e.temporaryProof) throw e.code = "credential-already-in-use", pi(e);
Ni(e);
}
}, ah = {
Pb: {
USER_NOT_FOUND: "user-not-found"
},
endpoint: "verifyPhoneNumber",
D: Ii,
O: Ni
}, sh = {
Mc: {
Va: "https://www.googleapis.com/identitytoolkit/v3/relyingparty/",
ab: "https://securetoken.googleapis.com/v1/token",
id: "p"
},
Oc: {
Va: "https://staging-www.sandbox.googleapis.com/identitytoolkit/v3/relyingparty/",
ab: "https://staging-securetoken.sandbox.googleapis.com/v1/token",
id: "s"
},
Pc: {
Va: "https://www-googleapis-test.sandbox.google.com/identitytoolkit/v3/relyingparty/",
ab: "https://test-securetoken.sandbox.googleapis.com/v1/token",
id: "t"
}
};
xl = ji("__EID__") ? "__EID__" : void 0;
var uh = Ee("https://apis.google.com/js/api.js?onload=%{onload}"), ch = new yr(3e4, 6e4), lh = new yr(5e3, 15e3), hh = null;
Gi.prototype.toString = function() {
this.f ? $t(this.a, "v", this.f) : ln(this.a.a, "v");
this.b ? $t(this.a, "eid", this.b) : ln(this.a.a, "eid");
this.c.length ? $t(this.a, "fw", this.c.join(",")) : ln(this.a.a, "fw");
return this.a.toString();
};
Hi.prototype.toString = function() {
var e = tn(this.m, "/__/auth/handler");
$t(e, "apiKey", this.u);
$t(e, "appName", this.c);
$t(e, "authType", this.l);
if (this.a.isOAuthProvider) {
var t = this.a;
try {
var n = ou.app(this.c).auth().$();
} catch (e) {
n = null;
}
t.Ua = n;
$t(e, "providerId", this.a.providerId);
t = this.a;
n = hr(t.rb);
for (var r in n) n[r] = n[r].toString();
r = t.rc;
n = M(n);
for (var i = 0; i < r.length; i++) {
var o = r[i];
o in n && delete n[o];
}
t.Wa && t.Ua && !n[t.Wa] && (n[t.Wa] = t.Ua);
D(n) || $t(e, "customParameters", lr(n));
}
"function" == typeof this.a.wb && (t = this.a.wb()).length && $t(e, "scopes", t.join(","));
this.h ? $t(e, "redirectUrl", this.h) : ln(e.a, "redirectUrl");
this.g ? $t(e, "eventId", this.g) : ln(e.a, "eventId");
this.i ? $t(e, "v", this.i) : ln(e.a, "v");
if (this.b) for (var a in this.b) this.b.hasOwnProperty(a) && !Zt(e, a) && $t(e, a, this.b[a]);
this.f ? $t(e, "eid", this.f) : ln(e.a, "eid");
(a = Qi(this.c)).length && $t(e, "fw", a.join(","));
return e.toString();
};
(iu = Xi.prototype).Ca = function(e, t, n) {
var r = new Pr("popup-closed-by-user"), i = new Pr("web-storage-unsupported"), o = this, a = !1;
return this.ba().then(function() {
eo(o).then(function(n) {
n || (e && Hn(e), t(i), a = !0);
});
}).s(function() {}).then(function() {
if (!a) return Xn(e);
}).then(function() {
if (!a) return _t(n).then(function() {
t(r);
});
});
};
iu.Db = function() {
var e = rr();
return !cr(e) && !dr(e);
};
iu.yb = function() {
return !1;
};
iu.vb = function(e, t, n, r, i, o, a) {
if (!e) return ne(new Pr("popup-blocked"));
if (a && !cr()) return this.ba().s(function(t) {
Hn(e);
i(t);
}), r(), te();
this.a || (this.a = Yi(Ji(this)));
var s = this;
return this.a.then(function() {
var t = s.ba().s(function(t) {
Hn(e);
i(t);
throw t;
});
r();
return t;
}).then(function() {
ui(n);
a || Vn($i(s.u, s.f, s.b, t, n, null, o, s.c, void 0, s.h), e);
}).s(function(e) {
"auth/network-request-failed" == e.code && (s.a = null);
throw e;
});
};
iu.Aa = function(e, t, n) {
this.a || (this.a = Yi(Ji(this)));
var r = this;
return this.a.then(function() {
ui(t);
Vn($i(r.u, r.f, r.b, e, t, Bn(), n, r.c, void 0, r.h));
}).s(function(e) {
"auth/network-request-failed" == e.code && (r.a = null);
throw e;
});
};
iu.ba = function() {
var e = this;
return zi(this).then(function() {
return e.i.Ya;
}).s(function() {
e.a = null;
throw new Pr("network-request-failed");
});
};
iu.Hb = function() {
return !0;
};
iu.ua = function(e) {
this.g.push(e);
};
iu.Ja = function(e) {
N(this.g, function(t) {
return t == e;
});
};
(iu = to.prototype).get = function(e) {
return te(this.a.getItem(e)).then(function(e) {
return e && fr(e);
});
};
iu.set = function(e, t) {
return te(this.a.setItem(e, lr(t)));
};
iu.X = function(e) {
return te(this.a.removeItem(e));
};
iu.ia = function() {};
iu.da = function() {};
(iu = no.prototype).get = function(e) {
return te(this.a[e]);
};
iu.set = function(e, t) {
this.a[e] = t;
return te();
};
iu.X = function(e) {
delete this.a[e];
return te();
};
iu.ia = function() {};
iu.da = function() {};
var fh;
(iu = ro.prototype).set = function(e, t) {
var n, r = !1, i = this;
return ae(oo(this).then(function(t) {
return uo((t = ao(i, so(i, n = t, !0))).get(e));
}).then(function(o) {
var a = ao(i, so(i, n, !0));
if (o) return o.value = t, uo(a.put(o));
i.a++;
r = !0;
(o = {})[i.g] = e;
o[i.l] = t;
return uo(a.add(o));
}).then(function() {
i.f[e] = t;
}), function() {
r && i.a--;
});
};
iu.get = function(e) {
var t = this;
return oo(this).then(function(n) {
return uo(ao(t, so(t, n, !1)).get(e));
}).then(function(e) {
return e && e.value;
});
};
iu.X = function(e) {
var t = !1, n = this;
return ae(oo(this).then(function(r) {
t = !0;
n.a++;
return uo(ao(n, so(n, r, !0)).delete(e));
}).then(function() {
delete n.f[e];
}), function() {
t && n.a--;
});
};
iu.zc = function() {
var e = this;
return oo(this).then(function(t) {
var n = ao(e, so(e, t, !1));
return n.getAll ? uo(n.getAll()) : new $(function(e, t) {
var r = [], i = n.openCursor();
i.onsuccess = function(t) {
(t = t.target.result) ? (r.push(t.value), t.continue()) : e(r);
};
i.onerror = function(e) {
t(Error(e.target.errorCode));
};
});
}).then(function(t) {
var n = {}, r = [];
if (0 == e.a) {
for (r = 0; r < t.length; r++) n[t[r][e.g]] = t[r][e.l];
r = qn(e.f, n);
e.f = n;
}
return r;
});
};
iu.ia = function(e) {
0 == this.c.length && co(this);
this.c.push(e);
};
iu.da = function(e) {
N(this.c, function(t) {
return t == e;
});
0 == this.c.length && this.b && this.b.cancel("STOP_EVENT");
};
(iu = lo.prototype).get = function(e) {
var t = this;
return te().then(function() {
return fr(t.a.getItem(e));
});
};
iu.set = function(e, t) {
var n = this;
return te().then(function() {
var r = lr(t);
null === r ? n.X(e) : n.a.setItem(e, r);
});
};
iu.X = function(e) {
var t = this;
return te().then(function() {
t.a.removeItem(e);
});
};
iu.ia = function(e) {
su.window && $e(su.window, "storage", e);
};
iu.da = function(e) {
su.window && nt(su.window, "storage", e);
};
(iu = po.prototype).get = function() {
return te(null);
};
iu.set = function() {
return te();
};
iu.X = function() {
return te();
};
iu.ia = function() {};
iu.da = function() {};
(iu = vo.prototype).get = function(e) {
var t = this;
return te().then(function() {
return fr(t.a.getItem(e));
});
};
iu.set = function(e, t) {
var n = this;
return te().then(function() {
var r = lr(t);
null === r ? n.X(e) : n.a.setItem(e, r);
});
};
iu.X = function(e) {
var t = this;
return te().then(function() {
t.a.removeItem(e);
});
};
iu.ia = function() {};
iu.da = function() {};
var ph, dh, vh = {
C: lo,
jb: vo
}, _h = {
C: lo,
jb: vo
}, yh = {
C: to,
jb: po
}, gh = {
Lc: "local",
NONE: "none",
Nc: "session"
};
bo.prototype.get = function(e, t) {
return wo(this, e.C).get(Co(this, e, t));
};
bo.prototype.set = function(e, t, n) {
var r = Co(this, e, n), i = this, o = wo(this, e.C);
return o.set(r, t).then(function() {
return o.get(r);
}).then(function(t) {
"local" == e.C && (i.b[r] = t);
});
};
bo.prototype.m = function(e) {
if (e && e.g) {
var t = e.a.key;
if (null == t) for (var n in this.a) {
var r = this.b[n];
"undefined" == typeof r && (r = null);
var i = su.localStorage.getItem(n);
i !== r && (this.b[n] = i, this.c(n));
} else if (0 == t.indexOf(this.i + this.g) && this.a[t]) {
"undefined" != typeof e.a.a ? wo(this, "local").da(this.h) : Po(this);
if (this.A) if (n = su.localStorage.getItem(t), (r = e.a.newValue) !== n) null !== r ? su.localStorage.setItem(t, r) : su.localStorage.removeItem(t); else if (this.b[t] === r && "undefined" == typeof e.a.a) return;
var o = this;
n = function() {
"undefined" == typeof e.a.a && o.b[t] === su.localStorage.getItem(t) || (o.b[t] = su.localStorage.getItem(t), 
o.c(t));
};
Ou && Fu && 10 == Fu && su.localStorage.getItem(t) !== e.a.newValue && e.a.newValue !== e.a.oldValue ? setTimeout(n, 10) : n();
}
} else Eu(e, f(this.c, this));
};
bo.prototype.c = function(e) {
this.a[e] && Eu(this.a[e], function(e) {
e();
});
};
var mh = {
name: "authEvent",
C: "local"
};
(iu = Do.prototype).ba = function() {
return this.xa ? this.xa : this.xa = zn().then(function() {
if ("function" != typeof ir("universalLinks.subscribe", su)) throw Lo("cordova-universal-links-plugin is not installed");
if ("undefined" == typeof ir("BuildInfo.packageName", su)) throw Lo("cordova-plugin-buildinfo is not installed");
if ("function" != typeof ir("cordova.plugins.browsertab.openUrl", su)) throw Lo("cordova-plugin-browsertab is not installed");
if ("function" != typeof ir("cordova.InAppBrowser.open", su)) throw Lo("cordova-plugin-inappbrowser is not installed");
}, function() {
throw new Pr("cordova-not-ready");
});
};
iu.Ca = function(e, t) {
t(new Pr("operation-not-supported-in-this-environment"));
return te();
};
iu.vb = function() {
return ne(new Pr("operation-not-supported-in-this-environment"));
};
iu.Hb = function() {
return !1;
};
iu.Db = function() {
return !0;
};
iu.yb = function() {
return !0;
};
iu.Aa = function(e, t, n) {
if (this.c) return ne(new Pr("redirect-operation-pending"));
var r = this, i = su.document, o = null, a = null, s = null, u = null;
return this.c = ae(te().then(function() {
ui(t);
return jo(r);
}).then(function() {
return Fo(r, e, t, n);
}).then(function() {
return new $(function(e, t) {
a = function() {
var t = ir("cordova.plugins.browsertab.close", su);
e();
"function" == typeof t && t();
r.a && "function" == typeof r.a.close && (r.a.close(), r.a = null);
return !1;
};
r.ua(a);
s = function() {
o || (o = _t(r.w).then(function() {
t(new Pr("redirect-cancelled-by-user"));
}));
};
u = function() {
gr() && s();
};
i.addEventListener("resume", s, !1);
rr().toLowerCase().match(/android/) || i.addEventListener("visibilitychange", u, !1);
}).s(function(e) {
return Wo(r).then(function() {
throw e;
});
});
}), function() {
s && i.removeEventListener("resume", s, !1);
u && i.removeEventListener("visibilitychange", u, !1);
o && o.cancel();
a && r.Ja(a);
r.c = null;
});
};
iu.ua = function(e) {
this.b.push(e);
jo(this).s(function(t) {
"auth/invalid-cordova-configuration" === t.code && (t = new ci("unknown", null, null, null, new Pr("no-auth-event")), 
e(t));
});
};
iu.Ja = function(e) {
N(this.b, function(t) {
return t == e;
});
};
var bh = {
name: "pendingRedirect",
C: "session"
};
Ho.prototype.reset = function() {
this.f = !1;
this.a.Ja(this.i);
this.a = Qo(this.v, this.l, this.u);
};
Ho.prototype.subscribe = function(e) {
S(this.h, e) || this.h.push(e);
if (!this.f) {
var t = this;
Go(this.g).then(function(e) {
e ? Ko(t.g).then(function() {
Xo(t).s(function(e) {
var n = new ci("unknown", null, null, null, new Pr("operation-not-supported-in-this-environment"));
Jo(e) && t.m(n);
});
}) : Yo(t);
}).s(function() {
Yo(t);
});
}
};
Ho.prototype.unsubscribe = function(e) {
N(this.h, function(t) {
return t == e;
});
};
Ho.prototype.m = function(e) {
if (!e) throw new Pr("invalid-auth-event");
for (var t = !1, n = 0; n < this.h.length; n++) {
var r = this.h[n];
if (r.ob(e.b, e.c)) {
(t = this.b[e.b]) && t.h(e, r);
t = !0;
break;
}
}
ea(this.c);
return t;
};
var Eh = new yr(2e3, 1e4), wh = new yr(3e4, 6e4);
Ho.prototype.aa = function() {
return this.c.aa();
};
Ho.prototype.Aa = function(e, t, n) {
var r, i = this;
return qo(this.g).then(function() {
return i.a.Aa(e, t, n).s(function(e) {
if (Jo(e)) throw new Pr("operation-not-supported-in-this-environment");
r = e;
return Ko(i.g).then(function() {
throw r;
});
}).then(function() {
return i.a.Hb() ? new $(function() {}) : Ko(i.g).then(function() {
return i.aa();
}).then(function() {}).s(function() {});
});
});
};
Ho.prototype.Ca = function(e, t, n, r) {
return this.a.Ca(n, function(n) {
e.fa(t, null, n, r);
}, Eh.get());
};
var Ch = {};
Zo.prototype.reset = function() {
this.b = null;
this.a && (this.a.cancel(), this.a = null);
};
Zo.prototype.h = function(e, t) {
if (!e) return ne(new Pr("invalid-auth-event"));
this.reset();
this.g = !0;
var n = e.b, r = e.c, i = e.a && "auth/web-storage-unsupported" == e.a.code, o = e.a && "auth/operation-not-supported-in-this-environment" == e.a.code;
"unknown" != n || i || o ? e.a ? (ia(this, !0, null, e.a), e = te()) : e = t.va(n, r) ? ta(this, e, t) : ne(new Pr("invalid-auth-event")) : (ia(this, !1, null, null), 
e = te());
return e;
};
Zo.prototype.aa = function() {
var e = this;
return new $(function(t, n) {
e.b ? e.b().then(t, n) : (e.f.push(t), e.c.push(n), oa(e));
});
};
aa.prototype.h = function(e, t) {
if (!e) return ne(new Pr("invalid-auth-event"));
var n = e.b, r = e.c;
e.a ? (t.fa(e.b, null, e.a, e.c), e = te()) : e = t.va(n, r) ? sa(e, t) : ne(new Pr("invalid-auth-event"));
return e;
};
ua.prototype.confirm = function(e) {
e = ai(this.verificationId, e);
return this.a(e);
};
la.prototype.start = function() {
this.a = this.c;
fa(this, !0);
};
da.prototype.B = function() {
return {
apiKey: this.f.b,
refreshToken: this.a,
accessToken: this.b,
expirationTime: this.c
};
};
da.prototype.getToken = function(e) {
e = !!e;
return this.b && !this.a ? ne(new Pr("user-token-expired")) : e || !this.b || lu() > this.c - 3e4 ? this.a ? ya(this, {
grant_type: "refresh_token",
refresh_token: this.a
}) : te(null) : te({
accessToken: this.b,
expirationTime: this.c,
refreshToken: this.a
});
};
ga.prototype.B = function() {
return {
lastLoginAt: this.b,
createdAt: this.a
};
};
d(Ea, Ke);
d(wa, lt);
wa.prototype.na = function(e) {
this.ha = e;
_i(this.c, e);
};
wa.prototype.$ = function() {
return this.ha;
};
wa.prototype.Ka = function() {
return P(this.R);
};
wa.prototype.Ga = function() {
this.l.b && (pa(this.l), this.l.start());
};
Er(wa.prototype, "providerId", "firebase");
(iu = wa.prototype).reload = function() {
var e = this;
return $a(this, xa(this).then(function() {
return Va(e).then(function() {
return ka(e);
}).then(Ma);
}));
};
iu.F = function(e) {
var t = this;
return $a(this, xa(this).then(function() {
return t.h.getToken(e);
}).then(function(e) {
if (!e) throw new Pr("internal-error");
e.accessToken != t.pa && (Ra(t, e.accessToken), ht(t, new Ea("tokenChanged")));
Wa(t, "refreshToken", e.refreshToken);
return e.accessToken;
}));
};
iu.getToken = function(e) {
rl["firebase.User.prototype.getToken is deprecated. Please use firebase.User.prototype.getIdToken instead."] || (rl["firebase.User.prototype.getToken is deprecated. Please use firebase.User.prototype.getIdToken instead."] = !0, 
"undefined" != typeof console && "function" == typeof console.warn && console.warn("firebase.User.prototype.getToken is deprecated. Please use firebase.User.prototype.getIdToken instead."));
return this.F(e);
};
iu.kc = function(e) {
if (!(e = e.users) || !e.length) throw new Pr("internal-error");
La(this, {
uid: (e = e[0]).localId,
displayName: e.displayName,
photoURL: e.photoUrl,
email: e.email,
emailVerified: !!e.emailVerified,
phoneNumber: e.phoneNumber,
lastLoginAt: e.lastLoginAt,
createdAt: e.createdAt
});
for (var t = Ga(e), n = 0; n < t.length; n++) Ua(this, t[n]);
Wa(this, "isAnonymous", !(this.email && e.passwordHash || this.providerData && this.providerData.length));
};
iu.Za = function(e) {
var t = this, n = null;
return $a(this, e.c(this.c, this.uid).then(function(e) {
qa(t, e);
n = Qa(t, e, "reauthenticate");
t.i = null;
return t.reload();
}).then(function() {
return n;
}), !0);
};
iu.mc = function(e) {
return this.Za(e).then(function() {});
};
iu.Xa = function(e) {
var t = this, n = null;
return $a(this, Ha(this, e.providerId).then(function() {
return t.F();
}).then(function(n) {
return e.b(t.c, n);
}).then(function(e) {
n = Qa(t, e, "link");
return Xa(t, e);
}).then(function() {
return n;
}));
};
iu.cc = function(e) {
return this.Xa(e).then(function(e) {
return e.user;
});
};
iu.dc = function(e, t) {
var n = this;
return $a(this, Ha(this, "phone").then(function() {
return ca(Na(n), e, t, f(n.Xa, n));
}));
};
iu.nc = function(e, t) {
var n = this;
return $a(this, te().then(function() {
return ca(Na(n), e, t, f(n.Za, n));
}), !0);
};
iu.kb = function(e) {
var t = this;
return $a(this, this.F().then(function(n) {
return t.c.kb(n, e);
}).then(function(e) {
qa(t, e);
return t.reload();
}));
};
iu.Dc = function(e) {
var t = this;
return $a(this, this.F().then(function(n) {
return e.b(t.c, n);
}).then(function(e) {
qa(t, e);
return t.reload();
}));
};
iu.lb = function(e) {
var t = this;
return $a(this, this.F().then(function(n) {
return t.c.lb(n, e);
}).then(function(e) {
qa(t, e);
return t.reload();
}));
};
iu.mb = function(e) {
if (void 0 === e.displayName && void 0 === e.photoURL) return xa(this);
var t = this;
return $a(this, this.F().then(function(n) {
return t.c.mb(n, {
displayName: e.displayName,
photoUrl: e.photoURL
});
}).then(function(e) {
qa(t, e);
Wa(t, "displayName", e.displayName || null);
Wa(t, "photoURL", e.photoUrl || null);
Eu(t.providerData, function(e) {
"password" === e.providerId && (Er(e, "displayName", t.displayName), Er(e, "photoURL", t.photoURL));
});
return ka(t);
}).then(Ma));
};
iu.Cc = function(e) {
var t = this;
return $a(this, Va(this).then(function(n) {
return S(Fa(t), e) ? Oi(t.c, n, [ e ]).then(function(e) {
var n = {};
Eu(e.providerUserInfo || [], function(e) {
n[e.providerId] = !0;
});
Eu(Fa(t), function(e) {
n[e] || ja(t, e);
});
n[oi.PROVIDER_ID] || Er(t, "phoneNumber", null);
return ka(t);
}) : ka(t).then(function() {
throw new Pr("no-such-provider");
});
}));
};
iu.delete = function() {
var e = this;
return $a(this, this.F().then(function(t) {
return xi(e.c, Bl, {
idToken: t
});
}).then(function() {
ht(e, new Ea("userDeleted"));
})).then(function() {
for (var t = 0; t < e.A.length; t++) e.A[t].cancel("app-deleted");
Ca(e, null);
Ta(e, null);
e.A = [];
e.m = !0;
Oa(e);
Er(e, "refreshToken", null);
e.a && e.a.unsubscribe(e);
});
};
iu.ob = function(e, t) {
return !!("linkViaPopup" == e && (this.g || null) == t && this.f || "reauthViaPopup" == e && (this.g || null) == t && this.f || "linkViaRedirect" == e && (this.Z || null) == t || "reauthViaRedirect" == e && (this.Z || null) == t);
};
iu.fa = function(e, t, n, r) {
"linkViaPopup" != e && "reauthViaPopup" != e || r != (this.g || null) || (n && this.v ? this.v(n) : t && !n && this.f && this.f(t), 
this.b && (this.b.cancel(), this.b = null), delete this.f, delete this.v);
};
iu.va = function(e, t) {
return "linkViaPopup" == e && t == (this.g || null) ? f(this.tb, this) : "reauthViaPopup" == e && t == (this.g || null) ? f(this.ub, this) : "linkViaRedirect" == e && (this.Z || null) == t ? f(this.tb, this) : "reauthViaRedirect" == e && (this.Z || null) == t ? f(this.ub, this) : null;
};
iu.ec = function(e) {
var t = this;
return Ya(this, "linkViaPopup", e, function() {
return Ha(t, e.providerId).then(function() {
return ka(t);
});
}, !1);
};
iu.oc = function(e) {
return Ya(this, "reauthViaPopup", e, function() {
return te();
}, !0);
};
iu.fc = function(e) {
var t = this;
return za(this, "linkViaRedirect", e, function() {
return Ha(t, e.providerId);
}, !1);
};
iu.pc = function(e) {
return za(this, "reauthViaRedirect", e, function() {
return te();
}, !0);
};
iu.tb = function(e, t) {
var n = this;
this.b && (this.b.cancel(), this.b = null);
var r = null;
return $a(this, this.F().then(function(r) {
return Di(n.c, {
requestUri: e,
sessionId: t,
idToken: r
});
}).then(function(e) {
r = Qa(n, e, "link");
return Xa(n, e);
}).then(function() {
return r;
}));
};
iu.ub = function(e, t) {
var n = this;
this.b && (this.b.cancel(), this.b = null);
var r = null;
return $a(this, te().then(function() {
return Vr(Li(n.c, {
requestUri: e,
sessionId: t
}), n.uid);
}).then(function(e) {
r = Qa(n, e, "reauthenticate");
qa(n, e);
n.i = null;
return n.reload();
}).then(function() {
return r;
}), !0);
};
iu.bb = function(e) {
var t = this, n = null;
return $a(this, this.F().then(function(t) {
n = t;
return "undefined" == typeof e || D(e) ? {} : Ar(new Rr(e));
}).then(function(e) {
return t.c.bb(n, e);
}).then(function(e) {
if (t.email != e) return t.reload();
}).then(function() {}));
};
iu.toJSON = function() {
return this.B();
};
iu.B = function() {
var e = {
uid: this.uid,
displayName: this.displayName,
photoURL: this.photoURL,
email: this.email,
emailVerified: this.emailVerified,
phoneNumber: this.phoneNumber,
isAnonymous: this.isAnonymous,
providerData: [],
apiKey: this.G,
appName: this.o,
authDomain: this.w,
stsTokenManager: this.h.B(),
redirectEventId: this.Z || null
};
this.metadata && x(e, this.metadata.B());
Eu(this.providerData, function(t) {
e.providerData.push(Sr(t));
});
return e;
};
var Sh = {
name: "redirectUser",
C: "session"
};
os.prototype.g = function() {
var e = this, t = us("local");
ps(this, function() {
return te().then(function() {
return e.c && "local" != e.c.C ? e.b.get(t, e.a) : null;
}).then(function(n) {
if (n) return as(e, "local").then(function() {
e.c = t;
});
});
});
};
var Th = {
name: "persistence",
C: "session"
};
os.prototype.eb = function(e) {
var t = null, n = this;
mo(e);
return ps(this, function() {
return e != n.c.C ? n.b.get(n.c, n.a).then(function(r) {
t = r;
return as(n, e);
}).then(function() {
n.c = us(e);
if (t) return n.b.set(n.c, t, n.a);
}) : te();
});
};
d(ds, lt);
d(vs, Ke);
d(_s, Ke);
(iu = ds.prototype).eb = function(e) {
return As(this, e = this.h.eb(e));
};
iu.na = function(e) {
this.V === e || this.l || (this.V = e, _i(this.c, this.V), ht(this, new vs(this.$())));
};
iu.$ = function() {
return this.V;
};
iu.Ec = function() {
var e = su.navigator;
this.na(e ? e.languages && e.languages[0] || e.language || e.userLanguage || null : null);
};
iu.gc = function(e) {
this.A.push(e);
yi(this.c, ou.SDK_VERSION ? nr(ou.SDK_VERSION, this.A) : null);
ht(this, new _s(this.A));
};
iu.Ka = function() {
return P(this.A);
};
iu.toJSON = function() {
return {
apiKey: Ns(this).options.apiKey,
authDomain: Ns(this).options.authDomain,
appName: Ns(this).name,
currentUser: Is(this) && Is(this).B()
};
};
iu.ob = function(e, t) {
switch (e) {
case "unknown":
case "signInViaRedirect":
return !0;

case "signInViaPopup":
return this.g == t && !!this.f;

default:
return !1;
}
};
iu.fa = function(e, t, n, r) {
"signInViaPopup" == e && this.g == r && (n && this.v ? this.v(n) : t && !n && this.f && this.f(t), 
this.b && (this.b.cancel(), this.b = null), delete this.f, delete this.v);
};
iu.va = function(e, t) {
return "signInViaRedirect" == e || "signInViaPopup" == e && this.g == t && this.f ? f(this.Sb, this) : null;
};
iu.Sb = function(e, t) {
var n = this;
e = {
requestUri: e,
sessionId: t
};
this.b && (this.b.cancel(), this.b = null);
var r = null, i = null, o = ki(n.c, e).then(function(e) {
r = si(e);
i = Mr(e);
return e;
});
return As(this, e = n.U.then(function() {
return o;
}).then(function(e) {
return bs(n, e);
}).then(function() {
return Cr({
user: Is(n),
credential: r,
additionalUserInfo: i,
operationType: "signIn"
});
}));
};
iu.wc = function(e) {
if (!ar()) return ne(new Pr("operation-not-supported-in-this-environment"));
var t = this, n = Lr(e.providerId), r = pr(), i = null;
(!cr() || $n()) && Ns(this).options.authDomain && e.isOAuthProvider && (i = $i(Ns(this).options.authDomain, Ns(this).options.apiKey, Ns(this).name, "signInViaPopup", e, null, r, ou.SDK_VERSION || null));
var o = Qn(i, n && n.za, n && n.ya);
return As(this, n = gs(this).then(function(t) {
return zo(t, o, "signInViaPopup", e, r, !!i);
}).then(function() {
return new $(function(e, n) {
t.fa("signInViaPopup", null, new Pr("cancelled-popup-request"), t.g);
t.f = e;
t.v = n;
t.g = r;
t.b = t.a.Ca(t, "signInViaPopup", o, r);
});
}).then(function(e) {
o && Hn(o);
return e ? Cr(e) : null;
}).s(function(e) {
o && Hn(o);
throw e;
}));
};
iu.xc = function(e) {
if (!ar()) return ne(new Pr("operation-not-supported-in-this-environment"));
var t = this;
return As(this, gs(this).then(function() {
return cs(t.h);
}).then(function() {
return t.a.Aa("signInViaRedirect", e);
}));
};
iu.aa = function() {
if (!ar()) return ne(new Pr("operation-not-supported-in-this-environment"));
var e = this;
return As(this, gs(this).then(function() {
return e.a.aa();
}).then(function(e) {
return e ? Cr(e) : null;
}));
};
iu.hb = function() {
var e = this;
return As(this, this.i.then(function() {
if (!Is(e)) return te();
Es(e, null);
return hs(e.h).then(function() {
Os(e);
});
}));
};
iu.yc = function() {
var e = this;
return fs(this.h, Ns(this).options.authDomain).then(function(t) {
if (!e.l) {
var n;
if (n = Is(e) && t) {
n = Is(e).uid;
var r = t.uid;
n = void 0 !== n && null !== n && "" !== n && void 0 !== r && null !== r && "" !== r && n == r;
}
if (n) return Ba(Is(e), t), Is(e).F();
(Is(e) || t) && (Es(e, t), t && (Da(t), t.ca = e.G), e.a && e.a.subscribe(e), Os(e));
}
});
};
iu.ka = function(e) {
return ls(this.h, e);
};
iu.Tb = function() {
Os(this);
this.ka(Is(this));
};
iu.Zb = function() {
this.hb();
};
iu.$b = function() {
this.hb();
};
iu.ac = function(e) {
var t = this;
this.addAuthTokenListener(function() {
e.next(Is(t));
});
};
iu.bc = function(e) {
var t = this;
Rs(this, function() {
e.next(Is(t));
});
};
iu.ic = function(e, t, n) {
var r = this;
this.W && ou.Promise.resolve().then(function() {
u(e) ? e(Is(r)) : u(e.next) && e.next(Is(r));
});
return this.Kb(e, t, n);
};
iu.hc = function(e, t, n) {
var r = this;
this.W && ou.Promise.resolve().then(function() {
r.R = r.getUid();
u(e) ? e(Is(r)) : u(e.next) && e.next(Is(r));
});
return this.Lb(e, t, n);
};
iu.Vb = function(e) {
var t = this;
return As(this, this.i.then(function() {
return Is(t) ? Is(t).F(e).then(function(e) {
return {
accessToken: e
};
}) : null;
}));
};
iu.tc = function(e) {
return this.Eb(e).then(function(e) {
return e.user;
});
};
iu.Eb = function(e) {
var t = this;
return this.i.then(function() {
return Ts(t, xi(t.c, nh, {
token: e
}));
}).then(function(e) {
var n = e.user;
Wa(n, "isAnonymous", !1);
t.ka(n);
return e;
});
};
iu.Fb = function(e, t) {
var n = this;
return this.i.then(function() {
return Ts(n, xi(n.c, rh, {
email: e,
password: t
}));
});
};
iu.uc = function(e, t) {
return this.Fb(e, t).then(function(e) {
return e.user;
});
};
iu.Ob = function(e, t) {
return this.qb(e, t).then(function(e) {
return e.user;
});
};
iu.qb = function(e, t) {
var n = this;
return this.i.then(function() {
return Ts(n, xi(n.c, jl, {
email: e,
password: t
}));
});
};
iu.sc = function(e) {
return this.fb(e).then(function(e) {
return e.user;
});
};
iu.fb = function(e) {
var t = this;
return this.i.then(function() {
return Ts(t, e.wa(t.c));
});
};
iu.gb = function() {
return this.Gb().then(function(e) {
return e.user;
});
};
iu.Gb = function() {
var e = this;
return this.i.then(function() {
var t = Is(e);
return t && t.isAnonymous ? Cr({
user: t,
credential: null,
additionalUserInfo: Cr({
providerId: null,
isNewUser: !1
}),
operationType: "signIn"
}) : Ts(e, e.c.gb()).then(function(t) {
var n = t.user;
Wa(n, "isAnonymous", !0);
e.ka(n);
return t;
});
});
};
iu.getUid = function() {
return Is(this) && Is(this).uid || null;
};
iu.Mb = function(e) {
this.addAuthTokenListener(e);
0 < ++this.o && Is(this) && Pa(Is(this));
};
iu.qc = function(e) {
var t = this;
Eu(this.m, function(n) {
n == e && t.o--;
});
0 > this.o && (this.o = 0);
0 == this.o && Is(this) && Oa(Is(this));
this.removeAuthTokenListener(e);
};
iu.addAuthTokenListener = function(e) {
var t = this;
this.m.push(e);
As(this, this.i.then(function() {
t.l || S(t.m, e) && e(Ps(t));
}));
};
iu.removeAuthTokenListener = function(e) {
N(this.m, function(t) {
return t == e;
});
};
iu.delete = function() {
this.l = !0;
for (var e = 0; e < this.N.length; e++) this.N[e].cancel("app-deleted");
this.N = [];
this.h && (e = this.h, No(e.b, e.a, this.ha));
this.a && this.a.unsubscribe(this);
return ou.Promise.resolve();
};
iu.Rb = function(e) {
return As(this, Si(this.c, e));
};
iu.Fc = function(e) {
return this.Ia(e).then(function(e) {
return e.data.email;
});
};
iu.Ta = function(e, t) {
return As(this, this.c.Ta(e, t).then(function() {}));
};
iu.Ia = function(e) {
return As(this, this.c.Ia(e).then(function(e) {
return new Ir(e);
}));
};
iu.Sa = function(e) {
return As(this, this.c.Sa(e).then(function() {}));
};
iu.cb = function(e, t) {
var n = this;
return As(this, te().then(function() {
return "undefined" == typeof t || D(t) ? {} : Ar(new Rr(t));
}).then(function(t) {
return n.c.cb(e, t);
}).then(function() {}));
};
iu.vc = function(e, t) {
return As(this, ca(this, e, t, f(this.fb, this)));
};
var Nh = "First Second Third Fourth Fifth Sixth Seventh Eighth Ninth".split(" "), Ih = "callback", Ph = "expired-callback", Oh = "sitekey", Rh = "size";
(iu = Gs.prototype).xa = function() {
var e = this;
return this.c ? this.c : this.c = Xs(this, te().then(function() {
if (sr()) return Yn();
throw new Pr("operation-not-supported-in-this-environment", "RecaptchaVerifier is only supported in a browser HTTP/HTTPS environment.");
}).then(function() {
return Js($s(), e.o());
}).then(function() {
return xi(e.u, Ql, {});
}).then(function(t) {
e.a[Oh] = t.recaptchaSiteKey;
}).s(function(t) {
e.c = null;
throw t;
}));
};
iu.render = function() {
Ys(this);
var e = this;
return Xs(this, this.xa().then(function() {
if (null === e.b) {
var t = e.l;
if (!e.h) {
var n = De(t);
t = Me("DIV");
n.appendChild(t);
}
e.b = grecaptcha.render(t, e.a);
}
return e.b;
}));
};
iu.verify = function() {
Ys(this);
var e = this;
return Xs(this, this.render().then(function(t) {
return new $(function(n) {
var r = grecaptcha.getResponse(t);
if (r) n(r); else {
var i = function(t) {
t && (Qs(e, i), n(t));
};
e.i.push(i);
e.h && grecaptcha.execute(e.b);
}
});
}));
};
iu.reset = function() {
Ys(this);
null !== this.b && grecaptcha.reset(this.b);
};
iu.clear = function() {
Ys(this);
this.m = !0;
$s().b--;
for (var e = 0; e < this.g.length; e++) this.g[e].cancel("RecaptchaVerifier instance has been destroyed.");
if (!this.h) {
e = De(this.l);
for (var t; t = e.firstChild; ) e.removeChild(t);
}
};
var Ah = Ee("https://www.google.com/recaptcha/api.js?onload=%{onload}&render=explicit&hl=%{hl}"), kh = null;
d(Zs, Gs);
eu(ds.prototype, {
Sa: {
name: "applyActionCode",
j: [ Ds("code") ]
},
Ia: {
name: "checkActionCode",
j: [ Ds("code") ]
},
Ta: {
name: "confirmPasswordReset",
j: [ Ds("code"), Ds("newPassword") ]
},
Ob: {
name: "createUserWithEmailAndPassword",
j: [ Ds("email"), Ds("password") ]
},
qb: {
name: "createUserAndRetrieveDataWithEmailAndPassword",
j: [ Ds("email"), Ds("password") ]
},
Rb: {
name: "fetchProvidersForEmail",
j: [ Ds("email") ]
},
aa: {
name: "getRedirectResult",
j: []
},
hc: {
name: "onAuthStateChanged",
j: [ Ks(Ms(), xs(), "nextOrObserver"), xs("opt_error", !0), xs("opt_completed", !0) ]
},
ic: {
name: "onIdTokenChanged",
j: [ Ks(Ms(), xs(), "nextOrObserver"), xs("opt_error", !0), xs("opt_completed", !0) ]
},
cb: {
name: "sendPasswordResetEmail",
j: [ Ds("email"), Ks(Ms("opt_actionCodeSettings", !0), Fs(null, !0), "opt_actionCodeSettings", !0) ]
},
eb: {
name: "setPersistence",
j: [ Ds("persistence") ]
},
fb: {
name: "signInAndRetrieveDataWithCredential",
j: [ Bs() ]
},
gb: {
name: "signInAnonymously",
j: []
},
Gb: {
name: "signInAnonymouslyAndRetrieveData",
j: []
},
sc: {
name: "signInWithCredential",
j: [ Bs() ]
},
tc: {
name: "signInWithCustomToken",
j: [ Ds("token") ]
},
Eb: {
name: "signInAndRetrieveDataWithCustomToken",
j: [ Ds("token") ]
},
uc: {
name: "signInWithEmailAndPassword",
j: [ Ds("email"), Ds("password") ]
},
Fb: {
name: "signInAndRetrieveDataWithEmailAndPassword",
j: [ Ds("email"), Ds("password") ]
},
vc: {
name: "signInWithPhoneNumber",
j: [ Ds("phoneNumber"), qs() ]
},
wc: {
name: "signInWithPopup",
j: [ Vs() ]
},
xc: {
name: "signInWithRedirect",
j: [ Vs() ]
},
hb: {
name: "signOut",
j: []
},
toJSON: {
name: "toJSON",
j: [ Ds(null, !0) ]
},
Ec: {
name: "useDeviceLanguage",
j: []
},
Fc: {
name: "verifyPasswordResetCode",
j: [ Ds("code") ]
}
});
!function(e, t) {
for (var n in t) {
var r = t[n].name;
if (r !== n) {
var i = t[n].Nb;
Object.defineProperty(e, r, {
get: function() {
return this[n];
},
set: function(e) {
ks(r, [ i ], [ e ], !0);
this[n] = e;
},
enumerable: !0
});
}
}
}(ds.prototype, {
lc: {
name: "languageCode",
Nb: Ks(Ds(), Fs(), "languageCode")
}
});
ds.Persistence = gh;
ds.Persistence.LOCAL = "local";
ds.Persistence.SESSION = "session";
ds.Persistence.NONE = "none";
eu(wa.prototype, {
delete: {
name: "delete",
j: []
},
F: {
name: "getIdToken",
j: [ Ls() ]
},
getToken: {
name: "getToken",
j: [ Ls() ]
},
Xa: {
name: "linkAndRetrieveDataWithCredential",
j: [ Bs() ]
},
cc: {
name: "linkWithCredential",
j: [ Bs() ]
},
dc: {
name: "linkWithPhoneNumber",
j: [ Ds("phoneNumber"), qs() ]
},
ec: {
name: "linkWithPopup",
j: [ Vs() ]
},
fc: {
name: "linkWithRedirect",
j: [ Vs() ]
},
Za: {
name: "reauthenticateAndRetrieveDataWithCredential",
j: [ Bs() ]
},
mc: {
name: "reauthenticateWithCredential",
j: [ Bs() ]
},
nc: {
name: "reauthenticateWithPhoneNumber",
j: [ Ds("phoneNumber"), qs() ]
},
oc: {
name: "reauthenticateWithPopup",
j: [ Vs() ]
},
pc: {
name: "reauthenticateWithRedirect",
j: [ Vs() ]
},
reload: {
name: "reload",
j: []
},
bb: {
name: "sendEmailVerification",
j: [ Ks(Ms("opt_actionCodeSettings", !0), Fs(null, !0), "opt_actionCodeSettings", !0) ]
},
toJSON: {
name: "toJSON",
j: [ Ds(null, !0) ]
},
Cc: {
name: "unlink",
j: [ Ds("provider") ]
},
kb: {
name: "updateEmail",
j: [ Ds("email") ]
},
lb: {
name: "updatePassword",
j: [ Ds("password") ]
},
Dc: {
name: "updatePhoneNumber",
j: [ Bs("phone") ]
},
mb: {
name: "updateProfile",
j: [ Ms("profile") ]
}
});
eu($.prototype, {
s: {
name: "catch"
},
then: {
name: "then"
}
});
eu(ua.prototype, {
confirm: {
name: "confirm",
j: [ Ds("verificationCode") ]
}
});
tu(ni, "credential", function(e, t) {
return new ti(e, t);
}, [ Ds("email"), Ds("password") ]);
eu(Qr.prototype, {
sa: {
name: "addScope",
j: [ Ds("scope") ]
},
Ba: {
name: "setCustomParameters",
j: [ Ms("customOAuthParameters") ]
}
});
tu(Qr, "credential", Xr, [ Ks(Ds(), Ms(), "token") ]);
eu(Yr.prototype, {
sa: {
name: "addScope",
j: [ Ds("scope") ]
},
Ba: {
name: "setCustomParameters",
j: [ Ms("customOAuthParameters") ]
}
});
tu(Yr, "credential", zr, [ Ks(Ds(), Ms(), "token") ]);
eu(Jr.prototype, {
sa: {
name: "addScope",
j: [ Ds("scope") ]
},
Ba: {
name: "setCustomParameters",
j: [ Ms("customOAuthParameters") ]
}
});
tu(Jr, "credential", $r, [ Ks(Ds(), Ks(Ms(), Fs()), "idToken"), Ks(Ds(), Fs(), "accessToken", !0) ]);
eu(Zr.prototype, {
Ba: {
name: "setCustomParameters",
j: [ Ms("customOAuthParameters") ]
}
});
tu(Zr, "credential", ei, [ Ks(Ds(), Ms(), "token"), Ds("secret", !0) ]);
eu(Hr.prototype, {
sa: {
name: "addScope",
j: [ Ds("scope") ]
},
credential: {
name: "credential",
j: [ Ks(Ds(), Fs(), "idToken", !0), Ks(Ds(), Fs(), "accessToken", !0) ]
},
Ba: {
name: "setCustomParameters",
j: [ Ms("customOAuthParameters") ]
}
});
tu(oi, "credential", ai, [ Ds("verificationId"), Ds("verificationCode") ]);
eu(oi.prototype, {
Qa: {
name: "verifyPhoneNumber",
j: [ Ds("phoneNumber"), qs() ]
}
});
eu(Pr.prototype, {
toJSON: {
name: "toJSON",
j: [ Ds(null, !0) ]
}
});
eu(fi.prototype, {
toJSON: {
name: "toJSON",
j: [ Ds(null, !0) ]
}
});
eu(hi.prototype, {
toJSON: {
name: "toJSON",
j: [ Ds(null, !0) ]
}
});
eu(Zs.prototype, {
clear: {
name: "clear",
j: []
},
render: {
name: "render",
j: []
},
verify: {
name: "verify",
j: []
}
});
!function() {
if ("undefined" == typeof ou || !ou.INTERNAL || !ou.INTERNAL.registerService) throw Error("Cannot find the firebase namespace; be sure to include firebase-app.js before this library.");
var e = {
Auth: ds,
Error: Pr
};
tu(e, "EmailAuthProvider", ni, []);
tu(e, "FacebookAuthProvider", Qr, []);
tu(e, "GithubAuthProvider", Yr, []);
tu(e, "GoogleAuthProvider", Jr, []);
tu(e, "TwitterAuthProvider", Zr, []);
tu(e, "OAuthProvider", Hr, [ Ds("providerId") ]);
tu(e, "PhoneAuthProvider", oi, [ js() ]);
tu(e, "RecaptchaVerifier", Zs, [ Ks(Ds(), Us(), "recaptchaContainer"), Ms("recaptchaParameters", !0), Ws() ]);
ou.INTERNAL.registerService("auth", function(e, t) {
t({
INTERNAL: {
getUid: f((e = new ds(e)).getUid, e),
getToken: f(e.Vb, e),
addAuthTokenListener: f(e.Mb, e),
removeAuthTokenListener: f(e.qc, e)
}
});
return e;
}, e, function(e, t) {
if ("create" === e) try {
t.auth();
} catch (e) {}
});
ou.INTERNAL.extendNamespace({
User: wa
});
}();
}).call("undefined" != typeof t ? t : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"@firebase/app": 2
} ],
5: [ function(e, t, n) {
"use strict";
function r(e) {
var n = e.INTERNAL.registerService("database", function(e, t, n) {
return c.RepoManager.getInstance().databaseFromApp(e, n);
}, {
Reference: s.Reference,
Query: a.Query,
Database: o.Database,
enableLogging: u.enableLogging,
INTERNAL: l,
ServerValue: p,
TEST_ACCESS: h
}, null, !0);
f.isNodeSdk() && (t.exports = n);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("@firebase/app"), o = e("./src/api/Database");
n.Database = o.Database;
var a = e("./src/api/Query");
n.Query = a.Query;
var s = e("./src/api/Reference");
n.Reference = s.Reference;
var u = e("./src/core/util/util");
n.enableLogging = u.enableLogging;
var c = e("./src/core/RepoManager"), l = e("./src/api/internal"), h = e("./src/api/test_access"), f = e("@firebase/util"), p = o.Database.ServerValue;
n.ServerValue = p;
n.registerDatabase = r;
r(i.default);
var d = e("./src/api/DataSnapshot");
n.DataSnapshot = d.DataSnapshot;
var v = e("./src/api/onDisconnect");
n.OnDisconnect = v.OnDisconnect;
}, {
"./src/api/DataSnapshot": 6,
"./src/api/Database": 7,
"./src/api/Query": 8,
"./src/api/Reference": 9,
"./src/api/internal": 11,
"./src/api/onDisconnect": 12,
"./src/api/test_access": 13,
"./src/core/RepoManager": 20,
"./src/core/util/util": 64,
"@firebase/app": 2,
"@firebase/util": 135
} ],
6: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../core/util/validation"), o = e("../core/util/Path"), a = e("../core/snap/indexes/PriorityIndex"), s = function() {
function e(e, t, n) {
this.node_ = e;
this.ref_ = t;
this.index_ = n;
}
e.prototype.val = function() {
r.validateArgCount("DataSnapshot.val", 0, 0, arguments.length);
return this.node_.val();
};
e.prototype.exportVal = function() {
r.validateArgCount("DataSnapshot.exportVal", 0, 0, arguments.length);
return this.node_.val(!0);
};
e.prototype.toJSON = function() {
r.validateArgCount("DataSnapshot.toJSON", 0, 1, arguments.length);
return this.exportVal();
};
e.prototype.exists = function() {
r.validateArgCount("DataSnapshot.exists", 0, 0, arguments.length);
return !this.node_.isEmpty();
};
e.prototype.child = function(t) {
r.validateArgCount("DataSnapshot.child", 0, 1, arguments.length);
t = String(t);
i.validatePathString("DataSnapshot.child", 1, t, !1);
var n = new o.Path(t), s = this.ref_.child(n);
return new e(this.node_.getChild(n), s, a.PRIORITY_INDEX);
};
e.prototype.hasChild = function(e) {
r.validateArgCount("DataSnapshot.hasChild", 1, 1, arguments.length);
i.validatePathString("DataSnapshot.hasChild", 1, e, !1);
var t = new o.Path(e);
return !this.node_.getChild(t).isEmpty();
};
e.prototype.getPriority = function() {
r.validateArgCount("DataSnapshot.getPriority", 0, 0, arguments.length);
return this.node_.getPriority().val();
};
e.prototype.forEach = function(t) {
var n = this;
r.validateArgCount("DataSnapshot.forEach", 1, 1, arguments.length);
r.validateCallback("DataSnapshot.forEach", 1, t, !1);
return !this.node_.isLeafNode() && !!this.node_.forEachChild(this.index_, function(r, i) {
return t(new e(i, n.ref_.child(r), a.PRIORITY_INDEX));
});
};
e.prototype.hasChildren = function() {
r.validateArgCount("DataSnapshot.hasChildren", 0, 0, arguments.length);
return !this.node_.isLeafNode() && !this.node_.isEmpty();
};
Object.defineProperty(e.prototype, "key", {
get: function() {
return this.ref_.getKey();
},
enumerable: !0,
configurable: !0
});
e.prototype.numChildren = function() {
r.validateArgCount("DataSnapshot.numChildren", 0, 0, arguments.length);
return this.node_.numChildren();
};
e.prototype.getRef = function() {
r.validateArgCount("DataSnapshot.ref", 0, 0, arguments.length);
return this.ref_;
};
Object.defineProperty(e.prototype, "ref", {
get: function() {
return this.getRef();
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.DataSnapshot = s;
}, {
"../core/snap/indexes/PriorityIndex": 42,
"../core/util/Path": 58,
"../core/util/validation": 65,
"@firebase/util": 135
} ],
7: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../core/util/util"), i = e("../core/util/libs/parser"), o = e("../core/util/Path"), a = e("./Reference"), s = e("../core/Repo"), u = e("../core/RepoManager"), c = e("@firebase/util"), l = e("../core/util/validation"), h = function() {
function e(e) {
this.repo_ = e;
e instanceof s.Repo || r.fatal("Don't call new Database() directly - please use firebase.database().");
this.root_ = new a.Reference(e, o.Path.Empty);
this.INTERNAL = new f(this);
}
Object.defineProperty(e.prototype, "app", {
get: function() {
return this.repo_.app;
},
enumerable: !0,
configurable: !0
});
e.prototype.ref = function(e) {
this.checkDeleted_("ref");
c.validateArgCount("database.ref", 0, 1, arguments.length);
return void 0 !== e ? this.root_.child(e) : this.root_;
};
e.prototype.refFromURL = function(e) {
var t = "database.refFromURL";
this.checkDeleted_(t);
c.validateArgCount(t, 1, 1, arguments.length);
var n = i.parseRepoInfo(e);
l.validateUrl(t, 1, n);
var o = n.repoInfo;
o.host !== this.repo_.repoInfo_.host && r.fatal(t + ": Host name does not match the current database: (found " + o.host + " but expected " + this.repo_.repoInfo_.host + ")");
return this.ref(n.path.toString());
};
e.prototype.checkDeleted_ = function(e) {
null === this.repo_ && r.fatal("Cannot call " + e + " on a deleted database.");
};
e.prototype.goOffline = function() {
c.validateArgCount("database.goOffline", 0, 0, arguments.length);
this.checkDeleted_("goOffline");
this.repo_.interrupt();
};
e.prototype.goOnline = function() {
c.validateArgCount("database.goOnline", 0, 0, arguments.length);
this.checkDeleted_("goOnline");
this.repo_.resume();
};
e.ServerValue = {
TIMESTAMP: {
".sv": "timestamp"
}
};
return e;
}();
n.Database = h;
var f = function() {
function e(e) {
this.database = e;
}
e.prototype.delete = function() {
this.database.checkDeleted_("delete");
u.RepoManager.getInstance().deleteRepo(this.database.repo_);
this.database.repo_ = null;
this.database.root_ = null;
this.database.INTERNAL = null;
this.database = null;
return Promise.resolve();
};
return e;
}();
n.DatabaseInternals = f;
}, {
"../core/Repo": 18,
"../core/RepoManager": 20,
"../core/util/Path": 58,
"../core/util/libs/parser": 63,
"../core/util/util": 64,
"../core/util/validation": 65,
"./Reference": 9,
"@firebase/util": 135
} ],
8: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("@firebase/util"), o = e("../core/snap/indexes/KeyIndex"), a = e("../core/snap/indexes/PriorityIndex"), s = e("../core/snap/indexes/ValueIndex"), u = e("../core/snap/indexes/PathIndex"), c = e("../core/util/util"), l = e("../core/util/Path"), h = e("../core/util/validation"), f = e("@firebase/util"), p = e("../core/view/EventRegistration"), d = e("@firebase/util"), v = function() {
function e(e, t, n, r) {
this.repo = e;
this.path = t;
this.queryParams_ = n;
this.orderByCalled_ = r;
}
Object.defineProperty(e, "__referenceConstructor", {
get: function() {
i.assert(r, "Reference.ts has not been loaded");
return r;
},
set: function(e) {
r = e;
},
enumerable: !0,
configurable: !0
});
e.validateQueryEndpoints_ = function(e) {
var t = null, n = null;
e.hasStart() && (t = e.getIndexStartValue());
e.hasEnd() && (n = e.getIndexEndValue());
if (e.getIndex() === o.KEY_INDEX) {
var r = "Query: When ordering by key, you may only pass one argument to startAt(), endAt(), or equalTo().", l = "Query: When ordering by key, the argument passed to startAt(), endAt(),or equalTo() must be a string.";
if (e.hasStart()) {
if (e.getIndexStartName() != c.MIN_NAME) throw new Error(r);
if ("string" != typeof t) throw new Error(l);
}
if (e.hasEnd()) {
if (e.getIndexEndName() != c.MAX_NAME) throw new Error(r);
if ("string" != typeof n) throw new Error(l);
}
} else if (e.getIndex() === a.PRIORITY_INDEX) {
if (null != t && !h.isValidPriority(t) || null != n && !h.isValidPriority(n)) throw new Error("Query: When ordering by priority, the first argument passed to startAt(), endAt(), or equalTo() must be a valid priority value (null, a number, or a string).");
} else {
i.assert(e.getIndex() instanceof u.PathIndex || e.getIndex() === s.VALUE_INDEX, "unknown index type.");
if (null != t && "object" == typeof t || null != n && "object" == typeof n) throw new Error("Query: First argument passed to startAt(), endAt(), or equalTo() cannot be an object.");
}
};
e.validateLimit_ = function(e) {
if (e.hasStart() && e.hasEnd() && e.hasLimit() && !e.hasAnchoredLimit()) throw new Error("Query: Can't combine startAt(), endAt(), and limit(). Use limitToFirst() or limitToLast() instead.");
};
e.prototype.validateNoPreviousOrderByCall_ = function(e) {
if (!0 === this.orderByCalled_) throw new Error(e + ": You can't combine multiple orderBy calls.");
};
e.prototype.getQueryParams = function() {
return this.queryParams_;
};
e.prototype.getRef = function() {
f.validateArgCount("Query.ref", 0, 0, arguments.length);
return new e.__referenceConstructor(this.repo, this.path);
};
e.prototype.on = function(t, n, r, i) {
f.validateArgCount("Query.on", 2, 4, arguments.length);
h.validateEventType("Query.on", 1, t, !1);
f.validateCallback("Query.on", 2, n, !1);
var o = e.getCancelAndContextArgs_("Query.on", r, i);
if ("value" === t) this.onValueEvent(n, o.cancel, o.context); else {
var a = {};
a[t] = n;
this.onChildEvent(a, o.cancel, o.context);
}
return n;
};
e.prototype.onValueEvent = function(e, t, n) {
var r = new p.ValueEventRegistration(e, t || null, n || null);
this.repo.addEventCallbackForQuery(this, r);
};
e.prototype.onChildEvent = function(e, t, n) {
var r = new p.ChildEventRegistration(e, t, n);
this.repo.addEventCallbackForQuery(this, r);
};
e.prototype.off = function(e, t, n) {
f.validateArgCount("Query.off", 0, 3, arguments.length);
h.validateEventType("Query.off", 1, e, !0);
f.validateCallback("Query.off", 2, t, !0);
f.validateContextObject("Query.off", 3, n, !0);
var r = null, i = null;
if ("value" === e) {
var o = t || null;
r = new p.ValueEventRegistration(o, null, n || null);
} else if (e) {
t && ((i = {})[e] = t);
r = new p.ChildEventRegistration(i, null, n || null);
}
this.repo.removeEventCallbackForQuery(this, r);
};
e.prototype.once = function(t, n, r, i) {
var o = this;
f.validateArgCount("Query.once", 1, 4, arguments.length);
h.validateEventType("Query.once", 1, t, !1);
f.validateCallback("Query.once", 2, n, !0);
var a = e.getCancelAndContextArgs_("Query.once", r, i), s = !0, u = new d.Deferred();
u.promise.catch(function() {});
var c = function(e) {
if (s) {
s = !1;
o.off(t, c);
n && n.bind(a.context)(e);
u.resolve(e);
}
};
this.on(t, c, function(e) {
o.off(t, c);
a.cancel && a.cancel.bind(a.context)(e);
u.reject(e);
});
return u.promise;
};
e.prototype.limitToFirst = function(t) {
f.validateArgCount("Query.limitToFirst", 1, 1, arguments.length);
if ("number" != typeof t || Math.floor(t) !== t || t <= 0) throw new Error("Query.limitToFirst: First argument must be a positive integer.");
if (this.queryParams_.hasLimit()) throw new Error("Query.limitToFirst: Limit was already set (by another call to limit, limitToFirst, or limitToLast).");
return new e(this.repo, this.path, this.queryParams_.limitToFirst(t), this.orderByCalled_);
};
e.prototype.limitToLast = function(t) {
f.validateArgCount("Query.limitToLast", 1, 1, arguments.length);
if ("number" != typeof t || Math.floor(t) !== t || t <= 0) throw new Error("Query.limitToLast: First argument must be a positive integer.");
if (this.queryParams_.hasLimit()) throw new Error("Query.limitToLast: Limit was already set (by another call to limit, limitToFirst, or limitToLast).");
return new e(this.repo, this.path, this.queryParams_.limitToLast(t), this.orderByCalled_);
};
e.prototype.orderByChild = function(t) {
f.validateArgCount("Query.orderByChild", 1, 1, arguments.length);
if ("$key" === t) throw new Error('Query.orderByChild: "$key" is invalid.  Use Query.orderByKey() instead.');
if ("$priority" === t) throw new Error('Query.orderByChild: "$priority" is invalid.  Use Query.orderByPriority() instead.');
if ("$value" === t) throw new Error('Query.orderByChild: "$value" is invalid.  Use Query.orderByValue() instead.');
h.validatePathString("Query.orderByChild", 1, t, !1);
this.validateNoPreviousOrderByCall_("Query.orderByChild");
var n = new l.Path(t);
if (n.isEmpty()) throw new Error("Query.orderByChild: cannot pass in empty path.  Use Query.orderByValue() instead.");
var r = new u.PathIndex(n), i = this.queryParams_.orderBy(r);
e.validateQueryEndpoints_(i);
return new e(this.repo, this.path, i, !0);
};
e.prototype.orderByKey = function() {
f.validateArgCount("Query.orderByKey", 0, 0, arguments.length);
this.validateNoPreviousOrderByCall_("Query.orderByKey");
var t = this.queryParams_.orderBy(o.KEY_INDEX);
e.validateQueryEndpoints_(t);
return new e(this.repo, this.path, t, !0);
};
e.prototype.orderByPriority = function() {
f.validateArgCount("Query.orderByPriority", 0, 0, arguments.length);
this.validateNoPreviousOrderByCall_("Query.orderByPriority");
var t = this.queryParams_.orderBy(a.PRIORITY_INDEX);
e.validateQueryEndpoints_(t);
return new e(this.repo, this.path, t, !0);
};
e.prototype.orderByValue = function() {
f.validateArgCount("Query.orderByValue", 0, 0, arguments.length);
this.validateNoPreviousOrderByCall_("Query.orderByValue");
var t = this.queryParams_.orderBy(s.VALUE_INDEX);
e.validateQueryEndpoints_(t);
return new e(this.repo, this.path, t, !0);
};
e.prototype.startAt = function(t, n) {
void 0 === t && (t = null);
f.validateArgCount("Query.startAt", 0, 2, arguments.length);
h.validateFirebaseDataArg("Query.startAt", 1, t, this.path, !0);
h.validateKey("Query.startAt", 2, n, !0);
var r = this.queryParams_.startAt(t, n);
e.validateLimit_(r);
e.validateQueryEndpoints_(r);
if (this.queryParams_.hasStart()) throw new Error("Query.startAt: Starting point was already set (by another call to startAt or equalTo).");
if (void 0 === t) {
t = null;
n = null;
}
return new e(this.repo, this.path, r, this.orderByCalled_);
};
e.prototype.endAt = function(t, n) {
void 0 === t && (t = null);
f.validateArgCount("Query.endAt", 0, 2, arguments.length);
h.validateFirebaseDataArg("Query.endAt", 1, t, this.path, !0);
h.validateKey("Query.endAt", 2, n, !0);
var r = this.queryParams_.endAt(t, n);
e.validateLimit_(r);
e.validateQueryEndpoints_(r);
if (this.queryParams_.hasEnd()) throw new Error("Query.endAt: Ending point was already set (by another call to endAt or equalTo).");
return new e(this.repo, this.path, r, this.orderByCalled_);
};
e.prototype.equalTo = function(e, t) {
f.validateArgCount("Query.equalTo", 1, 2, arguments.length);
h.validateFirebaseDataArg("Query.equalTo", 1, e, this.path, !1);
h.validateKey("Query.equalTo", 2, t, !0);
if (this.queryParams_.hasStart()) throw new Error("Query.equalTo: Starting point was already set (by another call to startAt or equalTo).");
if (this.queryParams_.hasEnd()) throw new Error("Query.equalTo: Ending point was already set (by another call to endAt or equalTo).");
return this.startAt(e, t).endAt(e, t);
};
e.prototype.toString = function() {
f.validateArgCount("Query.toString", 0, 0, arguments.length);
return this.repo.toString() + this.path.toUrlEncodedString();
};
e.prototype.toJSON = function() {
f.validateArgCount("Query.toJSON", 0, 1, arguments.length);
return this.toString();
};
e.prototype.queryObject = function() {
return this.queryParams_.getQueryObject();
};
e.prototype.queryIdentifier = function() {
var e = this.queryObject(), t = c.ObjectToUniqueKey(e);
return "{}" === t ? "default" : t;
};
e.prototype.isEqual = function(t) {
f.validateArgCount("Query.isEqual", 1, 1, arguments.length);
if (!(t instanceof e)) {
throw new Error("Query.isEqual failed: First argument must be an instance of firebase.database.Query.");
}
var n = this.repo === t.repo, r = this.path.equals(t.path), i = this.queryIdentifier() === t.queryIdentifier();
return n && r && i;
};
e.getCancelAndContextArgs_ = function(e, t, n) {
var r = {
cancel: null,
context: null
};
if (t && n) {
r.cancel = t;
f.validateCallback(e, 3, r.cancel, !0);
r.context = n;
f.validateContextObject(e, 4, r.context, !0);
} else if (t) if ("object" == typeof t && null !== t) r.context = t; else {
if ("function" != typeof t) throw new Error(f.errorPrefix(e, 3, !0) + " must either be a cancel callback or a context object.");
r.cancel = t;
}
return r;
};
Object.defineProperty(e.prototype, "ref", {
get: function() {
return this.getRef();
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.Query = v;
}, {
"../core/snap/indexes/KeyIndex": 40,
"../core/snap/indexes/PathIndex": 41,
"../core/snap/indexes/PriorityIndex": 42,
"../core/snap/indexes/ValueIndex": 43,
"../core/util/Path": 58,
"../core/util/util": 64,
"../core/util/validation": 65,
"../core/view/EventRegistration": 73,
"@firebase/util": 135
} ],
9: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./onDisconnect"), o = e("./TransactionResult"), a = e("../core/util/util"), s = e("../core/util/NextPushId"), u = e("./Query"), c = e("../core/Repo"), l = e("../core/util/Path"), h = e("../core/view/QueryParams"), f = e("../core/util/validation"), p = e("@firebase/util"), d = e("@firebase/util"), v = e("../core/SyncPoint"), _ = function(e) {
function t(t, n) {
if (!(t instanceof c.Repo)) throw new Error("new Reference() no longer supported - use app.database().");
return e.call(this, t, n, h.QueryParams.DEFAULT, !1) || this;
}
r(t, e);
t.prototype.getKey = function() {
p.validateArgCount("Reference.key", 0, 0, arguments.length);
return this.path.isEmpty() ? null : this.path.getBack();
};
t.prototype.child = function(e) {
p.validateArgCount("Reference.child", 1, 1, arguments.length);
"number" == typeof e ? e = String(e) : e instanceof l.Path || (null === this.path.getFront() ? f.validateRootPathString("Reference.child", 1, e, !1) : f.validatePathString("Reference.child", 1, e, !1));
return new t(this.repo, this.path.child(e));
};
t.prototype.getParent = function() {
p.validateArgCount("Reference.parent", 0, 0, arguments.length);
var e = this.path.parent();
return null === e ? null : new t(this.repo, e);
};
t.prototype.getRoot = function() {
p.validateArgCount("Reference.root", 0, 0, arguments.length);
for (var e = this; null !== e.getParent(); ) e = e.getParent();
return e;
};
t.prototype.databaseProp = function() {
return this.repo.database;
};
t.prototype.set = function(e, t) {
p.validateArgCount("Reference.set", 1, 2, arguments.length);
f.validateWritablePath("Reference.set", this.path);
f.validateFirebaseDataArg("Reference.set", 1, e, this.path, !1);
p.validateCallback("Reference.set", 2, t, !0);
var n = new d.Deferred();
this.repo.setWithPriority(this.path, e, null, n.wrapCallback(t));
return n.promise;
};
t.prototype.update = function(e, t) {
p.validateArgCount("Reference.update", 1, 2, arguments.length);
f.validateWritablePath("Reference.update", this.path);
if (Array.isArray(e)) {
for (var n = {}, r = 0; r < e.length; ++r) n["" + r] = e[r];
e = n;
a.warn("Passing an Array to Firebase.update() is deprecated. Use set() if you want to overwrite the existing data, or an Object with integer keys if you really do want to only update some of the children.");
}
f.validateFirebaseMergeDataArg("Reference.update", 1, e, this.path, !1);
p.validateCallback("Reference.update", 2, t, !0);
var i = new d.Deferred();
this.repo.update(this.path, e, i.wrapCallback(t));
return i.promise;
};
t.prototype.setWithPriority = function(e, t, n) {
p.validateArgCount("Reference.setWithPriority", 2, 3, arguments.length);
f.validateWritablePath("Reference.setWithPriority", this.path);
f.validateFirebaseDataArg("Reference.setWithPriority", 1, e, this.path, !1);
f.validatePriority("Reference.setWithPriority", 2, t, !1);
p.validateCallback("Reference.setWithPriority", 3, n, !0);
if (".length" === this.getKey() || ".keys" === this.getKey()) throw "Reference.setWithPriority failed: " + this.getKey() + " is a read-only object.";
var r = new d.Deferred();
this.repo.setWithPriority(this.path, e, t, r.wrapCallback(n));
return r.promise;
};
t.prototype.remove = function(e) {
p.validateArgCount("Reference.remove", 0, 1, arguments.length);
f.validateWritablePath("Reference.remove", this.path);
p.validateCallback("Reference.remove", 1, e, !0);
return this.set(null, e);
};
t.prototype.transaction = function(e, t, n) {
p.validateArgCount("Reference.transaction", 1, 3, arguments.length);
f.validateWritablePath("Reference.transaction", this.path);
p.validateCallback("Reference.transaction", 1, e, !1);
p.validateCallback("Reference.transaction", 2, t, !0);
f.validateBoolean("Reference.transaction", 3, n, !0);
if (".length" === this.getKey() || ".keys" === this.getKey()) throw "Reference.transaction failed: " + this.getKey() + " is a read-only object.";
void 0 === n && (n = !0);
var r = new d.Deferred();
"function" == typeof t && r.promise.catch(function() {});
this.repo.startTransaction(this.path, e, function(e, n, i) {
e ? r.reject(e) : r.resolve(new o.TransactionResult(n, i));
"function" == typeof t && t(e, n, i);
}, n);
return r.promise;
};
t.prototype.setPriority = function(e, t) {
p.validateArgCount("Reference.setPriority", 1, 2, arguments.length);
f.validateWritablePath("Reference.setPriority", this.path);
f.validatePriority("Reference.setPriority", 1, e, !1);
p.validateCallback("Reference.setPriority", 2, t, !0);
var n = new d.Deferred();
this.repo.setWithPriority(this.path.child(".priority"), e, null, n.wrapCallback(t));
return n.promise;
};
t.prototype.push = function(e, t) {
p.validateArgCount("Reference.push", 0, 2, arguments.length);
f.validateWritablePath("Reference.push", this.path);
f.validateFirebaseDataArg("Reference.push", 1, e, this.path, !0);
p.validateCallback("Reference.push", 2, t, !0);
var n, r = this.repo.serverTime(), i = s.nextPushId(r), o = this.child(i), a = this.child(i);
n = null != e ? o.set(e, t).then(function() {
return a;
}) : Promise.resolve(a);
o.then = n.then.bind(n);
o.catch = n.then.bind(n, void 0);
"function" == typeof t && n.catch(function() {});
return o;
};
t.prototype.onDisconnect = function() {
f.validateWritablePath("Reference.onDisconnect", this.path);
return new i.OnDisconnect(this.repo, this.path);
};
Object.defineProperty(t.prototype, "database", {
get: function() {
return this.databaseProp();
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(t.prototype, "key", {
get: function() {
return this.getKey();
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(t.prototype, "parent", {
get: function() {
return this.getParent();
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(t.prototype, "root", {
get: function() {
return this.getRoot();
},
enumerable: !0,
configurable: !0
});
return t;
}(u.Query);
n.Reference = _;
u.Query.__referenceConstructor = _;
v.SyncPoint.__referenceConstructor = _;
}, {
"../core/Repo": 18,
"../core/SyncPoint": 25,
"../core/util/NextPushId": 56,
"../core/util/Path": 58,
"../core/util/util": 64,
"../core/util/validation": 65,
"../core/view/QueryParams": 74,
"./Query": 8,
"./TransactionResult": 10,
"./onDisconnect": 12,
"@firebase/util": 135
} ],
10: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e(e, t) {
this.committed = e;
this.snapshot = t;
}
e.prototype.toJSON = function() {
r.validateArgCount("TransactionResult.toJSON", 0, 1, arguments.length);
return {
committed: this.committed,
snapshot: this.snapshot.toJSON()
};
};
return e;
}();
n.TransactionResult = i;
}, {
"@firebase/util": 135
} ],
11: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../realtime/WebSocketConnection"), i = e("../realtime/BrowserPollConnection");
n.forceLongPolling = function() {
r.WebSocketConnection.forceDisallow();
i.BrowserPollConnection.forceAllow();
};
n.forceWebSockets = function() {
i.BrowserPollConnection.forceDisallow();
};
n.isWebSocketsAvailable = function() {
return r.WebSocketConnection.isAvailable();
};
n.setSecurityDebugCallback = function(e, t) {
e.repo.persistentConnection_.securityDebugCallback_ = t;
};
n.stats = function(e, t) {
e.repo.stats(t);
};
n.statsIncrementCounter = function(e, t) {
e.repo.statsIncrementCounter(t);
};
n.dataUpdateCount = function(e) {
return e.repo.dataUpdateCount;
};
n.interceptServerData = function(e, t) {
return e.repo.interceptServerData_(t);
};
}, {
"../realtime/BrowserPollConnection": 81,
"../realtime/WebSocketConnection": 85
} ],
12: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../core/util/validation"), o = e("../core/util/util"), a = e("@firebase/util"), s = function() {
function e(e, t) {
this.repo_ = e;
this.path_ = t;
}
e.prototype.cancel = function(e) {
r.validateArgCount("OnDisconnect.cancel", 0, 1, arguments.length);
r.validateCallback("OnDisconnect.cancel", 1, e, !0);
var t = new a.Deferred();
this.repo_.onDisconnectCancel(this.path_, t.wrapCallback(e));
return t.promise;
};
e.prototype.remove = function(e) {
r.validateArgCount("OnDisconnect.remove", 0, 1, arguments.length);
i.validateWritablePath("OnDisconnect.remove", this.path_);
r.validateCallback("OnDisconnect.remove", 1, e, !0);
var t = new a.Deferred();
this.repo_.onDisconnectSet(this.path_, null, t.wrapCallback(e));
return t.promise;
};
e.prototype.set = function(e, t) {
r.validateArgCount("OnDisconnect.set", 1, 2, arguments.length);
i.validateWritablePath("OnDisconnect.set", this.path_);
i.validateFirebaseDataArg("OnDisconnect.set", 1, e, this.path_, !1);
r.validateCallback("OnDisconnect.set", 2, t, !0);
var n = new a.Deferred();
this.repo_.onDisconnectSet(this.path_, e, n.wrapCallback(t));
return n.promise;
};
e.prototype.setWithPriority = function(e, t, n) {
r.validateArgCount("OnDisconnect.setWithPriority", 2, 3, arguments.length);
i.validateWritablePath("OnDisconnect.setWithPriority", this.path_);
i.validateFirebaseDataArg("OnDisconnect.setWithPriority", 1, e, this.path_, !1);
i.validatePriority("OnDisconnect.setWithPriority", 2, t, !1);
r.validateCallback("OnDisconnect.setWithPriority", 3, n, !0);
var o = new a.Deferred();
this.repo_.onDisconnectSetWithPriority(this.path_, e, t, o.wrapCallback(n));
return o.promise;
};
e.prototype.update = function(e, t) {
r.validateArgCount("OnDisconnect.update", 1, 2, arguments.length);
i.validateWritablePath("OnDisconnect.update", this.path_);
if (Array.isArray(e)) {
for (var n = {}, s = 0; s < e.length; ++s) n["" + s] = e[s];
e = n;
o.warn("Passing an Array to firebase.database.onDisconnect().update() is deprecated. Use set() if you want to overwrite the existing data, or an Object with integer keys if you really do want to only update some of the children.");
}
i.validateFirebaseMergeDataArg("OnDisconnect.update", 1, e, this.path_, !1);
r.validateCallback("OnDisconnect.update", 2, t, !0);
var u = new a.Deferred();
this.repo_.onDisconnectUpdate(this.path_, e, u.wrapCallback(t));
return u.promise;
};
return e;
}();
n.OnDisconnect = s;
}, {
"../core/util/util": 64,
"../core/util/validation": 65,
"@firebase/util": 135
} ],
13: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../core/RepoInfo"), i = e("../core/PersistentConnection"), o = e("../core/RepoManager"), a = e("../realtime/Connection");
n.DataConnection = i.PersistentConnection;
i.PersistentConnection.prototype.simpleListen = function(e, t) {
this.sendRequest("q", {
p: e
}, t);
};
i.PersistentConnection.prototype.echo = function(e, t) {
this.sendRequest("echo", {
d: e
}, t);
};
n.RealTimeConnection = a.Connection;
n.hijackHash = function(e) {
var t = i.PersistentConnection.prototype.put;
i.PersistentConnection.prototype.put = function(n, r, i, o) {
void 0 !== o && (o = e());
t.call(this, n, r, i, o);
};
return function() {
i.PersistentConnection.prototype.put = t;
};
};
n.ConnectionTarget = r.RepoInfo;
n.queryIdentifier = function(e) {
return e.queryIdentifier();
};
n.listens = function(e) {
return e.repo.persistentConnection_.listens_;
};
n.forceRestClient = function(e) {
o.RepoManager.getInstance().forceRestClient(e);
};
}, {
"../core/PersistentConnection": 16,
"../core/RepoInfo": 19,
"../core/RepoManager": 20,
"../realtime/Connection": 82
} ],
14: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./util/util"), i = function() {
function e(e) {
this.app_ = e;
}
e.prototype.getToken = function(e) {
return this.app_.INTERNAL.getToken(e).then(null, function(e) {
if (e && "auth/token-not-initialized" === e.code) {
r.log("Got auth/token-not-initialized error.  Treating as null token.");
return null;
}
return Promise.reject(e);
});
};
e.prototype.addTokenChangeListener = function(e) {
this.app_.INTERNAL.addAuthTokenListener(e);
};
e.prototype.removeTokenChangeListener = function(e) {
this.app_.INTERNAL.removeAuthTokenListener(e);
};
e.prototype.notifyForInvalidToken = function() {
var e = 'Provided authentication credentials for the app named "' + this.app_.name + '" are invalid. This usually indicates your app was not initialized correctly. ';
"credential" in this.app_.options ? e += 'Make sure the "credential" property provided to initializeApp() is authorized to access the specified "databaseURL" and is from the correct project.' : "serviceAccount" in this.app_.options ? e += 'Make sure the "serviceAccount" property provided to initializeApp() is authorized to access the specified "databaseURL" and is from the correct project.' : e += 'Make sure the "apiKey" and "databaseURL" properties provided to initializeApp() match the values provided for your app at https://console.firebase.google.com/.';
r.warn(e);
};
return e;
}();
n.AuthTokenProvider = i;
}, {
"./util/util": 64
} ],
15: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./util/ImmutableTree"), i = e("./util/Path"), o = e("@firebase/util"), a = e("./snap/Node"), s = e("./snap/indexes/PriorityIndex"), u = e("@firebase/util"), c = function() {
function e(e) {
this.writeTree_ = e;
}
e.prototype.addWrite = function(t, n) {
if (t.isEmpty()) return new e(new r.ImmutableTree(n));
var o = this.writeTree_.findRootMostValueAndPath(t);
if (null != o) {
var a = o.path, s = o.value, u = i.Path.relativePath(a, t);
s = s.updateChild(u, n);
return new e(this.writeTree_.set(a, s));
}
var c = new r.ImmutableTree(n);
return new e(this.writeTree_.setTree(t, c));
};
e.prototype.addWrites = function(e, t) {
var n = this;
o.forEach(t, function(t, r) {
n = n.addWrite(e.child(t), r);
});
return n;
};
e.prototype.removeWrite = function(t) {
return t.isEmpty() ? e.Empty : new e(this.writeTree_.setTree(t, r.ImmutableTree.Empty));
};
e.prototype.hasCompleteWrite = function(e) {
return null != this.getCompleteNode(e);
};
e.prototype.getCompleteNode = function(e) {
var t = this.writeTree_.findRootMostValueAndPath(e);
return null != t ? this.writeTree_.get(t.path).getChild(i.Path.relativePath(t.path, e)) : null;
};
e.prototype.getCompleteChildren = function() {
var e = [], t = this.writeTree_.value;
null != t ? t.isLeafNode() || t.forEachChild(s.PRIORITY_INDEX, function(t, n) {
e.push(new a.NamedNode(t, n));
}) : this.writeTree_.children.inorderTraversal(function(t, n) {
null != n.value && e.push(new a.NamedNode(t, n.value));
});
return e;
};
e.prototype.childCompoundWrite = function(t) {
if (t.isEmpty()) return this;
var n = this.getCompleteNode(t);
return new e(null != n ? new r.ImmutableTree(n) : this.writeTree_.subtree(t));
};
e.prototype.isEmpty = function() {
return this.writeTree_.isEmpty();
};
e.prototype.apply = function(t) {
return e.applySubtreeWrite_(i.Path.Empty, this.writeTree_, t);
};
e.Empty = new e(new r.ImmutableTree(null));
e.applySubtreeWrite_ = function(t, n, r) {
if (null != n.value) return r.updateChild(t, n.value);
var i = null;
n.children.inorderTraversal(function(n, o) {
if (".priority" === n) {
u.assert(null !== o.value, "Priority writes must always be leaf nodes");
i = o.value;
} else r = e.applySubtreeWrite_(t.child(n), o, r);
});
r.getChild(t).isEmpty() || null === i || (r = r.updateChild(t.child(".priority"), i));
return r;
};
return e;
}();
n.CompoundWrite = c;
}, {
"./snap/Node": 36,
"./snap/indexes/PriorityIndex": 42,
"./util/ImmutableTree": 55,
"./util/Path": 58,
"@firebase/util": 135
} ],
16: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("@firebase/app"), o = e("@firebase/util"), a = e("@firebase/util"), s = e("@firebase/util"), u = e("./util/util"), c = e("./util/Path"), l = e("./util/VisibilityMonitor"), h = e("./util/OnlineMonitor"), f = e("@firebase/util"), p = e("../realtime/Connection"), d = e("@firebase/util"), v = e("@firebase/util"), _ = 1e3, y = 3e5, g = function(e) {
function t(n, r, i, o, a, s) {
var c = e.call(this) || this;
c.repoInfo_ = n;
c.onDataUpdate_ = r;
c.onConnectStatus_ = i;
c.onServerInfoUpdate_ = o;
c.authTokenProvider_ = a;
c.authOverride_ = s;
c.id = t.nextPersistentConnectionId_++;
c.log_ = u.logWrapper("p:" + c.id + ":");
c.interruptReasons_ = {};
c.listens_ = {};
c.outstandingPuts_ = [];
c.outstandingPutCount_ = 0;
c.onDisconnectRequestQueue_ = [];
c.connected_ = !1;
c.reconnectDelay_ = _;
c.maxReconnectDelay_ = y;
c.securityDebugCallback_ = null;
c.lastSessionId = null;
c.establishConnectionTimer_ = null;
c.visible_ = !1;
c.requestCBHash_ = {};
c.requestNumber_ = 0;
c.realtime_ = null;
c.authToken_ = null;
c.forceTokenRefresh_ = !1;
c.invalidAuthTokenCount_ = 0;
c.firstConnection_ = !0;
c.lastConnectionAttemptTime_ = null;
c.lastConnectionEstablishedTime_ = null;
if (s && !v.isNodeSdk()) throw new Error("Auth override specified in options, but not supported on non Node.js platforms");
c.scheduleConnect_(0);
l.VisibilityMonitor.getInstance().on("visible", c.onVisible_, c);
-1 === n.host.indexOf("fblocal") && h.OnlineMonitor.getInstance().on("online", c.onOnline_, c);
return c;
}
r(t, e);
t.prototype.sendRequest = function(e, t, n) {
var r = ++this.requestNumber_, i = {
r: r,
a: e,
b: t
};
this.log_(a.stringify(i));
s.assert(this.connected_, "sendRequest call when we're not connected not allowed.");
this.realtime_.sendRequest(i);
n && (this.requestCBHash_[r] = n);
};
t.prototype.listen = function(e, t, n, r) {
var i = e.queryIdentifier(), o = e.path.toString();
this.log_("Listen called for " + o + " " + i);
this.listens_[o] = this.listens_[o] || {};
s.assert(e.getQueryParams().isDefault() || !e.getQueryParams().loadsAllData(), "listen() called for non-default but complete query");
s.assert(!this.listens_[o][i], "listen() called twice for same path/queryId.");
var a = {
onComplete: r,
hashFn: t,
query: e,
tag: n
};
this.listens_[o][i] = a;
this.connected_ && this.sendListen_(a);
};
t.prototype.sendListen_ = function(e) {
var n = this, r = e.query, i = r.path.toString(), o = r.queryIdentifier();
this.log_("Listen on " + i + " for " + o);
var a = {
p: i
};
if (e.tag) {
a.q = r.queryObject();
a.t = e.tag;
}
a.h = e.hashFn();
this.sendRequest("q", a, function(a) {
var s = a.d, u = a.s;
t.warnOnListenWarnings_(s, r);
if ((n.listens_[i] && n.listens_[i][o]) === e) {
n.log_("listen response", a);
"ok" !== u && n.removeListen_(i, o);
e.onComplete && e.onComplete(u, s);
}
});
};
t.warnOnListenWarnings_ = function(e, t) {
if (e && "object" == typeof e && o.contains(e, "w")) {
var n = o.safeGet(e, "w");
if (Array.isArray(n) && ~n.indexOf("no_index")) {
var r = '".indexOn": "' + t.getQueryParams().getIndex().toString() + '"', i = t.path.toString();
u.warn("Using an unspecified index. Your data will be downloaded and filtered on the client. Consider adding " + r + " at " + i + " to your security rules for better performance.");
}
}
};
t.prototype.refreshAuthToken = function(e) {
this.authToken_ = e;
this.log_("Auth token refreshed");
this.authToken_ ? this.tryAuth() : this.connected_ && this.sendRequest("unauth", {}, function() {});
this.reduceReconnectDelayIfAdminCredential_(e);
};
t.prototype.reduceReconnectDelayIfAdminCredential_ = function(e) {
if (e && 40 === e.length || f.isAdmin(e)) {
this.log_("Admin auth credential detected.  Reducing max reconnect time.");
this.maxReconnectDelay_ = 3e4;
}
};
t.prototype.tryAuth = function() {
var e = this;
if (this.connected_ && this.authToken_) {
var t = this.authToken_, n = f.isValidFormat(t) ? "auth" : "gauth", r = {
cred: t
};
null === this.authOverride_ ? r.noauth = !0 : "object" == typeof this.authOverride_ && (r.authvar = this.authOverride_);
this.sendRequest(n, r, function(n) {
var r = n.s, i = n.d || "error";
e.authToken_ === t && ("ok" === r ? e.invalidAuthTokenCount_ = 0 : e.onAuthRevoked_(r, i));
});
}
};
t.prototype.unlisten = function(e, t) {
var n = e.path.toString(), r = e.queryIdentifier();
this.log_("Unlisten called for " + n + " " + r);
s.assert(e.getQueryParams().isDefault() || !e.getQueryParams().loadsAllData(), "unlisten() called for non-default but complete query");
this.removeListen_(n, r) && this.connected_ && this.sendUnlisten_(n, r, e.queryObject(), t);
};
t.prototype.sendUnlisten_ = function(e, t, n, r) {
this.log_("Unlisten on " + e + " for " + t);
var i = {
p: e
};
if (r) {
i.q = n;
i.t = r;
}
this.sendRequest("n", i);
};
t.prototype.onDisconnectPut = function(e, t, n) {
this.connected_ ? this.sendOnDisconnect_("o", e, t, n) : this.onDisconnectRequestQueue_.push({
pathString: e,
action: "o",
data: t,
onComplete: n
});
};
t.prototype.onDisconnectMerge = function(e, t, n) {
this.connected_ ? this.sendOnDisconnect_("om", e, t, n) : this.onDisconnectRequestQueue_.push({
pathString: e,
action: "om",
data: t,
onComplete: n
});
};
t.prototype.onDisconnectCancel = function(e, t) {
this.connected_ ? this.sendOnDisconnect_("oc", e, null, t) : this.onDisconnectRequestQueue_.push({
pathString: e,
action: "oc",
data: null,
onComplete: t
});
};
t.prototype.sendOnDisconnect_ = function(e, t, n, r) {
var i = {
p: t,
d: n
};
this.log_("onDisconnect " + e, i);
this.sendRequest(e, i, function(e) {
r && setTimeout(function() {
r(e.s, e.d);
}, Math.floor(0));
});
};
t.prototype.put = function(e, t, n, r) {
this.putInternal("p", e, t, n, r);
};
t.prototype.merge = function(e, t, n, r) {
this.putInternal("m", e, t, n, r);
};
t.prototype.putInternal = function(e, t, n, r, i) {
var o = {
p: t,
d: n
};
void 0 !== i && (o.h = i);
this.outstandingPuts_.push({
action: e,
request: o,
onComplete: r
});
this.outstandingPutCount_++;
var a = this.outstandingPuts_.length - 1;
this.connected_ ? this.sendPut_(a) : this.log_("Buffering put: " + t);
};
t.prototype.sendPut_ = function(e) {
var t = this, n = this.outstandingPuts_[e].action, r = this.outstandingPuts_[e].request, i = this.outstandingPuts_[e].onComplete;
this.outstandingPuts_[e].queued = this.connected_;
this.sendRequest(n, r, function(r) {
t.log_(n + " response", r);
delete t.outstandingPuts_[e];
t.outstandingPutCount_--;
0 === t.outstandingPutCount_ && (t.outstandingPuts_ = []);
i && i(r.s, r.d);
});
};
t.prototype.reportStats = function(e) {
var t = this;
if (this.connected_) {
var n = {
c: e
};
this.log_("reportStats", n);
this.sendRequest("s", n, function(e) {
if ("ok" !== e.s) {
var n = e.d;
t.log_("reportStats", "Error sending stats: " + n);
}
});
}
};
t.prototype.onDataMessage_ = function(e) {
if ("r" in e) {
this.log_("from server: " + a.stringify(e));
var t = e.r, n = this.requestCBHash_[t];
if (n) {
delete this.requestCBHash_[t];
n(e.b);
}
} else {
if ("error" in e) throw "A server-side error has occurred: " + e.error;
"a" in e && this.onDataPush_(e.a, e.b);
}
};
t.prototype.onDataPush_ = function(e, t) {
this.log_("handleServerMessage", e, t);
"d" === e ? this.onDataUpdate_(t.p, t.d, !1, t.t) : "m" === e ? this.onDataUpdate_(t.p, t.d, !0, t.t) : "c" === e ? this.onListenRevoked_(t.p, t.q) : "ac" === e ? this.onAuthRevoked_(t.s, t.d) : "sd" === e ? this.onSecurityDebugPacket_(t) : u.error("Unrecognized action received from server: " + a.stringify(e) + "\nAre you using the latest client?");
};
t.prototype.onReady_ = function(e, t) {
this.log_("connection ready");
this.connected_ = !0;
this.lastConnectionEstablishedTime_ = new Date().getTime();
this.handleTimestamp_(e);
this.lastSessionId = t;
this.firstConnection_ && this.sendConnectStats_();
this.restoreState_();
this.firstConnection_ = !1;
this.onConnectStatus_(!0);
};
t.prototype.scheduleConnect_ = function(e) {
var t = this;
s.assert(!this.realtime_, "Scheduling a connect when we're already connected/ing?");
this.establishConnectionTimer_ && clearTimeout(this.establishConnectionTimer_);
this.establishConnectionTimer_ = setTimeout(function() {
t.establishConnectionTimer_ = null;
t.establishConnection_();
}, Math.floor(e));
};
t.prototype.onVisible_ = function(e) {
if (e && !this.visible_ && this.reconnectDelay_ === this.maxReconnectDelay_) {
this.log_("Window became visible.  Reducing delay.");
this.reconnectDelay_ = _;
this.realtime_ || this.scheduleConnect_(0);
}
this.visible_ = e;
};
t.prototype.onOnline_ = function(e) {
if (e) {
this.log_("Browser went online.");
this.reconnectDelay_ = _;
this.realtime_ || this.scheduleConnect_(0);
} else {
this.log_("Browser went offline.  Killing connection.");
this.realtime_ && this.realtime_.close();
}
};
t.prototype.onRealtimeDisconnect_ = function() {
this.log_("data client disconnected");
this.connected_ = !1;
this.realtime_ = null;
this.cancelSentTransactions_();
this.requestCBHash_ = {};
if (this.shouldReconnect_()) {
if (this.visible_) {
if (this.lastConnectionEstablishedTime_) {
new Date().getTime() - this.lastConnectionEstablishedTime_ > 3e4 && (this.reconnectDelay_ = _);
this.lastConnectionEstablishedTime_ = null;
}
} else {
this.log_("Window isn't visible.  Delaying reconnect.");
this.reconnectDelay_ = this.maxReconnectDelay_;
this.lastConnectionAttemptTime_ = new Date().getTime();
}
var e = new Date().getTime() - this.lastConnectionAttemptTime_, t = Math.max(0, this.reconnectDelay_ - e);
t = Math.random() * t;
this.log_("Trying to reconnect in " + t + "ms");
this.scheduleConnect_(t);
this.reconnectDelay_ = Math.min(this.maxReconnectDelay_, 1.3 * this.reconnectDelay_);
}
this.onConnectStatus_(!1);
};
t.prototype.establishConnection_ = function() {
if (this.shouldReconnect_()) {
this.log_("Making a connection attempt");
this.lastConnectionAttemptTime_ = new Date().getTime();
this.lastConnectionEstablishedTime_ = null;
var e = this.onDataMessage_.bind(this), n = this.onReady_.bind(this), r = this.onRealtimeDisconnect_.bind(this), i = this.id + ":" + t.nextConnectionId_++, o = this, a = this.lastSessionId, c = !1, l = null, h = function() {
if (l) l.close(); else {
c = !0;
r();
}
};
this.realtime_ = {
close: h,
sendRequest: function(e) {
s.assert(l, "sendRequest call when we're not connected not allowed.");
l.sendRequest(e);
}
};
var f = this.forceTokenRefresh_;
this.forceTokenRefresh_ = !1;
this.authTokenProvider_.getToken(f).then(function(t) {
if (c) u.log("getToken() completed but was canceled"); else {
u.log("getToken() completed. Creating connection.");
o.authToken_ = t && t.accessToken;
l = new p.Connection(i, o.repoInfo_, e, n, r, function(e) {
u.warn(e + " (" + o.repoInfo_.toString() + ")");
o.interrupt("server_kill");
}, a);
}
}).then(null, function(e) {
o.log_("Failed to get token: " + e);
if (!c) {
d.CONSTANTS.NODE_ADMIN && u.warn(e);
h();
}
});
}
};
t.prototype.interrupt = function(e) {
u.log("Interrupting connection for reason: " + e);
this.interruptReasons_[e] = !0;
if (this.realtime_) this.realtime_.close(); else {
if (this.establishConnectionTimer_) {
clearTimeout(this.establishConnectionTimer_);
this.establishConnectionTimer_ = null;
}
this.connected_ && this.onRealtimeDisconnect_();
}
};
t.prototype.resume = function(e) {
u.log("Resuming connection for reason: " + e);
delete this.interruptReasons_[e];
if (o.isEmpty(this.interruptReasons_)) {
this.reconnectDelay_ = _;
this.realtime_ || this.scheduleConnect_(0);
}
};
t.prototype.handleTimestamp_ = function(e) {
var t = e - new Date().getTime();
this.onServerInfoUpdate_({
serverTimeOffset: t
});
};
t.prototype.cancelSentTransactions_ = function() {
for (var e = 0; e < this.outstandingPuts_.length; e++) {
var t = this.outstandingPuts_[e];
if (t && "h" in t.request && t.queued) {
t.onComplete && t.onComplete("disconnect");
delete this.outstandingPuts_[e];
this.outstandingPutCount_--;
}
}
0 === this.outstandingPutCount_ && (this.outstandingPuts_ = []);
};
t.prototype.onListenRevoked_ = function(e, t) {
var n;
n = t ? t.map(function(e) {
return u.ObjectToUniqueKey(e);
}).join("$") : "default";
var r = this.removeListen_(e, n);
r && r.onComplete && r.onComplete("permission_denied");
};
t.prototype.removeListen_ = function(e, t) {
var n, r = new c.Path(e).toString();
if (void 0 !== this.listens_[r]) {
n = this.listens_[r][t];
delete this.listens_[r][t];
0 === o.getCount(this.listens_[r]) && delete this.listens_[r];
} else n = void 0;
return n;
};
t.prototype.onAuthRevoked_ = function(e, t) {
u.log("Auth token revoked: " + e + "/" + t);
this.authToken_ = null;
this.forceTokenRefresh_ = !0;
this.realtime_.close();
if (("invalid_token" === e || "permission_denied" === e) && ++this.invalidAuthTokenCount_ >= 3) {
this.reconnectDelay_ = 3e4;
this.authTokenProvider_.notifyForInvalidToken();
}
};
t.prototype.onSecurityDebugPacket_ = function(e) {
this.securityDebugCallback_ ? this.securityDebugCallback_(e) : "msg" in e && "undefined" != typeof console && console.log("FIREBASE: " + e.msg.replace("\n", "\nFIREBASE: "));
};
t.prototype.restoreState_ = function() {
var e = this;
this.tryAuth();
o.forEach(this.listens_, function(t, n) {
o.forEach(n, function(t, n) {
e.sendListen_(n);
});
});
for (var t = 0; t < this.outstandingPuts_.length; t++) this.outstandingPuts_[t] && this.sendPut_(t);
for (;this.onDisconnectRequestQueue_.length; ) {
var n = this.onDisconnectRequestQueue_.shift();
this.sendOnDisconnect_(n.action, n.pathString, n.data, n.onComplete);
}
};
t.prototype.sendConnectStats_ = function() {
var e = {}, t = "js";
d.CONSTANTS.NODE_ADMIN ? t = "admin_node" : d.CONSTANTS.NODE_CLIENT && (t = "node");
e["sdk." + t + "." + i.default.SDK_VERSION.replace(/\./g, "-")] = 1;
v.isMobileCordova() ? e["framework.cordova"] = 1 : v.isReactNative() && (e["framework.reactnative"] = 1);
this.reportStats(e);
};
t.prototype.shouldReconnect_ = function() {
var e = h.OnlineMonitor.getInstance().currentlyOnline();
return o.isEmpty(this.interruptReasons_) && e;
};
t.nextPersistentConnectionId_ = 0;
t.nextConnectionId_ = 0;
return t;
}(e("./ServerActions").ServerActions);
n.PersistentConnection = g;
}, {
"../realtime/Connection": 82,
"./ServerActions": 22,
"./util/OnlineMonitor": 57,
"./util/Path": 58,
"./util/VisibilityMonitor": 62,
"./util/util": 64,
"@firebase/app": 2,
"@firebase/util": 135
} ],
17: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("@firebase/util"), o = e("./util/util"), a = e("@firebase/util"), s = e("@firebase/util"), u = e("@firebase/util"), c = function(e) {
function t(t, n, r) {
var i = e.call(this) || this;
i.repoInfo_ = t;
i.onDataUpdate_ = n;
i.authTokenProvider_ = r;
i.log_ = o.logWrapper("p:rest:");
i.listens_ = {};
return i;
}
r(t, e);
t.prototype.reportStats = function(e) {
throw new Error("Method not implemented.");
};
t.getListenId_ = function(e, t) {
if (void 0 !== t) return "tag$" + t;
i.assert(e.getQueryParams().isDefault(), "should have a tag if it's not a default query.");
return e.path.toString();
};
t.prototype.listen = function(e, n, r, i) {
var o = this, a = e.path.toString();
this.log_("Listen called for " + a + " " + e.queryIdentifier());
var u = t.getListenId_(e, r), c = {};
this.listens_[u] = c;
var l = e.getQueryParams().toRestQueryStringParameters();
this.restRequest_(a + ".json", l, function(e, t) {
var n = t;
if (404 === e) {
n = null;
e = null;
}
null === e && o.onDataUpdate_(a, n, !1, r);
if (s.safeGet(o.listens_, u) === c) {
i(e ? 401 == e ? "permission_denied" : "rest_error:" + e : "ok", null);
}
});
};
t.prototype.unlisten = function(e, n) {
var r = t.getListenId_(e, n);
delete this.listens_[r];
};
t.prototype.refreshAuthToken = function(e) {};
t.prototype.restRequest_ = function(e, t, n) {
var r = this;
void 0 === t && (t = {});
t.format = "export";
this.authTokenProvider_.getToken(!1).then(function(i) {
var s = i && i.accessToken;
s && (t.auth = s);
var c = (r.repoInfo_.secure ? "https://" : "http://") + r.repoInfo_.host + e + "?" + u.querystring(t);
r.log_("Sending REST request for " + c);
var l = new XMLHttpRequest();
l.onreadystatechange = function() {
if (n && 4 === l.readyState) {
r.log_("REST Response for " + c + " received. status:", l.status, "response:", l.responseText);
var e = null;
if (l.status >= 200 && l.status < 300) {
try {
e = a.jsonEval(l.responseText);
} catch (e) {
o.warn("Failed to parse JSON response for " + c + ": " + l.responseText);
}
n(null, e);
} else {
401 !== l.status && 404 !== l.status && o.warn("Got unsuccessful REST response for " + c + " Status: " + l.status);
n(l.status);
}
n = null;
}
};
l.open("GET", c, !0);
l.send();
});
};
return t;
}(e("./ServerActions").ServerActions);
n.ReadonlyRestClient = c;
}, {
"./ServerActions": 22,
"./util/util": 64,
"@firebase/util": 135
} ],
18: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./util/ServerValues"), i = e("./snap/nodeFromJSON"), o = e("./util/Path"), a = e("./SparseSnapshotTree"), s = e("./SyncTree"), u = e("./SnapshotHolder"), c = e("@firebase/util"), l = e("./util/util"), h = e("@firebase/util"), f = e("./AuthTokenProvider"), p = e("./stats/StatsManager"), d = e("./stats/StatsReporter"), v = e("./stats/StatsListener"), _ = e("./view/EventQueue"), y = e("./PersistentConnection"), g = e("./ReadonlyRestClient"), m = e("../api/Database"), b = function() {
function e(e, t, n) {
var r = this;
this.repoInfo_ = e;
this.app = n;
this.dataUpdateCount = 0;
this.statsListener_ = null;
this.eventQueue_ = new _.EventQueue();
this.nextWriteId_ = 1;
this.interceptServerDataCallback_ = null;
this.onDisconnect_ = new a.SparseSnapshotTree();
this.persistentConnection_ = null;
var i = new f.AuthTokenProvider(n);
this.stats_ = p.StatsManager.getCollection(e);
if (t || l.beingCrawled()) {
this.server_ = new g.ReadonlyRestClient(this.repoInfo_, this.onDataUpdate_.bind(this), i);
setTimeout(this.onConnectStatus_.bind(this, !0), 0);
} else {
var o = n.options.databaseAuthVariableOverride;
if ("undefined" != typeof o && null !== o) {
if ("object" != typeof o) throw new Error("Only objects are supported for option databaseAuthVariableOverride");
try {
c.stringify(o);
} catch (e) {
throw new Error("Invalid authOverride provided: " + e);
}
}
this.persistentConnection_ = new y.PersistentConnection(this.repoInfo_, this.onDataUpdate_.bind(this), this.onConnectStatus_.bind(this), this.onServerInfoUpdate_.bind(this), i, o);
this.server_ = this.persistentConnection_;
}
i.addTokenChangeListener(function(e) {
r.server_.refreshAuthToken(e);
});
this.statsReporter_ = p.StatsManager.getOrCreateReporter(e, function() {
return new d.StatsReporter(r.stats_, r.server_);
});
this.transactions_init_();
this.infoData_ = new u.SnapshotHolder();
this.infoSyncTree_ = new s.SyncTree({
startListening: function(e, t, n, i) {
var o = [], a = r.infoData_.getNode(e.path);
if (!a.isEmpty()) {
o = r.infoSyncTree_.applyServerOverwrite(e.path, a);
setTimeout(function() {
i("ok");
}, 0);
}
return o;
},
stopListening: function() {}
});
this.updateInfo_("connected", !1);
this.serverSyncTree_ = new s.SyncTree({
startListening: function(e, t, n, i) {
r.server_.listen(e, n, t, function(t, n) {
var o = i(t, n);
r.eventQueue_.raiseEventsForChangedPath(e.path, o);
});
return [];
},
stopListening: function(e, t) {
r.server_.unlisten(e, t);
}
});
}
e.prototype.toString = function() {
return (this.repoInfo_.secure ? "https://" : "http://") + this.repoInfo_.host;
};
e.prototype.name = function() {
return this.repoInfo_.namespace;
};
e.prototype.serverTime = function() {
var e = this.infoData_.getNode(new o.Path(".info/serverTimeOffset")).val() || 0;
return new Date().getTime() + e;
};
e.prototype.generateServerValues = function() {
return r.generateWithValues({
timestamp: this.serverTime()
});
};
e.prototype.onDataUpdate_ = function(e, t, n, r) {
this.dataUpdateCount++;
var a = new o.Path(e);
t = this.interceptServerDataCallback_ ? this.interceptServerDataCallback_(e, t) : t;
var s = [];
if (r) if (n) {
var u = h.map(t, function(e) {
return i.nodeFromJSON(e);
});
s = this.serverSyncTree_.applyTaggedQueryMerge(a, u, r);
} else {
var c = i.nodeFromJSON(t);
s = this.serverSyncTree_.applyTaggedQueryOverwrite(a, c, r);
} else if (n) {
var l = h.map(t, function(e) {
return i.nodeFromJSON(e);
});
s = this.serverSyncTree_.applyServerMerge(a, l);
} else {
var f = i.nodeFromJSON(t);
s = this.serverSyncTree_.applyServerOverwrite(a, f);
}
var p = a;
s.length > 0 && (p = this.rerunTransactions_(a));
this.eventQueue_.raiseEventsForChangedPath(p, s);
};
e.prototype.interceptServerData_ = function(e) {
this.interceptServerDataCallback_ = e;
};
e.prototype.onConnectStatus_ = function(e) {
this.updateInfo_("connected", e);
!1 === e && this.runOnDisconnectEvents_();
};
e.prototype.onServerInfoUpdate_ = function(e) {
var t = this;
l.each(e, function(e, n) {
t.updateInfo_(n, e);
});
};
e.prototype.updateInfo_ = function(e, t) {
var n = new o.Path("/.info/" + e), r = i.nodeFromJSON(t);
this.infoData_.updateSnapshot(n, r);
var a = this.infoSyncTree_.applyServerOverwrite(n, r);
this.eventQueue_.raiseEventsForChangedPath(n, a);
};
e.prototype.getNextWriteId_ = function() {
return this.nextWriteId_++;
};
e.prototype.setWithPriority = function(e, t, n, o) {
var a = this;
this.log_("set", {
path: e.toString(),
value: t,
priority: n
});
var s = this.generateServerValues(), u = i.nodeFromJSON(t, n), c = r.resolveDeferredValueSnapshot(u, s), h = this.getNextWriteId_(), f = this.serverSyncTree_.applyUserOverwrite(e, c, h, !0);
this.eventQueue_.queueEvents(f);
this.server_.put(e.toString(), u.val(!0), function(t, n) {
var r = "ok" === t;
r || l.warn("set at " + e + " failed: " + t);
var i = a.serverSyncTree_.ackUserWrite(h, !r);
a.eventQueue_.raiseEventsForChangedPath(e, i);
a.callOnCompleteCallback(o, t, n);
});
var p = this.abortTransactions_(e);
this.rerunTransactions_(p);
this.eventQueue_.raiseEventsForChangedPath(p, []);
};
e.prototype.update = function(e, t, n) {
var o = this;
this.log_("update", {
path: e.toString(),
value: t
});
var a = !0, s = this.generateServerValues(), u = {};
h.forEach(t, function(e, t) {
a = !1;
var n = i.nodeFromJSON(t);
u[e] = r.resolveDeferredValueSnapshot(n, s);
});
if (a) {
l.log("update() called with empty data.  Don't do anything.");
this.callOnCompleteCallback(n, "ok");
} else {
var c = this.getNextWriteId_(), f = this.serverSyncTree_.applyUserMerge(e, u, c);
this.eventQueue_.queueEvents(f);
this.server_.merge(e.toString(), t, function(t, r) {
var i = "ok" === t;
i || l.warn("update at " + e + " failed: " + t);
var a = o.serverSyncTree_.ackUserWrite(c, !i), s = a.length > 0 ? o.rerunTransactions_(e) : e;
o.eventQueue_.raiseEventsForChangedPath(s, a);
o.callOnCompleteCallback(n, t, r);
});
h.forEach(t, function(t) {
var n = o.abortTransactions_(e.child(t));
o.rerunTransactions_(n);
});
this.eventQueue_.raiseEventsForChangedPath(e, []);
}
};
e.prototype.runOnDisconnectEvents_ = function() {
var e = this;
this.log_("onDisconnectEvents");
var t = this.generateServerValues(), n = [];
r.resolveDeferredValueTree(this.onDisconnect_, t).forEachTree(o.Path.Empty, function(t, r) {
n = n.concat(e.serverSyncTree_.applyServerOverwrite(t, r));
var i = e.abortTransactions_(t);
e.rerunTransactions_(i);
});
this.onDisconnect_ = new a.SparseSnapshotTree();
this.eventQueue_.raiseEventsForChangedPath(o.Path.Empty, n);
};
e.prototype.onDisconnectCancel = function(e, t) {
var n = this;
this.server_.onDisconnectCancel(e.toString(), function(r, i) {
"ok" === r && n.onDisconnect_.forget(e);
n.callOnCompleteCallback(t, r, i);
});
};
e.prototype.onDisconnectSet = function(e, t, n) {
var r = this, o = i.nodeFromJSON(t);
this.server_.onDisconnectPut(e.toString(), o.val(!0), function(t, i) {
"ok" === t && r.onDisconnect_.remember(e, o);
r.callOnCompleteCallback(n, t, i);
});
};
e.prototype.onDisconnectSetWithPriority = function(e, t, n, r) {
var o = this, a = i.nodeFromJSON(t, n);
this.server_.onDisconnectPut(e.toString(), a.val(!0), function(t, n) {
"ok" === t && o.onDisconnect_.remember(e, a);
o.callOnCompleteCallback(r, t, n);
});
};
e.prototype.onDisconnectUpdate = function(e, t, n) {
var r = this;
if (h.isEmpty(t)) {
l.log("onDisconnect().update() called with empty data.  Don't do anything.");
this.callOnCompleteCallback(n, "ok");
} else this.server_.onDisconnectMerge(e.toString(), t, function(o, a) {
"ok" === o && h.forEach(t, function(t, n) {
var o = i.nodeFromJSON(n);
r.onDisconnect_.remember(e.child(t), o);
});
r.callOnCompleteCallback(n, o, a);
});
};
e.prototype.addEventCallbackForQuery = function(e, t) {
var n;
n = ".info" === e.path.getFront() ? this.infoSyncTree_.addEventRegistration(e, t) : this.serverSyncTree_.addEventRegistration(e, t);
this.eventQueue_.raiseEventsAtPath(e.path, n);
};
e.prototype.removeEventCallbackForQuery = function(e, t) {
var n;
n = ".info" === e.path.getFront() ? this.infoSyncTree_.removeEventRegistration(e, t) : this.serverSyncTree_.removeEventRegistration(e, t);
this.eventQueue_.raiseEventsAtPath(e.path, n);
};
e.prototype.interrupt = function() {
this.persistentConnection_ && this.persistentConnection_.interrupt("repo_interrupt");
};
e.prototype.resume = function() {
this.persistentConnection_ && this.persistentConnection_.resume("repo_interrupt");
};
e.prototype.stats = function(e) {
void 0 === e && (e = !1);
if ("undefined" != typeof console) {
var t;
if (e) {
this.statsListener_ || (this.statsListener_ = new v.StatsListener(this.stats_));
t = this.statsListener_.get();
} else t = this.stats_.get();
var n = Object.keys(t).reduce(function(e, t) {
return Math.max(t.length, e);
}, 0);
h.forEach(t, function(e, t) {
for (var r = e.length; r < n + 2; r++) e += " ";
console.log(e + t);
});
}
};
e.prototype.statsIncrementCounter = function(e) {
this.stats_.incrementCounter(e);
this.statsReporter_.includeStat(e);
};
e.prototype.log_ = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
var n = "";
this.persistentConnection_ && (n = this.persistentConnection_.id + ":");
l.log.apply(void 0, [ n ].concat(e));
};
e.prototype.callOnCompleteCallback = function(e, t, n) {
e && l.exceptionGuard(function() {
if ("ok" == t) e(null); else {
var r = (t || "error").toUpperCase(), i = r;
n && (i += ": " + n);
var o = new Error(i);
o.code = r;
e(o);
}
});
};
Object.defineProperty(e.prototype, "database", {
get: function() {
return this.__database || (this.__database = new m.Database(this));
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.Repo = b;
}, {
"../api/Database": 7,
"./AuthTokenProvider": 14,
"./PersistentConnection": 16,
"./ReadonlyRestClient": 17,
"./SnapshotHolder": 23,
"./SparseSnapshotTree": 24,
"./SyncTree": 26,
"./snap/nodeFromJSON": 44,
"./stats/StatsListener": 47,
"./stats/StatsManager": 48,
"./stats/StatsReporter": 49,
"./util/Path": 58,
"./util/ServerValues": 59,
"./util/util": 64,
"./view/EventQueue": 72,
"@firebase/util": 135
} ],
19: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("@firebase/util"), o = e("./storage/storage"), a = e("../realtime/Constants"), s = function() {
function e(e, t, n, r, i) {
void 0 === i && (i = "");
this.secure = t;
this.namespace = n;
this.webSocketOnly = r;
this.persistenceKey = i;
this.host = e.toLowerCase();
this.domain = this.host.substr(this.host.indexOf(".") + 1);
this.internalHost = o.PersistentStorage.get("host:" + e) || this.host;
}
e.prototype.needsQueryParam = function() {
return this.host !== this.internalHost;
};
e.prototype.isCacheableHost = function() {
return "s-" === this.internalHost.substr(0, 2);
};
e.prototype.isDemoHost = function() {
return "firebaseio-demo.com" === this.domain;
};
e.prototype.isCustomHost = function() {
return "firebaseio.com" !== this.domain && "firebaseio-demo.com" !== this.domain;
};
e.prototype.updateHost = function(e) {
if (e !== this.internalHost) {
this.internalHost = e;
this.isCacheableHost() && o.PersistentStorage.set("host:" + this.host, this.internalHost);
}
};
e.prototype.connectionURL = function(e, t) {
r.assert("string" == typeof e, "typeof type must == string");
r.assert("object" == typeof t, "typeof params must == object");
var n;
if (e === a.WEBSOCKET) n = (this.secure ? "wss://" : "ws://") + this.internalHost + "/.ws?"; else {
if (e !== a.LONG_POLLING) throw new Error("Unknown connection type: " + e);
n = (this.secure ? "https://" : "http://") + this.internalHost + "/.lp?";
}
this.needsQueryParam() && (t.ns = this.namespace);
var o = [];
i.forEach(t, function(e, t) {
o.push(e + "=" + t);
});
return n + o.join("&");
};
e.prototype.toString = function() {
var e = this.toURLString();
this.persistenceKey && (e += "<" + this.persistenceKey + ">");
return e;
};
e.prototype.toURLString = function() {
return (this.secure ? "https://" : "http://") + this.host;
};
return e;
}();
n.RepoInfo = s;
}, {
"../realtime/Constants": 83,
"./storage/storage": 52,
"@firebase/util": 135
} ],
20: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./Repo"), o = e("./util/util"), a = e("./util/libs/parser"), s = e("./util/validation");
e("./Repo_transaction");
var u, c = function() {
function e() {
this.repos_ = {};
this.useRestClient_ = !1;
}
e.getInstance = function() {
u || (u = new e());
return u;
};
e.prototype.interrupt = function() {
for (var e in this.repos_) for (var t in this.repos_[e]) this.repos_[e][t].interrupt();
};
e.prototype.resume = function() {
for (var e in this.repos_) for (var t in this.repos_[e]) this.repos_[e][t].resume();
};
e.prototype.databaseFromApp = function(e, t) {
var n = t || e.options.databaseURL;
void 0 === n && o.fatal("Can't determine Firebase Database URL.  Be sure to include databaseURL option when calling firebase.intializeApp().");
var r = a.parseRepoInfo(n), i = r.repoInfo;
s.validateUrl("Invalid Firebase Database URL", 1, r);
r.path.isEmpty() || o.fatal("Database URL must point to the root of a Firebase Database (not including a child path).");
return this.createRepo(i, e).database;
};
e.prototype.deleteRepo = function(e) {
var t = r.safeGet(this.repos_, e.app.name);
t && r.safeGet(t, e.repoInfo_.toURLString()) === e || o.fatal("Database " + e.app.name + "(" + e.repoInfo_ + ") has already been deleted.");
e.interrupt();
delete t[e.repoInfo_.toURLString()];
};
e.prototype.createRepo = function(e, t) {
var n = r.safeGet(this.repos_, t.name);
if (!n) {
n = {};
this.repos_[t.name] = n;
}
var a = r.safeGet(n, e.toURLString());
a && o.fatal("Database initialized multiple times. Please make sure the format of the database URL matches with each database() call.");
a = new i.Repo(e, this.useRestClient_, t);
n[e.toURLString()] = a;
return a;
};
e.prototype.forceRestClient = function(e) {
this.useRestClient_ = e;
};
return e;
}();
n.RepoManager = c;
}, {
"./Repo": 18,
"./Repo_transaction": 21,
"./util/libs/parser": 63,
"./util/util": 64,
"./util/validation": 65,
"@firebase/util": 135
} ],
21: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("@firebase/util"), o = e("../api/Reference"), a = e("../api/DataSnapshot"), s = e("./util/Path"), u = e("./util/Tree"), c = e("./snap/indexes/PriorityIndex"), l = e("./util/util"), h = e("./util/ServerValues"), f = e("./util/validation"), p = e("@firebase/util"), d = e("./snap/nodeFromJSON"), v = e("./snap/ChildrenNode"), _ = e("./Repo");
!function(e) {
e[e.RUN = 0] = "RUN";
e[e.SENT = 1] = "SENT";
e[e.COMPLETED = 2] = "COMPLETED";
e[e.SENT_NEEDS_ABORT = 3] = "SENT_NEEDS_ABORT";
e[e.NEEDS_ABORT = 4] = "NEEDS_ABORT";
}(r = n.TransactionStatus || (n.TransactionStatus = {}));
_.Repo.MAX_TRANSACTION_RETRIES_ = 25;
_.Repo.prototype.transactions_init_ = function() {
this.transactionQueueTree_ = new u.Tree();
};
_.Repo.prototype.startTransaction = function(e, t, n, s) {
this.log_("transaction on " + e);
var u = function() {}, _ = new o.Reference(this, e);
_.on("value", u);
var y = {
path: e,
update: t,
onComplete: n,
status: null,
order: l.LUIDGenerator(),
applyLocally: s,
retryCount: 0,
unwatcher: function() {
_.off("value", u);
},
abortReason: null,
currentWriteId: null,
currentInputSnapshot: null,
currentOutputSnapshotRaw: null,
currentOutputSnapshotResolved: null
}, g = this.getLatestState_(e);
y.currentInputSnapshot = g;
var m = y.update(g.val());
if (void 0 === m) {
y.unwatcher();
y.currentOutputSnapshotRaw = null;
y.currentOutputSnapshotResolved = null;
if (y.onComplete) {
var b = new a.DataSnapshot(y.currentInputSnapshot, new o.Reference(this, y.path), c.PRIORITY_INDEX);
y.onComplete(null, !1, b);
}
} else {
f.validateFirebaseData("transaction failed: Data returned ", m, y.path);
y.status = r.RUN;
var E = this.transactionQueueTree_.subTree(e), w = E.getValue() || [];
w.push(y);
E.setValue(w);
var C = void 0;
if ("object" == typeof m && null !== m && p.contains(m, ".priority")) {
C = p.safeGet(m, ".priority");
i.assert(f.isValidPriority(C), "Invalid priority returned by transaction. Priority must be a valid string, finite number, server value, or null.");
} else C = (this.serverSyncTree_.calcCompleteEventCache(e) || v.ChildrenNode.EMPTY_NODE).getPriority().val();
C = C;
var S = this.generateServerValues(), T = d.nodeFromJSON(m, C), N = h.resolveDeferredValueSnapshot(T, S);
y.currentOutputSnapshotRaw = T;
y.currentOutputSnapshotResolved = N;
y.currentWriteId = this.getNextWriteId_();
var I = this.serverSyncTree_.applyUserOverwrite(e, N, y.currentWriteId, y.applyLocally);
this.eventQueue_.raiseEventsForChangedPath(e, I);
this.sendReadyTransactions_();
}
};
_.Repo.prototype.getLatestState_ = function(e, t) {
return this.serverSyncTree_.calcCompleteEventCache(e, t) || v.ChildrenNode.EMPTY_NODE;
};
_.Repo.prototype.sendReadyTransactions_ = function(e) {
var t = this;
void 0 === e && (e = this.transactionQueueTree_);
e || this.pruneCompletedTransactionsBelowNode_(e);
if (null !== e.getValue()) {
var n = this.buildTransactionQueue_(e);
i.assert(n.length > 0, "Sending zero length transaction queue");
n.every(function(e) {
return e.status === r.RUN;
}) && this.sendTransactionQueue_(e.path(), n);
} else e.hasChildren() && e.forEachChild(function(e) {
t.sendReadyTransactions_(e);
});
};
_.Repo.prototype.sendTransactionQueue_ = function(e, t) {
for (var n = this, u = t.map(function(e) {
return e.currentWriteId;
}), h = this.getLatestState_(e, u), f = h, p = h.hash(), d = 0; d < t.length; d++) {
var v = t[d];
i.assert(v.status === r.RUN, "tryToSendTransactionQueue_: items in queue should all be run.");
v.status = r.SENT;
v.retryCount++;
var _ = s.Path.relativePath(e, v.path);
f = f.updateChild(_, v.currentOutputSnapshotRaw);
}
var y = f.val(!0), g = e;
this.server_.put(g.toString(), y, function(i) {
n.log_("transaction put response", {
path: g.toString(),
status: i
});
var s = [];
if ("ok" === i) {
for (var u = [], h = 0; h < t.length; h++) {
t[h].status = r.COMPLETED;
s = s.concat(n.serverSyncTree_.ackUserWrite(t[h].currentWriteId));
if (t[h].onComplete) {
var f = t[h].currentOutputSnapshotResolved, p = new o.Reference(n, t[h].path), d = new a.DataSnapshot(f, p, c.PRIORITY_INDEX);
u.push(t[h].onComplete.bind(null, null, !0, d));
}
t[h].unwatcher();
}
n.pruneCompletedTransactionsBelowNode_(n.transactionQueueTree_.subTree(e));
n.sendReadyTransactions_();
n.eventQueue_.raiseEventsForChangedPath(e, s);
for (h = 0; h < u.length; h++) l.exceptionGuard(u[h]);
} else {
if ("datastale" === i) for (h = 0; h < t.length; h++) t[h].status === r.SENT_NEEDS_ABORT ? t[h].status = r.NEEDS_ABORT : t[h].status = r.RUN; else {
l.warn("transaction at " + g.toString() + " failed: " + i);
for (h = 0; h < t.length; h++) {
t[h].status = r.NEEDS_ABORT;
t[h].abortReason = i;
}
}
n.rerunTransactions_(e);
}
}, p);
};
_.Repo.prototype.rerunTransactions_ = function(e) {
var t = this.getAncestorTransactionNode_(e), n = t.path(), r = this.buildTransactionQueue_(t);
this.rerunTransactionQueue_(r, n);
return n;
};
_.Repo.prototype.rerunTransactionQueue_ = function(e, t) {
if (0 !== e.length) {
for (var n = [], u = [], v = e.filter(function(e) {
return e.status === r.RUN;
}).map(function(e) {
return e.currentWriteId;
}), y = 0; y < e.length; y++) {
var g = e[y], m = s.Path.relativePath(t, g.path), b = !1, E = void 0;
i.assert(null !== m, "rerunTransactionsUnderNode_: relativePath should not be null.");
if (g.status === r.NEEDS_ABORT) {
b = !0;
E = g.abortReason;
u = u.concat(this.serverSyncTree_.ackUserWrite(g.currentWriteId, !0));
} else if (g.status === r.RUN) if (g.retryCount >= _.Repo.MAX_TRANSACTION_RETRIES_) {
b = !0;
E = "maxretry";
u = u.concat(this.serverSyncTree_.ackUserWrite(g.currentWriteId, !0));
} else {
var w = this.getLatestState_(g.path, v);
g.currentInputSnapshot = w;
var C = e[y].update(w.val());
if (void 0 !== C) {
f.validateFirebaseData("transaction failed: Data returned ", C, g.path);
var S = d.nodeFromJSON(C);
"object" == typeof C && null != C && p.contains(C, ".priority") || (S = S.updatePriority(w.getPriority()));
var T = g.currentWriteId, N = this.generateServerValues(), I = h.resolveDeferredValueSnapshot(S, N);
g.currentOutputSnapshotRaw = S;
g.currentOutputSnapshotResolved = I;
g.currentWriteId = this.getNextWriteId_();
v.splice(v.indexOf(T), 1);
u = (u = u.concat(this.serverSyncTree_.applyUserOverwrite(g.path, I, g.currentWriteId, g.applyLocally))).concat(this.serverSyncTree_.ackUserWrite(T, !0));
} else {
b = !0;
E = "nodata";
u = u.concat(this.serverSyncTree_.ackUserWrite(g.currentWriteId, !0));
}
}
this.eventQueue_.raiseEventsForChangedPath(t, u);
u = [];
if (b) {
e[y].status = r.COMPLETED;
!function(e) {
setTimeout(e, Math.floor(0));
}(e[y].unwatcher);
if (e[y].onComplete) if ("nodata" === E) {
var P = new o.Reference(this, e[y].path), O = e[y].currentInputSnapshot, R = new a.DataSnapshot(O, P, c.PRIORITY_INDEX);
n.push(e[y].onComplete.bind(null, null, !1, R));
} else n.push(e[y].onComplete.bind(null, new Error(E), !1, null));
}
}
this.pruneCompletedTransactionsBelowNode_(this.transactionQueueTree_);
for (y = 0; y < n.length; y++) l.exceptionGuard(n[y]);
this.sendReadyTransactions_();
}
};
_.Repo.prototype.getAncestorTransactionNode_ = function(e) {
for (var t, n = this.transactionQueueTree_; null !== (t = e.getFront()) && null === n.getValue(); ) {
n = n.subTree(t);
e = e.popFront();
}
return n;
};
_.Repo.prototype.buildTransactionQueue_ = function(e) {
var t = [];
this.aggregateTransactionQueuesForNode_(e, t);
t.sort(function(e, t) {
return e.order - t.order;
});
return t;
};
_.Repo.prototype.aggregateTransactionQueuesForNode_ = function(e, t) {
var n = this, r = e.getValue();
if (null !== r) for (var i = 0; i < r.length; i++) t.push(r[i]);
e.forEachChild(function(e) {
n.aggregateTransactionQueuesForNode_(e, t);
});
};
_.Repo.prototype.pruneCompletedTransactionsBelowNode_ = function(e) {
var t = this, n = e.getValue();
if (n) {
for (var i = 0, o = 0; o < n.length; o++) if (n[o].status !== r.COMPLETED) {
n[i] = n[o];
i++;
}
n.length = i;
e.setValue(n.length > 0 ? n : null);
}
e.forEachChild(function(e) {
t.pruneCompletedTransactionsBelowNode_(e);
});
};
_.Repo.prototype.abortTransactions_ = function(e) {
var t = this, n = this.getAncestorTransactionNode_(e).path(), r = this.transactionQueueTree_.subTree(e);
r.forEachAncestor(function(e) {
t.abortTransactionsOnNode_(e);
});
this.abortTransactionsOnNode_(r);
r.forEachDescendant(function(e) {
t.abortTransactionsOnNode_(e);
});
return n;
};
_.Repo.prototype.abortTransactionsOnNode_ = function(e) {
var t = e.getValue();
if (null !== t) {
for (var n = [], o = [], a = -1, s = 0; s < t.length; s++) if (t[s].status === r.SENT_NEEDS_ABORT) ; else if (t[s].status === r.SENT) {
i.assert(a === s - 1, "All SENT items should be at beginning of queue.");
a = s;
t[s].status = r.SENT_NEEDS_ABORT;
t[s].abortReason = "set";
} else {
i.assert(t[s].status === r.RUN, "Unexpected transaction status in abort");
t[s].unwatcher();
o = o.concat(this.serverSyncTree_.ackUserWrite(t[s].currentWriteId, !0));
if (t[s].onComplete) {
n.push(t[s].onComplete.bind(null, new Error("set"), !1, null));
}
}
-1 === a ? e.setValue(null) : t.length = a + 1;
this.eventQueue_.raiseEventsForChangedPath(e.path(), o);
for (s = 0; s < n.length; s++) l.exceptionGuard(n[s]);
}
};
}, {
"../api/DataSnapshot": 6,
"../api/Reference": 9,
"./Repo": 18,
"./snap/ChildrenNode": 33,
"./snap/indexes/PriorityIndex": 42,
"./snap/nodeFromJSON": 44,
"./util/Path": 58,
"./util/ServerValues": 59,
"./util/Tree": 61,
"./util/util": 64,
"./util/validation": 65,
"@firebase/util": 135
} ],
22: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {}
e.prototype.put = function(e, t, n, r) {};
e.prototype.merge = function(e, t, n, r) {};
e.prototype.refreshAuthToken = function(e) {};
e.prototype.onDisconnectPut = function(e, t, n) {};
e.prototype.onDisconnectMerge = function(e, t, n) {};
e.prototype.onDisconnectCancel = function(e, t) {};
e.prototype.reportStats = function(e) {};
return e;
}();
n.ServerActions = r;
}, {} ],
23: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./snap/ChildrenNode"), i = function() {
function e() {
this.rootNode_ = r.ChildrenNode.EMPTY_NODE;
}
e.prototype.getNode = function(e) {
return this.rootNode_.getChild(e);
};
e.prototype.updateSnapshot = function(e, t) {
this.rootNode_ = this.rootNode_.updateChild(e, t);
};
return e;
}();
n.SnapshotHolder = i;
}, {
"./snap/ChildrenNode": 33
} ],
24: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./util/Path"), i = e("./snap/indexes/PriorityIndex"), o = e("./util/CountedSet"), a = function() {
function e() {
this.value_ = null;
this.children_ = null;
}
e.prototype.find = function(e) {
if (null != this.value_) return this.value_.getChild(e);
if (e.isEmpty() || null == this.children_) return null;
var t = e.getFront();
e = e.popFront();
return this.children_.contains(t) ? this.children_.get(t).find(e) : null;
};
e.prototype.remember = function(t, n) {
if (t.isEmpty()) {
this.value_ = n;
this.children_ = null;
} else if (null !== this.value_) this.value_ = this.value_.updateChild(t, n); else {
null == this.children_ && (this.children_ = new o.CountedSet());
var r = t.getFront();
this.children_.contains(r) || this.children_.add(r, new e());
var i = this.children_.get(r);
t = t.popFront();
i.remember(t, n);
}
};
e.prototype.forget = function(e) {
if (e.isEmpty()) {
this.value_ = null;
this.children_ = null;
return !0;
}
if (null !== this.value_) {
if (this.value_.isLeafNode()) return !1;
var t = this.value_;
this.value_ = null;
var n = this;
t.forEachChild(i.PRIORITY_INDEX, function(e, t) {
n.remember(new r.Path(e), t);
});
return this.forget(e);
}
if (null !== this.children_) {
var o = e.getFront();
e = e.popFront();
this.children_.contains(o) && this.children_.get(o).forget(e) && this.children_.remove(o);
if (this.children_.isEmpty()) {
this.children_ = null;
return !0;
}
return !1;
}
return !0;
};
e.prototype.forEachTree = function(e, t) {
null !== this.value_ ? t(e, this.value_) : this.forEachChild(function(n, i) {
var o = new r.Path(e.toString() + "/" + n);
i.forEachTree(o, t);
});
};
e.prototype.forEachChild = function(e) {
null !== this.children_ && this.children_.each(function(t, n) {
e(t, n);
});
};
return e;
}();
n.SparseSnapshotTree = a;
}, {
"./snap/indexes/PriorityIndex": 42,
"./util/CountedSet": 53,
"./util/Path": 58
} ],
25: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("./view/CacheNode"), o = e("./snap/ChildrenNode"), a = e("@firebase/util"), s = e("@firebase/util"), u = e("./view/ViewCache"), c = e("./view/View"), l = function() {
function e() {
this.views_ = {};
}
Object.defineProperty(e, "__referenceConstructor", {
get: function() {
a.assert(r, "Reference.ts has not been loaded");
return r;
},
set: function(e) {
a.assert(!r, "__referenceConstructor has already been defined");
r = e;
},
enumerable: !0,
configurable: !0
});
e.prototype.isEmpty = function() {
return s.isEmpty(this.views_);
};
e.prototype.applyOperation = function(e, t, n) {
var r = e.source.queryId;
if (null !== r) {
var i = s.safeGet(this.views_, r);
a.assert(null != i, "SyncTree gave us an op for an invalid query.");
return i.applyOperation(e, t, n);
}
var o = [];
s.forEach(this.views_, function(r, i) {
o = o.concat(i.applyOperation(e, t, n));
});
return o;
};
e.prototype.addEventRegistration = function(e, t, n, r, a) {
var l = e.queryIdentifier(), h = s.safeGet(this.views_, l);
if (!h) {
var f = n.calcCompleteEventCache(a ? r : null), p = !1;
if (f) p = !0; else if (r instanceof o.ChildrenNode) {
f = n.calcCompleteEventChildren(r);
p = !1;
} else {
f = o.ChildrenNode.EMPTY_NODE;
p = !1;
}
var d = new u.ViewCache(new i.CacheNode(f, p, !1), new i.CacheNode(r, a, !1));
h = new c.View(e, d);
this.views_[l] = h;
}
h.addEventRegistration(t);
return h.getInitialEvents(t);
};
e.prototype.removeEventRegistration = function(t, n, r) {
var i = t.queryIdentifier(), o = [], a = [], u = this.hasCompleteView();
if ("default" === i) {
var c = this;
s.forEach(this.views_, function(e, t) {
a = a.concat(t.removeEventRegistration(n, r));
if (t.isEmpty()) {
delete c.views_[e];
t.getQuery().getQueryParams().loadsAllData() || o.push(t.getQuery());
}
});
} else {
var l = s.safeGet(this.views_, i);
if (l) {
a = a.concat(l.removeEventRegistration(n, r));
if (l.isEmpty()) {
delete this.views_[i];
l.getQuery().getQueryParams().loadsAllData() || o.push(l.getQuery());
}
}
}
u && !this.hasCompleteView() && o.push(new e.__referenceConstructor(t.repo, t.path));
return {
removed: o,
events: a
};
};
e.prototype.getQueryViews = function() {
var e = this;
return Object.keys(this.views_).map(function(t) {
return e.views_[t];
}).filter(function(e) {
return !e.getQuery().getQueryParams().loadsAllData();
});
};
e.prototype.getCompleteServerCache = function(e) {
var t = null;
s.forEach(this.views_, function(n, r) {
t = t || r.getCompleteServerCache(e);
});
return t;
};
e.prototype.viewForQuery = function(e) {
if (e.getQueryParams().loadsAllData()) return this.getCompleteView();
var t = e.queryIdentifier();
return s.safeGet(this.views_, t);
};
e.prototype.viewExistsForQuery = function(e) {
return null != this.viewForQuery(e);
};
e.prototype.hasCompleteView = function() {
return null != this.getCompleteView();
};
e.prototype.getCompleteView = function() {
return s.findValue(this.views_, function(e) {
return e.getQuery().getQueryParams().loadsAllData();
}) || null;
};
return e;
}();
n.SyncPoint = l;
}, {
"./snap/ChildrenNode": 33,
"./view/CacheNode": 66,
"./view/View": 75,
"./view/ViewCache": 76,
"@firebase/util": 135
} ],
26: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./util/util"), o = e("./operation/AckUserWrite"), a = e("./snap/ChildrenNode"), s = e("@firebase/util"), u = e("./util/ImmutableTree"), c = e("./operation/ListenComplete"), l = e("./operation/Merge"), h = e("./operation/Operation"), f = e("./operation/Overwrite"), p = e("./util/Path"), d = e("./SyncPoint"), v = e("./WriteTree"), _ = function() {
function e(e) {
this.listenProvider_ = e;
this.syncPointTree_ = u.ImmutableTree.Empty;
this.pendingWriteTree_ = new v.WriteTree();
this.tagToQueryMap_ = {};
this.queryToTagMap_ = {};
}
e.prototype.applyUserOverwrite = function(e, t, n, r) {
this.pendingWriteTree_.addOverwrite(e, t, n, r);
return r ? this.applyOperationToSyncPoints_(new f.Overwrite(h.OperationSource.User, e, t)) : [];
};
e.prototype.applyUserMerge = function(e, t, n) {
this.pendingWriteTree_.addMerge(e, t, n);
var r = u.ImmutableTree.fromObject(t);
return this.applyOperationToSyncPoints_(new l.Merge(h.OperationSource.User, e, r));
};
e.prototype.ackUserWrite = function(e, t) {
void 0 === t && (t = !1);
var n = this.pendingWriteTree_.getWrite(e);
if (this.pendingWriteTree_.removeWrite(e)) {
var r = u.ImmutableTree.Empty;
null != n.snap ? r = r.set(p.Path.Empty, !0) : s.forEach(n.children, function(e, t) {
r = r.set(new p.Path(e), t);
});
return this.applyOperationToSyncPoints_(new o.AckUserWrite(n.path, r, t));
}
return [];
};
e.prototype.applyServerOverwrite = function(e, t) {
return this.applyOperationToSyncPoints_(new f.Overwrite(h.OperationSource.Server, e, t));
};
e.prototype.applyServerMerge = function(e, t) {
var n = u.ImmutableTree.fromObject(t);
return this.applyOperationToSyncPoints_(new l.Merge(h.OperationSource.Server, e, n));
};
e.prototype.applyListenComplete = function(e) {
return this.applyOperationToSyncPoints_(new c.ListenComplete(h.OperationSource.Server, e));
};
e.prototype.applyTaggedQueryOverwrite = function(t, n, r) {
var i = this.queryKeyForTag_(r);
if (null != i) {
var o = e.parseQueryKey_(i), a = o.path, s = o.queryId, u = p.Path.relativePath(a, t), c = new f.Overwrite(h.OperationSource.forServerTaggedQuery(s), u, n);
return this.applyTaggedOperation_(a, c);
}
return [];
};
e.prototype.applyTaggedQueryMerge = function(t, n, r) {
var i = this.queryKeyForTag_(r);
if (i) {
var o = e.parseQueryKey_(i), a = o.path, s = o.queryId, c = p.Path.relativePath(a, t), f = u.ImmutableTree.fromObject(n), d = new l.Merge(h.OperationSource.forServerTaggedQuery(s), c, f);
return this.applyTaggedOperation_(a, d);
}
return [];
};
e.prototype.applyTaggedListenComplete = function(t, n) {
var r = this.queryKeyForTag_(n);
if (r) {
var i = e.parseQueryKey_(r), o = i.path, a = i.queryId, s = p.Path.relativePath(o, t), u = new c.ListenComplete(h.OperationSource.forServerTaggedQuery(a), s);
return this.applyTaggedOperation_(o, u);
}
return [];
};
e.prototype.addEventRegistration = function(t, n) {
var i = t.path, o = null, s = !1;
this.syncPointTree_.foreachOnPath(i, function(e, t) {
var n = p.Path.relativePath(e, i);
o = o || t.getCompleteServerCache(n);
s = s || t.hasCompleteView();
});
var u = this.syncPointTree_.get(i);
if (u) {
s = s || u.hasCompleteView();
o = o || u.getCompleteServerCache(p.Path.Empty);
} else {
u = new d.SyncPoint();
this.syncPointTree_ = this.syncPointTree_.set(i, u);
}
var c;
if (null != o) c = !0; else {
c = !1;
o = a.ChildrenNode.EMPTY_NODE;
this.syncPointTree_.subtree(i).foreachChild(function(e, t) {
var n = t.getCompleteServerCache(p.Path.Empty);
n && (o = o.updateImmediateChild(e, n));
});
}
var l = u.viewExistsForQuery(t);
if (!l && !t.getQueryParams().loadsAllData()) {
var h = e.makeQueryKey_(t);
r.assert(!(h in this.queryToTagMap_), "View does not exist, but we have a tag");
var f = e.getNextQueryTag_();
this.queryToTagMap_[h] = f;
this.tagToQueryMap_["_" + f] = h;
}
var v = this.pendingWriteTree_.childWrites(i), _ = u.addEventRegistration(t, n, v, o, c);
if (!l && !s) {
var y = u.viewForQuery(t);
_ = _.concat(this.setupListener_(t, y));
}
return _;
};
e.prototype.removeEventRegistration = function(t, n, r) {
var i = this, o = t.path, a = this.syncPointTree_.get(o), s = [];
if (a && ("default" === t.queryIdentifier() || a.viewExistsForQuery(t))) {
var u = a.removeEventRegistration(t, n, r);
a.isEmpty() && (this.syncPointTree_ = this.syncPointTree_.remove(o));
var c = u.removed;
s = u.events;
var l = -1 !== c.findIndex(function(e) {
return e.getQueryParams().loadsAllData();
}), h = this.syncPointTree_.findOnPath(o, function(e, t) {
return t.hasCompleteView();
});
if (l && !h) {
var f = this.syncPointTree_.subtree(o);
if (!f.isEmpty()) for (var p = this.collectDistinctViewsForSubTree_(f), d = 0; d < p.length; ++d) {
var v = p[d], _ = v.getQuery(), y = this.createListenerForView_(v);
this.listenProvider_.startListening(e.queryForListening_(_), this.tagForQuery_(_), y.hashFn, y.onComplete);
}
}
if (!h && c.length > 0 && !r) if (l) {
this.listenProvider_.stopListening(e.queryForListening_(t), null);
} else c.forEach(function(t) {
var n = i.queryToTagMap_[e.makeQueryKey_(t)];
i.listenProvider_.stopListening(e.queryForListening_(t), n);
});
this.removeTags_(c);
}
return s;
};
e.prototype.calcCompleteEventCache = function(e, t) {
var n = this.pendingWriteTree_, r = this.syncPointTree_.findOnPath(e, function(t, n) {
var r = p.Path.relativePath(t, e), i = n.getCompleteServerCache(r);
if (i) return i;
});
return n.calcCompleteEventCache(e, r, t, !0);
};
e.prototype.collectDistinctViewsForSubTree_ = function(e) {
return e.fold(function(e, t, n) {
if (t && t.hasCompleteView()) return [ t.getCompleteView() ];
var r = [];
t && (r = t.getQueryViews());
s.forEach(n, function(e, t) {
r = r.concat(t);
});
return r;
});
};
e.prototype.removeTags_ = function(t) {
for (var n = 0; n < t.length; ++n) {
var r = t[n];
if (!r.getQueryParams().loadsAllData()) {
var i = e.makeQueryKey_(r), o = this.queryToTagMap_[i];
delete this.queryToTagMap_[i];
delete this.tagToQueryMap_["_" + o];
}
}
};
e.queryForListening_ = function(e) {
return e.getQueryParams().loadsAllData() && !e.getQueryParams().isDefault() ? e.getRef() : e;
};
e.prototype.setupListener_ = function(t, n) {
var i = t.path, o = this.tagForQuery_(t), a = this.createListenerForView_(n), u = this.listenProvider_.startListening(e.queryForListening_(t), o, a.hashFn, a.onComplete), c = this.syncPointTree_.subtree(i);
if (o) r.assert(!c.value.hasCompleteView(), "If we're adding a query, it shouldn't be shadowed"); else for (var l = c.fold(function(e, t, n) {
if (!e.isEmpty() && t && t.hasCompleteView()) return [ t.getCompleteView().getQuery() ];
var r = [];
t && (r = r.concat(t.getQueryViews().map(function(e) {
return e.getQuery();
})));
s.forEach(n, function(e, t) {
r = r.concat(t);
});
return r;
}), h = 0; h < l.length; ++h) {
var f = l[h];
this.listenProvider_.stopListening(e.queryForListening_(f), this.tagForQuery_(f));
}
return u;
};
e.prototype.createListenerForView_ = function(e) {
var t = this, n = e.getQuery(), r = this.tagForQuery_(n);
return {
hashFn: function() {
return (e.getServerCache() || a.ChildrenNode.EMPTY_NODE).hash();
},
onComplete: function(e) {
if ("ok" === e) return r ? t.applyTaggedListenComplete(n.path, r) : t.applyListenComplete(n.path);
var o = i.errorForServerCode(e, n);
return t.removeEventRegistration(n, null, o);
}
};
};
e.makeQueryKey_ = function(e) {
return e.path.toString() + "$" + e.queryIdentifier();
};
e.parseQueryKey_ = function(e) {
var t = e.indexOf("$");
r.assert(-1 !== t && t < e.length - 1, "Bad queryKey.");
return {
queryId: e.substr(t + 1),
path: new p.Path(e.substr(0, t))
};
};
e.prototype.queryKeyForTag_ = function(e) {
return this.tagToQueryMap_["_" + e];
};
e.prototype.tagForQuery_ = function(t) {
var n = e.makeQueryKey_(t);
return s.safeGet(this.queryToTagMap_, n);
};
e.getNextQueryTag_ = function() {
return e.nextQueryTag_++;
};
e.prototype.applyTaggedOperation_ = function(e, t) {
var n = this.syncPointTree_.get(e);
r.assert(n, "Missing sync point for query tag that we're tracking");
var i = this.pendingWriteTree_.childWrites(e);
return n.applyOperation(t, i, null);
};
e.prototype.applyOperationToSyncPoints_ = function(e) {
return this.applyOperationHelper_(e, this.syncPointTree_, null, this.pendingWriteTree_.childWrites(p.Path.Empty));
};
e.prototype.applyOperationHelper_ = function(e, t, n, r) {
if (e.path.isEmpty()) return this.applyOperationDescendantsHelper_(e, t, n, r);
var i = t.get(p.Path.Empty);
null == n && null != i && (n = i.getCompleteServerCache(p.Path.Empty));
var o = [], a = e.path.getFront(), s = e.operationForChild(a), u = t.children.get(a);
if (u && s) {
var c = n ? n.getImmediateChild(a) : null, l = r.child(a);
o = o.concat(this.applyOperationHelper_(s, u, c, l));
}
i && (o = o.concat(i.applyOperation(e, r, n)));
return o;
};
e.prototype.applyOperationDescendantsHelper_ = function(e, t, n, r) {
var i = this, o = t.get(p.Path.Empty);
null == n && null != o && (n = o.getCompleteServerCache(p.Path.Empty));
var a = [];
t.children.inorderTraversal(function(t, o) {
var s = n ? n.getImmediateChild(t) : null, u = r.child(t), c = e.operationForChild(t);
c && (a = a.concat(i.applyOperationDescendantsHelper_(c, o, s, u)));
});
o && (a = a.concat(o.applyOperation(e, r, n)));
return a;
};
e.nextQueryTag_ = 1;
return e;
}();
n.SyncTree = _;
}, {
"./SyncPoint": 25,
"./WriteTree": 27,
"./operation/AckUserWrite": 28,
"./operation/ListenComplete": 29,
"./operation/Merge": 30,
"./operation/Operation": 31,
"./operation/Overwrite": 32,
"./snap/ChildrenNode": 33,
"./util/ImmutableTree": 55,
"./util/Path": 58,
"./util/util": 64,
"@firebase/util": 135
} ],
27: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("@firebase/util"), o = e("./util/Path"), a = e("./CompoundWrite"), s = e("./snap/indexes/PriorityIndex"), u = e("./snap/ChildrenNode"), c = function() {
function e() {
this.visibleWrites_ = a.CompoundWrite.Empty;
this.allWrites_ = [];
this.lastWriteId_ = -1;
}
e.prototype.childWrites = function(e) {
return new l(e, this);
};
e.prototype.addOverwrite = function(e, t, n, r) {
i.assert(n > this.lastWriteId_, "Stacking an older write on top of newer ones");
void 0 === r && (r = !0);
this.allWrites_.push({
path: e,
snap: t,
writeId: n,
visible: r
});
r && (this.visibleWrites_ = this.visibleWrites_.addWrite(e, t));
this.lastWriteId_ = n;
};
e.prototype.addMerge = function(e, t, n) {
i.assert(n > this.lastWriteId_, "Stacking an older merge on top of newer ones");
this.allWrites_.push({
path: e,
children: t,
writeId: n,
visible: !0
});
this.visibleWrites_ = this.visibleWrites_.addWrites(e, t);
this.lastWriteId_ = n;
};
e.prototype.getWrite = function(e) {
for (var t = 0; t < this.allWrites_.length; t++) {
var n = this.allWrites_[t];
if (n.writeId === e) return n;
}
return null;
};
e.prototype.removeWrite = function(e) {
var t = this, n = this.allWrites_.findIndex(function(t) {
return t.writeId === e;
});
i.assert(n >= 0, "removeWrite called with nonexistent writeId.");
var o = this.allWrites_[n];
this.allWrites_.splice(n, 1);
for (var a = o.visible, s = !1, u = this.allWrites_.length - 1; a && u >= 0; ) {
var c = this.allWrites_[u];
c.visible && (u >= n && this.recordContainsPath_(c, o.path) ? a = !1 : o.path.contains(c.path) && (s = !0));
u--;
}
if (a) {
if (s) {
this.resetTree_();
return !0;
}
if (o.snap) this.visibleWrites_ = this.visibleWrites_.removeWrite(o.path); else {
var l = o.children;
r.forEach(l, function(e) {
t.visibleWrites_ = t.visibleWrites_.removeWrite(o.path.child(e));
});
}
return !0;
}
return !1;
};
e.prototype.getCompleteWriteData = function(e) {
return this.visibleWrites_.getCompleteNode(e);
};
e.prototype.calcCompleteEventCache = function(t, n, r, i) {
if (r || i) {
var a = this.visibleWrites_.childCompoundWrite(t);
if (!i && a.isEmpty()) return n;
if (i || null != n || a.hasCompleteWrite(o.Path.Empty)) {
var s = e.layerTree_(this.allWrites_, function(e) {
return (e.visible || i) && (!r || !~r.indexOf(e.writeId)) && (e.path.contains(t) || t.contains(e.path));
}, t), c = n || u.ChildrenNode.EMPTY_NODE;
return s.apply(c);
}
return null;
}
var l = this.visibleWrites_.getCompleteNode(t);
if (null != l) return l;
var h = this.visibleWrites_.childCompoundWrite(t);
if (h.isEmpty()) return n;
if (null != n || h.hasCompleteWrite(o.Path.Empty)) {
c = n || u.ChildrenNode.EMPTY_NODE;
return h.apply(c);
}
return null;
};
e.prototype.calcCompleteEventChildren = function(e, t) {
var n = u.ChildrenNode.EMPTY_NODE, r = this.visibleWrites_.getCompleteNode(e);
if (r) {
r.isLeafNode() || r.forEachChild(s.PRIORITY_INDEX, function(e, t) {
n = n.updateImmediateChild(e, t);
});
return n;
}
if (t) {
var i = this.visibleWrites_.childCompoundWrite(e);
t.forEachChild(s.PRIORITY_INDEX, function(e, t) {
var r = i.childCompoundWrite(new o.Path(e)).apply(t);
n = n.updateImmediateChild(e, r);
});
i.getCompleteChildren().forEach(function(e) {
n = n.updateImmediateChild(e.name, e.node);
});
return n;
}
this.visibleWrites_.childCompoundWrite(e).getCompleteChildren().forEach(function(e) {
n = n.updateImmediateChild(e.name, e.node);
});
return n;
};
e.prototype.calcEventCacheAfterServerOverwrite = function(e, t, n, r) {
i.assert(n || r, "Either existingEventSnap or existingServerSnap must exist");
var o = e.child(t);
if (this.visibleWrites_.hasCompleteWrite(o)) return null;
var a = this.visibleWrites_.childCompoundWrite(o);
return a.isEmpty() ? r.getChild(t) : a.apply(r.getChild(t));
};
e.prototype.calcCompleteChild = function(e, t, n) {
var r = e.child(t), i = this.visibleWrites_.getCompleteNode(r);
return null != i ? i : n.isCompleteForChild(t) ? this.visibleWrites_.childCompoundWrite(r).apply(n.getNode().getImmediateChild(t)) : null;
};
e.prototype.shadowingWrite = function(e) {
return this.visibleWrites_.getCompleteNode(e);
};
e.prototype.calcIndexedSlice = function(e, t, n, r, i, a) {
var s, u = this.visibleWrites_.childCompoundWrite(e), c = u.getCompleteNode(o.Path.Empty);
if (null != c) s = c; else {
if (null == t) return [];
s = u.apply(t);
}
if ((s = s.withIndex(a)).isEmpty() || s.isLeafNode()) return [];
for (var l = [], h = a.getCompare(), f = i ? s.getReverseIteratorFrom(n, a) : s.getIteratorFrom(n, a), p = f.getNext(); p && l.length < r; ) {
0 !== h(p, n) && l.push(p);
p = f.getNext();
}
return l;
};
e.prototype.recordContainsPath_ = function(e, t) {
return e.snap ? e.path.contains(t) : !!r.findKey(e.children, function(n, r) {
return e.path.child(r).contains(t);
});
};
e.prototype.resetTree_ = function() {
this.visibleWrites_ = e.layerTree_(this.allWrites_, e.DefaultFilter_, o.Path.Empty);
this.allWrites_.length > 0 ? this.lastWriteId_ = this.allWrites_[this.allWrites_.length - 1].writeId : this.lastWriteId_ = -1;
};
e.DefaultFilter_ = function(e) {
return e.visible;
};
e.layerTree_ = function(e, t, n) {
for (var s = a.CompoundWrite.Empty, u = 0; u < e.length; ++u) {
var c = e[u];
if (t(c)) {
var l = c.path, h = void 0;
if (c.snap) {
if (n.contains(l)) {
h = o.Path.relativePath(n, l);
s = s.addWrite(h, c.snap);
} else if (l.contains(n)) {
h = o.Path.relativePath(l, n);
s = s.addWrite(o.Path.Empty, c.snap.getChild(h));
}
} else {
if (!c.children) throw i.assertionError("WriteRecord should have .snap or .children");
if (n.contains(l)) {
h = o.Path.relativePath(n, l);
s = s.addWrites(h, c.children);
} else if (l.contains(n)) if ((h = o.Path.relativePath(l, n)).isEmpty()) s = s.addWrites(o.Path.Empty, c.children); else {
var f = r.safeGet(c.children, h.getFront());
if (f) {
var p = f.getChild(h.popFront());
s = s.addWrite(o.Path.Empty, p);
}
}
}
}
}
return s;
};
return e;
}();
n.WriteTree = c;
var l = function() {
function e(e, t) {
this.treePath_ = e;
this.writeTree_ = t;
}
e.prototype.calcCompleteEventCache = function(e, t, n) {
return this.writeTree_.calcCompleteEventCache(this.treePath_, e, t, n);
};
e.prototype.calcCompleteEventChildren = function(e) {
return this.writeTree_.calcCompleteEventChildren(this.treePath_, e);
};
e.prototype.calcEventCacheAfterServerOverwrite = function(e, t, n) {
return this.writeTree_.calcEventCacheAfterServerOverwrite(this.treePath_, e, t, n);
};
e.prototype.shadowingWrite = function(e) {
return this.writeTree_.shadowingWrite(this.treePath_.child(e));
};
e.prototype.calcIndexedSlice = function(e, t, n, r, i) {
return this.writeTree_.calcIndexedSlice(this.treePath_, e, t, n, r, i);
};
e.prototype.calcCompleteChild = function(e, t) {
return this.writeTree_.calcCompleteChild(this.treePath_, e, t);
};
e.prototype.child = function(t) {
return new e(this.treePath_.child(t), this.writeTree_);
};
return e;
}();
n.WriteTreeRef = l;
}, {
"./CompoundWrite": 15,
"./snap/ChildrenNode": 33,
"./snap/indexes/PriorityIndex": 42,
"./util/Path": 58,
"@firebase/util": 135
} ],
28: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../util/Path"), o = e("./Operation"), a = function() {
function e(e, t, n) {
this.path = e;
this.affectedTree = t;
this.revert = n;
this.type = o.OperationType.ACK_USER_WRITE;
this.source = o.OperationSource.User;
}
e.prototype.operationForChild = function(t) {
if (this.path.isEmpty()) {
if (null != this.affectedTree.value) {
r.assert(this.affectedTree.children.isEmpty(), "affectedTree should not have overlapping affected paths.");
return this;
}
var n = this.affectedTree.subtree(new i.Path(t));
return new e(i.Path.Empty, n, this.revert);
}
r.assert(this.path.getFront() === t, "operationForChild called for unrelated child.");
return new e(this.path.popFront(), this.affectedTree, this.revert);
};
return e;
}();
n.AckUserWrite = a;
}, {
"../util/Path": 58,
"./Operation": 31,
"@firebase/util": 135
} ],
29: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../util/Path"), i = e("./Operation"), o = function() {
function e(e, t) {
this.source = e;
this.path = t;
this.type = i.OperationType.LISTEN_COMPLETE;
}
e.prototype.operationForChild = function(t) {
return this.path.isEmpty() ? new e(this.source, r.Path.Empty) : new e(this.source, this.path.popFront());
};
return e;
}();
n.ListenComplete = o;
}, {
"../util/Path": 58,
"./Operation": 31
} ],
30: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./Operation"), i = e("./Overwrite"), o = e("../util/Path"), a = e("@firebase/util"), s = function() {
function e(e, t, n) {
this.source = e;
this.path = t;
this.children = n;
this.type = r.OperationType.MERGE;
}
e.prototype.operationForChild = function(t) {
if (this.path.isEmpty()) {
var n = this.children.subtree(new o.Path(t));
return n.isEmpty() ? null : n.value ? new i.Overwrite(this.source, o.Path.Empty, n.value) : new e(this.source, o.Path.Empty, n);
}
a.assert(this.path.getFront() === t, "Can't get a merge for a child not on the path of the operation");
return new e(this.source, this.path.popFront(), this.children);
};
e.prototype.toString = function() {
return "Operation(" + this.path + ": " + this.source.toString() + " merge: " + this.children.toString() + ")";
};
return e;
}();
n.Merge = s;
}, {
"../util/Path": 58,
"./Operation": 31,
"./Overwrite": 32,
"@firebase/util": 135
} ],
31: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util");
!function(e) {
e[e.OVERWRITE = 0] = "OVERWRITE";
e[e.MERGE = 1] = "MERGE";
e[e.ACK_USER_WRITE = 2] = "ACK_USER_WRITE";
e[e.LISTEN_COMPLETE = 3] = "LISTEN_COMPLETE";
}(n.OperationType || (n.OperationType = {}));
var i = function() {
function e(e, t, n, i) {
this.fromUser = e;
this.fromServer = t;
this.queryId = n;
this.tagged = i;
r.assert(!i || t, "Tagged queries must be from server.");
}
e.User = new e(!0, !1, null, !1);
e.Server = new e(!1, !0, null, !1);
e.forServerTaggedQuery = function(t) {
return new e(!1, !0, t, !0);
};
return e;
}();
n.OperationSource = i;
}, {
"@firebase/util": 135
} ],
32: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./Operation"), i = e("../util/Path"), o = function() {
function e(e, t, n) {
this.source = e;
this.path = t;
this.snap = n;
this.type = r.OperationType.OVERWRITE;
}
e.prototype.operationForChild = function(t) {
return this.path.isEmpty() ? new e(this.source, i.Path.Empty, this.snap.getImmediateChild(t)) : new e(this.source, this.path.popFront(), this.snap);
};
return e;
}();
n.Overwrite = o;
}, {
"../util/Path": 58,
"./Operation": 31
} ],
33: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i, o = e("@firebase/util"), a = e("../util/util"), s = e("../util/SortedMap"), u = e("./Node"), c = e("./snap"), l = e("./indexes/PriorityIndex"), h = e("./indexes/KeyIndex"), f = e("./IndexMap"), p = e("./LeafNode"), d = e("./comparators"), v = function() {
function e(e, t, n) {
this.children_ = e;
this.priorityNode_ = t;
this.indexMap_ = n;
this.lazyHash_ = null;
this.priorityNode_ && c.validatePriorityNode(this.priorityNode_);
this.children_.isEmpty() && o.assert(!this.priorityNode_ || this.priorityNode_.isEmpty(), "An empty node cannot have a priority");
}
Object.defineProperty(e, "EMPTY_NODE", {
get: function() {
return i || (i = new e(new s.SortedMap(d.NAME_COMPARATOR), null, f.IndexMap.Default));
},
enumerable: !0,
configurable: !0
});
e.prototype.isLeafNode = function() {
return !1;
};
e.prototype.getPriority = function() {
return this.priorityNode_ || i;
};
e.prototype.updatePriority = function(t) {
return this.children_.isEmpty() ? this : new e(this.children_, t, this.indexMap_);
};
e.prototype.getImmediateChild = function(e) {
if (".priority" === e) return this.getPriority();
var t = this.children_.get(e);
return null === t ? i : t;
};
e.prototype.getChild = function(e) {
var t = e.getFront();
return null === t ? this : this.getImmediateChild(t).getChild(e.popFront());
};
e.prototype.hasChild = function(e) {
return null !== this.children_.get(e);
};
e.prototype.updateImmediateChild = function(t, n) {
o.assert(n, "We should always be passing snapshot nodes");
if (".priority" === t) return this.updatePriority(n);
var r = new u.NamedNode(t, n), a = void 0, s = void 0;
if (n.isEmpty()) {
a = this.children_.remove(t);
s = this.indexMap_.removeFromIndexes(r, this.children_);
} else {
a = this.children_.insert(t, n);
s = this.indexMap_.addToIndexes(r, this.children_);
}
return new e(a, a.isEmpty() ? i : this.priorityNode_, s);
};
e.prototype.updateChild = function(e, t) {
var n = e.getFront();
if (null === n) return t;
o.assert(".priority" !== e.getFront() || 1 === e.getLength(), ".priority must be the last token in a path");
var r = this.getImmediateChild(n).updateChild(e.popFront(), t);
return this.updateImmediateChild(n, r);
};
e.prototype.isEmpty = function() {
return this.children_.isEmpty();
};
e.prototype.numChildren = function() {
return this.children_.count();
};
e.prototype.val = function(t) {
if (this.isEmpty()) return null;
var n = {}, r = 0, i = 0, o = !0;
this.forEachChild(l.PRIORITY_INDEX, function(a, s) {
n[a] = s.val(t);
r++;
o && e.INTEGER_REGEXP_.test(a) ? i = Math.max(i, Number(a)) : o = !1;
});
if (!t && o && i < 2 * r) {
var a = [];
for (var s in n) a[s] = n[s];
return a;
}
t && !this.getPriority().isEmpty() && (n[".priority"] = this.getPriority().val());
return n;
};
e.prototype.hash = function() {
if (null === this.lazyHash_) {
var e = "";
this.getPriority().isEmpty() || (e += "priority:" + c.priorityHashText(this.getPriority().val()) + ":");
this.forEachChild(l.PRIORITY_INDEX, function(t, n) {
var r = n.hash();
"" !== r && (e += ":" + t + ":" + r);
});
this.lazyHash_ = "" === e ? "" : a.sha1(e);
}
return this.lazyHash_;
};
e.prototype.getPredecessorChildName = function(e, t, n) {
var r = this.resolveIndex_(n);
if (r) {
var i = r.getPredecessorKey(new u.NamedNode(e, t));
return i ? i.name : null;
}
return this.children_.getPredecessorKey(e);
};
e.prototype.getFirstChildName = function(e) {
var t = this.resolveIndex_(e);
if (t) {
var n = t.minKey();
return n && n.name;
}
return this.children_.minKey();
};
e.prototype.getFirstChild = function(e) {
var t = this.getFirstChildName(e);
return t ? new u.NamedNode(t, this.children_.get(t)) : null;
};
e.prototype.getLastChildName = function(e) {
var t = this.resolveIndex_(e);
if (t) {
var n = t.maxKey();
return n && n.name;
}
return this.children_.maxKey();
};
e.prototype.getLastChild = function(e) {
var t = this.getLastChildName(e);
return t ? new u.NamedNode(t, this.children_.get(t)) : null;
};
e.prototype.forEachChild = function(e, t) {
var n = this.resolveIndex_(e);
return n ? n.inorderTraversal(function(e) {
return t(e.name, e.node);
}) : this.children_.inorderTraversal(t);
};
e.prototype.getIterator = function(e) {
return this.getIteratorFrom(e.minPost(), e);
};
e.prototype.getIteratorFrom = function(e, t) {
var n = this.resolveIndex_(t);
if (n) return n.getIteratorFrom(e, function(e) {
return e;
});
for (var r = this.children_.getIteratorFrom(e.name, u.NamedNode.Wrap), i = r.peek(); null != i && t.compare(i, e) < 0; ) {
r.getNext();
i = r.peek();
}
return r;
};
e.prototype.getReverseIterator = function(e) {
return this.getReverseIteratorFrom(e.maxPost(), e);
};
e.prototype.getReverseIteratorFrom = function(e, t) {
var n = this.resolveIndex_(t);
if (n) return n.getReverseIteratorFrom(e, function(e) {
return e;
});
for (var r = this.children_.getReverseIteratorFrom(e.name, u.NamedNode.Wrap), i = r.peek(); null != i && t.compare(i, e) > 0; ) {
r.getNext();
i = r.peek();
}
return r;
};
e.prototype.compareTo = function(e) {
return this.isEmpty() ? e.isEmpty() ? 0 : -1 : e.isLeafNode() || e.isEmpty() ? 1 : e === n.MAX_NODE ? -1 : 0;
};
e.prototype.withIndex = function(t) {
if (t === h.KEY_INDEX || this.indexMap_.hasIndex(t)) return this;
var n = this.indexMap_.addIndex(t, this.children_);
return new e(this.children_, this.priorityNode_, n);
};
e.prototype.isIndexed = function(e) {
return e === h.KEY_INDEX || this.indexMap_.hasIndex(e);
};
e.prototype.equals = function(e) {
if (e === this) return !0;
if (e.isLeafNode()) return !1;
var t = e;
if (this.getPriority().equals(t.getPriority())) {
if (this.children_.count() === t.children_.count()) {
for (var n = this.getIterator(l.PRIORITY_INDEX), r = t.getIterator(l.PRIORITY_INDEX), i = n.getNext(), o = r.getNext(); i && o; ) {
if (i.name !== o.name || !i.node.equals(o.node)) return !1;
i = n.getNext();
o = r.getNext();
}
return null === i && null === o;
}
return !1;
}
return !1;
};
e.prototype.resolveIndex_ = function(e) {
return e === h.KEY_INDEX ? null : this.indexMap_.get(e.toString());
};
e.INTEGER_REGEXP_ = /^(0|[1-9]\d*)$/;
return e;
}();
n.ChildrenNode = v;
var _ = function(e) {
function t() {
return e.call(this, new s.SortedMap(d.NAME_COMPARATOR), v.EMPTY_NODE, f.IndexMap.Default) || this;
}
r(t, e);
t.prototype.compareTo = function(e) {
return e === this ? 0 : 1;
};
t.prototype.equals = function(e) {
return e === this;
};
t.prototype.getPriority = function() {
return this;
};
t.prototype.getImmediateChild = function(e) {
return v.EMPTY_NODE;
};
t.prototype.isEmpty = function() {
return !1;
};
return t;
}(v);
n.MaxNode = _;
n.MAX_NODE = new _();
Object.defineProperties(u.NamedNode, {
MIN: {
value: new u.NamedNode(a.MIN_NAME, v.EMPTY_NODE)
},
MAX: {
value: new u.NamedNode(a.MAX_NAME, n.MAX_NODE)
}
});
h.KeyIndex.__EMPTY_NODE = v.EMPTY_NODE;
p.LeafNode.__childrenNodeConstructor = v;
c.setMaxNode(n.MAX_NODE);
l.setMaxNode(n.MAX_NODE);
}, {
"../util/SortedMap": 60,
"../util/util": 64,
"./IndexMap": 34,
"./LeafNode": 35,
"./Node": 36,
"./comparators": 38,
"./indexes/KeyIndex": 40,
"./indexes/PriorityIndex": 42,
"./snap": 45,
"@firebase/util": 135
} ],
34: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("@firebase/util"), o = e("./childSet"), a = e("@firebase/util"), s = e("./Node"), u = e("./indexes/PriorityIndex"), c = e("./indexes/KeyIndex"), l = {}, h = function() {
function e(e, t) {
this.indexes_ = e;
this.indexSet_ = t;
}
Object.defineProperty(e, "Default", {
get: function() {
i.assert(l && u.PRIORITY_INDEX, "ChildrenNode.ts has not been loaded");
return r = r || new e({
".priority": l
}, {
".priority": u.PRIORITY_INDEX
});
},
enumerable: !0,
configurable: !0
});
e.prototype.get = function(e) {
var t = a.safeGet(this.indexes_, e);
if (!t) throw new Error("No index defined for " + e);
return t === l ? null : t;
};
e.prototype.hasIndex = function(e) {
return a.contains(this.indexSet_, e.toString());
};
e.prototype.addIndex = function(t, n) {
i.assert(t !== c.KEY_INDEX, "KeyIndex always exists and isn't meant to be added to the IndexMap.");
for (var r = [], u = !1, h = n.getIterator(s.NamedNode.Wrap), f = h.getNext(); f; ) {
u = u || t.isDefinedOn(f.node);
r.push(f);
f = h.getNext();
}
var p;
p = u ? o.buildChildSet(r, t.getCompare()) : l;
var d = t.toString(), v = a.clone(this.indexSet_);
v[d] = t;
var _ = a.clone(this.indexes_);
_[d] = p;
return new e(_, v);
};
e.prototype.addToIndexes = function(t, n) {
var r = this;
return new e(a.map(this.indexes_, function(e, u) {
var c = a.safeGet(r.indexSet_, u);
i.assert(c, "Missing index implementation for " + u);
if (e === l) {
if (c.isDefinedOn(t.node)) {
for (var h = [], f = n.getIterator(s.NamedNode.Wrap), p = f.getNext(); p; ) {
p.name != t.name && h.push(p);
p = f.getNext();
}
h.push(t);
return o.buildChildSet(h, c.getCompare());
}
return l;
}
var d = n.get(t.name), v = e;
d && (v = v.remove(new s.NamedNode(t.name, d)));
return v.insert(t, t.node);
}), this.indexSet_);
};
e.prototype.removeFromIndexes = function(t, n) {
return new e(a.map(this.indexes_, function(e) {
if (e === l) return e;
var r = n.get(t.name);
return r ? e.remove(new s.NamedNode(t.name, r)) : e;
}), this.indexSet_);
};
return e;
}();
n.IndexMap = h;
}, {
"./Node": 36,
"./childSet": 37,
"./indexes/KeyIndex": 40,
"./indexes/PriorityIndex": 42,
"@firebase/util": 135
} ],
35: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("@firebase/util"), o = e("../util/util"), a = e("./snap"), s = function() {
function e(t, n) {
void 0 === n && (n = e.__childrenNodeConstructor.EMPTY_NODE);
this.value_ = t;
this.priorityNode_ = n;
this.lazyHash_ = null;
i.assert(void 0 !== this.value_ && null !== this.value_, "LeafNode shouldn't be created with null/undefined value.");
a.validatePriorityNode(this.priorityNode_);
}
Object.defineProperty(e, "__childrenNodeConstructor", {
get: function() {
return r;
},
set: function(e) {
r = e;
},
enumerable: !0,
configurable: !0
});
e.prototype.isLeafNode = function() {
return !0;
};
e.prototype.getPriority = function() {
return this.priorityNode_;
};
e.prototype.updatePriority = function(t) {
return new e(this.value_, t);
};
e.prototype.getImmediateChild = function(t) {
return ".priority" === t ? this.priorityNode_ : e.__childrenNodeConstructor.EMPTY_NODE;
};
e.prototype.getChild = function(t) {
return t.isEmpty() ? this : ".priority" === t.getFront() ? this.priorityNode_ : e.__childrenNodeConstructor.EMPTY_NODE;
};
e.prototype.hasChild = function() {
return !1;
};
e.prototype.getPredecessorChildName = function(e, t) {
return null;
};
e.prototype.updateImmediateChild = function(t, n) {
return ".priority" === t ? this.updatePriority(n) : n.isEmpty() && ".priority" !== t ? this : e.__childrenNodeConstructor.EMPTY_NODE.updateImmediateChild(t, n).updatePriority(this.priorityNode_);
};
e.prototype.updateChild = function(t, n) {
var r = t.getFront();
if (null === r) return n;
if (n.isEmpty() && ".priority" !== r) return this;
i.assert(".priority" !== r || 1 === t.getLength(), ".priority must be the last token in a path");
return this.updateImmediateChild(r, e.__childrenNodeConstructor.EMPTY_NODE.updateChild(t.popFront(), n));
};
e.prototype.isEmpty = function() {
return !1;
};
e.prototype.numChildren = function() {
return 0;
};
e.prototype.forEachChild = function(e, t) {
return !1;
};
e.prototype.val = function(e) {
return e && !this.getPriority().isEmpty() ? {
".value": this.getValue(),
".priority": this.getPriority().val()
} : this.getValue();
};
e.prototype.hash = function() {
if (null === this.lazyHash_) {
var e = "";
this.priorityNode_.isEmpty() || (e += "priority:" + a.priorityHashText(this.priorityNode_.val()) + ":");
var t = typeof this.value_;
e += t + ":";
e += "number" === t ? o.doubleToIEEE754String(this.value_) : this.value_;
this.lazyHash_ = o.sha1(e);
}
return this.lazyHash_;
};
e.prototype.getValue = function() {
return this.value_;
};
e.prototype.compareTo = function(t) {
if (t === e.__childrenNodeConstructor.EMPTY_NODE) return 1;
if (t instanceof e.__childrenNodeConstructor) return -1;
i.assert(t.isLeafNode(), "Unknown node type");
return this.compareToLeafNode_(t);
};
e.prototype.compareToLeafNode_ = function(t) {
var n = typeof t.value_, r = typeof this.value_, o = e.VALUE_TYPE_ORDER.indexOf(n), a = e.VALUE_TYPE_ORDER.indexOf(r);
i.assert(o >= 0, "Unknown leaf type: " + n);
i.assert(a >= 0, "Unknown leaf type: " + r);
return o === a ? "object" === r ? 0 : this.value_ < t.value_ ? -1 : this.value_ === t.value_ ? 0 : 1 : a - o;
};
e.prototype.withIndex = function() {
return this;
};
e.prototype.isIndexed = function() {
return !0;
};
e.prototype.equals = function(e) {
if (e === this) return !0;
if (e.isLeafNode()) {
var t = e;
return this.value_ === t.value_ && this.priorityNode_.equals(t.priorityNode_);
}
return !1;
};
e.VALUE_TYPE_ORDER = [ "object", "boolean", "number", "string" ];
return e;
}();
n.LeafNode = s;
}, {
"../util/util": 64,
"./snap": 45,
"@firebase/util": 135
} ],
36: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e(e, t) {
this.name = e;
this.node = t;
}
e.Wrap = function(t, n) {
return new e(t, n);
};
return e;
}();
n.NamedNode = r;
}, {} ],
37: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../util/SortedMap"), i = e("../util/SortedMap"), o = Math.log(2), a = function() {
function e(e) {
this.count = function(e) {
return parseInt(Math.log(e) / o, 10);
}(e + 1);
this.current_ = this.count - 1;
var t = function(e) {
return parseInt(Array(e + 1).join("1"), 2);
}(this.count);
this.bits_ = e + 1 & t;
}
e.prototype.nextBitIsOne = function() {
var e = !(this.bits_ & 1 << this.current_);
this.current_--;
return e;
};
return e;
}();
n.buildChildSet = function(e, t, n, o) {
e.sort(t);
var s = function(t, i) {
var o, a, u = i - t;
if (0 == u) return null;
if (1 == u) {
o = e[t];
a = n ? n(o) : o;
return new r.LLRBNode(a, o.node, r.LLRBNode.BLACK, null, null);
}
var c = parseInt(u / 2, 10) + t, l = s(t, c), h = s(c + 1, i);
o = e[c];
a = n ? n(o) : o;
return new r.LLRBNode(a, o.node, r.LLRBNode.BLACK, l, h);
}, u = function(t) {
for (var i = null, o = null, a = e.length, u = function(t, i) {
var o = a - t, u = a;
a -= t;
var l = s(o + 1, u), h = e[o], f = n ? n(h) : h;
c(new r.LLRBNode(f, h.node, i, null, l));
}, c = function(e) {
if (i) {
i.left = e;
i = e;
} else {
o = e;
i = e;
}
}, l = 0; l < t.count; ++l) {
var h = t.nextBitIsOne(), f = Math.pow(2, t.count - (l + 1));
if (h) u(f, r.LLRBNode.BLACK); else {
u(f, r.LLRBNode.BLACK);
u(f, r.LLRBNode.RED);
}
}
return o;
}(new a(e.length));
return new i.SortedMap(o || t, u);
};
}, {
"../util/SortedMap": 60
} ],
38: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../util/util");
n.NAME_ONLY_COMPARATOR = function(e, t) {
return r.nameCompare(e.name, t.name);
};
n.NAME_COMPARATOR = function(e, t) {
return r.nameCompare(e, t);
};
}, {
"../util/util": 64
} ],
39: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../Node"), i = e("../../util/util"), o = function() {
function e() {}
e.prototype.getCompare = function() {
return this.compare.bind(this);
};
e.prototype.indexedValueChanged = function(e, t) {
var n = new r.NamedNode(i.MIN_NAME, e), o = new r.NamedNode(i.MIN_NAME, t);
return 0 !== this.compare(n, o);
};
e.prototype.minPost = function() {
return r.NamedNode.MIN;
};
return e;
}();
n.Index = o;
}, {
"../../util/util": 64,
"../Node": 36
} ],
40: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i, o = e("./Index"), a = e("../Node"), s = e("../../util/util"), u = e("@firebase/util"), c = function(e) {
function t() {
return null !== e && e.apply(this, arguments) || this;
}
r(t, e);
Object.defineProperty(t, "__EMPTY_NODE", {
get: function() {
return i;
},
set: function(e) {
i = e;
},
enumerable: !0,
configurable: !0
});
t.prototype.compare = function(e, t) {
return s.nameCompare(e.name, t.name);
};
t.prototype.isDefinedOn = function(e) {
throw u.assertionError("KeyIndex.isDefinedOn not expected to be called.");
};
t.prototype.indexedValueChanged = function(e, t) {
return !1;
};
t.prototype.minPost = function() {
return a.NamedNode.MIN;
};
t.prototype.maxPost = function() {
return new a.NamedNode(s.MAX_NAME, i);
};
t.prototype.makePost = function(e, t) {
u.assert("string" == typeof e, "KeyIndex indexValue must always be a string.");
return new a.NamedNode(e, i);
};
t.prototype.toString = function() {
return ".key";
};
return t;
}(o.Index);
n.KeyIndex = c;
n.KEY_INDEX = new c();
}, {
"../../util/util": 64,
"../Node": 36,
"./Index": 39,
"@firebase/util": 135
} ],
41: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("@firebase/util"), o = e("../../util/util"), a = e("./Index"), s = e("../ChildrenNode"), u = e("../Node"), c = e("../nodeFromJSON"), l = function(e) {
function t(t) {
var n = e.call(this) || this;
n.indexPath_ = t;
i.assert(!t.isEmpty() && ".priority" !== t.getFront(), "Can't create PathIndex with empty path or .priority key");
return n;
}
r(t, e);
t.prototype.extractChild = function(e) {
return e.getChild(this.indexPath_);
};
t.prototype.isDefinedOn = function(e) {
return !e.getChild(this.indexPath_).isEmpty();
};
t.prototype.compare = function(e, t) {
var n = this.extractChild(e.node), r = this.extractChild(t.node), i = n.compareTo(r);
return 0 === i ? o.nameCompare(e.name, t.name) : i;
};
t.prototype.makePost = function(e, t) {
var n = c.nodeFromJSON(e), r = s.ChildrenNode.EMPTY_NODE.updateChild(this.indexPath_, n);
return new u.NamedNode(t, r);
};
t.prototype.maxPost = function() {
var e = s.ChildrenNode.EMPTY_NODE.updateChild(this.indexPath_, s.MAX_NODE);
return new u.NamedNode(o.MAX_NAME, e);
};
t.prototype.toString = function() {
return this.indexPath_.slice().join("/");
};
return t;
}(a.Index);
n.PathIndex = l;
}, {
"../../util/util": 64,
"../ChildrenNode": 33,
"../Node": 36,
"../nodeFromJSON": 44,
"./Index": 39,
"@firebase/util": 135
} ],
42: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i, o, a = e("./Index"), s = e("../../util/util"), u = e("../Node"), c = e("../LeafNode");
n.setNodeFromJSON = function(e) {
i = e;
};
n.setMaxNode = function(e) {
o = e;
};
var l = function(e) {
function t() {
return null !== e && e.apply(this, arguments) || this;
}
r(t, e);
t.prototype.compare = function(e, t) {
var n = e.node.getPriority(), r = t.node.getPriority(), i = n.compareTo(r);
return 0 === i ? s.nameCompare(e.name, t.name) : i;
};
t.prototype.isDefinedOn = function(e) {
return !e.getPriority().isEmpty();
};
t.prototype.indexedValueChanged = function(e, t) {
return !e.getPriority().equals(t.getPriority());
};
t.prototype.minPost = function() {
return u.NamedNode.MIN;
};
t.prototype.maxPost = function() {
return new u.NamedNode(s.MAX_NAME, new c.LeafNode("[PRIORITY-POST]", o));
};
t.prototype.makePost = function(e, t) {
var n = i(e);
return new u.NamedNode(t, new c.LeafNode("[PRIORITY-POST]", n));
};
t.prototype.toString = function() {
return ".priority";
};
return t;
}(a.Index);
n.PriorityIndex = l;
n.PRIORITY_INDEX = new l();
}, {
"../../util/util": 64,
"../LeafNode": 35,
"../Node": 36,
"./Index": 39
} ],
43: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./Index"), o = e("../Node"), a = e("../../util/util"), s = e("../nodeFromJSON"), u = function(e) {
function t() {
return null !== e && e.apply(this, arguments) || this;
}
r(t, e);
t.prototype.compare = function(e, t) {
var n = e.node.compareTo(t.node);
return 0 === n ? a.nameCompare(e.name, t.name) : n;
};
t.prototype.isDefinedOn = function(e) {
return !0;
};
t.prototype.indexedValueChanged = function(e, t) {
return !e.equals(t);
};
t.prototype.minPost = function() {
return o.NamedNode.MIN;
};
t.prototype.maxPost = function() {
return o.NamedNode.MAX;
};
t.prototype.makePost = function(e, t) {
var n = s.nodeFromJSON(e);
return new o.NamedNode(t, n);
};
t.prototype.toString = function() {
return ".value";
};
return t;
}(i.Index);
n.ValueIndex = u;
n.VALUE_INDEX = new u();
}, {
"../../util/util": 64,
"../Node": 36,
"../nodeFromJSON": 44,
"./Index": 39
} ],
44: [ function(e, t, n) {
"use strict";
function r(e, t) {
void 0 === t && (t = null);
if (null === e) return i.ChildrenNode.EMPTY_NODE;
"object" == typeof e && ".priority" in e && (t = e[".priority"]);
u.assert(null === t || "string" == typeof t || "number" == typeof t || "object" == typeof t && ".sv" in t, "Invalid priority type found: " + typeof t);
"object" == typeof e && ".value" in e && null !== e[".value"] && (e = e[".value"]);
if ("object" != typeof e || ".sv" in e) {
var n = e;
return new o.LeafNode(n, r(t));
}
if (e instanceof Array || !p) {
var d = i.ChildrenNode.EMPTY_NODE, v = e;
s.forEach(v, function(e, t) {
if (s.contains(v, e) && "." !== e.substring(0, 1)) {
var n = r(t);
!n.isLeafNode() && n.isEmpty() || (d = d.updateImmediateChild(e, n));
}
});
return d.updatePriority(r(t));
}
var _ = [], y = !1, g = e;
s.forEach(g, function(e, t) {
if ("string" != typeof e || "." !== e.substring(0, 1)) {
var n = r(g[e]);
if (!n.isEmpty()) {
y = y || !n.getPriority().isEmpty();
_.push(new a.NamedNode(e, n));
}
}
});
if (0 == _.length) return i.ChildrenNode.EMPTY_NODE;
var m = c.buildChildSet(_, l.NAME_ONLY_COMPARATOR, function(e) {
return e.name;
}, l.NAME_COMPARATOR);
if (y) {
var b = c.buildChildSet(_, f.PRIORITY_INDEX.getCompare());
return new i.ChildrenNode(m, r(t), new h.IndexMap({
".priority": b
}, {
".priority": f.PRIORITY_INDEX
}));
}
return new i.ChildrenNode(m, r(t), h.IndexMap.Default);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./ChildrenNode"), o = e("./LeafNode"), a = e("./Node"), s = e("@firebase/util"), u = e("@firebase/util"), c = e("./childSet"), l = e("./comparators"), h = e("./IndexMap"), f = e("./indexes/PriorityIndex"), p = !0;
n.nodeFromJSON = r;
f.setNodeFromJSON(r);
}, {
"./ChildrenNode": 33,
"./IndexMap": 34,
"./LeafNode": 35,
"./Node": 36,
"./childSet": 37,
"./comparators": 38,
"./indexes/PriorityIndex": 42,
"@firebase/util": 135
} ],
45: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("@firebase/util"), o = e("../util/util"), a = e("@firebase/util");
n.setMaxNode = function(e) {
r = e;
};
n.priorityHashText = function(e) {
return "number" == typeof e ? "number:" + o.doubleToIEEE754String(e) : "string:" + e;
};
n.validatePriorityNode = function(e) {
if (e.isLeafNode()) {
var t = e.val();
i.assert("string" == typeof t || "number" == typeof t || "object" == typeof t && a.contains(t, ".sv"), "Priority must be a string or number.");
} else i.assert(e === r || e.isEmpty(), "priority of unexpected type.");
i.assert(e === r || e.getPriority().isEmpty(), "Priority nodes can't have a priority of their own.");
};
}, {
"../util/util": 64,
"@firebase/util": 135
} ],
46: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("@firebase/util"), o = function() {
function e() {
this.counters_ = {};
}
e.prototype.incrementCounter = function(e, t) {
void 0 === t && (t = 1);
i.contains(this.counters_, e) || (this.counters_[e] = 0);
this.counters_[e] += t;
};
e.prototype.get = function() {
return r.deepCopy(this.counters_);
};
return e;
}();
n.StatsCollection = o;
}, {
"@firebase/util": 135
} ],
47: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e(e) {
this.collection_ = e;
this.last_ = null;
}
e.prototype.get = function() {
var e = this.collection_.get(), t = r.clone(e);
this.last_ && r.forEach(this.last_, function(e, n) {
t[e] = t[e] - n;
});
this.last_ = e;
return t;
};
return e;
}();
n.StatsListener = i;
}, {
"@firebase/util": 135
} ],
48: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./StatsCollection"), i = function() {
function e() {}
e.getCollection = function(e) {
var t = e.toString();
this.collections_[t] || (this.collections_[t] = new r.StatsCollection());
return this.collections_[t];
};
e.getOrCreateReporter = function(e, t) {
var n = e.toString();
this.reporters_[n] || (this.reporters_[n] = t());
return this.reporters_[n];
};
e.collections_ = {};
e.reporters_ = {};
return e;
}();
n.StatsManager = i;
}, {
"./StatsCollection": 46
} ],
49: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../util/util"), o = e("./StatsListener"), a = 1e4, s = 3e4, u = function() {
function e(e, t) {
this.server_ = t;
this.statsToReport_ = {};
this.statsListener_ = new o.StatsListener(e);
var n = a + (s - a) * Math.random();
i.setTimeoutNonBlocking(this.reportStats_.bind(this), Math.floor(n));
}
e.prototype.includeStat = function(e) {
this.statsToReport_[e] = !0;
};
e.prototype.reportStats_ = function() {
var e = this, t = this.statsListener_.get(), n = {}, o = !1;
r.forEach(t, function(t, i) {
if (i > 0 && r.contains(e.statsToReport_, t)) {
n[t] = i;
o = !0;
}
});
o && this.server_.reportStats(n);
i.setTimeoutNonBlocking(this.reportStats_.bind(this), Math.floor(2 * Math.random() * 3e5));
};
return e;
}();
n.StatsReporter = u;
}, {
"../util/util": 64,
"./StatsListener": 47,
"@firebase/util": 135
} ],
50: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e(e) {
this.domStorage_ = e;
this.prefix_ = "firebase:";
}
e.prototype.set = function(e, t) {
null == t ? this.domStorage_.removeItem(this.prefixedName_(e)) : this.domStorage_.setItem(this.prefixedName_(e), r.stringify(t));
};
e.prototype.get = function(e) {
var t = this.domStorage_.getItem(this.prefixedName_(e));
return null == t ? null : r.jsonEval(t);
};
e.prototype.remove = function(e) {
this.domStorage_.removeItem(this.prefixedName_(e));
};
e.prototype.prefixedName_ = function(e) {
return this.prefix_ + e;
};
e.prototype.toString = function() {
return this.domStorage_.toString();
};
return e;
}();
n.DOMStorageWrapper = i;
}, {
"@firebase/util": 135
} ],
51: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e() {
this.cache_ = {};
this.isInMemoryStorage = !0;
}
e.prototype.set = function(e, t) {
null == t ? delete this.cache_[e] : this.cache_[e] = t;
};
e.prototype.get = function(e) {
return r.contains(this.cache_, e) ? this.cache_[e] : null;
};
e.prototype.remove = function(e) {
delete this.cache_[e];
};
return e;
}();
n.MemoryStorage = i;
}, {
"@firebase/util": 135
} ],
52: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./DOMStorageWrapper"), i = e("./MemoryStorage"), o = function(e) {
try {
if ("undefined" != typeof window && "undefined" != typeof window[e]) {
var t = window[e];
t.setItem("firebase:sentinel", "cache");
t.removeItem("firebase:sentinel");
return new r.DOMStorageWrapper(t);
}
} catch (e) {}
return new i.MemoryStorage();
};
n.PersistentStorage = o("localStorage");
n.SessionStorage = o("sessionStorage");
}, {
"./DOMStorageWrapper": 50,
"./MemoryStorage": 51
} ],
53: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e() {
this.set = {};
}
e.prototype.add = function(e, t) {
this.set[e] = null === t || t;
};
e.prototype.contains = function(e) {
return r.contains(this.set, e);
};
e.prototype.get = function(e) {
return this.contains(e) ? this.set[e] : void 0;
};
e.prototype.remove = function(e) {
delete this.set[e];
};
e.prototype.clear = function() {
this.set = {};
};
e.prototype.isEmpty = function() {
return r.isEmpty(this.set);
};
e.prototype.count = function() {
return r.getCount(this.set);
};
e.prototype.each = function(e) {
r.forEach(this.set, function(t, n) {
return e(t, n);
});
};
e.prototype.keys = function() {
var e = [];
r.forEach(this.set, function(t) {
e.push(t);
});
return e;
};
return e;
}();
n.CountedSet = i;
}, {
"@firebase/util": 135
} ],
54: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e(e) {
this.allowedEvents_ = e;
this.listeners_ = {};
r.assert(Array.isArray(e) && e.length > 0, "Requires a non-empty array");
}
e.prototype.trigger = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
if (Array.isArray(this.listeners_[e])) for (var r = this.listeners_[e].slice(), i = 0; i < r.length; i++) r[i].callback.apply(r[i].context, t);
};
e.prototype.on = function(e, t, n) {
this.validateEventType_(e);
this.listeners_[e] = this.listeners_[e] || [];
this.listeners_[e].push({
callback: t,
context: n
});
var r = this.getInitialEvent(e);
r && t.apply(n, r);
};
e.prototype.off = function(e, t, n) {
this.validateEventType_(e);
for (var r = this.listeners_[e] || [], i = 0; i < r.length; i++) if (r[i].callback === t && (!n || n === r[i].context)) {
r.splice(i, 1);
return;
}
};
e.prototype.validateEventType_ = function(e) {
r.assert(this.allowedEvents_.find(function(t) {
return t === e;
}), "Unknown event: " + e);
};
return e;
}();
n.EventEmitter = i;
}, {
"@firebase/util": 135
} ],
55: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r, i = e("./SortedMap"), o = e("./Path"), a = e("./util"), s = e("@firebase/util"), u = function() {
r || (r = new i.SortedMap(a.stringCompare));
return r;
}, c = function() {
function e(e, t) {
void 0 === t && (t = u());
this.value = e;
this.children = t;
}
e.fromObject = function(t) {
var n = e.Empty;
s.forEach(t, function(e, t) {
n = n.set(new o.Path(e), t);
});
return n;
};
e.prototype.isEmpty = function() {
return null === this.value && this.children.isEmpty();
};
e.prototype.findRootMostMatchingPathAndValue = function(e, t) {
if (null != this.value && t(this.value)) return {
path: o.Path.Empty,
value: this.value
};
if (e.isEmpty()) return null;
var n = e.getFront(), r = this.children.get(n);
if (null !== r) {
var i = r.findRootMostMatchingPathAndValue(e.popFront(), t);
return null != i ? {
path: new o.Path(n).child(i.path),
value: i.value
} : null;
}
return null;
};
e.prototype.findRootMostValueAndPath = function(e) {
return this.findRootMostMatchingPathAndValue(e, function() {
return !0;
});
};
e.prototype.subtree = function(t) {
if (t.isEmpty()) return this;
var n = t.getFront(), r = this.children.get(n);
return null !== r ? r.subtree(t.popFront()) : e.Empty;
};
e.prototype.set = function(t, n) {
if (t.isEmpty()) return new e(n, this.children);
var r = t.getFront(), i = (this.children.get(r) || e.Empty).set(t.popFront(), n), o = this.children.insert(r, i);
return new e(this.value, o);
};
e.prototype.remove = function(t) {
if (t.isEmpty()) return this.children.isEmpty() ? e.Empty : new e(null, this.children);
var n = t.getFront(), r = this.children.get(n);
if (r) {
var i = r.remove(t.popFront()), o = void 0;
o = i.isEmpty() ? this.children.remove(n) : this.children.insert(n, i);
return null === this.value && o.isEmpty() ? e.Empty : new e(this.value, o);
}
return this;
};
e.prototype.get = function(e) {
if (e.isEmpty()) return this.value;
var t = e.getFront(), n = this.children.get(t);
return n ? n.get(e.popFront()) : null;
};
e.prototype.setTree = function(t, n) {
if (t.isEmpty()) return n;
var r = t.getFront(), i = (this.children.get(r) || e.Empty).setTree(t.popFront(), n), o = void 0;
o = i.isEmpty() ? this.children.remove(r) : this.children.insert(r, i);
return new e(this.value, o);
};
e.prototype.fold = function(e) {
return this.fold_(o.Path.Empty, e);
};
e.prototype.fold_ = function(e, t) {
var n = {};
this.children.inorderTraversal(function(r, i) {
n[r] = i.fold_(e.child(r), t);
});
return t(e, this.value, n);
};
e.prototype.findOnPath = function(e, t) {
return this.findOnPath_(e, o.Path.Empty, t);
};
e.prototype.findOnPath_ = function(e, t, n) {
var r = !!this.value && n(t, this.value);
if (r) return r;
if (e.isEmpty()) return null;
var i = e.getFront(), o = this.children.get(i);
return o ? o.findOnPath_(e.popFront(), t.child(i), n) : null;
};
e.prototype.foreachOnPath = function(e, t) {
return this.foreachOnPath_(e, o.Path.Empty, t);
};
e.prototype.foreachOnPath_ = function(t, n, r) {
if (t.isEmpty()) return this;
this.value && r(n, this.value);
var i = t.getFront(), o = this.children.get(i);
return o ? o.foreachOnPath_(t.popFront(), n.child(i), r) : e.Empty;
};
e.prototype.foreach = function(e) {
this.foreach_(o.Path.Empty, e);
};
e.prototype.foreach_ = function(e, t) {
this.children.inorderTraversal(function(n, r) {
r.foreach_(e.child(n), t);
});
this.value && t(e, this.value);
};
e.prototype.foreachChild = function(e) {
this.children.inorderTraversal(function(t, n) {
n.value && e(t, n.value);
});
};
e.Empty = new e(null);
return e;
}();
n.ImmutableTree = c;
}, {
"./Path": 58,
"./SortedMap": 60,
"./util": 64,
"@firebase/util": 135
} ],
56: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util");
n.nextPushId = function() {
var e = "-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz", t = 0, n = [];
return function(i) {
var o = i === t;
t = i;
var a, s = new Array(8);
for (a = 7; a >= 0; a--) {
s[a] = e.charAt(i % 64);
i = Math.floor(i / 64);
}
r.assert(0 === i, "Cannot push at time == 0");
var u = s.join("");
if (o) {
for (a = 11; a >= 0 && 63 === n[a]; a--) n[a] = 0;
n[a]++;
} else for (a = 0; a < 12; a++) n[a] = Math.floor(64 * Math.random());
for (a = 0; a < 12; a++) u += e.charAt(n[a]);
r.assert(20 === u.length, "nextPushId: Length should be 20.");
return u;
};
}();
}, {
"@firebase/util": 135
} ],
57: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("@firebase/util"), o = e("./EventEmitter"), a = e("@firebase/util"), s = function(e) {
function t() {
var t = e.call(this, [ "online" ]) || this;
t.online_ = !0;
if ("undefined" != typeof window && "undefined" != typeof window.addEventListener && !a.isMobileCordova()) {
window.addEventListener("online", function() {
if (!t.online_) {
t.online_ = !0;
t.trigger("online", !0);
}
}, !1);
window.addEventListener("offline", function() {
if (t.online_) {
t.online_ = !1;
t.trigger("online", !1);
}
}, !1);
}
return t;
}
r(t, e);
t.getInstance = function() {
return new t();
};
t.prototype.getInitialEvent = function(e) {
i.assert("online" === e, "Unknown event type: " + e);
return [ this.online_ ];
};
t.prototype.currentlyOnline = function() {
return this.online_;
};
return t;
}(o.EventEmitter);
n.OnlineMonitor = s;
}, {
"./EventEmitter": 54,
"@firebase/util": 135
} ],
58: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./util"), i = e("@firebase/util"), o = function() {
function e(e, t) {
if (void 0 === t) {
this.pieces_ = e.split("/");
for (var n = 0, r = 0; r < this.pieces_.length; r++) if (this.pieces_[r].length > 0) {
this.pieces_[n] = this.pieces_[r];
n++;
}
this.pieces_.length = n;
this.pieceNum_ = 0;
} else {
this.pieces_ = e;
this.pieceNum_ = t;
}
}
Object.defineProperty(e, "Empty", {
get: function() {
return new e("");
},
enumerable: !0,
configurable: !0
});
e.prototype.getFront = function() {
return this.pieceNum_ >= this.pieces_.length ? null : this.pieces_[this.pieceNum_];
};
e.prototype.getLength = function() {
return this.pieces_.length - this.pieceNum_;
};
e.prototype.popFront = function() {
var t = this.pieceNum_;
t < this.pieces_.length && t++;
return new e(this.pieces_, t);
};
e.prototype.getBack = function() {
return this.pieceNum_ < this.pieces_.length ? this.pieces_[this.pieces_.length - 1] : null;
};
e.prototype.toString = function() {
for (var e = "", t = this.pieceNum_; t < this.pieces_.length; t++) "" !== this.pieces_[t] && (e += "/" + this.pieces_[t]);
return e || "/";
};
e.prototype.toUrlEncodedString = function() {
for (var e = "", t = this.pieceNum_; t < this.pieces_.length; t++) "" !== this.pieces_[t] && (e += "/" + encodeURIComponent(String(this.pieces_[t])));
return e || "/";
};
e.prototype.slice = function(e) {
void 0 === e && (e = 0);
return this.pieces_.slice(this.pieceNum_ + e);
};
e.prototype.parent = function() {
if (this.pieceNum_ >= this.pieces_.length) return null;
for (var t = [], n = this.pieceNum_; n < this.pieces_.length - 1; n++) t.push(this.pieces_[n]);
return new e(t, 0);
};
e.prototype.child = function(t) {
for (var n = [], r = this.pieceNum_; r < this.pieces_.length; r++) n.push(this.pieces_[r]);
if (t instanceof e) for (r = t.pieceNum_; r < t.pieces_.length; r++) n.push(t.pieces_[r]); else for (var i = t.split("/"), r = 0; r < i.length; r++) i[r].length > 0 && n.push(i[r]);
return new e(n, 0);
};
e.prototype.isEmpty = function() {
return this.pieceNum_ >= this.pieces_.length;
};
e.relativePath = function(t, n) {
var r = t.getFront(), i = n.getFront();
if (null === r) return n;
if (r === i) return e.relativePath(t.popFront(), n.popFront());
throw new Error("INTERNAL ERROR: innerPath (" + n + ") is not within outerPath (" + t + ")");
};
e.comparePaths = function(e, t) {
for (var n = e.slice(), i = t.slice(), o = 0; o < n.length && o < i.length; o++) {
var a = r.nameCompare(n[o], i[o]);
if (0 !== a) return a;
}
return n.length === i.length ? 0 : n.length < i.length ? -1 : 1;
};
e.prototype.equals = function(e) {
if (this.getLength() !== e.getLength()) return !1;
for (var t = this.pieceNum_, n = e.pieceNum_; t <= this.pieces_.length; t++, n++) if (this.pieces_[t] !== e.pieces_[n]) return !1;
return !0;
};
e.prototype.contains = function(e) {
var t = this.pieceNum_, n = e.pieceNum_;
if (this.getLength() > e.getLength()) return !1;
for (;t < this.pieces_.length; ) {
if (this.pieces_[t] !== e.pieces_[n]) return !1;
++t;
++n;
}
return !0;
};
return e;
}();
n.Path = o;
var a = function() {
function e(e, t) {
this.errorPrefix_ = t;
this.parts_ = e.slice();
this.byteLength_ = Math.max(1, this.parts_.length);
for (var n = 0; n < this.parts_.length; n++) this.byteLength_ += i.stringLength(this.parts_[n]);
this.checkValid_();
}
Object.defineProperty(e, "MAX_PATH_DEPTH", {
get: function() {
return 32;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e, "MAX_PATH_LENGTH_BYTES", {
get: function() {
return 768;
},
enumerable: !0,
configurable: !0
});
e.prototype.push = function(e) {
this.parts_.length > 0 && (this.byteLength_ += 1);
this.parts_.push(e);
this.byteLength_ += i.stringLength(e);
this.checkValid_();
};
e.prototype.pop = function() {
var e = this.parts_.pop();
this.byteLength_ -= i.stringLength(e);
this.parts_.length > 0 && (this.byteLength_ -= 1);
};
e.prototype.checkValid_ = function() {
if (this.byteLength_ > e.MAX_PATH_LENGTH_BYTES) throw new Error(this.errorPrefix_ + "has a key path longer than " + e.MAX_PATH_LENGTH_BYTES + " bytes (" + this.byteLength_ + ").");
if (this.parts_.length > e.MAX_PATH_DEPTH) throw new Error(this.errorPrefix_ + "path specified exceeds the maximum depth that can be written (" + e.MAX_PATH_DEPTH + ") or object contains a cycle " + this.toErrorString());
};
e.prototype.toErrorString = function() {
return 0 == this.parts_.length ? "" : "in property '" + this.parts_.join(".") + "'";
};
return e;
}();
n.ValidationPath = a;
}, {
"./util": 64,
"@firebase/util": 135
} ],
59: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./Path"), o = e("../SparseSnapshotTree"), a = e("../snap/LeafNode"), s = e("../snap/nodeFromJSON"), u = e("../snap/indexes/PriorityIndex");
n.generateWithValues = function(e) {
(e = e || {}).timestamp = e.timestamp || new Date().getTime();
return e;
};
n.resolveDeferredValue = function(e, t) {
if (e && "object" == typeof e) {
r.assert(".sv" in e, "Unexpected leaf node or priority contents");
return t[e[".sv"]];
}
return e;
};
n.resolveDeferredValueTree = function(e, t) {
var r = new o.SparseSnapshotTree();
e.forEachTree(new i.Path(""), function(e, i) {
r.remember(e, n.resolveDeferredValueSnapshot(i, t));
});
return r;
};
n.resolveDeferredValueSnapshot = function(e, t) {
var r, i = e.getPriority().val(), o = n.resolveDeferredValue(i, t);
if (e.isLeafNode()) {
var c = e, l = n.resolveDeferredValue(c.getValue(), t);
return l !== c.getValue() || o !== c.getPriority().val() ? new a.LeafNode(l, s.nodeFromJSON(o)) : e;
}
var h = e;
r = h;
o !== h.getPriority().val() && (r = r.updatePriority(new a.LeafNode(o)));
h.forEachChild(u.PRIORITY_INDEX, function(e, i) {
var o = n.resolveDeferredValueSnapshot(i, t);
o !== i && (r = r.updateImmediateChild(e, o));
});
return r;
};
}, {
"../SparseSnapshotTree": 24,
"../snap/LeafNode": 35,
"../snap/indexes/PriorityIndex": 42,
"../snap/nodeFromJSON": 44,
"./Path": 58,
"@firebase/util": 135
} ],
60: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e(e, t, n, r, i) {
void 0 === i && (i = null);
this.isReverse_ = r;
this.resultGenerator_ = i;
this.nodeStack_ = [];
for (var o = 1; !e.isEmpty(); ) {
e = e;
o = t ? n(e.key, t) : 1;
r && (o *= -1);
if (o < 0) e = this.isReverse_ ? e.left : e.right; else {
if (0 === o) {
this.nodeStack_.push(e);
break;
}
this.nodeStack_.push(e);
e = this.isReverse_ ? e.right : e.left;
}
}
}
e.prototype.getNext = function() {
if (0 === this.nodeStack_.length) return null;
var e, t = this.nodeStack_.pop();
e = this.resultGenerator_ ? this.resultGenerator_(t.key, t.value) : {
key: t.key,
value: t.value
};
if (this.isReverse_) {
t = t.left;
for (;!t.isEmpty(); ) {
this.nodeStack_.push(t);
t = t.right;
}
} else {
t = t.right;
for (;!t.isEmpty(); ) {
this.nodeStack_.push(t);
t = t.left;
}
}
return e;
};
e.prototype.hasNext = function() {
return this.nodeStack_.length > 0;
};
e.prototype.peek = function() {
if (0 === this.nodeStack_.length) return null;
var e = this.nodeStack_[this.nodeStack_.length - 1];
return this.resultGenerator_ ? this.resultGenerator_(e.key, e.value) : {
key: e.key,
value: e.value
};
};
return e;
}();
n.SortedMapIterator = r;
var i = function() {
function e(t, n, r, i, o) {
this.key = t;
this.value = n;
this.color = null != r ? r : e.RED;
this.left = null != i ? i : a.EMPTY_NODE;
this.right = null != o ? o : a.EMPTY_NODE;
}
e.prototype.copy = function(t, n, r, i, o) {
return new e(null != t ? t : this.key, null != n ? n : this.value, null != r ? r : this.color, null != i ? i : this.left, null != o ? o : this.right);
};
e.prototype.count = function() {
return this.left.count() + 1 + this.right.count();
};
e.prototype.isEmpty = function() {
return !1;
};
e.prototype.inorderTraversal = function(e) {
return this.left.inorderTraversal(e) || e(this.key, this.value) || this.right.inorderTraversal(e);
};
e.prototype.reverseTraversal = function(e) {
return this.right.reverseTraversal(e) || e(this.key, this.value) || this.left.reverseTraversal(e);
};
e.prototype.min_ = function() {
return this.left.isEmpty() ? this : this.left.min_();
};
e.prototype.minKey = function() {
return this.min_().key;
};
e.prototype.maxKey = function() {
return this.right.isEmpty() ? this.key : this.right.maxKey();
};
e.prototype.insert = function(e, t, n) {
var r, i;
return (i = (r = n(e, (i = this).key)) < 0 ? i.copy(null, null, null, i.left.insert(e, t, n), null) : 0 === r ? i.copy(null, t, null, null, null) : i.copy(null, null, null, null, i.right.insert(e, t, n))).fixUp_();
};
e.prototype.removeMin_ = function() {
if (this.left.isEmpty()) return a.EMPTY_NODE;
var e = this;
e.left.isRed_() || e.left.left.isRed_() || (e = e.moveRedLeft_());
return (e = e.copy(null, null, null, e.left.removeMin_(), null)).fixUp_();
};
e.prototype.remove = function(e, t) {
var n, r;
if (t(e, (n = this).key) < 0) {
n.left.isEmpty() || n.left.isRed_() || n.left.left.isRed_() || (n = n.moveRedLeft_());
n = n.copy(null, null, null, n.left.remove(e, t), null);
} else {
n.left.isRed_() && (n = n.rotateRight_());
n.right.isEmpty() || n.right.isRed_() || n.right.left.isRed_() || (n = n.moveRedRight_());
if (0 === t(e, n.key)) {
if (n.right.isEmpty()) return a.EMPTY_NODE;
r = n.right.min_();
n = n.copy(r.key, r.value, null, null, n.right.removeMin_());
}
n = n.copy(null, null, null, null, n.right.remove(e, t));
}
return n.fixUp_();
};
e.prototype.isRed_ = function() {
return this.color;
};
e.prototype.fixUp_ = function() {
var e = this;
e.right.isRed_() && !e.left.isRed_() && (e = e.rotateLeft_());
e.left.isRed_() && e.left.left.isRed_() && (e = e.rotateRight_());
e.left.isRed_() && e.right.isRed_() && (e = e.colorFlip_());
return e;
};
e.prototype.moveRedLeft_ = function() {
var e = this.colorFlip_();
e.right.left.isRed_() && (e = (e = (e = e.copy(null, null, null, null, e.right.rotateRight_())).rotateLeft_()).colorFlip_());
return e;
};
e.prototype.moveRedRight_ = function() {
var e = this.colorFlip_();
e.left.left.isRed_() && (e = (e = e.rotateRight_()).colorFlip_());
return e;
};
e.prototype.rotateLeft_ = function() {
var t = this.copy(null, null, e.RED, null, this.right.left);
return this.right.copy(null, null, this.color, t, null);
};
e.prototype.rotateRight_ = function() {
var t = this.copy(null, null, e.RED, this.left.right, null);
return this.left.copy(null, null, this.color, null, t);
};
e.prototype.colorFlip_ = function() {
var e = this.left.copy(null, null, !this.left.color, null, null), t = this.right.copy(null, null, !this.right.color, null, null);
return this.copy(null, null, !this.color, e, t);
};
e.prototype.checkMaxDepth_ = function() {
var e = this.check_();
return Math.pow(2, e) <= this.count() + 1;
};
e.prototype.check_ = function() {
var e;
if (this.isRed_() && this.left.isRed_()) throw new Error("Red node has red child(" + this.key + "," + this.value + ")");
if (this.right.isRed_()) throw new Error("Right child of (" + this.key + "," + this.value + ") is red");
if ((e = this.left.check_()) !== this.right.check_()) throw new Error("Black depths differ");
return e + (this.isRed_() ? 0 : 1);
};
e.RED = !0;
e.BLACK = !1;
return e;
}();
n.LLRBNode = i;
var o = function() {
function e() {}
e.prototype.copy = function(e, t, n, r, i) {
return this;
};
e.prototype.insert = function(e, t, n) {
return new i(e, t, null);
};
e.prototype.remove = function(e, t) {
return this;
};
e.prototype.count = function() {
return 0;
};
e.prototype.isEmpty = function() {
return !0;
};
e.prototype.inorderTraversal = function(e) {
return !1;
};
e.prototype.reverseTraversal = function(e) {
return !1;
};
e.prototype.minKey = function() {
return null;
};
e.prototype.maxKey = function() {
return null;
};
e.prototype.check_ = function() {
return 0;
};
e.prototype.isRed_ = function() {
return !1;
};
return e;
}();
n.LLRBEmptyNode = o;
var a = function() {
function e(t, n) {
void 0 === n && (n = e.EMPTY_NODE);
this.comparator_ = t;
this.root_ = n;
}
e.prototype.insert = function(t, n) {
return new e(this.comparator_, this.root_.insert(t, n, this.comparator_).copy(null, null, i.BLACK, null, null));
};
e.prototype.remove = function(t) {
return new e(this.comparator_, this.root_.remove(t, this.comparator_).copy(null, null, i.BLACK, null, null));
};
e.prototype.get = function(e) {
for (var t, n = this.root_; !n.isEmpty(); ) {
if (0 === (t = this.comparator_(e, n.key))) return n.value;
t < 0 ? n = n.left : t > 0 && (n = n.right);
}
return null;
};
e.prototype.getPredecessorKey = function(e) {
for (var t, n = this.root_, r = null; !n.isEmpty(); ) {
if (0 === (t = this.comparator_(e, n.key))) {
if (n.left.isEmpty()) return r ? r.key : null;
n = n.left;
for (;!n.right.isEmpty(); ) n = n.right;
return n.key;
}
if (t < 0) n = n.left; else if (t > 0) {
r = n;
n = n.right;
}
}
throw new Error("Attempted to find predecessor key for a nonexistent key.  What gives?");
};
e.prototype.isEmpty = function() {
return this.root_.isEmpty();
};
e.prototype.count = function() {
return this.root_.count();
};
e.prototype.minKey = function() {
return this.root_.minKey();
};
e.prototype.maxKey = function() {
return this.root_.maxKey();
};
e.prototype.inorderTraversal = function(e) {
return this.root_.inorderTraversal(e);
};
e.prototype.reverseTraversal = function(e) {
return this.root_.reverseTraversal(e);
};
e.prototype.getIterator = function(e) {
return new r(this.root_, null, this.comparator_, !1, e);
};
e.prototype.getIteratorFrom = function(e, t) {
return new r(this.root_, e, this.comparator_, !1, t);
};
e.prototype.getReverseIteratorFrom = function(e, t) {
return new r(this.root_, e, this.comparator_, !0, t);
};
e.prototype.getReverseIterator = function(e) {
return new r(this.root_, null, this.comparator_, !0, e);
};
e.EMPTY_NODE = new o();
return e;
}();
n.SortedMap = a;
}, {} ],
61: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./Path"), o = e("@firebase/util"), a = function() {
return function() {
this.children = {};
this.childCount = 0;
this.value = null;
};
}();
n.TreeNode = a;
var s = function() {
function e(e, t, n) {
void 0 === e && (e = "");
void 0 === t && (t = null);
void 0 === n && (n = new a());
this.name_ = e;
this.parent_ = t;
this.node_ = n;
}
e.prototype.subTree = function(t) {
for (var n, r = t instanceof i.Path ? t : new i.Path(t), s = this; null !== (n = r.getFront()); ) {
s = new e(n, s, o.safeGet(s.node_.children, n) || new a());
r = r.popFront();
}
return s;
};
e.prototype.getValue = function() {
return this.node_.value;
};
e.prototype.setValue = function(e) {
r.assert("undefined" != typeof e, "Cannot set value to undefined");
this.node_.value = e;
this.updateParents_();
};
e.prototype.clear = function() {
this.node_.value = null;
this.node_.children = {};
this.node_.childCount = 0;
this.updateParents_();
};
e.prototype.hasChildren = function() {
return this.node_.childCount > 0;
};
e.prototype.isEmpty = function() {
return null === this.getValue() && !this.hasChildren();
};
e.prototype.forEachChild = function(t) {
var n = this;
o.forEach(this.node_.children, function(r, i) {
t(new e(r, n, i));
});
};
e.prototype.forEachDescendant = function(e, t, n) {
t && !n && e(this);
this.forEachChild(function(t) {
t.forEachDescendant(e, !0, n);
});
t && n && e(this);
};
e.prototype.forEachAncestor = function(e, t) {
for (var n = t ? this : this.parent(); null !== n; ) {
if (e(n)) return !0;
n = n.parent();
}
return !1;
};
e.prototype.forEachImmediateDescendantWithValue = function(e) {
this.forEachChild(function(t) {
null !== t.getValue() ? e(t) : t.forEachImmediateDescendantWithValue(e);
});
};
e.prototype.path = function() {
return new i.Path(null === this.parent_ ? this.name_ : this.parent_.path() + "/" + this.name_);
};
e.prototype.name = function() {
return this.name_;
};
e.prototype.parent = function() {
return this.parent_;
};
e.prototype.updateParents_ = function() {
null !== this.parent_ && this.parent_.updateChild_(this.name_, this);
};
e.prototype.updateChild_ = function(e, t) {
var n = t.isEmpty(), r = o.contains(this.node_.children, e);
if (n && r) {
delete this.node_.children[e];
this.node_.childCount--;
this.updateParents_();
} else if (!n && !r) {
this.node_.children[e] = t.node_;
this.node_.childCount++;
this.updateParents_();
}
};
return e;
}();
n.Tree = s;
}, {
"./Path": 58,
"@firebase/util": 135
} ],
62: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./EventEmitter"), o = e("@firebase/util"), a = function(e) {
function t() {
var t, n, r = e.call(this, [ "visible" ]) || this;
if ("undefined" != typeof document && "undefined" != typeof document.addEventListener) if ("undefined" != typeof document.hidden) {
n = "visibilitychange";
t = "hidden";
} else if ("undefined" != typeof document.mozHidden) {
n = "mozvisibilitychange";
t = "mozHidden";
} else if ("undefined" != typeof document.msHidden) {
n = "msvisibilitychange";
t = "msHidden";
} else if ("undefined" != typeof document.webkitHidden) {
n = "webkitvisibilitychange";
t = "webkitHidden";
}
r.visible_ = !0;
n && document.addEventListener(n, function() {
var e = !document[t];
if (e !== r.visible_) {
r.visible_ = e;
r.trigger("visible", e);
}
}, !1);
return r;
}
r(t, e);
t.getInstance = function() {
return new t();
};
t.prototype.getInitialEvent = function(e) {
o.assert("visible" === e, "Unknown event type: " + e);
return [ this.visible_ ];
};
return t;
}(i.EventEmitter);
n.VisibilityMonitor = a;
}, {
"./EventEmitter": 54,
"@firebase/util": 135
} ],
63: [ function(e, t, n) {
"use strict";
function r(e) {
for (var t = "", n = e.split("/"), r = 0; r < n.length; r++) if (n[r].length > 0) {
var i = n[r];
try {
i = decodeURIComponent(i.replace(/\+/g, " "));
} catch (e) {}
t += "/" + i;
}
return t;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Path"), o = e("../../RepoInfo"), a = e("../util");
n.parseRepoInfo = function(e) {
var t = n.parseURL(e), r = t.subdomain;
"firebase" === t.domain && a.fatal(t.host + " is no longer supported. Please use <YOUR FIREBASE>.firebaseio.com instead");
r && "undefined" != r || a.fatal("Cannot parse Firebase url. Please use https://<YOUR FIREBASE>.firebaseio.com");
t.secure || a.warnIfPageIsSecure();
var s = "ws" === t.scheme || "wss" === t.scheme;
return {
repoInfo: new o.RepoInfo(t.host, t.secure, r, s),
path: new i.Path(t.pathString)
};
};
n.parseURL = function(e) {
var t = "", n = "", i = "", o = "", a = !0, s = "https", u = 443;
if ("string" == typeof e) {
var c = e.indexOf("//");
if (c >= 0) {
s = e.substring(0, c - 1);
e = e.substring(c + 2);
}
var l = e.indexOf("/");
-1 === l && (l = e.length);
t = e.substring(0, l);
o = r(e.substring(l));
var h = t.split(".");
if (3 === h.length) {
n = h[1];
i = h[0].toLowerCase();
} else 2 === h.length && (n = h[0]);
if ((c = t.indexOf(":")) >= 0) {
a = "https" === s || "wss" === s;
u = parseInt(t.substring(c + 1), 10);
}
}
return {
host: t,
port: u,
domain: n,
subdomain: i,
secure: a,
scheme: s,
pathString: o
};
};
}, {
"../../RepoInfo": 19,
"../Path": 58,
"../util": 64
} ],
64: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("@firebase/util"), o = e("@firebase/util"), a = e("@firebase/util"), s = e("@firebase/util"), u = e("@firebase/util"), c = e("../storage/storage"), l = e("@firebase/util");
n.LUIDGenerator = function() {
var e = 1;
return function() {
return e++;
};
}();
n.sha1 = function(e) {
var t = s.stringToByteArray(e), n = new a.Sha1();
n.update(t);
var r = n.digest();
return o.base64.encodeByteArray(r);
};
var h = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
for (var n = "", r = 0; r < e.length; r++) {
Array.isArray(e[r]) || e[r] && "object" == typeof e[r] && "number" == typeof e[r].length ? n += h.apply(null, e[r]) : "object" == typeof e[r] ? n += u.stringify(e[r]) : n += e[r];
n += " ";
}
return n;
};
n.logger = null;
var f = !0;
n.enableLogging = function(e, t) {
r.assert(!t || !0 === e || !1 === e, "Can't turn on custom loggers persistently.");
if (!0 === e) {
"undefined" != typeof console && ("function" == typeof console.log ? n.logger = console.log.bind(console) : "object" == typeof console.log && (n.logger = function(e) {
console.log(e);
}));
t && c.SessionStorage.set("logging_enabled", !0);
} else if ("function" == typeof e) n.logger = e; else {
n.logger = null;
c.SessionStorage.remove("logging_enabled");
}
};
n.log = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
if (!0 === f) {
f = !1;
null === n.logger && !0 === c.SessionStorage.get("logging_enabled") && n.enableLogging(!0);
}
if (n.logger) {
var r = h.apply(null, e);
n.logger(r);
}
};
n.logWrapper = function(e) {
return function() {
for (var t = [], r = 0; r < arguments.length; r++) t[r] = arguments[r];
n.log.apply(void 0, [ e ].concat(t));
};
};
n.error = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
if ("undefined" != typeof console) {
var n = "FIREBASE INTERNAL ERROR: " + h.apply(void 0, e);
"undefined" != typeof console.error ? console.error(n) : console.log(n);
}
};
n.fatal = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
var n = h.apply(void 0, e);
throw new Error("FIREBASE FATAL ERROR: " + n);
};
n.warn = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
if ("undefined" != typeof console) {
var n = "FIREBASE WARNING: " + h.apply(void 0, e);
"undefined" != typeof console.warn ? console.warn(n) : console.log(n);
}
};
n.warnIfPageIsSecure = function() {
"undefined" != typeof window && window.location && window.location.protocol && -1 !== window.location.protocol.indexOf("https:") && n.warn("Insecure Firebase access from a secure page. Please use https in calls to new Firebase().");
};
n.warnAboutUnsupportedMethod = function(e) {
n.warn(e + " is unsupported and will likely change soon.  Please do not use.");
};
n.isInvalidJSONNumber = function(e) {
return "number" == typeof e && (e != e || e == Number.POSITIVE_INFINITY || e == Number.NEGATIVE_INFINITY);
};
n.executeWhenDOMReady = function(e) {
if (l.isNodeSdk() || "complete" === document.readyState) e(); else {
var t = !1, n = function() {
if (document.body) {
if (!t) {
t = !0;
e();
}
} else setTimeout(n, Math.floor(10));
};
if (document.addEventListener) {
document.addEventListener("DOMContentLoaded", n, !1);
window.addEventListener("load", n, !1);
} else if (document.attachEvent) {
document.attachEvent("onreadystatechange", function() {
"complete" === document.readyState && n();
});
window.attachEvent("onload", n);
}
}
};
n.MIN_NAME = "[MIN_NAME]";
n.MAX_NAME = "[MAX_NAME]";
n.nameCompare = function(e, t) {
if (e === t) return 0;
if (e === n.MIN_NAME || t === n.MAX_NAME) return -1;
if (t === n.MIN_NAME || e === n.MAX_NAME) return 1;
var r = n.tryParseInt(e), i = n.tryParseInt(t);
return null !== r ? null !== i ? r - i == 0 ? e.length - t.length : r - i : -1 : null !== i ? 1 : e < t ? -1 : 1;
};
n.stringCompare = function(e, t) {
return e === t ? 0 : e < t ? -1 : 1;
};
n.requireKey = function(e, t) {
if (t && e in t) return t[e];
throw new Error("Missing required key (" + e + ") in object: " + u.stringify(t));
};
n.ObjectToUniqueKey = function(e) {
if ("object" != typeof e || null === e) return u.stringify(e);
var t = [];
for (var r in e) t.push(r);
t.sort();
for (var i = "{", o = 0; o < t.length; o++) {
0 !== o && (i += ",");
i += u.stringify(t[o]);
i += ":";
i += n.ObjectToUniqueKey(e[t[o]]);
}
return i += "}";
};
n.splitStringBySize = function(e, t) {
var n = e.length;
if (n <= t) return [ e ];
for (var r = [], i = 0; i < n; i += t) i + t > n ? r.push(e.substring(i, n)) : r.push(e.substring(i, i + t));
return r;
};
n.each = function(e, t) {
if (Array.isArray(e)) for (var n = 0; n < e.length; ++n) t(n, e[n]); else i.forEach(e, function(e, n) {
return t(n, e);
});
};
n.bindCallback = function(e, t) {
return t ? e.bind(t) : e;
};
n.doubleToIEEE754String = function(e) {
r.assert(!n.isInvalidJSONNumber(e), "Invalid JSON number");
var t, i, o, a, s, u, c;
if (0 === e) {
i = 0;
o = 0;
t = 1 / e == -Infinity ? 1 : 0;
} else {
t = e < 0;
if ((e = Math.abs(e)) >= Math.pow(2, -1022)) {
i = (a = Math.min(Math.floor(Math.log(e) / Math.LN2), 1023)) + 1023;
o = Math.round(e * Math.pow(2, 52 - a) - Math.pow(2, 52));
} else {
i = 0;
o = Math.round(e / Math.pow(2, -1074));
}
}
u = [];
for (s = 52; s; s -= 1) {
u.push(o % 2 ? 1 : 0);
o = Math.floor(o / 2);
}
for (s = 11; s; s -= 1) {
u.push(i % 2 ? 1 : 0);
i = Math.floor(i / 2);
}
u.push(t ? 1 : 0);
u.reverse();
c = u.join("");
var l = "";
for (s = 0; s < 64; s += 8) {
var h = parseInt(c.substr(s, 8), 2).toString(16);
1 === h.length && (h = "0" + h);
l += h;
}
return l.toLowerCase();
};
n.isChromeExtensionContentScript = function() {
return !("object" != typeof window || !window.chrome || !window.chrome.extension || /^chrome/.test(window.location.href));
};
n.isWindowsStoreApp = function() {
return "object" == typeof Windows && "object" == typeof Windows.UI;
};
n.errorForServerCode = function(e, t) {
var n = "Unknown Error";
"too_big" === e ? n = "The data requested exceeds the maximum size that can be accessed with a single request." : "permission_denied" == e ? n = "Client doesn't have permission to access the desired data." : "unavailable" == e && (n = "The service is unavailable");
var r = new Error(e + " at " + t.path.toString() + ": " + n);
r.code = e.toUpperCase();
return r;
};
n.INTEGER_REGEXP_ = new RegExp("^-?\\d{1,10}$");
n.tryParseInt = function(e) {
if (n.INTEGER_REGEXP_.test(e)) {
var t = Number(e);
if (t >= -2147483648 && t <= 2147483647) return t;
}
return null;
};
n.exceptionGuard = function(e) {
try {
e();
} catch (e) {
setTimeout(function() {
var t = e.stack || "";
n.warn("Exception was thrown by user callback.", t);
throw e;
}, Math.floor(0));
}
};
n.callUserCallback = function(e) {
for (var t = [], r = 1; r < arguments.length; r++) t[r - 1] = arguments[r];
"function" == typeof e && n.exceptionGuard(function() {
e.apply(void 0, t);
});
};
n.beingCrawled = function() {
return ("object" == typeof window && window.navigator && window.navigator.userAgent || "").search(/googlebot|google webmaster tools|bingbot|yahoo! slurp|baiduspider|yandexbot|duckduckbot/i) >= 0;
};
n.exportPropGetter = function(e, t, n) {
Object.defineProperty(e, t, {
get: n
});
};
n.setTimeoutNonBlocking = function(e, t) {
var n = setTimeout(e, t);
"object" == typeof n && n.unref && n.unref();
return n;
};
}, {
"../storage/storage": 52,
"@firebase/util": 135
} ],
65: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./Path"), i = e("@firebase/util"), o = e("./util"), a = e("@firebase/util"), s = e("@firebase/util");
n.INVALID_KEY_REGEX_ = /[\[\].#$\/\u0000-\u001F\u007F]/;
n.INVALID_PATH_REGEX_ = /[\[\].#$\u0000-\u001F\u007F]/;
n.MAX_LEAF_SIZE_ = 10485760;
n.isValidKey = function(e) {
return "string" == typeof e && 0 !== e.length && !n.INVALID_KEY_REGEX_.test(e);
};
n.isValidPathString = function(e) {
return "string" == typeof e && 0 !== e.length && !n.INVALID_PATH_REGEX_.test(e);
};
n.isValidRootPathString = function(e) {
e && (e = e.replace(/^\/*\.info(\/|$)/, "/"));
return n.isValidPathString(e);
};
n.isValidPriority = function(e) {
return null === e || "string" == typeof e || "number" == typeof e && !o.isInvalidJSONNumber(e) || e && "object" == typeof e && i.contains(e, ".sv");
};
n.validateFirebaseDataArg = function(e, t, r, i, o) {
o && void 0 === r || n.validateFirebaseData(a.errorPrefix(e, t, o), r, i);
};
n.validateFirebaseData = function(e, t, a) {
var u = a instanceof r.Path ? new r.ValidationPath(a, e) : a;
if (void 0 === t) throw new Error(e + "contains undefined " + u.toErrorString());
if ("function" == typeof t) throw new Error(e + "contains a function " + u.toErrorString() + " with contents = " + t.toString());
if (o.isInvalidJSONNumber(t)) throw new Error(e + "contains " + t.toString() + " " + u.toErrorString());
if ("string" == typeof t && t.length > n.MAX_LEAF_SIZE_ / 3 && s.stringLength(t) > n.MAX_LEAF_SIZE_) throw new Error(e + "contains a string greater than " + n.MAX_LEAF_SIZE_ + " utf8 bytes " + u.toErrorString() + " ('" + t.substring(0, 50) + "...')");
if (t && "object" == typeof t) {
var c = !1, l = !1;
i.forEach(t, function(t, r) {
if (".value" === t) c = !0; else if (".priority" !== t && ".sv" !== t) {
l = !0;
if (!n.isValidKey(t)) throw new Error(e + " contains an invalid key (" + t + ") " + u.toErrorString() + '.  Keys must be non-empty strings and can\'t contain ".", "#", "$", "/", "[", or "]"');
}
u.push(t);
n.validateFirebaseData(e, r, u);
u.pop();
});
if (c && l) throw new Error(e + ' contains ".value" child ' + u.toErrorString() + " in addition to actual children.");
}
};
n.validateFirebaseMergePaths = function(e, t) {
var i, o;
for (i = 0; i < t.length; i++) for (var a = (o = t[i]).slice(), s = 0; s < a.length; s++) if (".priority" === a[s] && s === a.length - 1) ; else if (!n.isValidKey(a[s])) throw new Error(e + "contains an invalid key (" + a[s] + ") in path " + o.toString() + '. Keys must be non-empty strings and can\'t contain ".", "#", "$", "/", "[", or "]"');
t.sort(r.Path.comparePaths);
var u = null;
for (i = 0; i < t.length; i++) {
o = t[i];
if (null !== u && u.contains(o)) throw new Error(e + "contains a path " + u.toString() + " that is ancestor of another path " + o.toString());
u = o;
}
};
n.validateFirebaseMergeDataArg = function(e, t, o, s, u) {
if (!u || void 0 !== o) {
var c = a.errorPrefix(e, t, u);
if (!o || "object" != typeof o || Array.isArray(o)) throw new Error(c + " must be an object containing the children to replace.");
var l = [];
i.forEach(o, function(e, t) {
var i = new r.Path(e);
n.validateFirebaseData(c, t, s.child(i));
if (".priority" === i.getBack() && !n.isValidPriority(t)) throw new Error(c + "contains an invalid value for '" + i.toString() + "', which must be a valid Firebase priority (a string, finite number, server value, or null).");
l.push(i);
});
n.validateFirebaseMergePaths(c, l);
}
};
n.validatePriority = function(e, t, r, i) {
if (!i || void 0 !== r) {
if (o.isInvalidJSONNumber(r)) throw new Error(a.errorPrefix(e, t, i) + "is " + r.toString() + ", but must be a valid Firebase priority (a string, finite number, server value, or null).");
if (!n.isValidPriority(r)) throw new Error(a.errorPrefix(e, t, i) + "must be a valid Firebase priority (a string, finite number, server value, or null).");
}
};
n.validateEventType = function(e, t, n, r) {
if (!r || void 0 !== n) switch (n) {
case "value":
case "child_added":
case "child_removed":
case "child_changed":
case "child_moved":
break;

default:
throw new Error(a.errorPrefix(e, t, r) + 'must be a valid event type = "value", "child_added", "child_removed", "child_changed", or "child_moved".');
}
};
n.validateKey = function(e, t, r, i) {
if (!(i && void 0 === r || n.isValidKey(r))) throw new Error(a.errorPrefix(e, t, i) + 'was an invalid key = "' + r + '".  Firebase keys must be non-empty strings and can\'t contain ".", "#", "$", "/", "[", or "]").');
};
n.validatePathString = function(e, t, r, i) {
if (!(i && void 0 === r || n.isValidPathString(r))) throw new Error(a.errorPrefix(e, t, i) + 'was an invalid path = "' + r + '". Paths must be non-empty strings and can\'t contain ".", "#", "$", "[", or "]"');
};
n.validateRootPathString = function(e, t, r, i) {
r && (r = r.replace(/^\/*\.info(\/|$)/, "/"));
n.validatePathString(e, t, r, i);
};
n.validateWritablePath = function(e, t) {
if (".info" === t.getFront()) throw new Error(e + " failed = Can't modify data under /.info/");
};
n.validateUrl = function(e, t, r) {
var i = r.path.toString();
if ("string" != typeof r.repoInfo.host || 0 === r.repoInfo.host.length || !n.isValidKey(r.repoInfo.namespace) || 0 !== i.length && !n.isValidRootPathString(i)) throw new Error(a.errorPrefix(e, t, !1) + 'must be a valid firebase URL and the path can\'t contain ".", "#", "$", "[", or "]".');
};
n.validateCredential = function(e, t, n, r) {
if ((!r || void 0 !== n) && "string" != typeof n) throw new Error(a.errorPrefix(e, t, r) + "must be a valid credential (a string).");
};
n.validateBoolean = function(e, t, n, r) {
if ((!r || void 0 !== n) && "boolean" != typeof n) throw new Error(a.errorPrefix(e, t, r) + "must be a boolean.");
};
n.validateString = function(e, t, n, r) {
if ((!r || void 0 !== n) && "string" != typeof n) throw new Error(a.errorPrefix(e, t, r) + "must be a valid string.");
};
n.validateObject = function(e, t, n, r) {
if (!(r && void 0 === n || n && "object" == typeof n && null !== n)) throw new Error(a.errorPrefix(e, t, r) + "must be a valid object.");
};
n.validateObjectContainsKey = function(e, t, n, r, o, s) {
if (!(n && "object" == typeof n && i.contains(n, r))) {
if (o) return;
throw new Error(a.errorPrefix(e, t, o) + 'must contain the key "' + r + '"');
}
if (s) {
var u = i.safeGet(n, r);
if ("number" === s && "number" != typeof u || "string" === s && "string" != typeof u || "boolean" === s && "boolean" != typeof u || "function" === s && "function" != typeof u || "object" === s && "object" != typeof u && u) throw o ? new Error(a.errorPrefix(e, t, o) + 'contains invalid value for key "' + r + '" (must be of type "' + s + '")') : new Error(a.errorPrefix(e, t, o) + 'must contain the key "' + r + '" with type "' + s + '"');
}
};
}, {
"./Path": 58,
"./util": 64,
"@firebase/util": 135
} ],
66: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e(e, t, n) {
this.node_ = e;
this.fullyInitialized_ = t;
this.filtered_ = n;
}
e.prototype.isFullyInitialized = function() {
return this.fullyInitialized_;
};
e.prototype.isFiltered = function() {
return this.filtered_;
};
e.prototype.isCompleteForPath = function(e) {
if (e.isEmpty()) return this.isFullyInitialized() && !this.filtered_;
var t = e.getFront();
return this.isCompleteForChild(t);
};
e.prototype.isCompleteForChild = function(e) {
return this.isFullyInitialized() && !this.filtered_ || this.node_.hasChild(e);
};
e.prototype.getNode = function() {
return this.node_;
};
return e;
}();
n.CacheNode = r;
}, {} ],
67: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e(e, t, n, r, i) {
this.type = e;
this.snapshotNode = t;
this.childName = n;
this.oldSnap = r;
this.prevName = i;
}
e.valueChange = function(t) {
return new e(e.VALUE, t);
};
e.childAddedChange = function(t, n) {
return new e(e.CHILD_ADDED, n, t);
};
e.childRemovedChange = function(t, n) {
return new e(e.CHILD_REMOVED, n, t);
};
e.childChangedChange = function(t, n, r) {
return new e(e.CHILD_CHANGED, n, t, r);
};
e.childMovedChange = function(t, n) {
return new e(e.CHILD_MOVED, n, t);
};
e.CHILD_ADDED = "child_added";
e.CHILD_REMOVED = "child_removed";
e.CHILD_CHANGED = "child_changed";
e.CHILD_MOVED = "child_moved";
e.VALUE = "value";
return e;
}();
n.Change = r;
}, {} ],
68: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./Change"), o = e("@firebase/util"), a = function() {
function e() {
this.changeMap_ = {};
}
e.prototype.trackChildChange = function(e) {
var t = e.type, n = e.childName;
o.assert(t == i.Change.CHILD_ADDED || t == i.Change.CHILD_CHANGED || t == i.Change.CHILD_REMOVED, "Only child changes supported for tracking");
o.assert(".priority" !== n, "Only non-priority child changes can be tracked.");
var a = r.safeGet(this.changeMap_, n);
if (a) {
var s = a.type;
if (t == i.Change.CHILD_ADDED && s == i.Change.CHILD_REMOVED) this.changeMap_[n] = i.Change.childChangedChange(n, e.snapshotNode, a.snapshotNode); else if (t == i.Change.CHILD_REMOVED && s == i.Change.CHILD_ADDED) delete this.changeMap_[n]; else if (t == i.Change.CHILD_REMOVED && s == i.Change.CHILD_CHANGED) this.changeMap_[n] = i.Change.childRemovedChange(n, a.oldSnap); else if (t == i.Change.CHILD_CHANGED && s == i.Change.CHILD_ADDED) this.changeMap_[n] = i.Change.childAddedChange(n, e.snapshotNode); else {
if (t != i.Change.CHILD_CHANGED || s != i.Change.CHILD_CHANGED) throw o.assertionError("Illegal combination of changes: " + e + " occurred after " + a);
this.changeMap_[n] = i.Change.childChangedChange(n, e.snapshotNode, a.oldSnap);
}
} else this.changeMap_[n] = e;
};
e.prototype.getChanges = function() {
return r.getValues(this.changeMap_);
};
return e;
}();
n.ChildChangeAccumulator = a;
}, {
"./Change": 67,
"@firebase/util": 135
} ],
69: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./CacheNode"), i = function() {
function e() {}
e.prototype.getCompleteChild = function(e) {
return null;
};
e.prototype.getChildAfterChild = function(e, t, n) {
return null;
};
return e;
}();
n.NoCompleteChildSource_ = i;
n.NO_COMPLETE_CHILD_SOURCE = new i();
var o = function() {
function e(e, t, n) {
void 0 === n && (n = null);
this.writes_ = e;
this.viewCache_ = t;
this.optCompleteServerCache_ = n;
}
e.prototype.getCompleteChild = function(e) {
var t = this.viewCache_.getEventCache();
if (t.isCompleteForChild(e)) return t.getNode().getImmediateChild(e);
var n = null != this.optCompleteServerCache_ ? new r.CacheNode(this.optCompleteServerCache_, !0, !1) : this.viewCache_.getServerCache();
return this.writes_.calcCompleteChild(e, n);
};
e.prototype.getChildAfterChild = function(e, t, n) {
var r = null != this.optCompleteServerCache_ ? this.optCompleteServerCache_ : this.viewCache_.getCompleteServerSnap(), i = this.writes_.calcIndexedSlice(r, t, 1, n, e);
return 0 === i.length ? null : i[0];
};
return e;
}();
n.WriteTreeCompleteChildSource = o;
}, {
"./CacheNode": 66
} ],
70: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = function() {
function e(e, t, n, r) {
this.eventType = e;
this.eventRegistration = t;
this.snapshot = n;
this.prevName = r;
}
e.prototype.getPath = function() {
var e = this.snapshot.getRef();
return "value" === this.eventType ? e.path : e.getParent().path;
};
e.prototype.getEventType = function() {
return this.eventType;
};
e.prototype.getEventRunner = function() {
return this.eventRegistration.getEventRunner(this);
};
e.prototype.toString = function() {
return this.getPath().toString() + ":" + this.eventType + ":" + r.stringify(this.snapshot.exportVal());
};
return e;
}();
n.DataEvent = i;
var o = function() {
function e(e, t, n) {
this.eventRegistration = e;
this.error = t;
this.path = n;
}
e.prototype.getPath = function() {
return this.path;
};
e.prototype.getEventType = function() {
return "cancel";
};
e.prototype.getEventRunner = function() {
return this.eventRegistration.getEventRunner(this);
};
e.prototype.toString = function() {
return this.path.toString() + ":cancel";
};
return e;
}();
n.CancelEvent = o;
}, {
"@firebase/util": 135
} ],
71: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../snap/Node"), i = e("./Change"), o = e("@firebase/util"), a = function() {
function e(e) {
this.query_ = e;
this.index_ = this.query_.getQueryParams().getIndex();
}
e.prototype.generateEventsForChanges = function(e, t, n) {
var r = this, o = [], a = [];
e.forEach(function(e) {
e.type === i.Change.CHILD_CHANGED && r.index_.indexedValueChanged(e.oldSnap, e.snapshotNode) && a.push(i.Change.childMovedChange(e.childName, e.snapshotNode));
});
this.generateEventsForType_(o, i.Change.CHILD_REMOVED, e, n, t);
this.generateEventsForType_(o, i.Change.CHILD_ADDED, e, n, t);
this.generateEventsForType_(o, i.Change.CHILD_MOVED, a, n, t);
this.generateEventsForType_(o, i.Change.CHILD_CHANGED, e, n, t);
this.generateEventsForType_(o, i.Change.VALUE, e, n, t);
return o;
};
e.prototype.generateEventsForType_ = function(e, t, n, r, i) {
var o = this, a = n.filter(function(e) {
return e.type === t;
});
a.sort(this.compareChanges_.bind(this));
a.forEach(function(t) {
var n = o.materializeSingleChange_(t, i);
r.forEach(function(r) {
r.respondsTo(t.type) && e.push(r.createEvent(n, o.query_));
});
});
};
e.prototype.materializeSingleChange_ = function(e, t) {
if ("value" === e.type || "child_removed" === e.type) return e;
e.prevName = t.getPredecessorChildName(e.childName, e.snapshotNode, this.index_);
return e;
};
e.prototype.compareChanges_ = function(e, t) {
if (null == e.childName || null == t.childName) throw o.assertionError("Should only compare child_ events.");
var n = new r.NamedNode(e.childName, e.snapshotNode), i = new r.NamedNode(t.childName, t.snapshotNode);
return this.index_.compare(n, i);
};
return e;
}();
n.EventGenerator = a;
}, {
"../snap/Node": 36,
"./Change": 67,
"@firebase/util": 135
} ],
72: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../util/util"), i = function() {
function e() {
this.eventLists_ = [];
this.recursionDepth_ = 0;
}
e.prototype.queueEvents = function(e) {
for (var t = null, n = 0; n < e.length; n++) {
var r = e[n], i = r.getPath();
if (null !== t && !i.equals(t.getPath())) {
this.eventLists_.push(t);
t = null;
}
null === t && (t = new o(i));
t.add(r);
}
t && this.eventLists_.push(t);
};
e.prototype.raiseEventsAtPath = function(e, t) {
this.queueEvents(t);
this.raiseQueuedEventsMatchingPredicate_(function(t) {
return t.equals(e);
});
};
e.prototype.raiseEventsForChangedPath = function(e, t) {
this.queueEvents(t);
this.raiseQueuedEventsMatchingPredicate_(function(t) {
return t.contains(e) || e.contains(t);
});
};
e.prototype.raiseQueuedEventsMatchingPredicate_ = function(e) {
this.recursionDepth_++;
for (var t = !0, n = 0; n < this.eventLists_.length; n++) {
var r = this.eventLists_[n];
if (r) if (e(r.getPath())) {
this.eventLists_[n].raise();
this.eventLists_[n] = null;
} else t = !1;
}
t && (this.eventLists_ = []);
this.recursionDepth_--;
};
return e;
}();
n.EventQueue = i;
var o = function() {
function e(e) {
this.path_ = e;
this.events_ = [];
}
e.prototype.add = function(e) {
this.events_.push(e);
};
e.prototype.raise = function() {
for (var e = 0; e < this.events_.length; e++) {
var t = this.events_[e];
if (null !== t) {
this.events_[e] = null;
var n = t.getEventRunner();
r.logger && r.log("event: " + t.toString());
r.exceptionGuard(n);
}
}
};
e.prototype.getPath = function() {
return this.path_;
};
return e;
}();
n.EventList = o;
}, {
"../util/util": 64
} ],
73: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../../api/DataSnapshot"), i = e("./Event"), o = e("@firebase/util"), a = e("@firebase/util"), s = function() {
function e(e, t, n) {
this.callback_ = e;
this.cancelCallback_ = t;
this.context_ = n;
}
e.prototype.respondsTo = function(e) {
return "value" === e;
};
e.prototype.createEvent = function(e, t) {
var n = t.getQueryParams().getIndex();
return new i.DataEvent("value", this, new r.DataSnapshot(e.snapshotNode, t.getRef(), n));
};
e.prototype.getEventRunner = function(e) {
var t = this.context_;
if ("cancel" === e.getEventType()) {
a.assert(this.cancelCallback_, "Raising a cancel event on a listener with no cancel callback");
var n = this.cancelCallback_;
return function() {
n.call(t, e.error);
};
}
var r = this.callback_;
return function() {
r.call(t, e.snapshot);
};
};
e.prototype.createCancelEvent = function(e, t) {
return this.cancelCallback_ ? new i.CancelEvent(this, e, t) : null;
};
e.prototype.matches = function(t) {
return t instanceof e && (!t.callback_ || !this.callback_ || t.callback_ === this.callback_ && t.context_ === this.context_);
};
e.prototype.hasAnyCallback = function() {
return null !== this.callback_;
};
return e;
}();
n.ValueEventRegistration = s;
var u = function() {
function e(e, t, n) {
this.callbacks_ = e;
this.cancelCallback_ = t;
this.context_ = n;
}
e.prototype.respondsTo = function(e) {
var t = "children_added" === e ? "child_added" : e;
t = "children_removed" === t ? "child_removed" : t;
return o.contains(this.callbacks_, t);
};
e.prototype.createCancelEvent = function(e, t) {
return this.cancelCallback_ ? new i.CancelEvent(this, e, t) : null;
};
e.prototype.createEvent = function(e, t) {
a.assert(null != e.childName, "Child events should have a childName.");
var n = t.getRef().child(e.childName), o = t.getQueryParams().getIndex();
return new i.DataEvent(e.type, this, new r.DataSnapshot(e.snapshotNode, n, o), e.prevName);
};
e.prototype.getEventRunner = function(e) {
var t = this.context_;
if ("cancel" === e.getEventType()) {
a.assert(this.cancelCallback_, "Raising a cancel event on a listener with no cancel callback");
var n = this.cancelCallback_;
return function() {
n.call(t, e.error);
};
}
var r = this.callbacks_[e.eventType];
return function() {
r.call(t, e.snapshot, e.prevName);
};
};
e.prototype.matches = function(t) {
if (t instanceof e) {
if (!this.callbacks_ || !t.callbacks_) return !0;
if (this.context_ === t.context_) {
var n = o.getCount(t.callbacks_);
if (n === o.getCount(this.callbacks_)) {
if (1 === n) {
var r = o.getAnyKey(t.callbacks_), i = o.getAnyKey(this.callbacks_);
return !(i !== r || t.callbacks_[r] && this.callbacks_[i] && t.callbacks_[r] !== this.callbacks_[i]);
}
return o.every(this.callbacks_, function(e, n) {
return t.callbacks_[e] === n;
});
}
}
}
return !1;
};
e.prototype.hasAnyCallback = function() {
return null !== this.callbacks_;
};
return e;
}();
n.ChildEventRegistration = u;
}, {
"../../api/DataSnapshot": 6,
"./Event": 70,
"@firebase/util": 135
} ],
74: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../util/util"), o = e("../snap/indexes/KeyIndex"), a = e("../snap/indexes/PriorityIndex"), s = e("../snap/indexes/ValueIndex"), u = e("../snap/indexes/PathIndex"), c = e("./filter/IndexedFilter"), l = e("./filter/LimitedFilter"), h = e("./filter/RangedFilter"), f = e("@firebase/util"), p = function() {
function e() {
this.limitSet_ = !1;
this.startSet_ = !1;
this.startNameSet_ = !1;
this.endSet_ = !1;
this.endNameSet_ = !1;
this.limit_ = 0;
this.viewFrom_ = "";
this.indexStartValue_ = null;
this.indexStartName_ = "";
this.indexEndValue_ = null;
this.indexEndName_ = "";
this.index_ = a.PRIORITY_INDEX;
}
e.prototype.hasStart = function() {
return this.startSet_;
};
e.prototype.isViewFromLeft = function() {
return "" === this.viewFrom_ ? this.startSet_ : this.viewFrom_ === e.WIRE_PROTOCOL_CONSTANTS_.VIEW_FROM_LEFT;
};
e.prototype.getIndexStartValue = function() {
r.assert(this.startSet_, "Only valid if start has been set");
return this.indexStartValue_;
};
e.prototype.getIndexStartName = function() {
r.assert(this.startSet_, "Only valid if start has been set");
return this.startNameSet_ ? this.indexStartName_ : i.MIN_NAME;
};
e.prototype.hasEnd = function() {
return this.endSet_;
};
e.prototype.getIndexEndValue = function() {
r.assert(this.endSet_, "Only valid if end has been set");
return this.indexEndValue_;
};
e.prototype.getIndexEndName = function() {
r.assert(this.endSet_, "Only valid if end has been set");
return this.endNameSet_ ? this.indexEndName_ : i.MAX_NAME;
};
e.prototype.hasLimit = function() {
return this.limitSet_;
};
e.prototype.hasAnchoredLimit = function() {
return this.limitSet_ && "" !== this.viewFrom_;
};
e.prototype.getLimit = function() {
r.assert(this.limitSet_, "Only valid if limit has been set");
return this.limit_;
};
e.prototype.getIndex = function() {
return this.index_;
};
e.prototype.copy_ = function() {
var t = new e();
t.limitSet_ = this.limitSet_;
t.limit_ = this.limit_;
t.startSet_ = this.startSet_;
t.indexStartValue_ = this.indexStartValue_;
t.startNameSet_ = this.startNameSet_;
t.indexStartName_ = this.indexStartName_;
t.endSet_ = this.endSet_;
t.indexEndValue_ = this.indexEndValue_;
t.endNameSet_ = this.endNameSet_;
t.indexEndName_ = this.indexEndName_;
t.index_ = this.index_;
t.viewFrom_ = this.viewFrom_;
return t;
};
e.prototype.limit = function(e) {
var t = this.copy_();
t.limitSet_ = !0;
t.limit_ = e;
t.viewFrom_ = "";
return t;
};
e.prototype.limitToFirst = function(t) {
var n = this.copy_();
n.limitSet_ = !0;
n.limit_ = t;
n.viewFrom_ = e.WIRE_PROTOCOL_CONSTANTS_.VIEW_FROM_LEFT;
return n;
};
e.prototype.limitToLast = function(t) {
var n = this.copy_();
n.limitSet_ = !0;
n.limit_ = t;
n.viewFrom_ = e.WIRE_PROTOCOL_CONSTANTS_.VIEW_FROM_RIGHT;
return n;
};
e.prototype.startAt = function(e, t) {
var n = this.copy_();
n.startSet_ = !0;
void 0 === e && (e = null);
n.indexStartValue_ = e;
if (null != t) {
n.startNameSet_ = !0;
n.indexStartName_ = t;
} else {
n.startNameSet_ = !1;
n.indexStartName_ = "";
}
return n;
};
e.prototype.endAt = function(e, t) {
var n = this.copy_();
n.endSet_ = !0;
void 0 === e && (e = null);
n.indexEndValue_ = e;
if (void 0 !== t) {
n.endNameSet_ = !0;
n.indexEndName_ = t;
} else {
n.endNameSet_ = !1;
n.indexEndName_ = "";
}
return n;
};
e.prototype.orderBy = function(e) {
var t = this.copy_();
t.index_ = e;
return t;
};
e.prototype.getQueryObject = function() {
var t = e.WIRE_PROTOCOL_CONSTANTS_, n = {};
if (this.startSet_) {
n[t.INDEX_START_VALUE] = this.indexStartValue_;
this.startNameSet_ && (n[t.INDEX_START_NAME] = this.indexStartName_);
}
if (this.endSet_) {
n[t.INDEX_END_VALUE] = this.indexEndValue_;
this.endNameSet_ && (n[t.INDEX_END_NAME] = this.indexEndName_);
}
if (this.limitSet_) {
n[t.LIMIT] = this.limit_;
var r = this.viewFrom_;
"" === r && (r = this.isViewFromLeft() ? t.VIEW_FROM_LEFT : t.VIEW_FROM_RIGHT);
n[t.VIEW_FROM] = r;
}
this.index_ !== a.PRIORITY_INDEX && (n[t.INDEX] = this.index_.toString());
return n;
};
e.prototype.loadsAllData = function() {
return !(this.startSet_ || this.endSet_ || this.limitSet_);
};
e.prototype.isDefault = function() {
return this.loadsAllData() && this.index_ == a.PRIORITY_INDEX;
};
e.prototype.getNodeFilter = function() {
return this.loadsAllData() ? new c.IndexedFilter(this.getIndex()) : this.hasLimit() ? new l.LimitedFilter(this) : new h.RangedFilter(this);
};
e.prototype.toRestQueryStringParameters = function() {
var t = e.REST_QUERY_CONSTANTS_, n = {};
if (this.isDefault()) return n;
var i;
if (this.index_ === a.PRIORITY_INDEX) i = t.PRIORITY_INDEX; else if (this.index_ === s.VALUE_INDEX) i = t.VALUE_INDEX; else if (this.index_ === o.KEY_INDEX) i = t.KEY_INDEX; else {
r.assert(this.index_ instanceof u.PathIndex, "Unrecognized index type!");
i = this.index_.toString();
}
n[t.ORDER_BY] = f.stringify(i);
if (this.startSet_) {
n[t.START_AT] = f.stringify(this.indexStartValue_);
this.startNameSet_ && (n[t.START_AT] += "," + f.stringify(this.indexStartName_));
}
if (this.endSet_) {
n[t.END_AT] = f.stringify(this.indexEndValue_);
this.endNameSet_ && (n[t.END_AT] += "," + f.stringify(this.indexEndName_));
}
this.limitSet_ && (this.isViewFromLeft() ? n[t.LIMIT_TO_FIRST] = this.limit_ : n[t.LIMIT_TO_LAST] = this.limit_);
return n;
};
e.WIRE_PROTOCOL_CONSTANTS_ = {
INDEX_START_VALUE: "sp",
INDEX_START_NAME: "sn",
INDEX_END_VALUE: "ep",
INDEX_END_NAME: "en",
LIMIT: "l",
VIEW_FROM: "vf",
VIEW_FROM_LEFT: "l",
VIEW_FROM_RIGHT: "r",
INDEX: "i"
};
e.REST_QUERY_CONSTANTS_ = {
ORDER_BY: "orderBy",
PRIORITY_INDEX: "$priority",
VALUE_INDEX: "$value",
KEY_INDEX: "$key",
START_AT: "startAt",
END_AT: "endAt",
LIMIT_TO_FIRST: "limitToFirst",
LIMIT_TO_LAST: "limitToLast"
};
e.DEFAULT = new e();
return e;
}();
n.QueryParams = p;
}, {
"../snap/indexes/KeyIndex": 40,
"../snap/indexes/PathIndex": 41,
"../snap/indexes/PriorityIndex": 42,
"../snap/indexes/ValueIndex": 43,
"../util/util": 64,
"./filter/IndexedFilter": 78,
"./filter/LimitedFilter": 79,
"./filter/RangedFilter": 80,
"@firebase/util": 135
} ],
75: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./filter/IndexedFilter"), i = e("./ViewProcessor"), o = e("../snap/ChildrenNode"), a = e("./CacheNode"), s = e("./ViewCache"), u = e("./EventGenerator"), c = e("@firebase/util"), l = e("../operation/Operation"), h = e("./Change"), f = e("../snap/indexes/PriorityIndex"), p = function() {
function e(e, t) {
this.query_ = e;
this.eventRegistrations_ = [];
var n = this.query_.getQueryParams(), c = new r.IndexedFilter(n.getIndex()), l = n.getNodeFilter();
this.processor_ = new i.ViewProcessor(l);
var h = t.getServerCache(), f = t.getEventCache(), p = c.updateFullNode(o.ChildrenNode.EMPTY_NODE, h.getNode(), null), d = l.updateFullNode(o.ChildrenNode.EMPTY_NODE, f.getNode(), null), v = new a.CacheNode(p, h.isFullyInitialized(), c.filtersNodes()), _ = new a.CacheNode(d, f.isFullyInitialized(), l.filtersNodes());
this.viewCache_ = new s.ViewCache(_, v);
this.eventGenerator_ = new u.EventGenerator(this.query_);
}
e.prototype.getQuery = function() {
return this.query_;
};
e.prototype.getServerCache = function() {
return this.viewCache_.getServerCache().getNode();
};
e.prototype.getCompleteServerCache = function(e) {
var t = this.viewCache_.getCompleteServerSnap();
return t && (this.query_.getQueryParams().loadsAllData() || !e.isEmpty() && !t.getImmediateChild(e.getFront()).isEmpty()) ? t.getChild(e) : null;
};
e.prototype.isEmpty = function() {
return 0 === this.eventRegistrations_.length;
};
e.prototype.addEventRegistration = function(e) {
this.eventRegistrations_.push(e);
};
e.prototype.removeEventRegistration = function(e, t) {
var n = [];
if (t) {
c.assert(null == e, "A cancel should cancel all event registrations.");
var r = this.query_.path;
this.eventRegistrations_.forEach(function(e) {
t = t;
var i = e.createCancelEvent(t, r);
i && n.push(i);
});
}
if (e) {
for (var i = [], o = 0; o < this.eventRegistrations_.length; ++o) {
var a = this.eventRegistrations_[o];
if (a.matches(e)) {
if (e.hasAnyCallback()) {
i = i.concat(this.eventRegistrations_.slice(o + 1));
break;
}
} else i.push(a);
}
this.eventRegistrations_ = i;
} else this.eventRegistrations_ = [];
return n;
};
e.prototype.applyOperation = function(e, t, n) {
if (e.type === l.OperationType.MERGE && null !== e.source.queryId) {
c.assert(this.viewCache_.getCompleteServerSnap(), "We should always have a full cache before handling merges");
c.assert(this.viewCache_.getCompleteEventSnap(), "Missing event cache, even though we have a server cache");
}
var r = this.viewCache_, i = this.processor_.applyOperation(r, e, t, n);
this.processor_.assertIndexed(i.viewCache);
c.assert(i.viewCache.getServerCache().isFullyInitialized() || !r.getServerCache().isFullyInitialized(), "Once a server snap is complete, it should never go back");
this.viewCache_ = i.viewCache;
return this.generateEventsForChanges_(i.changes, i.viewCache.getEventCache().getNode(), null);
};
e.prototype.getInitialEvents = function(e) {
var t = this.viewCache_.getEventCache(), n = [];
t.getNode().isLeafNode() || t.getNode().forEachChild(f.PRIORITY_INDEX, function(e, t) {
n.push(h.Change.childAddedChange(e, t));
});
t.isFullyInitialized() && n.push(h.Change.valueChange(t.getNode()));
return this.generateEventsForChanges_(n, t.getNode(), e);
};
e.prototype.generateEventsForChanges_ = function(e, t, n) {
var r = n ? [ n ] : this.eventRegistrations_;
return this.eventGenerator_.generateEventsForChanges(e, t, r);
};
return e;
}();
n.View = p;
}, {
"../operation/Operation": 31,
"../snap/ChildrenNode": 33,
"../snap/indexes/PriorityIndex": 42,
"./CacheNode": 66,
"./Change": 67,
"./EventGenerator": 71,
"./ViewCache": 76,
"./ViewProcessor": 77,
"./filter/IndexedFilter": 78,
"@firebase/util": 135
} ],
76: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../snap/ChildrenNode"), i = e("./CacheNode"), o = function() {
function e(e, t) {
this.eventCache_ = e;
this.serverCache_ = t;
}
e.prototype.updateEventSnap = function(t, n, r) {
return new e(new i.CacheNode(t, n, r), this.serverCache_);
};
e.prototype.updateServerSnap = function(t, n, r) {
return new e(this.eventCache_, new i.CacheNode(t, n, r));
};
e.prototype.getEventCache = function() {
return this.eventCache_;
};
e.prototype.getCompleteEventSnap = function() {
return this.eventCache_.isFullyInitialized() ? this.eventCache_.getNode() : null;
};
e.prototype.getServerCache = function() {
return this.serverCache_;
};
e.prototype.getCompleteServerSnap = function() {
return this.serverCache_.isFullyInitialized() ? this.serverCache_.getNode() : null;
};
e.Empty = new e(new i.CacheNode(r.ChildrenNode.EMPTY_NODE, !1, !1), new i.CacheNode(r.ChildrenNode.EMPTY_NODE, !1, !1));
return e;
}();
n.ViewCache = o;
}, {
"../snap/ChildrenNode": 33,
"./CacheNode": 66
} ],
77: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../operation/Operation"), i = e("@firebase/util"), o = e("./ChildChangeAccumulator"), a = e("./Change"), s = e("../snap/ChildrenNode"), u = e("../snap/indexes/KeyIndex"), c = e("../util/ImmutableTree"), l = e("../util/Path"), h = e("./CompleteChildSource"), f = function() {
return function(e, t) {
this.viewCache = e;
this.changes = t;
};
}();
n.ProcessorResult = f;
var p = function() {
function e(e) {
this.filter_ = e;
}
e.prototype.assertIndexed = function(e) {
i.assert(e.getEventCache().getNode().isIndexed(this.filter_.getIndex()), "Event snap not indexed");
i.assert(e.getServerCache().getNode().isIndexed(this.filter_.getIndex()), "Server snap not indexed");
};
e.prototype.applyOperation = function(t, n, a, s) {
var u, c, l = new o.ChildChangeAccumulator();
if (n.type === r.OperationType.OVERWRITE) {
var h = n;
if (h.source.fromUser) u = this.applyUserOverwrite_(t, h.path, h.snap, a, s, l); else {
i.assert(h.source.fromServer, "Unknown source.");
c = h.source.tagged || t.getServerCache().isFiltered() && !h.path.isEmpty();
u = this.applyServerOverwrite_(t, h.path, h.snap, a, s, c, l);
}
} else if (n.type === r.OperationType.MERGE) {
var p = n;
if (p.source.fromUser) u = this.applyUserMerge_(t, p.path, p.children, a, s, l); else {
i.assert(p.source.fromServer, "Unknown source.");
c = p.source.tagged || t.getServerCache().isFiltered();
u = this.applyServerMerge_(t, p.path, p.children, a, s, c, l);
}
} else if (n.type === r.OperationType.ACK_USER_WRITE) {
var d = n;
u = d.revert ? this.revertUserWrite_(t, d.path, a, s, l) : this.ackUserWrite_(t, d.path, d.affectedTree, a, s, l);
} else {
if (n.type !== r.OperationType.LISTEN_COMPLETE) throw i.assertionError("Unknown operation type: " + n.type);
u = this.listenComplete_(t, n.path, a, l);
}
var v = l.getChanges();
e.maybeAddValueEvent_(t, u, v);
return new f(u, v);
};
e.maybeAddValueEvent_ = function(e, t, n) {
var r = t.getEventCache();
if (r.isFullyInitialized()) {
var i = r.getNode().isLeafNode() || r.getNode().isEmpty(), o = e.getCompleteEventSnap();
(n.length > 0 || !e.getEventCache().isFullyInitialized() || i && !r.getNode().equals(o) || !r.getNode().getPriority().equals(o.getPriority())) && n.push(a.Change.valueChange(t.getCompleteEventSnap()));
}
};
e.prototype.generateEventCacheAfterServerEvent_ = function(e, t, n, r, o) {
var a = e.getEventCache();
if (null != n.shadowingWrite(t)) return e;
var u = void 0, c = void 0;
if (t.isEmpty()) {
i.assert(e.getServerCache().isFullyInitialized(), "If change path is empty, we must have complete server data");
if (e.getServerCache().isFiltered()) {
var l = e.getCompleteServerSnap(), h = l instanceof s.ChildrenNode ? l : s.ChildrenNode.EMPTY_NODE, f = n.calcCompleteEventChildren(h);
u = this.filter_.updateFullNode(e.getEventCache().getNode(), f, o);
} else {
var p = n.calcCompleteEventCache(e.getCompleteServerSnap());
u = this.filter_.updateFullNode(e.getEventCache().getNode(), p, o);
}
} else {
var d = t.getFront();
if (".priority" == d) {
i.assert(1 == t.getLength(), "Can't have a priority with additional path components");
var v = a.getNode();
c = e.getServerCache().getNode();
var _ = n.calcEventCacheAfterServerOverwrite(t, v, c);
u = null != _ ? this.filter_.updatePriority(v, _) : a.getNode();
} else {
var y = t.popFront(), g = void 0;
if (a.isCompleteForChild(d)) {
c = e.getServerCache().getNode();
var m = n.calcEventCacheAfterServerOverwrite(t, a.getNode(), c);
g = null != m ? a.getNode().getImmediateChild(d).updateChild(y, m) : a.getNode().getImmediateChild(d);
} else g = n.calcCompleteChild(d, e.getServerCache());
u = null != g ? this.filter_.updateChild(a.getNode(), d, g, y, r, o) : a.getNode();
}
}
return e.updateEventSnap(u, a.isFullyInitialized() || t.isEmpty(), this.filter_.filtersNodes());
};
e.prototype.applyServerOverwrite_ = function(e, t, n, r, i, o, a) {
var s, u = e.getServerCache(), c = o ? this.filter_ : this.filter_.getIndexedFilter();
if (t.isEmpty()) s = c.updateFullNode(u.getNode(), n, null); else if (c.filtersNodes() && !u.isFiltered()) {
var l = u.getNode().updateChild(t, n);
s = c.updateFullNode(u.getNode(), l, null);
} else {
var f = t.getFront();
if (!u.isCompleteForPath(t) && t.getLength() > 1) return e;
var p = t.popFront(), d = u.getNode().getImmediateChild(f).updateChild(p, n);
s = ".priority" == f ? c.updatePriority(u.getNode(), d) : c.updateChild(u.getNode(), f, d, p, h.NO_COMPLETE_CHILD_SOURCE, null);
}
var v = e.updateServerSnap(s, u.isFullyInitialized() || t.isEmpty(), c.filtersNodes()), _ = new h.WriteTreeCompleteChildSource(r, v, i);
return this.generateEventCacheAfterServerEvent_(v, t, r, _, a);
};
e.prototype.applyUserOverwrite_ = function(e, t, n, r, i, o) {
var a, u, c = e.getEventCache(), l = new h.WriteTreeCompleteChildSource(r, e, i);
if (t.isEmpty()) {
u = this.filter_.updateFullNode(e.getEventCache().getNode(), n, o);
a = e.updateEventSnap(u, !0, this.filter_.filtersNodes());
} else {
var f = t.getFront();
if (".priority" === f) {
u = this.filter_.updatePriority(e.getEventCache().getNode(), n);
a = e.updateEventSnap(u, c.isFullyInitialized(), c.isFiltered());
} else {
var p = t.popFront(), d = c.getNode().getImmediateChild(f), v = void 0;
if (p.isEmpty()) v = n; else {
var _ = l.getCompleteChild(f);
v = null != _ ? ".priority" === p.getBack() && _.getChild(p.parent()).isEmpty() ? _ : _.updateChild(p, n) : s.ChildrenNode.EMPTY_NODE;
}
if (d.equals(v)) a = e; else {
var y = this.filter_.updateChild(c.getNode(), f, v, p, l, o);
a = e.updateEventSnap(y, c.isFullyInitialized(), this.filter_.filtersNodes());
}
}
}
return a;
};
e.cacheHasChild_ = function(e, t) {
return e.getEventCache().isCompleteForChild(t);
};
e.prototype.applyUserMerge_ = function(t, n, r, i, o, a) {
var s = this, u = t;
r.foreach(function(r, c) {
var l = n.child(r);
e.cacheHasChild_(t, l.getFront()) && (u = s.applyUserOverwrite_(u, l, c, i, o, a));
});
r.foreach(function(r, c) {
var l = n.child(r);
e.cacheHasChild_(t, l.getFront()) || (u = s.applyUserOverwrite_(u, l, c, i, o, a));
});
return u;
};
e.prototype.applyMerge_ = function(e, t) {
t.foreach(function(t, n) {
e = e.updateChild(t, n);
});
return e;
};
e.prototype.applyServerMerge_ = function(e, t, n, r, i, o, a) {
var s = this;
if (e.getServerCache().getNode().isEmpty() && !e.getServerCache().isFullyInitialized()) return e;
var u, h = e;
u = t.isEmpty() ? n : c.ImmutableTree.Empty.setTree(t, n);
var f = e.getServerCache().getNode();
u.children.inorderTraversal(function(t, n) {
if (f.hasChild(t)) {
var u = e.getServerCache().getNode().getImmediateChild(t), c = s.applyMerge_(u, n);
h = s.applyServerOverwrite_(h, new l.Path(t), c, r, i, o, a);
}
});
u.children.inorderTraversal(function(t, n) {
var u = !e.getServerCache().isCompleteForChild(t) && null == n.value;
if (!f.hasChild(t) && !u) {
var c = e.getServerCache().getNode().getImmediateChild(t), p = s.applyMerge_(c, n);
h = s.applyServerOverwrite_(h, new l.Path(t), p, r, i, o, a);
}
});
return h;
};
e.prototype.ackUserWrite_ = function(e, t, n, r, i, o) {
if (null != r.shadowingWrite(t)) return e;
var a = e.getServerCache().isFiltered(), s = e.getServerCache();
if (null != n.value) {
if (t.isEmpty() && s.isFullyInitialized() || s.isCompleteForPath(t)) return this.applyServerOverwrite_(e, t, s.getNode().getChild(t), r, i, a, o);
if (t.isEmpty()) {
var h = c.ImmutableTree.Empty;
s.getNode().forEachChild(u.KEY_INDEX, function(e, t) {
h = h.set(new l.Path(e), t);
});
return this.applyServerMerge_(e, t, h, r, i, a, o);
}
return e;
}
var f = c.ImmutableTree.Empty;
n.foreach(function(e, n) {
var r = t.child(e);
s.isCompleteForPath(r) && (f = f.set(e, s.getNode().getChild(r)));
});
return this.applyServerMerge_(e, t, f, r, i, a, o);
};
e.prototype.listenComplete_ = function(e, t, n, r) {
var i = e.getServerCache(), o = e.updateServerSnap(i.getNode(), i.isFullyInitialized() || t.isEmpty(), i.isFiltered());
return this.generateEventCacheAfterServerEvent_(o, t, n, h.NO_COMPLETE_CHILD_SOURCE, r);
};
e.prototype.revertUserWrite_ = function(e, t, n, r, o) {
var a;
if (null != n.shadowingWrite(t)) return e;
var u = new h.WriteTreeCompleteChildSource(n, e, r), c = e.getEventCache().getNode(), f = void 0;
if (t.isEmpty() || ".priority" === t.getFront()) {
var p = void 0;
if (e.getServerCache().isFullyInitialized()) p = n.calcCompleteEventCache(e.getCompleteServerSnap()); else {
var d = e.getServerCache().getNode();
i.assert(d instanceof s.ChildrenNode, "serverChildren would be complete if leaf node");
p = n.calcCompleteEventChildren(d);
}
p = p;
f = this.filter_.updateFullNode(c, p, o);
} else {
var v = t.getFront(), _ = n.calcCompleteChild(v, e.getServerCache());
null == _ && e.getServerCache().isCompleteForChild(v) && (_ = c.getImmediateChild(v));
(f = null != _ ? this.filter_.updateChild(c, v, _, t.popFront(), u, o) : e.getEventCache().getNode().hasChild(v) ? this.filter_.updateChild(c, v, s.ChildrenNode.EMPTY_NODE, t.popFront(), u, o) : c).isEmpty() && e.getServerCache().isFullyInitialized() && (a = n.calcCompleteEventCache(e.getCompleteServerSnap())).isLeafNode() && (f = this.filter_.updateFullNode(f, a, o));
}
a = e.getServerCache().isFullyInitialized() || null != n.shadowingWrite(l.Path.Empty);
return e.updateEventSnap(f, a, this.filter_.filtersNodes());
};
return e;
}();
n.ViewProcessor = p;
}, {
"../operation/Operation": 31,
"../snap/ChildrenNode": 33,
"../snap/indexes/KeyIndex": 40,
"../util/ImmutableTree": 55,
"../util/Path": 58,
"./Change": 67,
"./ChildChangeAccumulator": 68,
"./CompleteChildSource": 69,
"@firebase/util": 135
} ],
78: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../Change"), o = e("../../snap/ChildrenNode"), a = e("../../snap/indexes/PriorityIndex"), s = function() {
function e(e) {
this.index_ = e;
}
e.prototype.updateChild = function(e, t, n, o, a, s) {
r.assert(e.isIndexed(this.index_), "A node must be indexed if only a child is updated");
var u = e.getImmediateChild(t);
if (u.getChild(o).equals(n.getChild(o)) && u.isEmpty() == n.isEmpty()) return e;
null != s && (n.isEmpty() ? e.hasChild(t) ? s.trackChildChange(i.Change.childRemovedChange(t, u)) : r.assert(e.isLeafNode(), "A child remove without an old child only makes sense on a leaf node") : u.isEmpty() ? s.trackChildChange(i.Change.childAddedChange(t, n)) : s.trackChildChange(i.Change.childChangedChange(t, n, u)));
return e.isLeafNode() && n.isEmpty() ? e : e.updateImmediateChild(t, n).withIndex(this.index_);
};
e.prototype.updateFullNode = function(e, t, n) {
if (null != n) {
e.isLeafNode() || e.forEachChild(a.PRIORITY_INDEX, function(e, r) {
t.hasChild(e) || n.trackChildChange(i.Change.childRemovedChange(e, r));
});
t.isLeafNode() || t.forEachChild(a.PRIORITY_INDEX, function(t, r) {
if (e.hasChild(t)) {
var o = e.getImmediateChild(t);
o.equals(r) || n.trackChildChange(i.Change.childChangedChange(t, r, o));
} else n.trackChildChange(i.Change.childAddedChange(t, r));
});
}
return t.withIndex(this.index_);
};
e.prototype.updatePriority = function(e, t) {
return e.isEmpty() ? o.ChildrenNode.EMPTY_NODE : e.updatePriority(t);
};
e.prototype.filtersNodes = function() {
return !1;
};
e.prototype.getIndexedFilter = function() {
return this;
};
e.prototype.getIndex = function() {
return this.index_;
};
return e;
}();
n.IndexedFilter = s;
}, {
"../../snap/ChildrenNode": 33,
"../../snap/indexes/PriorityIndex": 42,
"../Change": 67,
"@firebase/util": 135
} ],
79: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./RangedFilter"), i = e("../../snap/ChildrenNode"), o = e("../../snap/Node"), a = e("@firebase/util"), s = e("../Change"), u = function() {
function e(e) {
this.rangedFilter_ = new r.RangedFilter(e);
this.index_ = e.getIndex();
this.limit_ = e.getLimit();
this.reverse_ = !e.isViewFromLeft();
}
e.prototype.updateChild = function(e, t, n, r, a, s) {
this.rangedFilter_.matches(new o.NamedNode(t, n)) || (n = i.ChildrenNode.EMPTY_NODE);
return e.getImmediateChild(t).equals(n) ? e : e.numChildren() < this.limit_ ? this.rangedFilter_.getIndexedFilter().updateChild(e, t, n, r, a, s) : this.fullLimitUpdateChild_(e, t, n, a, s);
};
e.prototype.updateFullNode = function(e, t, n) {
var r;
if (t.isLeafNode() || t.isEmpty()) r = i.ChildrenNode.EMPTY_NODE.withIndex(this.index_); else if (2 * this.limit_ < t.numChildren() && t.isIndexed(this.index_)) {
r = i.ChildrenNode.EMPTY_NODE.withIndex(this.index_);
u = void 0;
u = this.reverse_ ? t.getReverseIteratorFrom(this.rangedFilter_.getEndPost(), this.index_) : t.getIteratorFrom(this.rangedFilter_.getStartPost(), this.index_);
for (l = 0; u.hasNext() && l < this.limit_; ) {
f = u.getNext();
if (!(this.reverse_ ? this.index_.compare(this.rangedFilter_.getStartPost(), f) <= 0 : this.index_.compare(f, this.rangedFilter_.getEndPost()) <= 0)) break;
r = r.updateImmediateChild(f.name, f.node);
l++;
}
} else {
r = (r = t.withIndex(this.index_)).updatePriority(i.ChildrenNode.EMPTY_NODE);
var o = void 0, a = void 0, s = void 0, u = void 0;
if (this.reverse_) {
u = r.getReverseIterator(this.index_);
o = this.rangedFilter_.getEndPost();
a = this.rangedFilter_.getStartPost();
var c = this.index_.getCompare();
s = function(e, t) {
return c(t, e);
};
} else {
u = r.getIterator(this.index_);
o = this.rangedFilter_.getStartPost();
a = this.rangedFilter_.getEndPost();
s = this.index_.getCompare();
}
for (var l = 0, h = !1; u.hasNext(); ) {
var f = u.getNext();
!h && s(o, f) <= 0 && (h = !0);
h && l < this.limit_ && s(f, a) <= 0 ? l++ : r = r.updateImmediateChild(f.name, i.ChildrenNode.EMPTY_NODE);
}
}
return this.rangedFilter_.getIndexedFilter().updateFullNode(e, r, n);
};
e.prototype.updatePriority = function(e, t) {
return e;
};
e.prototype.filtersNodes = function() {
return !0;
};
e.prototype.getIndexedFilter = function() {
return this.rangedFilter_.getIndexedFilter();
};
e.prototype.getIndex = function() {
return this.index_;
};
e.prototype.fullLimitUpdateChild_ = function(e, t, n, r, u) {
var c;
if (this.reverse_) {
var l = this.index_.getCompare();
c = function(e, t) {
return l(t, e);
};
} else c = this.index_.getCompare();
var h = e;
a.assert(h.numChildren() == this.limit_, "");
var f = new o.NamedNode(t, n), p = this.reverse_ ? h.getFirstChild(this.index_) : h.getLastChild(this.index_), d = this.rangedFilter_.matches(f);
if (h.hasChild(t)) {
for (var v = h.getImmediateChild(t), _ = r.getChildAfterChild(this.index_, p, this.reverse_); null != _ && (_.name == t || h.hasChild(_.name)); ) _ = r.getChildAfterChild(this.index_, _, this.reverse_);
var y = null == _ ? 1 : c(_, f);
if (d && !n.isEmpty() && y >= 0) {
null != u && u.trackChildChange(s.Change.childChangedChange(t, n, v));
return h.updateImmediateChild(t, n);
}
null != u && u.trackChildChange(s.Change.childRemovedChange(t, v));
var g = h.updateImmediateChild(t, i.ChildrenNode.EMPTY_NODE);
if (null != _ && this.rangedFilter_.matches(_)) {
null != u && u.trackChildChange(s.Change.childAddedChange(_.name, _.node));
return g.updateImmediateChild(_.name, _.node);
}
return g;
}
if (n.isEmpty()) return e;
if (d) {
if (c(p, f) >= 0) {
if (null != u) {
u.trackChildChange(s.Change.childRemovedChange(p.name, p.node));
u.trackChildChange(s.Change.childAddedChange(t, n));
}
return h.updateImmediateChild(t, n).updateImmediateChild(p.name, i.ChildrenNode.EMPTY_NODE);
}
return e;
}
return e;
};
return e;
}();
n.LimitedFilter = u;
}, {
"../../snap/ChildrenNode": 33,
"../../snap/Node": 36,
"../Change": 67,
"./RangedFilter": 80,
"@firebase/util": 135
} ],
80: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./IndexedFilter"), i = e("../../snap/indexes/PriorityIndex"), o = e("../../../core/snap/Node"), a = e("../../snap/ChildrenNode"), s = function() {
function e(t) {
this.indexedFilter_ = new r.IndexedFilter(t.getIndex());
this.index_ = t.getIndex();
this.startPost_ = e.getStartPost_(t);
this.endPost_ = e.getEndPost_(t);
}
e.prototype.getStartPost = function() {
return this.startPost_;
};
e.prototype.getEndPost = function() {
return this.endPost_;
};
e.prototype.matches = function(e) {
return this.index_.compare(this.getStartPost(), e) <= 0 && this.index_.compare(e, this.getEndPost()) <= 0;
};
e.prototype.updateChild = function(e, t, n, r, i, s) {
this.matches(new o.NamedNode(t, n)) || (n = a.ChildrenNode.EMPTY_NODE);
return this.indexedFilter_.updateChild(e, t, n, r, i, s);
};
e.prototype.updateFullNode = function(e, t, n) {
t.isLeafNode() && (t = a.ChildrenNode.EMPTY_NODE);
var r = t.withIndex(this.index_);
r = r.updatePriority(a.ChildrenNode.EMPTY_NODE);
var s = this;
t.forEachChild(i.PRIORITY_INDEX, function(e, t) {
s.matches(new o.NamedNode(e, t)) || (r = r.updateImmediateChild(e, a.ChildrenNode.EMPTY_NODE));
});
return this.indexedFilter_.updateFullNode(e, r, n);
};
e.prototype.updatePriority = function(e, t) {
return e;
};
e.prototype.filtersNodes = function() {
return !0;
};
e.prototype.getIndexedFilter = function() {
return this.indexedFilter_;
};
e.prototype.getIndex = function() {
return this.index_;
};
e.getStartPost_ = function(e) {
if (e.hasStart()) {
var t = e.getIndexStartName();
return e.getIndex().makePost(e.getIndexStartValue(), t);
}
return e.getIndex().minPost();
};
e.getEndPost_ = function(e) {
if (e.hasEnd()) {
var t = e.getIndexEndName();
return e.getIndex().makePost(e.getIndexEndValue(), t);
}
return e.getIndex().maxPost();
};
return e;
}();
n.RangedFilter = s;
}, {
"../../../core/snap/Node": 36,
"../../snap/ChildrenNode": 33,
"../../snap/indexes/PriorityIndex": 42,
"./IndexedFilter": 78
} ],
81: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../core/util/util"), i = e("../core/util/CountedSet"), o = e("../core/stats/StatsManager"), a = e("./polling/PacketReceiver"), s = e("./Constants"), u = e("@firebase/util"), c = e("@firebase/util");
n.FIREBASE_LONGPOLL_START_PARAM = "start";
n.FIREBASE_LONGPOLL_CLOSE_COMMAND = "close";
n.FIREBASE_LONGPOLL_COMMAND_CB_NAME = "pLPCommand";
n.FIREBASE_LONGPOLL_DATA_CB_NAME = "pRTLPCB";
n.FIREBASE_LONGPOLL_ID_PARAM = "id";
n.FIREBASE_LONGPOLL_PW_PARAM = "pw";
n.FIREBASE_LONGPOLL_SERIAL_PARAM = "ser";
n.FIREBASE_LONGPOLL_CALLBACK_ID_PARAM = "cb";
n.FIREBASE_LONGPOLL_SEGMENT_NUM_PARAM = "seg";
n.FIREBASE_LONGPOLL_SEGMENTS_IN_PACKET = "ts";
n.FIREBASE_LONGPOLL_DATA_PARAM = "d";
n.FIREBASE_LONGPOLL_DISCONN_FRAME_PARAM = "disconn";
n.FIREBASE_LONGPOLL_DISCONN_FRAME_REQUEST_PARAM = "dframe";
var l = function() {
function e(e, t, n, i) {
this.connId = e;
this.repoInfo = t;
this.transportSessionId = n;
this.lastSessionId = i;
this.bytesSent = 0;
this.bytesReceived = 0;
this.everConnected_ = !1;
this.log_ = r.logWrapper(e);
this.stats_ = o.StatsManager.getCollection(t);
this.urlFn = function(e) {
return t.connectionURL(s.LONG_POLLING, e);
};
}
e.prototype.open = function(e, t) {
var i = this;
this.curSegmentNum = 0;
this.onDisconnect_ = t;
this.myPacketOrderer = new a.PacketReceiver(e);
this.isClosed_ = !1;
this.connectTimeoutTimer_ = setTimeout(function() {
i.log_("Timed out trying to connect.");
i.onClosed_();
i.connectTimeoutTimer_ = null;
}, Math.floor(3e4));
r.executeWhenDOMReady(function() {
if (!i.isClosed_) {
i.scriptTagHolder = new h(function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
var r = e[0], o = e[1], a = e[2];
e[3], e[4];
i.incrementIncomingBytes_(e);
if (i.scriptTagHolder) {
if (i.connectTimeoutTimer_) {
clearTimeout(i.connectTimeoutTimer_);
i.connectTimeoutTimer_ = null;
}
i.everConnected_ = !0;
if (r == n.FIREBASE_LONGPOLL_START_PARAM) {
i.id = o;
i.password = a;
} else {
if (r !== n.FIREBASE_LONGPOLL_CLOSE_COMMAND) throw new Error("Unrecognized command received: " + r);
if (o) {
i.scriptTagHolder.sendNewPolls = !1;
i.myPacketOrderer.closeAfter(o, function() {
i.onClosed_();
});
} else i.onClosed_();
}
}
}, function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
var n = e[0], r = e[1];
i.incrementIncomingBytes_(e);
i.myPacketOrderer.handleResponse(n, r);
}, function() {
i.onClosed_();
}, i.urlFn);
var e = {};
e[n.FIREBASE_LONGPOLL_START_PARAM] = "t";
e[n.FIREBASE_LONGPOLL_SERIAL_PARAM] = Math.floor(1e8 * Math.random());
i.scriptTagHolder.uniqueCallbackIdentifier && (e[n.FIREBASE_LONGPOLL_CALLBACK_ID_PARAM] = i.scriptTagHolder.uniqueCallbackIdentifier);
e[s.VERSION_PARAM] = s.PROTOCOL_VERSION;
i.transportSessionId && (e[s.TRANSPORT_SESSION_PARAM] = i.transportSessionId);
i.lastSessionId && (e[s.LAST_SESSION_PARAM] = i.lastSessionId);
!c.isNodeSdk() && "undefined" != typeof location && location.href && -1 !== location.href.indexOf(s.FORGE_DOMAIN) && (e[s.REFERER_PARAM] = s.FORGE_REF);
var t = i.urlFn(e);
i.log_("Connecting via long-poll to " + t);
i.scriptTagHolder.addTag(t, function() {});
}
});
};
e.prototype.start = function() {
this.scriptTagHolder.startLongPoll(this.id, this.password);
this.addDisconnectPingFrame(this.id, this.password);
};
e.forceAllow = function() {
e.forceAllow_ = !0;
};
e.forceDisallow = function() {
e.forceDisallow_ = !0;
};
e.isAvailable = function() {
return e.forceAllow_ || !e.forceDisallow_ && "undefined" != typeof document && null != document.createElement && !r.isChromeExtensionContentScript() && !r.isWindowsStoreApp() && !c.isNodeSdk();
};
e.prototype.markConnectionHealthy = function() {};
e.prototype.shutdown_ = function() {
this.isClosed_ = !0;
if (this.scriptTagHolder) {
this.scriptTagHolder.close();
this.scriptTagHolder = null;
}
if (this.myDisconnFrame) {
document.body.removeChild(this.myDisconnFrame);
this.myDisconnFrame = null;
}
if (this.connectTimeoutTimer_) {
clearTimeout(this.connectTimeoutTimer_);
this.connectTimeoutTimer_ = null;
}
};
e.prototype.onClosed_ = function() {
if (!this.isClosed_) {
this.log_("Longpoll is closing itself");
this.shutdown_();
if (this.onDisconnect_) {
this.onDisconnect_(this.everConnected_);
this.onDisconnect_ = null;
}
}
};
e.prototype.close = function() {
if (!this.isClosed_) {
this.log_("Longpoll is being closed.");
this.shutdown_();
}
};
e.prototype.send = function(e) {
var t = u.stringify(e);
this.bytesSent += t.length;
this.stats_.incrementCounter("bytes_sent", t.length);
for (var n = u.base64Encode(t), i = r.splitStringBySize(n, 1840), o = 0; o < i.length; o++) {
this.scriptTagHolder.enqueueSegment(this.curSegmentNum, i.length, i[o]);
this.curSegmentNum++;
}
};
e.prototype.addDisconnectPingFrame = function(e, t) {
if (!c.isNodeSdk()) {
this.myDisconnFrame = document.createElement("iframe");
var r = {};
r[n.FIREBASE_LONGPOLL_DISCONN_FRAME_REQUEST_PARAM] = "t";
r[n.FIREBASE_LONGPOLL_ID_PARAM] = e;
r[n.FIREBASE_LONGPOLL_PW_PARAM] = t;
this.myDisconnFrame.src = this.urlFn(r);
this.myDisconnFrame.style.display = "none";
document.body.appendChild(this.myDisconnFrame);
}
};
e.prototype.incrementIncomingBytes_ = function(e) {
var t = u.stringify(e).length;
this.bytesReceived += t;
this.stats_.incrementCounter("bytes_received", t);
};
return e;
}();
n.BrowserPollConnection = l;
var h = function() {
function e(t, o, a, s) {
this.onDisconnect = a;
this.urlFn = s;
this.outstandingRequests = new i.CountedSet();
this.pendingSegs = [];
this.currentSerial = Math.floor(1e8 * Math.random());
this.sendNewPolls = !0;
if (c.isNodeSdk()) {
this.commandCB = t;
this.onMessageCB = o;
} else {
this.uniqueCallbackIdentifier = r.LUIDGenerator();
window[n.FIREBASE_LONGPOLL_COMMAND_CB_NAME + this.uniqueCallbackIdentifier] = t;
window[n.FIREBASE_LONGPOLL_DATA_CB_NAME + this.uniqueCallbackIdentifier] = o;
this.myIFrame = e.createIFrame_();
var u = "";
this.myIFrame.src && "javascript:" === this.myIFrame.src.substr(0, "javascript:".length) && (u = '<script>document.domain="' + document.domain + '";<\/script>');
var l = "<html><body>" + u + "</body></html>";
try {
this.myIFrame.doc.open();
this.myIFrame.doc.write(l);
this.myIFrame.doc.close();
} catch (e) {
r.log("frame writing exception");
e.stack && r.log(e.stack);
r.log(e);
}
}
}
e.createIFrame_ = function() {
var e = document.createElement("iframe");
e.style.display = "none";
if (!document.body) throw "Document body has not initialized. Wait to initialize Firebase until after the document is ready.";
document.body.appendChild(e);
try {
e.contentWindow.document || r.log("No IE domain setting required");
} catch (n) {
var t = document.domain;
e.src = "javascript:void((function(){document.open();document.domain='" + t + "';document.close();})())";
}
e.contentDocument ? e.doc = e.contentDocument : e.contentWindow ? e.doc = e.contentWindow.document : e.document && (e.doc = e.document);
return e;
};
e.prototype.close = function() {
var t = this;
this.alive = !1;
if (this.myIFrame) {
this.myIFrame.doc.body.innerHTML = "";
setTimeout(function() {
if (null !== t.myIFrame) {
document.body.removeChild(t.myIFrame);
t.myIFrame = null;
}
}, Math.floor(0));
}
if (c.isNodeSdk() && this.myID) {
var r = {};
r[n.FIREBASE_LONGPOLL_DISCONN_FRAME_PARAM] = "t";
r[n.FIREBASE_LONGPOLL_ID_PARAM] = this.myID;
r[n.FIREBASE_LONGPOLL_PW_PARAM] = this.myPW;
var i = this.urlFn(r);
e.nodeRestRequest(i);
}
var o = this.onDisconnect;
if (o) {
this.onDisconnect = null;
o();
}
};
e.prototype.startLongPoll = function(e, t) {
this.myID = e;
this.myPW = t;
this.alive = !0;
for (;this.newRequest_(); ) ;
};
e.prototype.newRequest_ = function() {
if (this.alive && this.sendNewPolls && this.outstandingRequests.count() < (this.pendingSegs.length > 0 ? 2 : 1)) {
this.currentSerial++;
var e = {};
e[n.FIREBASE_LONGPOLL_ID_PARAM] = this.myID;
e[n.FIREBASE_LONGPOLL_PW_PARAM] = this.myPW;
e[n.FIREBASE_LONGPOLL_SERIAL_PARAM] = this.currentSerial;
for (var t = this.urlFn(e), r = "", i = 0; this.pendingSegs.length > 0 && this.pendingSegs[0].d.length + 30 + r.length <= 1870; ) {
var o = this.pendingSegs.shift();
r = r + "&" + n.FIREBASE_LONGPOLL_SEGMENT_NUM_PARAM + i + "=" + o.seg + "&" + n.FIREBASE_LONGPOLL_SEGMENTS_IN_PACKET + i + "=" + o.ts + "&" + n.FIREBASE_LONGPOLL_DATA_PARAM + i + "=" + o.d;
i++;
}
t += r;
this.addLongPollTag_(t, this.currentSerial);
return !0;
}
return !1;
};
e.prototype.enqueueSegment = function(e, t, n) {
this.pendingSegs.push({
seg: e,
ts: t,
d: n
});
this.alive && this.newRequest_();
};
e.prototype.addLongPollTag_ = function(e, t) {
var n = this;
this.outstandingRequests.add(t, 1);
var r = function() {
n.outstandingRequests.remove(t);
n.newRequest_();
}, i = setTimeout(r, Math.floor(25e3));
this.addTag(e, function() {
clearTimeout(i);
r();
});
};
e.prototype.addTag = function(e, t) {
var n = this;
c.isNodeSdk() ? this.doNodeLongPoll(e, t) : setTimeout(function() {
try {
if (!n.sendNewPolls) return;
var i = n.myIFrame.doc.createElement("script");
i.type = "text/javascript";
i.async = !0;
i.src = e;
i.onload = i.onreadystatechange = function() {
var e = i.readyState;
if (!e || "loaded" === e || "complete" === e) {
i.onload = i.onreadystatechange = null;
i.parentNode && i.parentNode.removeChild(i);
t();
}
};
i.onerror = function() {
r.log("Long-poll script failed to load: " + e);
n.sendNewPolls = !1;
n.close();
};
n.myIFrame.doc.body.appendChild(i);
} catch (e) {}
}, Math.floor(1));
};
return e;
}();
n.FirebaseIFrameScriptHolder = h;
}, {
"../core/stats/StatsManager": 48,
"../core/util/CountedSet": 53,
"../core/util/util": 64,
"./Constants": 83,
"./polling/PacketReceiver": 86,
"@firebase/util": 135
} ],
82: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../core/util/util"), i = e("../core/storage/storage"), o = e("./Constants"), a = e("./TransportManager"), s = function() {
function e(e, t, n, i, o, s, u) {
this.id = e;
this.repoInfo_ = t;
this.onMessage_ = n;
this.onReady_ = i;
this.onDisconnect_ = o;
this.onKill_ = s;
this.lastSessionId = u;
this.connectionCount = 0;
this.pendingDataMessages = [];
this.state_ = 0;
this.log_ = r.logWrapper("c:" + this.id + ":");
this.transportManager_ = new a.TransportManager(t);
this.log_("Connection created");
this.start_();
}
e.prototype.start_ = function() {
var e = this, t = this.transportManager_.initialTransport();
this.conn_ = new t(this.nextTransportId_(), this.repoInfo_, void 0, this.lastSessionId);
this.primaryResponsesRequired_ = t.responsesRequiredToBeHealthy || 0;
var n = this.connReceiver_(this.conn_), i = this.disconnReceiver_(this.conn_);
this.tx_ = this.conn_;
this.rx_ = this.conn_;
this.secondaryConn_ = null;
this.isHealthy_ = !1;
setTimeout(function() {
e.conn_ && e.conn_.open(n, i);
}, Math.floor(0));
var o = t.healthyTimeout || 0;
o > 0 && (this.healthyTimeout_ = r.setTimeoutNonBlocking(function() {
e.healthyTimeout_ = null;
if (!e.isHealthy_) if (e.conn_ && e.conn_.bytesReceived > 102400) {
e.log_("Connection exceeded healthy timeout but has received " + e.conn_.bytesReceived + " bytes.  Marking connection healthy.");
e.isHealthy_ = !0;
e.conn_.markConnectionHealthy();
} else if (e.conn_ && e.conn_.bytesSent > 10240) e.log_("Connection exceeded healthy timeout but has sent " + e.conn_.bytesSent + " bytes.  Leaving connection alive."); else {
e.log_("Closing unhealthy connection after timeout.");
e.close();
}
}, Math.floor(o)));
};
e.prototype.nextTransportId_ = function() {
return "c:" + this.id + ":" + this.connectionCount++;
};
e.prototype.disconnReceiver_ = function(e) {
var t = this;
return function(n) {
if (e === t.conn_) t.onConnectionLost_(n); else if (e === t.secondaryConn_) {
t.log_("Secondary connection lost.");
t.onSecondaryConnectionLost_();
} else t.log_("closing an old connection");
};
};
e.prototype.connReceiver_ = function(e) {
var t = this;
return function(n) {
2 != t.state_ && (e === t.rx_ ? t.onPrimaryMessageReceived_(n) : e === t.secondaryConn_ ? t.onSecondaryMessageReceived_(n) : t.log_("message on old connection"));
};
};
e.prototype.sendRequest = function(e) {
var t = {
t: "d",
d: e
};
this.sendData_(t);
};
e.prototype.tryCleanupConnection = function() {
if (this.tx_ === this.secondaryConn_ && this.rx_ === this.secondaryConn_) {
this.log_("cleaning up and promoting a connection: " + this.secondaryConn_.connId);
this.conn_ = this.secondaryConn_;
this.secondaryConn_ = null;
}
};
e.prototype.onSecondaryControl_ = function(e) {
if ("t" in e) {
var t = e.t;
if ("a" === t) this.upgradeIfSecondaryHealthy_(); else if ("r" === t) {
this.log_("Got a reset on secondary, closing it");
this.secondaryConn_.close();
this.tx_ !== this.secondaryConn_ && this.rx_ !== this.secondaryConn_ || this.close();
} else if ("o" === t) {
this.log_("got pong on secondary.");
this.secondaryResponsesRequired_--;
this.upgradeIfSecondaryHealthy_();
}
}
};
e.prototype.onSecondaryMessageReceived_ = function(e) {
var t = r.requireKey("t", e), n = r.requireKey("d", e);
if ("c" == t) this.onSecondaryControl_(n); else {
if ("d" != t) throw new Error("Unknown protocol layer: " + t);
this.pendingDataMessages.push(n);
}
};
e.prototype.upgradeIfSecondaryHealthy_ = function() {
if (this.secondaryResponsesRequired_ <= 0) {
this.log_("Secondary connection is healthy.");
this.isHealthy_ = !0;
this.secondaryConn_.markConnectionHealthy();
this.proceedWithUpgrade_();
} else {
this.log_("sending ping on secondary.");
this.secondaryConn_.send({
t: "c",
d: {
t: "p",
d: {}
}
});
}
};
e.prototype.proceedWithUpgrade_ = function() {
this.secondaryConn_.start();
this.log_("sending client ack on secondary");
this.secondaryConn_.send({
t: "c",
d: {
t: "a",
d: {}
}
});
this.log_("Ending transmission on primary");
this.conn_.send({
t: "c",
d: {
t: "n",
d: {}
}
});
this.tx_ = this.secondaryConn_;
this.tryCleanupConnection();
};
e.prototype.onPrimaryMessageReceived_ = function(e) {
var t = r.requireKey("t", e), n = r.requireKey("d", e);
"c" == t ? this.onControl_(n) : "d" == t && this.onDataMessage_(n);
};
e.prototype.onDataMessage_ = function(e) {
this.onPrimaryResponse_();
this.onMessage_(e);
};
e.prototype.onPrimaryResponse_ = function() {
if (!this.isHealthy_ && --this.primaryResponsesRequired_ <= 0) {
this.log_("Primary connection is healthy.");
this.isHealthy_ = !0;
this.conn_.markConnectionHealthy();
}
};
e.prototype.onControl_ = function(e) {
var t = r.requireKey("t", e);
if ("d" in e) {
var n = e.d;
if ("h" === t) this.onHandshake_(n); else if ("n" === t) {
this.log_("recvd end transmission on primary");
this.rx_ = this.secondaryConn_;
for (var i = 0; i < this.pendingDataMessages.length; ++i) this.onDataMessage_(this.pendingDataMessages[i]);
this.pendingDataMessages = [];
this.tryCleanupConnection();
} else if ("s" === t) this.onConnectionShutdown_(n); else if ("r" === t) this.onReset_(n); else if ("e" === t) r.error("Server Error: " + n); else if ("o" === t) {
this.log_("got pong on primary.");
this.onPrimaryResponse_();
this.sendPingOnPrimaryIfNecessary_();
} else r.error("Unknown control packet command: " + t);
}
};
e.prototype.onHandshake_ = function(e) {
var t = e.ts, n = e.v, i = e.h;
this.sessionId = e.s;
this.repoInfo_.updateHost(i);
if (0 == this.state_) {
this.conn_.start();
this.onConnectionEstablished_(this.conn_, t);
o.PROTOCOL_VERSION !== n && r.warn("Protocol version mismatch detected");
this.tryStartUpgrade_();
}
};
e.prototype.tryStartUpgrade_ = function() {
var e = this.transportManager_.upgradeTransport();
e && this.startUpgrade_(e);
};
e.prototype.startUpgrade_ = function(e) {
var t = this;
this.secondaryConn_ = new e(this.nextTransportId_(), this.repoInfo_, this.sessionId);
this.secondaryResponsesRequired_ = e.responsesRequiredToBeHealthy || 0;
var n = this.connReceiver_(this.secondaryConn_), i = this.disconnReceiver_(this.secondaryConn_);
this.secondaryConn_.open(n, i);
r.setTimeoutNonBlocking(function() {
if (t.secondaryConn_) {
t.log_("Timed out trying to upgrade.");
t.secondaryConn_.close();
}
}, Math.floor(6e4));
};
e.prototype.onReset_ = function(e) {
this.log_("Reset packet received.  New host: " + e);
this.repoInfo_.updateHost(e);
if (1 === this.state_) this.close(); else {
this.closeConnections_();
this.start_();
}
};
e.prototype.onConnectionEstablished_ = function(e, t) {
var n = this;
this.log_("Realtime connection established.");
this.conn_ = e;
this.state_ = 1;
if (this.onReady_) {
this.onReady_(t, this.sessionId);
this.onReady_ = null;
}
if (0 === this.primaryResponsesRequired_) {
this.log_("Primary connection is healthy.");
this.isHealthy_ = !0;
} else r.setTimeoutNonBlocking(function() {
n.sendPingOnPrimaryIfNecessary_();
}, Math.floor(5e3));
};
e.prototype.sendPingOnPrimaryIfNecessary_ = function() {
if (!this.isHealthy_ && 1 === this.state_) {
this.log_("sending ping on primary.");
this.sendData_({
t: "c",
d: {
t: "p",
d: {}
}
});
}
};
e.prototype.onSecondaryConnectionLost_ = function() {
var e = this.secondaryConn_;
this.secondaryConn_ = null;
this.tx_ !== e && this.rx_ !== e || this.close();
};
e.prototype.onConnectionLost_ = function(e) {
this.conn_ = null;
if (e || 0 !== this.state_) 1 === this.state_ && this.log_("Realtime connection lost."); else {
this.log_("Realtime connection failed.");
if (this.repoInfo_.isCacheableHost()) {
i.PersistentStorage.remove("host:" + this.repoInfo_.host);
this.repoInfo_.internalHost = this.repoInfo_.host;
}
}
this.close();
};
e.prototype.onConnectionShutdown_ = function(e) {
this.log_("Connection shutdown command received. Shutting down...");
if (this.onKill_) {
this.onKill_(e);
this.onKill_ = null;
}
this.onDisconnect_ = null;
this.close();
};
e.prototype.sendData_ = function(e) {
if (1 !== this.state_) throw "Connection is not connected";
this.tx_.send(e);
};
e.prototype.close = function() {
if (2 !== this.state_) {
this.log_("Closing realtime connection.");
this.state_ = 2;
this.closeConnections_();
if (this.onDisconnect_) {
this.onDisconnect_();
this.onDisconnect_ = null;
}
}
};
e.prototype.closeConnections_ = function() {
this.log_("Shutting down all connections");
if (this.conn_) {
this.conn_.close();
this.conn_ = null;
}
if (this.secondaryConn_) {
this.secondaryConn_.close();
this.secondaryConn_ = null;
}
if (this.healthyTimeout_) {
clearTimeout(this.healthyTimeout_);
this.healthyTimeout_ = null;
}
};
return e;
}();
n.Connection = s;
}, {
"../core/storage/storage": 52,
"../core/util/util": 64,
"./Constants": 83,
"./TransportManager": 84
} ],
83: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.PROTOCOL_VERSION = "5";
n.VERSION_PARAM = "v";
n.TRANSPORT_SESSION_PARAM = "s";
n.REFERER_PARAM = "r";
n.FORGE_REF = "f";
n.FORGE_DOMAIN = "firebaseio.com";
n.LAST_SESSION_PARAM = "ls";
n.WEBSOCKET = "websocket";
n.LONG_POLLING = "long_polling";
}, {} ],
84: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./BrowserPollConnection"), i = e("./WebSocketConnection"), o = e("../core/util/util"), a = function() {
function e(e) {
this.initTransports_(e);
}
Object.defineProperty(e, "ALL_TRANSPORTS", {
get: function() {
return [ r.BrowserPollConnection, i.WebSocketConnection ];
},
enumerable: !0,
configurable: !0
});
e.prototype.initTransports_ = function(t) {
var n = i.WebSocketConnection && i.WebSocketConnection.isAvailable(), r = n && !i.WebSocketConnection.previouslyFailed();
if (t.webSocketOnly) {
n || o.warn("wss:// URL used, but browser isn't known to support websockets.  Trying anyway.");
r = !0;
}
if (r) this.transports_ = [ i.WebSocketConnection ]; else {
var a = this.transports_ = [];
o.each(e.ALL_TRANSPORTS, function(e, t) {
t && t.isAvailable() && a.push(t);
});
}
};
e.prototype.initialTransport = function() {
if (this.transports_.length > 0) return this.transports_[0];
throw new Error("No transports available");
};
e.prototype.upgradeTransport = function() {
return this.transports_.length > 1 ? this.transports_[1] : null;
};
return e;
}();
n.TransportManager = a;
}, {
"../core/util/util": 64,
"./BrowserPollConnection": 81,
"./WebSocketConnection": 85
} ],
85: [ function(e, t, n) {
(function(t) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/app"), i = e("@firebase/util"), o = e("../core/util/util"), a = e("../core/stats/StatsManager"), s = e("./Constants"), u = e("@firebase/util"), c = e("../core/storage/storage"), l = e("@firebase/util"), h = e("@firebase/util"), f = null;
"undefined" != typeof MozWebSocket ? f = MozWebSocket : "undefined" != typeof WebSocket && (f = WebSocket);
n.setWebSocketImpl = function(e) {
f = e;
};
var p = function() {
function e(t, n, r, i) {
this.connId = t;
this.keepaliveTimer = null;
this.frames = null;
this.totalFrames = 0;
this.bytesSent = 0;
this.bytesReceived = 0;
this.log_ = o.logWrapper(this.connId);
this.stats_ = a.StatsManager.getCollection(n);
this.connURL = e.connectionURL_(n, r, i);
}
e.connectionURL_ = function(e, t, n) {
var r = {};
r[s.VERSION_PARAM] = s.PROTOCOL_VERSION;
!h.isNodeSdk() && "undefined" != typeof location && location.href && -1 !== location.href.indexOf(s.FORGE_DOMAIN) && (r[s.REFERER_PARAM] = s.FORGE_REF);
t && (r[s.TRANSPORT_SESSION_PARAM] = t);
n && (r[s.LAST_SESSION_PARAM] = n);
return e.connectionURL(s.WEBSOCKET, r);
};
e.prototype.open = function(e, n) {
var i = this;
this.onDisconnect = n;
this.onMessage = e;
this.log_("Websocket connecting to " + this.connURL);
this.everConnected_ = !1;
c.PersistentStorage.set("previous_websocket_failure", !0);
try {
if (h.isNodeSdk()) {
var o = u.CONSTANTS.NODE_ADMIN ? "AdminNode" : "Node", a = {
headers: {
"User-Agent": "Firebase/" + s.PROTOCOL_VERSION + "/" + r.default.SDK_VERSION + "/" + t.platform + "/" + o
}
}, l = t.env, p = 0 == this.connURL.indexOf("wss://") ? l.HTTPS_PROXY || l.https_proxy : l.HTTP_PROXY || l.http_proxy;
p && (a.proxy = {
origin: p
});
this.mySock = new f(this.connURL, [], a);
} else this.mySock = new f(this.connURL);
} catch (e) {
this.log_("Error instantiating WebSocket.");
var d = e.message || e.data;
d && this.log_(d);
this.onClosed_();
return;
}
this.mySock.onopen = function() {
i.log_("Websocket connected.");
i.everConnected_ = !0;
};
this.mySock.onclose = function() {
i.log_("Websocket connection was disconnected.");
i.mySock = null;
i.onClosed_();
};
this.mySock.onmessage = function(e) {
i.handleIncomingFrame(e);
};
this.mySock.onerror = function(e) {
i.log_("WebSocket error.  Closing connection.");
var t = e.message || e.data;
t && i.log_(t);
i.onClosed_();
};
};
e.prototype.start = function() {};
e.forceDisallow = function() {
e.forceDisallow_ = !0;
};
e.isAvailable = function() {
var t = !1;
if ("undefined" != typeof navigator && navigator.userAgent) {
var n = /Android ([0-9]{0,}\.[0-9]{0,})/, r = navigator.userAgent.match(n);
r && r.length > 1 && parseFloat(r[1]) < 4.4 && (t = !0);
}
return !t && null !== f && !e.forceDisallow_;
};
e.previouslyFailed = function() {
return c.PersistentStorage.isInMemoryStorage || !0 === c.PersistentStorage.get("previous_websocket_failure");
};
e.prototype.markConnectionHealthy = function() {
c.PersistentStorage.remove("previous_websocket_failure");
};
e.prototype.appendFrame_ = function(e) {
this.frames.push(e);
if (this.frames.length == this.totalFrames) {
var t = this.frames.join("");
this.frames = null;
var n = l.jsonEval(t);
this.onMessage(n);
}
};
e.prototype.handleNewFrameCount_ = function(e) {
this.totalFrames = e;
this.frames = [];
};
e.prototype.extractFrameCount_ = function(e) {
i.assert(null === this.frames, "We already have a frame buffer");
if (e.length <= 6) {
var t = Number(e);
if (!isNaN(t)) {
this.handleNewFrameCount_(t);
return null;
}
}
this.handleNewFrameCount_(1);
return e;
};
e.prototype.handleIncomingFrame = function(e) {
if (null !== this.mySock) {
var t = e.data;
this.bytesReceived += t.length;
this.stats_.incrementCounter("bytes_received", t.length);
this.resetKeepAlive();
if (null !== this.frames) this.appendFrame_(t); else {
var n = this.extractFrameCount_(t);
null !== n && this.appendFrame_(n);
}
}
};
e.prototype.send = function(e) {
this.resetKeepAlive();
var t = l.stringify(e);
this.bytesSent += t.length;
this.stats_.incrementCounter("bytes_sent", t.length);
var n = o.splitStringBySize(t, 16384);
n.length > 1 && this.sendString_(String(n.length));
for (var r = 0; r < n.length; r++) this.sendString_(n[r]);
};
e.prototype.shutdown_ = function() {
this.isClosed_ = !0;
if (this.keepaliveTimer) {
clearInterval(this.keepaliveTimer);
this.keepaliveTimer = null;
}
if (this.mySock) {
this.mySock.close();
this.mySock = null;
}
};
e.prototype.onClosed_ = function() {
if (!this.isClosed_) {
this.log_("WebSocket is closing itself");
this.shutdown_();
if (this.onDisconnect) {
this.onDisconnect(this.everConnected_);
this.onDisconnect = null;
}
}
};
e.prototype.close = function() {
if (!this.isClosed_) {
this.log_("WebSocket is being closed");
this.shutdown_();
}
};
e.prototype.resetKeepAlive = function() {
var e = this;
clearInterval(this.keepaliveTimer);
this.keepaliveTimer = setInterval(function() {
e.mySock && e.sendString_("0");
e.resetKeepAlive();
}, Math.floor(45e3));
};
e.prototype.sendString_ = function(e) {
try {
this.mySock.send(e);
} catch (e) {
this.log_("Exception thrown from WebSocket.send():", e.message || e.data, "Closing connection.");
setTimeout(this.onClosed_.bind(this), 0);
}
};
e.responsesRequiredToBeHealthy = 2;
e.healthyTimeout = 3e4;
return e;
}();
n.WebSocketConnection = p;
}).call(this, e("_process"));
}, {
"../core/stats/StatsManager": 48,
"../core/storage/storage": 52,
"../core/util/util": 64,
"./Constants": 83,
"@firebase/app": 2,
"@firebase/util": 135,
_process: 1
} ],
86: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("../../core/util/util"), i = function() {
function e(e) {
this.onMessage_ = e;
this.pendingResponses = [];
this.currentResponseNum = 0;
this.closeAfterResponse = -1;
this.onClose = null;
}
e.prototype.closeAfter = function(e, t) {
this.closeAfterResponse = e;
this.onClose = t;
if (this.closeAfterResponse < this.currentResponseNum) {
this.onClose();
this.onClose = null;
}
};
e.prototype.handleResponse = function(e, t) {
var n = this;
this.pendingResponses[e] = t;
for (var i = this; this.pendingResponses[this.currentResponseNum] && "break" !== function() {
var e = i.pendingResponses[i.currentResponseNum];
delete i.pendingResponses[i.currentResponseNum];
for (var t = 0; t < e.length; ++t) !function(t) {
e[t] && r.exceptionGuard(function() {
n.onMessage_(e[t]);
});
}(t);
if (i.currentResponseNum === i.closeAfterResponse) {
if (i.onClose) {
i.onClose();
i.onClose = null;
}
return "break";
}
i.currentResponseNum++;
}(); ) ;
};
return e;
}();
n.PacketReceiver = i;
}, {
"../../core/util/util": 64
} ],
87: [ function(e, t, n) {
"use strict";
function r(e) {
var t = {
Messaging: i.default
};
e.INTERNAL.registerService("messaging", function(e) {
return self && "ServiceWorkerGlobalScope" in self ? new o.default(e) : new i.default(e);
}, t);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./src/controllers/window-controller"), o = e("./src/controllers/sw-controller"), a = e("@firebase/app");
n.registerMessaging = r;
r(a.firebase);
}, {
"./src/controllers/sw-controller": 89,
"./src/controllers/window-controller": 90,
"@firebase/app": 2
} ],
88: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("../models/errors"), o = e("../models/token-manager"), a = e("../models/notification-permission"), s = "messagingSenderId", u = function() {
function e(e) {
var t = this;
this.errorFactory_ = new r.ErrorFactory("messaging", "Messaging", i.default.map);
if (!e.options[s] || "string" != typeof e.options[s]) throw this.errorFactory_.create(i.default.codes.BAD_SENDER_ID);
this.messagingSenderId_ = e.options[s];
this.tokenManager_ = new o.default();
this.app = e;
this.INTERNAL = {};
this.INTERNAL.delete = function() {
return t.delete;
};
}
e.prototype.getToken = function() {
var e = this, t = this.getNotificationPermission_();
return t !== a.default.granted ? t === a.default.denied ? Promise.reject(this.errorFactory_.create(i.default.codes.NOTIFICATIONS_BLOCKED)) : Promise.resolve(null) : this.getSWRegistration_().then(function(t) {
return e.tokenManager_.getSavedToken(e.messagingSenderId_, t).then(function(n) {
return n || e.tokenManager_.createToken(e.messagingSenderId_, t);
});
});
};
e.prototype.deleteToken = function(e) {
var t = this;
return this.tokenManager_.deleteToken(e).then(function() {
return t.getSWRegistration_().then(function(e) {
if (e) return e.pushManager.getSubscription();
}).then(function(e) {
if (e) return e.unsubscribe();
});
});
};
e.prototype.getSWRegistration_ = function() {
throw this.errorFactory_.create(i.default.codes.SHOULD_BE_INHERITED);
};
e.prototype.requestPermission = function() {
throw this.errorFactory_.create(i.default.codes.AVAILABLE_IN_WINDOW);
};
e.prototype.useServiceWorker = function(e) {
throw this.errorFactory_.create(i.default.codes.AVAILABLE_IN_WINDOW);
};
e.prototype.onMessage = function(e, t, n) {
throw this.errorFactory_.create(i.default.codes.AVAILABLE_IN_WINDOW);
};
e.prototype.onTokenRefresh = function(e, t, n) {
throw this.errorFactory_.create(i.default.codes.AVAILABLE_IN_WINDOW);
};
e.prototype.setBackgroundMessageHandler = function(e) {
throw this.errorFactory_.create(i.default.codes.AVAILABLE_IN_SW);
};
e.prototype.delete = function() {
return this.tokenManager_.closeDatabase();
};
e.prototype.getNotificationPermission_ = function() {
return Notification.permission;
};
e.prototype.getTokenManager = function() {
return this.tokenManager_;
};
return e;
}();
n.default = u;
}, {
"../models/errors": 93,
"../models/notification-permission": 95,
"../models/token-manager": 96,
"@firebase/util": 135
} ],
89: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./controller-interface"), o = e("../models/errors"), a = e("../models/worker-page-message"), s = e("../models/fcm-details"), u = function(e) {
function t(t) {
var n = e.call(this, t) || this;
self.addEventListener("push", function(e) {
return n.onPush_(e);
}, !1);
self.addEventListener("pushsubscriptionchange", function(e) {
return n.onSubChange_(e);
}, !1);
self.addEventListener("notificationclick", function(e) {
return n.onNotificationClick_(e);
}, !1);
n.bgMessageHandler_ = null;
return n;
}
r(t, e);
t.prototype.onPush_ = function(e) {
var t, n = this;
try {
t = e.data.json();
} catch (e) {
return;
}
var r = this.hasVisibleClients_().then(function(e) {
if (e) {
if (t.notification || n.bgMessageHandler_) return n.sendMessageToWindowClients_(t);
} else {
var r = n.getNotificationData_(t);
if (r) {
var i = r.title || "";
return self.registration.showNotification(i, r);
}
if (n.bgMessageHandler_) return n.bgMessageHandler_(t);
}
});
e.waitUntil(r);
};
t.prototype.onSubChange_ = function(e) {
var t = this, n = this.getToken().then(function(e) {
if (!e) throw t.errorFactory_.create(o.default.codes.NO_FCM_TOKEN_FOR_RESUBSCRIBE);
var n = null, r = t.getTokenManager();
return r.getTokenDetailsFromToken(e).then(function(e) {
if (!(n = e)) throw t.errorFactory_.create(o.default.codes.INVALID_SAVED_TOKEN);
return self.registration.pushManager.subscribe(s.default.SUBSCRIPTION_OPTIONS);
}).then(function(e) {
return r.subscribeToFCM(n.fcmSenderId, e, n.fcmPushSet);
}).catch(function(e) {
return r.deleteToken(n.fcmToken).then(function() {
throw t.errorFactory_.create(o.default.codes.UNABLE_TO_RESUBSCRIBE, {
message: e
});
});
});
});
e.waitUntil(n);
};
t.prototype.onNotificationClick_ = function(e) {
var t = this;
if (e.notification && e.notification.data && e.notification.data.FCM_MSG) {
e.stopImmediatePropagation();
e.notification.close();
var n = e.notification.data.FCM_MSG, r = n.notification.click_action;
if (r) {
var i = this.getWindowClient_(r).then(function(e) {
return e || self.clients.openWindow(r);
}).then(function(e) {
if (e) {
n.notification;
delete n.notification;
var r = a.default.createNewMsg(a.default.TYPES_OF_MSG.NOTIFICATION_CLICKED, n);
return t.attemptToMessageClient_(e, r);
}
});
e.waitUntil(i);
}
}
};
t.prototype.getNotificationData_ = function(e) {
if (e && "object" == typeof e.notification) {
var t = Object.assign({}, e.notification);
t.data = (n = {}, n.FCM_MSG = e, n);
return t;
var n;
}
};
t.prototype.setBackgroundMessageHandler = function(e) {
if (e && "function" != typeof e) throw this.errorFactory_.create(o.default.codes.BG_HANDLER_FUNCTION_EXPECTED);
this.bgMessageHandler_ = e;
};
t.prototype.getWindowClient_ = function(e) {
var t = new URL(e).href;
return self.clients.matchAll({
type: "window",
includeUncontrolled: !0
}).then(function(e) {
for (var n = null, r = 0; r < e.length; r++) if (new URL(e[r].url).href === t) {
n = e[r];
break;
}
if (n) {
n.focus();
return n;
}
});
};
t.prototype.attemptToMessageClient_ = function(e, t) {
var n = this;
return new Promise(function(r, i) {
if (!e) return i(n.errorFactory_.create(o.default.codes.NO_WINDOW_CLIENT_TO_MSG));
e.postMessage(t);
r();
});
};
t.prototype.hasVisibleClients_ = function() {
return self.clients.matchAll({
type: "window",
includeUncontrolled: !0
}).then(function(e) {
return e.some(function(e) {
return "visible" === e.visibilityState;
});
});
};
t.prototype.sendMessageToWindowClients_ = function(e) {
var t = this;
return self.clients.matchAll({
type: "window",
includeUncontrolled: !0
}).then(function(n) {
var r = a.default.createNewMsg(a.default.TYPES_OF_MSG.PUSH_MSG_RECEIVED, e);
return Promise.all(n.map(function(e) {
return t.attemptToMessageClient_(e, r);
}));
});
};
t.prototype.getSWRegistration_ = function() {
return Promise.resolve(self.registration);
};
return t;
}(i.default);
n.default = u;
}, {
"../models/errors": 93,
"../models/fcm-details": 94,
"../models/worker-page-message": 97,
"./controller-interface": 88
} ],
90: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./controller-interface"), o = e("../models/errors"), a = e("../models/worker-page-message"), s = e("../models/default-sw"), u = e("../models/notification-permission"), c = e("@firebase/util"), l = function(e) {
function t(t) {
var n = e.call(this, t) || this;
n.messageObserver_ = null;
n.onMessage_ = c.createSubscribe(function(e) {
n.messageObserver_ = e;
});
n.tokenRefreshObserver_ = null;
n.onTokenRefresh_ = c.createSubscribe(function(e) {
n.tokenRefreshObserver_ = e;
});
n.setupSWMessageListener_();
return n;
}
r(t, e);
t.prototype.getToken = function() {
var t = this;
return this.isSupported_() ? this.manifestCheck_().then(function() {
return e.prototype.getToken.call(t);
}) : Promise.reject(this.errorFactory_.create(o.default.codes.UNSUPPORTED_BROWSER));
};
t.prototype.manifestCheck_ = function() {
var e = this;
if (this.manifestCheckPromise_) return this.manifestCheckPromise_;
var t = document.querySelector('link[rel="manifest"]');
this.manifestCheckPromise_ = t ? fetch(t.href).then(function(e) {
return e.json();
}).catch(function() {
return Promise.resolve();
}).then(function(t) {
if (t && t.gcm_sender_id && "103953800507" !== t.gcm_sender_id) throw e.errorFactory_.create(o.default.codes.INCORRECT_GCM_SENDER_ID);
}) : Promise.resolve();
return this.manifestCheckPromise_;
};
t.prototype.requestPermission = function() {
var e = this;
return Notification.permission === u.default.granted ? Promise.resolve() : new Promise(function(t, n) {
var r = function(r) {
return r === u.default.granted ? t() : n(r === u.default.denied ? e.errorFactory_.create(o.default.codes.PERMISSION_BLOCKED) : e.errorFactory_.create(o.default.codes.PERMISSION_DEFAULT));
}, i = Notification.requestPermission(function(e) {
i || r(e);
});
i && i.then(r);
});
};
t.prototype.useServiceWorker = function(e) {
if (!(e instanceof ServiceWorkerRegistration)) throw this.errorFactory_.create(o.default.codes.SW_REGISTRATION_EXPECTED);
if ("undefined" != typeof this.registrationToUse_) throw this.errorFactory_.create(o.default.codes.USE_SW_BEFORE_GET_TOKEN);
this.registrationToUse_ = e;
};
t.prototype.onMessage = function(e, t, n) {
return this.onMessage_(e, t, n);
};
t.prototype.onTokenRefresh = function(e, t, n) {
return this.onTokenRefresh_(e, t, n);
};
t.prototype.waitForRegistrationToActivate_ = function(e) {
var t = this, n = e.installing || e.waiting || e.active;
return new Promise(function(r, i) {
if (n) if ("activated" !== n.state) if ("redundant" !== n.state) {
var a = function() {
if ("activated" === n.state) r(e); else {
if ("redundant" !== n.state) return;
i(t.errorFactory_.create(o.default.codes.SW_REG_REDUNDANT));
}
n.removeEventListener("statechange", a);
};
n.addEventListener("statechange", a);
} else i(t.errorFactory_.create(o.default.codes.SW_REG_REDUNDANT)); else r(e); else i(t.errorFactory_.create(o.default.codes.NO_SW_IN_REG));
});
};
t.prototype.getSWRegistration_ = function() {
var e = this;
if (this.registrationToUse_) return this.waitForRegistrationToActivate_(this.registrationToUse_);
this.registrationToUse_ = null;
return navigator.serviceWorker.register(s.default.path, {
scope: s.default.scope
}).catch(function(t) {
throw e.errorFactory_.create(o.default.codes.FAILED_DEFAULT_REGISTRATION, {
browserErrorMessage: t.message
});
}).then(function(t) {
return e.waitForRegistrationToActivate_(t).then(function() {
e.registrationToUse_ = t;
t.update();
return t;
});
});
};
t.prototype.setupSWMessageListener_ = function() {
var e = this;
"serviceWorker" in navigator && navigator.serviceWorker.addEventListener("message", function(t) {
if (t.data && t.data[a.default.PARAMS.TYPE_OF_MSG]) {
var n = t.data;
switch (n[a.default.PARAMS.TYPE_OF_MSG]) {
case a.default.TYPES_OF_MSG.PUSH_MSG_RECEIVED:
case a.default.TYPES_OF_MSG.NOTIFICATION_CLICKED:
var r = n[a.default.PARAMS.DATA];
e.messageObserver_.next(r);
}
}
}, !1);
};
t.prototype.isSupported_ = function() {
return "serviceWorker" in navigator && "PushManager" in window && "Notification" in window && "fetch" in window && ServiceWorkerRegistration.prototype.hasOwnProperty("showNotification") && PushSubscription.prototype.hasOwnProperty("getKey");
};
return t;
}(i.default);
n.default = l;
}, {
"../models/default-sw": 92,
"../models/errors": 93,
"../models/notification-permission": 95,
"../models/worker-page-message": 97,
"./controller-interface": 88,
"@firebase/util": 135
} ],
91: [ function(e, t, n) {
"use strict";
function r(e) {
var t = new Uint8Array(e);
return window.btoa(String.fromCharCode.apply(null, t));
}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.default = function(e) {
return r(e).replace(/=/g, "").replace(/\+/g, "-").replace(/\//g, "_");
};
}, {} ],
92: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.default = {
path: "/firebase-messaging-sw.js",
scope: "/firebase-cloud-messaging-push-scope"
};
}, {} ],
93: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = {
AVAILABLE_IN_WINDOW: "only-available-in-window",
AVAILABLE_IN_SW: "only-available-in-sw",
SHOULD_BE_INHERITED: "should-be-overriden",
BAD_SENDER_ID: "bad-sender-id",
INCORRECT_GCM_SENDER_ID: "incorrect-gcm-sender-id",
PERMISSION_DEFAULT: "permission-default",
PERMISSION_BLOCKED: "permission-blocked",
UNSUPPORTED_BROWSER: "unsupported-browser",
NOTIFICATIONS_BLOCKED: "notifications-blocked",
FAILED_DEFAULT_REGISTRATION: "failed-serviceworker-registration",
SW_REGISTRATION_EXPECTED: "sw-registration-expected",
GET_SUBSCRIPTION_FAILED: "get-subscription-failed",
INVALID_SAVED_TOKEN: "invalid-saved-token",
SW_REG_REDUNDANT: "sw-reg-redundant",
TOKEN_SUBSCRIBE_FAILED: "token-subscribe-failed",
TOKEN_SUBSCRIBE_NO_TOKEN: "token-subscribe-no-token",
TOKEN_SUBSCRIBE_NO_PUSH_SET: "token-subscribe-no-push-set",
USE_SW_BEFORE_GET_TOKEN: "use-sw-before-get-token",
INVALID_DELETE_TOKEN: "invalid-delete-token",
DELETE_TOKEN_NOT_FOUND: "delete-token-not-found",
DELETE_SCOPE_NOT_FOUND: "delete-scope-not-found",
BG_HANDLER_FUNCTION_EXPECTED: "bg-handler-function-expected",
NO_WINDOW_CLIENT_TO_MSG: "no-window-client-to-msg",
UNABLE_TO_RESUBSCRIBE: "unable-to-resubscribe",
NO_FCM_TOKEN_FOR_RESUBSCRIBE: "no-fcm-token-for-resubscribe",
FAILED_TO_DELETE_TOKEN: "failed-to-delete-token",
NO_SW_IN_REG: "no-sw-in-reg",
BAD_SCOPE: "bad-scope",
BAD_VAPID_KEY: "bad-vapid-key",
BAD_SUBSCRIPTION: "bad-subscription",
BAD_TOKEN: "bad-token",
BAD_PUSH_SET: "bad-push-set",
FAILED_DELETE_VAPID_KEY: "failed-delete-vapid-key"
}, i = (o = {}, o[r.AVAILABLE_IN_WINDOW] = "This method is available in a Window context.", 
o[r.AVAILABLE_IN_SW] = "This method is available in a service worker context.", 
o[r.SHOULD_BE_INHERITED] = "This method should be overriden by extended classes.", 
o[r.BAD_SENDER_ID] = "Please ensure that 'messagingSenderId' is set correctly in the options passed into firebase.initializeApp().", 
o[r.PERMISSION_DEFAULT] = "The required permissions were not granted and dismissed instead.", 
o[r.PERMISSION_BLOCKED] = "The required permissions were not granted and blocked instead.", 
o[r.UNSUPPORTED_BROWSER] = "This browser doesn't support the API's required to use the firebase SDK.", 
o[r.NOTIFICATIONS_BLOCKED] = "Notifications have been blocked.", o[r.FAILED_DEFAULT_REGISTRATION] = "We are unable to register the default service worker. {$browserErrorMessage}", 
o[r.SW_REGISTRATION_EXPECTED] = "A service worker registration was the expected input.", 
o[r.GET_SUBSCRIPTION_FAILED] = "There was an error when trying to get any existing Push Subscriptions.", 
o[r.INVALID_SAVED_TOKEN] = "Unable to access details of the saved token.", o[r.SW_REG_REDUNDANT] = "The service worker being used for push was made redundant.", 
o[r.TOKEN_SUBSCRIBE_FAILED] = "A problem occured while subscribing the user to FCM: {$message}", 
o[r.TOKEN_SUBSCRIBE_NO_TOKEN] = "FCM returned no token when subscribing the user to push.", 
o[r.TOKEN_SUBSCRIBE_NO_PUSH_SET] = "FCM returned an invalid response when getting an FCM token.", 
o[r.USE_SW_BEFORE_GET_TOKEN] = "You must call useServiceWorker() before calling getToken() to ensure your service worker is used.", 
o[r.INVALID_DELETE_TOKEN] = "You must pass a valid token into deleteToken(), i.e. the token from getToken().", 
o[r.DELETE_TOKEN_NOT_FOUND] = "The deletion attempt for token could not be performed as the token was not found.", 
o[r.DELETE_SCOPE_NOT_FOUND] = "The deletion attempt for service worker scope could not be performed as the scope was not found.", 
o[r.BG_HANDLER_FUNCTION_EXPECTED] = "The input to setBackgroundMessageHandler() must be a function.", 
o[r.NO_WINDOW_CLIENT_TO_MSG] = "An attempt was made to message a non-existant window client.", 
o[r.UNABLE_TO_RESUBSCRIBE] = "There was an error while re-subscribing the FCM token for push messaging. Will have to resubscribe the user on next visit. {$message}", 
o[r.NO_FCM_TOKEN_FOR_RESUBSCRIBE] = "Could not find an FCM token and as a result, unable to resubscribe. Will have to resubscribe the user on next visit.", 
o[r.FAILED_TO_DELETE_TOKEN] = "Unable to delete the currently saved token.", o[r.NO_SW_IN_REG] = "Even though the service worker registration was successful, there was a problem accessing the service worker itself.", 
o[r.INCORRECT_GCM_SENDER_ID] = "Please change your web app manifest's 'gcm_sender_id' value to '103953800507' to use Firebase messaging.", 
o[r.BAD_SCOPE] = "The service worker scope must be a string with at least one character.", 
o[r.BAD_VAPID_KEY] = "The public VAPID key must be a string with at least one character.", 
o[r.BAD_SUBSCRIPTION] = "The subscription must be a valid PushSubscription.", o[r.BAD_TOKEN] = "The FCM Token used for storage / lookup was not a valid token string.", 
o[r.BAD_PUSH_SET] = "The FCM push set used for storage / lookup was not not a valid push set string.", 
o[r.FAILED_DELETE_VAPID_KEY] = "The VAPID key could not be deleted.", o);
n.default = {
codes: r,
map: i
};
var o;
}, {} ],
94: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = [ 4, 51, 148, 247, 223, 161, 235, 177, 220, 3, 162, 94, 21, 113, 219, 72, 211, 46, 237, 237, 178, 52, 219, 183, 71, 58, 12, 143, 196, 204, 225, 111, 60, 140, 132, 223, 171, 182, 102, 62, 242, 12, 212, 139, 254, 227, 249, 118, 47, 20, 28, 99, 8, 106, 111, 45, 177, 26, 149, 176, 206, 55, 192, 156, 110 ], i = {
userVisibleOnly: !0,
applicationServerKey: new Uint8Array(r)
};
n.default = {
ENDPOINT: "https://fcm.googleapis.com",
APPLICATION_SERVER_KEY: r,
SUBSCRIPTION_OPTIONS: i
};
}, {} ],
95: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.default = {
granted: "granted",
default: "default",
denied: "denied"
};
}, {} ],
96: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("@firebase/util"), i = e("./errors"), o = e("../helpers/array-buffer-to-base64"), a = e("./fcm-details"), s = "fcm_token_object_Store", u = function() {
function e() {
this.errorFactory_ = new r.ErrorFactory("messaging", "Messaging", i.default.map);
this.openDbPromise_ = null;
}
e.prototype.openDatabase_ = function() {
if (this.openDbPromise_) return this.openDbPromise_;
this.openDbPromise_ = new Promise(function(t, n) {
var r = indexedDB.open(e.DB_NAME, 1);
r.onerror = function(e) {
n(e.target.error);
};
r.onsuccess = function(e) {
t(e.target.result);
};
r.onupgradeneeded = function(e) {
var t = e.target.result.createObjectStore(s, {
keyPath: "swScope"
});
t.createIndex("fcmSenderId", "fcmSenderId", {
unique: !1
});
t.createIndex("fcmToken", "fcmToken", {
unique: !0
});
};
});
return this.openDbPromise_;
};
e.prototype.closeDatabase = function() {
var e = this;
return this.openDbPromise_ ? this.openDbPromise_.then(function(t) {
t.close();
e.openDbPromise_ = null;
}) : Promise.resolve();
};
e.prototype.getTokenDetailsFromToken = function(e) {
return this.openDatabase_().then(function(t) {
return new Promise(function(n, r) {
var i = t.transaction([ s ]).objectStore(s).index("fcmToken").get(e);
i.onerror = function(e) {
r(e.target.error);
};
i.onsuccess = function(e) {
n(e.target.result);
};
});
});
};
e.prototype.getTokenDetailsFromSWScope_ = function(e) {
return this.openDatabase_().then(function(t) {
return new Promise(function(n, r) {
var i = t.transaction([ s ]).objectStore(s).get(e);
i.onerror = function(e) {
r(e.target.error);
};
i.onsuccess = function(e) {
n(e.target.result);
};
});
});
};
e.prototype.getAllTokenDetailsForSenderId_ = function(e) {
return this.openDatabase_().then(function(t) {
return new Promise(function(n, r) {
var i = [], o = t.transaction([ s ]).objectStore(s).openCursor();
o.onerror = function(e) {
r(e.target.error);
};
o.onsuccess = function(t) {
var r = t.target.result;
if (r) {
r.value.fcmSenderId === e && i.push(r.value);
r.continue();
} else n(i);
};
});
});
};
e.prototype.subscribeToFCM = function(e, t, n) {
var r = this, s = o.default(t.getKey("p256dh")), u = o.default(t.getKey("auth")), c = "authorized_entity=" + e + "&endpoint=" + t.endpoint + "&encryption_key=" + s + "&encryption_auth=" + u;
n && (c += "&pushSet=" + n);
var l = new Headers();
l.append("Content-Type", "application/x-www-form-urlencoded");
var h = {
method: "POST",
headers: l,
body: c
};
return fetch(a.default.ENDPOINT + "/fcm/connect/subscribe", h).then(function(e) {
return e.json();
}).then(function(e) {
var t = e;
if (t.error) {
var n = t.error.message;
throw r.errorFactory_.create(i.default.codes.TOKEN_SUBSCRIBE_FAILED, {
message: n
});
}
if (!t.token) throw r.errorFactory_.create(i.default.codes.TOKEN_SUBSCRIBE_NO_TOKEN);
if (!t.pushSet) throw r.errorFactory_.create(i.default.codes.TOKEN_SUBSCRIBE_NO_PUSH_SET);
return {
token: t.token,
pushSet: t.pushSet
};
});
};
e.prototype.isSameSubscription_ = function(e, t) {
return e.endpoint === t.endpoint && o.default(e.getKey("auth")) === t.auth && o.default(e.getKey("p256dh")) === t.p256dh;
};
e.prototype.saveTokenDetails_ = function(e, t, n, r, i) {
var a = {
swScope: t.scope,
endpoint: n.endpoint,
auth: o.default(n.getKey("auth")),
p256dh: o.default(n.getKey("p256dh")),
fcmToken: r,
fcmPushSet: i,
fcmSenderId: e
};
return this.openDatabase_().then(function(e) {
return new Promise(function(t, n) {
var r = e.transaction([ s ], "readwrite").objectStore(s).put(a);
r.onerror = function(e) {
n(e.target.error);
};
r.onsuccess = function(e) {
t();
};
});
});
};
e.prototype.getSavedToken = function(e, t) {
var n = this;
return t instanceof ServiceWorkerRegistration ? "string" != typeof e || 0 === e.length ? Promise.reject(this.errorFactory_.create(i.default.codes.BAD_SENDER_ID)) : this.getAllTokenDetailsForSenderId_(e).then(function(n) {
if (0 !== n.length) {
var r = n.findIndex(function(n) {
return t.scope === n.swScope && e === n.fcmSenderId;
});
if (-1 !== r) return n[r];
}
}).then(function(e) {
if (e) return t.pushManager.getSubscription().catch(function(e) {
throw n.errorFactory_.create(i.default.codes.GET_SUBSCRIPTION_FAILED);
}).then(function(t) {
if (t && n.isSameSubscription_(t, e)) return e.fcmToken;
});
}) : Promise.reject(this.errorFactory_.create(i.default.codes.SW_REGISTRATION_EXPECTED));
};
e.prototype.createToken = function(e, t) {
var n = this;
if ("string" != typeof e || 0 === e.length) return Promise.reject(this.errorFactory_.create(i.default.codes.BAD_SENDER_ID));
if (!(t instanceof ServiceWorkerRegistration)) return Promise.reject(this.errorFactory_.create(i.default.codes.SW_REGISTRATION_EXPECTED));
var r, o;
return t.pushManager.getSubscription().then(function(e) {
return e || t.pushManager.subscribe(a.default.SUBSCRIPTION_OPTIONS);
}).then(function(t) {
r = t;
return n.subscribeToFCM(e, r);
}).then(function(i) {
o = i;
return n.saveTokenDetails_(e, t, r, o.token, o.pushSet);
}).then(function() {
return o.token;
});
};
e.prototype.deleteToken = function(e) {
var t = this;
return "string" != typeof e || 0 === e.length ? Promise.reject(this.errorFactory_.create(i.default.codes.INVALID_DELETE_TOKEN)) : this.getTokenDetailsFromToken(e).then(function(e) {
if (!e) throw t.errorFactory_.create(i.default.codes.DELETE_TOKEN_NOT_FOUND);
return t.openDatabase_().then(function(n) {
return new Promise(function(r, o) {
var a = n.transaction([ s ], "readwrite").objectStore(s).delete(e.swScope);
a.onerror = function(e) {
o(e.target.error);
};
a.onsuccess = function(n) {
0 !== n.target.result ? r(e) : o(t.errorFactory_.create(i.default.codes.FAILED_TO_DELETE_TOKEN));
};
});
});
});
};
return e;
}();
n.default = u;
}, {
"../helpers/array-buffer-to-base64": 91,
"./errors": 93,
"./fcm-details": 94,
"@firebase/util": 135
} ],
97: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = {
TYPE_OF_MSG: "firebase-messaging-msg-type",
DATA: "firebase-messaging-msg-data"
}, i = {
PUSH_MSG_RECEIVED: "push-msg-received",
NOTIFICATION_CLICKED: "notification-clicked"
};
n.default = {
PARAMS: r,
TYPES_OF_MSG: i,
createNewMsg: function(e, t) {
return n = {}, n[r.TYPE_OF_MSG] = e, n[r.DATA] = t, n;
var n;
}
};
}, {} ],
98: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
e("./src/polyfills/promise");
e("./src/shims/find");
e("./src/shims/findIndex");
}, {
"./src/polyfills/promise": 99,
"./src/shims/find": 100,
"./src/shims/findIndex": 101
} ],
99: [ function(e, t, n) {
(function(t) {
var n = function() {
if ("undefined" != typeof t) return t;
if ("undefined" != typeof window) return window;
if ("undefined" != typeof self) return self;
throw new Error("unable to locate global object");
}();
"undefined" == typeof Promise && (n.Promise = Promise = e("promise-polyfill"));
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"promise-polyfill": 158
} ],
100: [ function(e, t, n) {
Array.prototype.find || Object.defineProperty(Array.prototype, "find", {
value: function(e) {
if (null == this) throw new TypeError('"this" is null or not defined');
var t = Object(this), n = t.length >>> 0;
if ("function" != typeof e) throw new TypeError("predicate must be a function");
for (var r = arguments[1], i = 0; i < n; ) {
var o = t[i];
if (e.call(r, o, i, t)) return o;
i++;
}
}
});
}, {} ],
101: [ function(e, t, n) {
Array.prototype.findIndex || Object.defineProperty(Array.prototype, "findIndex", {
value: function(e) {
if (null == this) throw new TypeError('"this" is null or not defined');
var t = Object(this), n = t.length >>> 0;
if ("function" != typeof e) throw new TypeError("predicate must be a function");
for (var r = arguments[1], i = 0; i < n; ) {
var o = t[i];
if (e.call(r, o, i, t)) return i;
i++;
}
return -1;
}
});
}, {} ],
102: [ function(e, t, n) {
"use strict";
function r(e, t, n) {
return new h.Service(e, new c.XhrIoPool(), n);
}
function i(e) {
var t = {
TaskState: u.TaskState,
TaskEvent: s.TaskEvent,
StringFormat: a.StringFormat,
Storage: h.Service,
Reference: l.Reference
};
e.INTERNAL.registerService(f, r, t, void 0, !0);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var o = e("@firebase/app"), a = e("./src/implementation/string"), s = e("./src/implementation/taskenums"), u = e("./src/implementation/taskenums"), c = e("./src/implementation/xhriopool"), l = e("./src/reference"), h = e("./src/service"), f = "storage";
n.registerStorage = i;
i(o.default);
}, {
"./src/implementation/string": 124,
"./src/implementation/taskenums": 125,
"./src/implementation/xhriopool": 130,
"./src/reference": 131,
"./src/service": 132,
"@firebase/app": 2
} ],
103: [ function(e, t, n) {
"use strict";
function r(e, t) {
return function(n) {
e(n);
t(n);
};
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./error"), o = e("./metadata"), a = e("./type");
n.validate = function(e, t, n) {
for (var r = t.length, o = t.length, a = 0; a < t.length; a++) if (t[a].optional) {
r = a;
break;
}
if (!(r <= n.length && n.length <= o)) throw i.invalidArgumentCount(r, o, e, n.length);
for (a = 0; a < n.length; a++) try {
t[a].validator(n[a]);
} catch (t) {
throw t instanceof Error ? i.invalidArgument(a, e, t.message) : i.invalidArgument(a, e, t);
}
};
var s = function() {
return function(e, t) {
var n = this;
this.validator = function(t) {
n.optional && !a.isJustDef(t) || e(t);
};
this.optional = !!t;
};
}();
n.ArgSpec = s;
n.and_ = r;
n.stringSpec = function(e, t) {
function n(e) {
if (!a.isString(e)) throw "Expected string.";
}
var i;
i = e ? r(n, e) : n;
return new s(i, t);
};
n.uploadDataSpec = function() {
return new s(function(e) {
if (!(e instanceof Uint8Array || e instanceof ArrayBuffer || a.isNativeBlobDefined() && e instanceof Blob)) throw "Expected Blob or File.";
});
};
n.metadataSpec = function(e) {
return new s(o.metadataValidator, e);
};
n.nonNegativeNumberSpec = function() {
return new s(function(e) {
if (!(a.isNumber(e) && e >= 0)) throw "Expected a number 0 or greater.";
});
};
n.looseObjectSpec = function(e, t) {
return new s(function(t) {
if (!(null === t || a.isDef(t) && t instanceof Object)) throw "Expected an Object.";
void 0 !== e && null !== e && e(t);
}, t);
};
n.nullFunctionSpec = function(e) {
return new s(function(e) {
if (null !== e && !a.isFunction(e)) throw "Expected a Function.";
}, e);
};
}, {
"./error": 110,
"./metadata": 115,
"./type": 126
} ],
104: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.contains = function(e, t) {
return -1 !== e.indexOf(t);
};
n.clone = function(e) {
return Array.prototype.slice.call(e);
};
n.remove = function(e, t) {
var n = e.indexOf(t);
-1 !== n && e.splice(n, 1);
};
}, {} ],
105: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./promise_external");
n.async = function(e) {
return function() {
for (var t = [], n = 0; n < arguments.length; n++) t[n] = arguments[n];
r.resolve(!0).then(function() {
e.apply(null, t);
});
};
};
}, {
"./promise_external": 119
} ],
106: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./constants"), i = e("./error"), o = e("./failrequest"), a = e("./location"), s = e("./promise_external"), u = e("./requestmap"), c = e("./type"), l = function() {
function e(t, n, i, o, a) {
this.bucket_ = null;
this.deleted_ = !1;
this.app_ = t;
if (null !== this.app_) {
var s = this.app_.options;
c.isDef(s) && (this.bucket_ = e.extractBucket_(s));
}
this.storageRefMaker_ = n;
this.requestMaker_ = i;
this.pool_ = a;
this.service_ = o;
this.maxOperationRetryTime_ = r.defaultMaxOperationRetryTime;
this.maxUploadRetryTime_ = r.defaultMaxUploadRetryTime;
this.requestMap_ = new u.RequestMap();
}
e.extractBucket_ = function(e) {
var t = e[r.configOption] || null;
return null == t ? null : a.Location.makeFromBucketSpec(t).bucket;
};
e.prototype.getAuthToken = function() {
return null !== this.app_ && c.isDef(this.app_.INTERNAL) && c.isDef(this.app_.INTERNAL.getToken) ? this.app_.INTERNAL.getToken().then(function(e) {
return null !== e ? e.accessToken : null;
}, function(e) {
return null;
}) : s.resolve(null);
};
e.prototype.bucket = function() {
if (this.deleted_) throw i.appDeleted();
return this.bucket_;
};
e.prototype.service = function() {
return this.service_;
};
e.prototype.makeStorageReference = function(e) {
return this.storageRefMaker_(this, e);
};
e.prototype.makeRequest = function(e, t) {
if (this.deleted_) return new o.FailRequest(i.appDeleted());
var n = this.requestMaker_(e, t, this.pool_);
this.requestMap_.addRequest(n);
return n;
};
e.prototype.deleteApp = function() {
this.deleted_ = !0;
this.app_ = null;
this.requestMap_.clear();
};
e.prototype.maxUploadRetryTime = function() {
return this.maxUploadRetryTime_;
};
e.prototype.setMaxUploadRetryTime = function(e) {
this.maxUploadRetryTime_ = e;
};
e.prototype.maxOperationRetryTime = function() {
return this.maxOperationRetryTime_;
};
e.prototype.setMaxOperationRetryTime = function(e) {
this.maxOperationRetryTime_ = e;
};
return e;
}();
n.AuthWrapper = l;
}, {
"./constants": 109,
"./error": 110,
"./failrequest": 111,
"./location": 114,
"./promise_external": 119,
"./requestmap": 122,
"./type": 126
} ],
107: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.start = function(e, t, n) {
function r() {
return 2 === h;
}
function i() {
if (!f) {
f = !0;
t.apply(null, arguments);
}
}
function o(t) {
c = setTimeout(function() {
c = null;
e(a, r());
}, t);
}
function a(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
if (!f) if (e) i.apply(null, arguments); else if (r() || l) i.apply(null, arguments); else {
u < 64 && (u *= 2);
var a;
if (1 === h) {
h = 2;
a = 0;
} else a = 1e3 * (u + Math.random());
o(a);
}
}
function s(e) {
if (!p) {
p = !0;
if (!f) if (null !== c) {
e || (h = 2);
clearTimeout(c);
o(0);
} else e || (h = 1);
}
}
var u = 1, c = null, l = !1, h = 0, f = !1, p = !1;
o(0);
setTimeout(function() {
l = !0;
s(!0);
}, n);
return s;
};
n.stop = function(e) {
e(!1);
};
}, {} ],
108: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./fs"), i = e("./string"), o = e("./string"), a = e("./type"), s = function() {
function e(e, t) {
var n = 0, r = "";
if (a.isNativeBlob(e)) {
this.data_ = e;
n = e.size;
r = e.type;
} else if (e instanceof ArrayBuffer) {
if (t) this.data_ = new Uint8Array(e); else {
this.data_ = new Uint8Array(e.byteLength);
this.data_.set(new Uint8Array(e));
}
n = this.data_.length;
} else if (e instanceof Uint8Array) {
if (t) this.data_ = e; else {
this.data_ = new Uint8Array(e.length);
this.data_.set(e);
}
n = e.length;
}
this.size_ = n;
this.type_ = r;
}
e.prototype.size = function() {
return this.size_;
};
e.prototype.type = function() {
return this.type_;
};
e.prototype.slice = function(t, n) {
if (a.isNativeBlob(this.data_)) {
var i = this.data_, o = r.sliceBlob(i, t, n);
return null === o ? null : new e(o);
}
return new e(new Uint8Array(this.data_.buffer, t, n - t), !0);
};
e.getBlob = function() {
for (var t = [], n = 0; n < arguments.length; n++) t[n] = arguments[n];
if (a.isNativeBlobDefined()) {
var s = t.map(function(t) {
return t instanceof e ? t.data_ : t;
});
return new e(r.getBlob.apply(null, s));
}
var u = t.map(function(e) {
return a.isString(e) ? i.dataFromString(o.StringFormat.RAW, e).data : e.data_;
}), c = 0;
u.forEach(function(e) {
c += e.byteLength;
});
var l = new Uint8Array(c), h = 0;
u.forEach(function(e) {
for (var t = 0; t < e.length; t++) l[h++] = e[t];
});
return new e(l, !0);
};
e.prototype.uploadData = function() {
return this.data_;
};
return e;
}();
n.FbsBlob = s;
}, {
"./fs": 112,
"./string": 124,
"./type": 126
} ],
109: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.domainBase = "https://firebasestorage.googleapis.com";
n.downloadBase = "https://firebasestorage.googleapis.com";
n.apiBaseUrl = "/v0";
n.apiUploadBaseUrl = "/v0";
n.setDomainBase = function(e) {
e = e;
};
n.configOption = "storageBucket";
n.shortMaxOperationRetryTime = 6e4;
n.defaultMaxOperationRetryTime = 12e4;
n.defaultMaxUploadRetryTime = 6e4;
n.minSafeInteger = -9007199254740991;
}, {} ],
110: [ function(e, t, n) {
"use strict";
function r(e) {
return "storage/" + e;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./constants"), o = function() {
function e(e, t) {
this.code_ = r(e);
this.message_ = "Firebase Storage: " + t;
this.serverResponse_ = null;
this.name_ = "FirebaseError";
}
e.prototype.codeProp = function() {
return this.code;
};
e.prototype.codeEquals = function(e) {
return r(e) === this.codeProp();
};
e.prototype.serverResponseProp = function() {
return this.serverResponse_;
};
e.prototype.setServerResponseProp = function(e) {
this.serverResponse_ = e;
};
Object.defineProperty(e.prototype, "name", {
get: function() {
return this.name_;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "code", {
get: function() {
return this.code_;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "message", {
get: function() {
return this.message_;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "serverResponse", {
get: function() {
return this.serverResponse_;
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.FirebaseStorageError = o;
n.errors = {};
n.Code = {
UNKNOWN: "unknown",
OBJECT_NOT_FOUND: "object-not-found",
BUCKET_NOT_FOUND: "bucket-not-found",
PROJECT_NOT_FOUND: "project-not-found",
QUOTA_EXCEEDED: "quota-exceeded",
UNAUTHENTICATED: "unauthenticated",
UNAUTHORIZED: "unauthorized",
RETRY_LIMIT_EXCEEDED: "retry-limit-exceeded",
INVALID_CHECKSUM: "invalid-checksum",
CANCELED: "canceled",
INVALID_EVENT_NAME: "invalid-event-name",
INVALID_URL: "invalid-url",
INVALID_DEFAULT_BUCKET: "invalid-default-bucket",
NO_DEFAULT_BUCKET: "no-default-bucket",
CANNOT_SLICE_BLOB: "cannot-slice-blob",
SERVER_FILE_WRONG_SIZE: "server-file-wrong-size",
NO_DOWNLOAD_URL: "no-download-url",
INVALID_ARGUMENT: "invalid-argument",
INVALID_ARGUMENT_COUNT: "invalid-argument-count",
APP_DELETED: "app-deleted",
INVALID_ROOT_OPERATION: "invalid-root-operation",
INVALID_FORMAT: "invalid-format",
INTERNAL_ERROR: "internal-error"
};
n.prependCode = r;
n.unknown = function() {
return new o(n.Code.UNKNOWN, "An unknown error occurred, please check the error payload for server response.");
};
n.objectNotFound = function(e) {
return new o(n.Code.OBJECT_NOT_FOUND, "Object '" + e + "' does not exist.");
};
n.bucketNotFound = function(e) {
return new o(n.Code.BUCKET_NOT_FOUND, "Bucket '" + e + "' does not exist.");
};
n.projectNotFound = function(e) {
return new o(n.Code.PROJECT_NOT_FOUND, "Project '" + e + "' does not exist.");
};
n.quotaExceeded = function(e) {
return new o(n.Code.QUOTA_EXCEEDED, "Quota for bucket '" + e + "' exceeded, please view quota on https://firebase.google.com/pricing/.");
};
n.unauthenticated = function() {
return new o(n.Code.UNAUTHENTICATED, "User is not authenticated, please authenticate using Firebase Authentication and try again.");
};
n.unauthorized = function(e) {
return new o(n.Code.UNAUTHORIZED, "User does not have permission to access '" + e + "'.");
};
n.retryLimitExceeded = function() {
return new o(n.Code.RETRY_LIMIT_EXCEEDED, "Max retry time for operation exceeded, please try again.");
};
n.invalidChecksum = function(e, t, r) {
return new o(n.Code.INVALID_CHECKSUM, "Uploaded/downloaded object '" + e + "' has checksum '" + t + "' which does not match '" + r + "'. Please retry the upload/download.");
};
n.canceled = function() {
return new o(n.Code.CANCELED, "User canceled the upload/download.");
};
n.invalidEventName = function(e) {
return new o(n.Code.INVALID_EVENT_NAME, "Invalid event name '" + e + "'.");
};
n.invalidUrl = function(e) {
return new o(n.Code.INVALID_URL, "Invalid URL '" + e + "'.");
};
n.invalidDefaultBucket = function(e) {
return new o(n.Code.INVALID_DEFAULT_BUCKET, "Invalid default bucket '" + e + "'.");
};
n.noDefaultBucket = function() {
return new o(n.Code.NO_DEFAULT_BUCKET, "No default bucket found. Did you set the '" + i.configOption + "' property when initializing the app?");
};
n.cannotSliceBlob = function() {
return new o(n.Code.CANNOT_SLICE_BLOB, "Cannot slice blob for upload. Please retry the upload.");
};
n.serverFileWrongSize = function() {
return new o(n.Code.SERVER_FILE_WRONG_SIZE, "Server recorded incorrect upload file size, please retry the upload.");
};
n.noDownloadURL = function() {
return new o(n.Code.NO_DOWNLOAD_URL, "The given file does not have any download URLs.");
};
n.invalidArgument = function(e, t, r) {
return new o(n.Code.INVALID_ARGUMENT, "Invalid argument in `" + t + "` at index " + e + ": " + r);
};
n.invalidArgumentCount = function(e, t, r, i) {
var a, s;
if (e === t) {
a = e;
s = 1 === e ? "argument" : "arguments";
} else {
a = "between " + e + " and " + t;
s = "arguments";
}
return new o(n.Code.INVALID_ARGUMENT_COUNT, "Invalid argument count in `" + r + "`: Expected " + a + " " + s + ", received " + i + ".");
};
n.appDeleted = function() {
return new o(n.Code.APP_DELETED, "The Firebase app was deleted.");
};
n.invalidRootOperation = function(e) {
return new o(n.Code.INVALID_ROOT_OPERATION, "The operation '" + e + "' cannot be performed on a root reference, create a non-root reference using child, such as .child('file.png').");
};
n.invalidFormat = function(e, t) {
return new o(n.Code.INVALID_FORMAT, "String does not match format '" + e + "': " + t);
};
n.internalError = function(e) {
throw new o(n.Code.INTERNAL_ERROR, "Internal error: " + e);
};
}, {
"./constants": 109
} ],
111: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./promise_external"), i = function() {
function e(e) {
this.promise_ = r.reject(e);
}
e.prototype.getPromise = function() {
return this.promise_;
};
e.prototype.cancel = function(e) {
void 0 === e && (e = !1);
};
return e;
}();
n.FailRequest = i;
}, {
"./promise_external": 119
} ],
112: [ function(e, t, n) {
"use strict";
function r() {
return "undefined" != typeof BlobBuilder ? BlobBuilder : "undefined" != typeof WebKitBlobBuilder ? WebKitBlobBuilder : void 0;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./type");
n.getBlob = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
var n = r();
if (void 0 !== n) {
for (var o = new n(), a = 0; a < e.length; a++) o.append(e[a]);
return o.getBlob();
}
if (i.isNativeBlobDefined()) return new Blob(e);
throw Error("This browser doesn't seem to support creating Blobs");
};
n.sliceBlob = function(e, t, n) {
return e.webkitSlice ? e.webkitSlice(t, n) : e.mozSlice ? e.mozSlice(t, n) : e.slice ? e.slice(t, n) : null;
};
}, {
"./type": 126
} ],
113: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./type");
n.jsonObjectOrNull = function(e) {
var t;
try {
t = JSON.parse(e);
} catch (e) {
return null;
}
return r.isNonArrayObject(t) ? t : null;
};
}, {
"./type": 126
} ],
114: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./error"), i = function() {
function e(e, t) {
this.bucket = e;
this.path_ = t;
}
Object.defineProperty(e.prototype, "path", {
get: function() {
return this.path_;
},
enumerable: !0,
configurable: !0
});
e.prototype.fullServerUrl = function() {
var e = encodeURIComponent;
return "/b/" + e(this.bucket) + "/o/" + e(this.path);
};
e.prototype.bucketOnlyServerUrl = function() {
return "/b/" + encodeURIComponent(this.bucket) + "/o";
};
e.makeFromBucketSpec = function(t) {
var n;
try {
n = e.makeFromUrl(t);
} catch (n) {
return new e(t, "");
}
if ("" === n.path) return n;
throw r.invalidDefaultBucket(t);
};
e.makeFromUrl = function(t) {
for (var n = null, i = [ {
regex: new RegExp("^gs://([A-Za-z0-9.\\-]+)(/(.*))?$", "i"),
indices: {
bucket: 1,
path: 3
},
postModify: function(e) {
"/" === e.path.charAt(e.path.length - 1) && (e.path_ = e.path_.slice(0, -1));
}
}, {
regex: new RegExp("^https?://firebasestorage\\.googleapis\\.com/v[A-Za-z0-9_]+/b/([A-Za-z0-9.\\-]+)/o(/([^?#]*).*)?$", "i"),
indices: {
bucket: 1,
path: 3
},
postModify: function(e) {
e.path_ = decodeURIComponent(e.path);
}
} ], o = 0; o < i.length; o++) {
var a = i[o], s = a.regex.exec(t);
if (s) {
var u = s[a.indices.bucket], c = s[a.indices.path];
c || (c = "");
n = new e(u, c);
a.postModify(n);
break;
}
}
if (null == n) throw r.invalidUrl(t);
return n;
};
return e;
}();
n.Location = i;
}, {
"./error": 110
} ],
115: [ function(e, t, n) {
"use strict";
function r(e, t) {
return t;
}
function i(e) {
if (!l.isString(e) || e.length < 2) return e;
e = e;
return c.lastComponent(e);
}
function o(e, t) {
Object.defineProperty(e, "ref", {
get: function() {
var n = e.bucket, r = e.fullPath, i = new u.Location(n, r);
return t.makeStorageReference(i);
}
});
}
function a(e, t, n) {
var r = {};
r.type = "file";
for (var i = n.length, a = 0; a < i; a++) {
var s = n[a];
r[s.local] = s.xform(r, t[s.server]);
}
o(r, e);
return r;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./json"), u = e("./location"), c = e("./path"), l = e("./type"), h = e("./url");
n.noXform_ = r;
var f = function() {
return function(e, t, n, i) {
this.server = e;
this.local = t || e;
this.writable = !!n;
this.xform = i || r;
};
}();
n.Mapping = f;
var p = null;
n.xformPath = i;
n.getMappings = function() {
if (p) return p;
var e = [];
e.push(new f("bucket"));
e.push(new f("generation"));
e.push(new f("metageneration"));
e.push(new f("name", "fullPath", !0));
var t = new f("name");
t.xform = function(e, t) {
return i(t);
};
e.push(t);
var n = new f("size");
n.xform = function(e, t) {
return l.isDef(t) ? +t : t;
};
e.push(n);
e.push(new f("timeCreated"));
e.push(new f("updated"));
e.push(new f("md5Hash", null, !0));
e.push(new f("cacheControl", null, !0));
e.push(new f("contentDisposition", null, !0));
e.push(new f("contentEncoding", null, !0));
e.push(new f("contentLanguage", null, !0));
e.push(new f("contentType", null, !0));
e.push(new f("metadata", "customMetadata", !0));
e.push(new f("downloadTokens", "downloadURLs", !1, function(e, t) {
if (!(l.isString(t) && t.length > 0)) return [];
var n = encodeURIComponent;
return t.split(",").map(function(t) {
var r = e.bucket, i = e.fullPath, o = "/b/" + n(r) + "/o/" + n(i);
return h.makeDownloadUrl(o) + h.makeQueryString({
alt: "media",
token: t
});
});
}));
return p = e;
};
n.addRef = o;
n.fromResource = a;
n.fromResourceString = function(e, t, n) {
var r = s.jsonObjectOrNull(t);
return null === r ? null : a(e, r, n);
};
n.toResourceString = function(e, t) {
for (var n = {}, r = t.length, i = 0; i < r; i++) {
var o = t[i];
o.writable && (n[o.server] = e[o.local]);
}
return JSON.stringify(n);
};
n.metadataValidator = function(e) {
if (!e || !l.isObject(e)) throw "Expected Metadata object.";
for (var t in e) {
var n = e[t];
if ("customMetadata" === t) {
if (!l.isObject(n)) throw "Expected object for 'customMetadata' mapping.";
} else if (l.isNonNullObject(n)) throw "Mapping for '" + t + "' cannot be an object.";
}
};
}, {
"./json": 113,
"./location": 114,
"./path": 118,
"./type": 126,
"./url": 127
} ],
116: [ function(e, t, n) {
"use strict";
function r(e, t) {
return Object.prototype.hasOwnProperty.call(e, t);
}
function i(e, t) {
for (var n in e) r(e, n) && t(n, e[n]);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.contains = r;
n.forEach = i;
n.clone = function(e) {
if (null == e) return {};
var t = {};
i(e, function(e, n) {
t[e] = n;
});
return t;
};
}, {} ],
117: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./type"), i = function() {
return function(e, t, n) {
if (r.isFunction(e) || r.isDef(t) || r.isDef(n)) {
this.next = e;
this.error = t || null;
this.complete = n || null;
} else {
var i = e;
this.next = i.next || null;
this.error = i.error || null;
this.complete = i.complete || null;
}
};
}();
n.Observer = i;
}, {
"./type": 126
} ],
118: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.parent = function(e) {
if (0 == e.length) return null;
var t = e.lastIndexOf("/");
return -1 === t ? "" : e.slice(0, t);
};
n.child = function(e, t) {
var n = t.split("/").filter(function(e) {
return e.length > 0;
}).join("/");
return 0 === e.length ? n : e + "/" + n;
};
n.lastComponent = function(e) {
var t = e.lastIndexOf("/", e.length - 2);
return -1 === t ? e : e.slice(t + 1);
};
}, {} ],
119: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.make = function(e) {
return new Promise(e);
};
n.resolve = function(e) {
return Promise.resolve(e);
};
n.reject = function(e) {
return Promise.reject(e);
};
}, {} ],
120: [ function(e, t, n) {
"use strict";
function r(e, t) {
null !== t && t.length > 0 && (e.Authorization = "Firebase " + t);
}
function i(e) {
var t = "undefined" != typeof o.default ? o.default.SDK_VERSION : "AppManager";
e["X-Firebase-Storage-Version"] = "webjs/" + t;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var o = e("@firebase/app"), a = e("./array"), s = e("./backoff"), u = e("./error"), c = e("./object"), l = e("./promise_external"), h = e("./type"), f = e("./url"), p = e("./xhrio"), d = function() {
function e(e, t, n, r, i, o, a, s, u, c, h) {
this.pendingXhr_ = null;
this.backoffId_ = null;
this.resolve_ = null;
this.reject_ = null;
this.canceled_ = !1;
this.appDelete_ = !1;
this.url_ = e;
this.method_ = t;
this.headers_ = n;
this.body_ = r;
this.successCodes_ = i.slice();
this.additionalRetryCodes_ = o.slice();
this.callback_ = a;
this.errorCallback_ = s;
this.progressCallback_ = c;
this.timeout_ = u;
this.pool_ = h;
var f = this;
this.promise_ = l.make(function(e, t) {
f.resolve_ = e;
f.reject_ = t;
f.start_();
});
}
e.prototype.start_ = function() {
function e(e, n) {
var r = t.resolve_, i = t.reject_, o = n.xhr;
if (n.wasSuccessCode) try {
var a = t.callback_(o, o.getResponseText());
h.isJustDef(a) ? r(a) : r();
} catch (e) {
i(e);
} else if (null !== o) {
(s = u.unknown()).setServerResponseProp(o.getResponseText());
i(t.errorCallback_ ? t.errorCallback_(o, s) : s);
} else if (n.canceled) {
s = t.appDelete_ ? u.appDeleted() : u.canceled();
i(s);
} else {
var s = u.retryLimitExceeded();
i(s);
}
}
var t = this;
this.canceled_ ? e(0, new v(!1, null, !0)) : this.backoffId_ = s.start(function(e, n) {
function r(e) {
var n = e.loaded, r = e.lengthComputable ? e.total : -1;
null !== t.progressCallback_ && t.progressCallback_(n, r);
}
if (n) e(!1, new v(!1, null, !0)); else {
var i = t.pool_.createXhrIo();
t.pendingXhr_ = i;
null !== t.progressCallback_ && i.addUploadProgressListener(r);
i.send(t.url_, t.method_, t.body_, t.headers_).then(function(n) {
null !== t.progressCallback_ && n.removeUploadProgressListener(r);
t.pendingXhr_ = null;
var i = (n = n).getErrorCode() === p.ErrorCode.NO_ERROR, o = n.getStatus();
if (i && !t.isRetryStatusCode_(o)) {
var s = a.contains(t.successCodes_, o);
e(!0, new v(s, n));
} else {
var u = n.getErrorCode() === p.ErrorCode.ABORT;
e(!1, new v(!1, null, u));
}
});
}
}, e, this.timeout_);
};
e.prototype.getPromise = function() {
return this.promise_;
};
e.prototype.cancel = function(e) {
this.canceled_ = !0;
this.appDelete_ = e || !1;
null !== this.backoffId_ && s.stop(this.backoffId_);
null !== this.pendingXhr_ && this.pendingXhr_.abort();
};
e.prototype.isRetryStatusCode_ = function(e) {
var t = e >= 500 && e < 600, n = [ 408, 429 ], r = a.contains(n, e), i = a.contains(this.additionalRetryCodes_, e);
return t || r || i;
};
return e;
}(), v = function() {
return function(e, t, n) {
this.wasSuccessCode = e;
this.xhr = t;
this.canceled = !!n;
};
}();
n.RequestEndStatus = v;
n.addAuthHeader_ = r;
n.addVersionHeader_ = i;
n.makeRequest = function(e, t, n) {
var o = f.makeQueryString(e.urlParams), a = e.url + o, s = c.clone(e.headers);
r(s, t);
i(s);
return new d(a, e.method, s, e.body, e.successCodes, e.additionalRetryCodes, e.handler, e.errorHandler, e.timeout, e.progressCallback, n);
};
}, {
"./array": 104,
"./backoff": 107,
"./error": 110,
"./object": 116,
"./promise_external": 119,
"./type": 126,
"./url": 127,
"./xhrio": 128,
"@firebase/app": 2
} ],
121: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
return function(e, t, n, r) {
this.url = e;
this.method = t;
this.handler = n;
this.timeout = r;
this.urlParams = {};
this.headers = {};
this.body = null;
this.errorHandler = null;
this.progressCallback = null;
this.successCodes = [ 200 ];
this.additionalRetryCodes = [];
};
}();
n.RequestInfo = r;
}, {} ],
122: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./object"), i = e("./constants"), o = function() {
function e() {
this.map_ = {};
this.id_ = i.minSafeInteger;
}
e.prototype.addRequest = function(e) {
function t() {
delete r.map_[n];
}
var n = this.id_;
this.id_++;
this.map_[n] = e;
var r = this;
e.getPromise().then(t, t);
};
e.prototype.clear = function() {
r.forEach(this.map_, function(e, t) {
t && t.cancel(!0);
});
this.map_ = {};
};
return e;
}();
n.RequestMap = o;
}, {
"./constants": 109,
"./object": 116
} ],
123: [ function(e, t, n) {
"use strict";
function r(e) {
if (!e) throw f.unknown();
}
function i(e, t) {
return function(n, i) {
var o = p.fromResourceString(e, i, t);
r(null !== o);
return o;
};
}
function o(e) {
return function(t, n) {
var r;
(r = 401 === t.getStatus() ? f.unauthenticated() : 402 === t.getStatus() ? f.quotaExceeded(e.bucket) : 403 === t.getStatus() ? f.unauthorized(e.path) : n).setServerResponseProp(n.serverResponseProp());
return r;
};
}
function a(e) {
var t = o(e);
return function(n, r) {
var i = t(n, r);
404 === n.getStatus() && (i = f.objectNotFound(e.path));
i.setServerResponseProp(r.serverResponseProp());
return i;
};
}
function s(e, t) {
return e && e.contentType || t && t.type() || "application/octet-stream";
}
function u(e, t, n) {
var r = d.clone(n);
r.fullPath = e.path;
r.size = t.size();
r.contentType || (r.contentType = s(null, t));
return r;
}
function c(e, t) {
var n;
try {
n = e.getResponseHeader("X-Goog-Upload-Status");
} catch (e) {
r(!1);
}
var i = t || [ "active" ];
r(l.contains(i, n));
return n;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var l = e("./array"), h = e("./blob"), f = e("./error"), p = e("./metadata"), d = e("./object"), v = e("./requestinfo"), _ = e("./type"), y = e("./url");
n.handlerCheck = r;
n.metadataHandler = i;
n.sharedErrorHandler = o;
n.objectErrorHandler = a;
n.getMetadata = function(e, t, n) {
var r = t.fullServerUrl(), o = y.makeNormalUrl(r), s = e.maxOperationRetryTime(), u = new v.RequestInfo(o, "GET", i(e, n), s);
u.errorHandler = a(t);
return u;
};
n.updateMetadata = function(e, t, n, r) {
var o = t.fullServerUrl(), s = y.makeNormalUrl(o), u = p.toResourceString(n, r), c = {
"Content-Type": "application/json; charset=utf-8"
}, l = e.maxOperationRetryTime(), h = new v.RequestInfo(s, "PATCH", i(e, r), l);
h.headers = c;
h.body = u;
h.errorHandler = a(t);
return h;
};
n.deleteObject = function(e, t) {
var n = t.fullServerUrl(), r = y.makeNormalUrl(n), i = e.maxOperationRetryTime(), o = new v.RequestInfo(r, "DELETE", function(e, t) {}, i);
o.successCodes = [ 200, 204 ];
o.errorHandler = a(t);
return o;
};
n.determineContentType_ = s;
n.metadataForUpload_ = u;
n.multipartUpload = function(e, t, n, r, a) {
var s = t.bucketOnlyServerUrl(), c = {
"X-Goog-Upload-Protocol": "multipart"
}, l = function() {
for (var e = "", t = 0; t < 2; t++) e += Math.random().toString().slice(2);
return e;
}();
c["Content-Type"] = "multipart/related; boundary=" + l;
var d = u(t, r, a), _ = "--" + l + "\r\nContent-Type: application/json; charset=utf-8\r\n\r\n" + p.toResourceString(d, n) + "\r\n--" + l + "\r\nContent-Type: " + d.contentType + "\r\n\r\n", g = "\r\n--" + l + "--", m = h.FbsBlob.getBlob(_, r, g);
if (null === m) throw f.cannotSliceBlob();
var b = {
name: d.fullPath
}, E = y.makeUploadUrl(s), w = e.maxUploadRetryTime(), C = new v.RequestInfo(E, "POST", i(e, n), w);
C.urlParams = b;
C.headers = c;
C.body = m.uploadData();
C.errorHandler = o(t);
return C;
};
var g = function() {
return function(e, t, n, r) {
this.current = e;
this.total = t;
this.finalized = !!n;
this.metadata = r || null;
};
}();
n.ResumableUploadStatus = g;
n.checkResumeHeader_ = c;
n.createResumableUpload = function(e, t, n, i, a) {
var s = t.bucketOnlyServerUrl(), l = u(t, i, a), h = {
name: l.fullPath
}, f = y.makeUploadUrl(s), d = {
"X-Goog-Upload-Protocol": "resumable",
"X-Goog-Upload-Command": "start",
"X-Goog-Upload-Header-Content-Length": i.size(),
"X-Goog-Upload-Header-Content-Type": l.contentType,
"Content-Type": "application/json; charset=utf-8"
}, g = p.toResourceString(l, n), m = e.maxUploadRetryTime(), b = new v.RequestInfo(f, "POST", function(e, t) {
c(e);
var n;
try {
n = e.getResponseHeader("X-Goog-Upload-URL");
} catch (e) {
r(!1);
}
r(_.isString(n));
return n;
}, m);
b.urlParams = h;
b.headers = d;
b.body = g;
b.errorHandler = o(t);
return b;
};
n.getResumableUploadStatus = function(e, t, n, i) {
var a = {
"X-Goog-Upload-Command": "query"
}, s = e.maxUploadRetryTime(), u = new v.RequestInfo(n, "POST", function(e, t) {
var n, o = c(e, [ "active", "final" ]);
try {
n = e.getResponseHeader("X-Goog-Upload-Size-Received");
} catch (e) {
r(!1);
}
var a = parseInt(n, 10);
r(!isNaN(a));
return new g(a, i.size(), "final" === o);
}, s);
u.headers = a;
u.errorHandler = o(t);
return u;
};
n.resumableUploadChunkSize = 262144;
n.continueResumableUpload = function(e, t, n, r, a, s, u, l) {
var h = new g(0, 0);
if (u) {
h.current = u.current;
h.total = u.total;
} else {
h.current = 0;
h.total = r.size();
}
if (r.size() !== h.total) throw f.serverFileWrongSize();
var p = h.total - h.current, d = p;
a > 0 && (d = Math.min(d, a));
var _ = h.current, y = _ + d, m = {
"X-Goog-Upload-Command": d === p ? "upload, finalize" : "upload",
"X-Goog-Upload-Offset": h.current
}, b = r.slice(_, y);
if (null === b) throw f.cannotSliceBlob();
var E = t.maxUploadRetryTime(), w = new v.RequestInfo(n, "POST", function(e, n) {
var o, a = c(e, [ "active", "final" ]), u = h.current + d, l = r.size();
o = "final" === a ? i(t, s)(e, n) : null;
return new g(u, l, "final" === a, o);
}, E);
w.headers = m;
w.body = b.uploadData();
w.progressCallback = l || null;
w.errorHandler = o(e);
return w;
};
}, {
"./array": 104,
"./blob": 108,
"./error": 110,
"./metadata": 115,
"./object": 116,
"./requestinfo": 121,
"./type": 126,
"./url": 127
} ],
124: [ function(e, t, n) {
"use strict";
function r(e) {
for (var t = [], n = 0; n < e.length; n++) {
var r = e.charCodeAt(n);
if (r <= 127) t.push(r); else if (r <= 2047) t.push(192 | r >> 6, 128 | 63 & r); else if (55296 == (64512 & r)) if (n < e.length - 1 && 56320 == (64512 & e.charCodeAt(n + 1))) {
r = 65536 | (1023 & r) << 10 | 1023 & e.charCodeAt(++n);
t.push(240 | r >> 18, 128 | r >> 12 & 63, 128 | r >> 6 & 63, 128 | 63 & r);
} else t.push(239, 191, 189); else 56320 == (64512 & r) ? t.push(239, 191, 189) : t.push(224 | r >> 12, 128 | r >> 6 & 63, 128 | 63 & r);
}
return new Uint8Array(t);
}
function i(e) {
var t;
try {
t = decodeURIComponent(e);
} catch (e) {
throw c.invalidFormat(n.StringFormat.DATA_URL, "Malformed data URL.");
}
return r(t);
}
function o(e, t) {
switch (e) {
case n.StringFormat.BASE64:
var r = -1 !== t.indexOf("-"), i = -1 !== t.indexOf("_");
if (r || i) {
s = r ? "-" : "_";
throw c.invalidFormat(e, "Invalid character '" + s + "' found: is it base64url encoded?");
}
break;

case n.StringFormat.BASE64URL:
var o = -1 !== t.indexOf("+"), a = -1 !== t.indexOf("/");
if (o || a) {
var s = o ? "+" : "/";
throw c.invalidFormat(e, "Invalid character '" + s + "' found: is it base64 encoded?");
}
t = t.replace(/-/g, "+").replace(/_/g, "/");
}
var u;
try {
u = atob(t);
} catch (t) {
throw c.invalidFormat(e, "Invalid character found");
}
for (var l = new Uint8Array(u.length), h = 0; h < u.length; h++) l[h] = u.charCodeAt(h);
return l;
}
function a(e) {
var t = new h(e);
return t.base64 ? o(n.StringFormat.BASE64, t.rest) : i(t.rest);
}
function s(e) {
return new h(e).contentType;
}
function u(e, t) {
return !!(e.length >= t.length) && e.substring(e.length - t.length) === t;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("./error");
n.StringFormat = {
RAW: "raw",
BASE64: "base64",
BASE64URL: "base64url",
DATA_URL: "data_url"
};
n.formatValidator = function(e) {
switch (e) {
case n.StringFormat.RAW:
case n.StringFormat.BASE64:
case n.StringFormat.BASE64URL:
case n.StringFormat.DATA_URL:
return;

default:
throw "Expected one of the event types: [" + n.StringFormat.RAW + ", " + n.StringFormat.BASE64 + ", " + n.StringFormat.BASE64URL + ", " + n.StringFormat.DATA_URL + "].";
}
};
var l = function() {
return function(e, t) {
this.data = e;
this.contentType = t || null;
};
}();
n.StringData = l;
n.dataFromString = function(e, t) {
switch (e) {
case n.StringFormat.RAW:
return new l(r(t));

case n.StringFormat.BASE64:
case n.StringFormat.BASE64URL:
return new l(o(e, t));

case n.StringFormat.DATA_URL:
return new l(a(t), s(t));
}
throw c.unknown();
};
n.utf8Bytes_ = r;
n.percentEncodedBytes_ = i;
n.base64Bytes_ = o;
var h = function() {
return function(e) {
this.base64 = !1;
this.contentType = null;
var t = e.match(/^data:([^,]+)?,/);
if (null === t) throw c.invalidFormat(n.StringFormat.DATA_URL, "Must be formatted 'data:[<mediatype>][;base64],<data>");
var r = t[1] || null;
if (null != r) {
this.base64 = u(r, ";base64");
this.contentType = this.base64 ? r.substring(0, r.length - ";base64".length) : r;
}
this.rest = e.substring(e.indexOf(",") + 1);
};
}();
n.dataURLBytes_ = a;
n.dataURLContentType_ = s;
}, {
"./error": 110
} ],
125: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.TaskEvent = {
STATE_CHANGED: "state_changed"
};
n.InternalTaskState = {
RUNNING: "running",
PAUSING: "pausing",
PAUSED: "paused",
SUCCESS: "success",
CANCELING: "canceling",
CANCELED: "canceled",
ERROR: "error"
};
n.TaskState = {
RUNNING: "running",
PAUSED: "paused",
SUCCESS: "success",
CANCELED: "canceled",
ERROR: "error"
};
n.taskStateFromInternalTaskState = function(e) {
switch (e) {
case n.InternalTaskState.RUNNING:
case n.InternalTaskState.PAUSING:
case n.InternalTaskState.CANCELING:
return n.TaskState.RUNNING;

case n.InternalTaskState.PAUSED:
return n.TaskState.PAUSED;

case n.InternalTaskState.SUCCESS:
return n.TaskState.SUCCESS;

case n.InternalTaskState.CANCELED:
return n.TaskState.CANCELED;

case n.InternalTaskState.ERROR:
default:
return n.TaskState.ERROR;
}
};
}, {} ],
126: [ function(e, t, n) {
"use strict";
function r(e) {
return "object" == typeof e;
}
function i() {
return "undefined" != typeof Blob;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.isDef = function(e) {
return null != e;
};
n.isJustDef = function(e) {
return void 0 !== e;
};
n.isFunction = function(e) {
return "function" == typeof e;
};
n.isObject = r;
n.isNonNullObject = function(e) {
return r(e) && null !== e;
};
n.isNonArrayObject = function(e) {
return r(e) && !Array.isArray(e);
};
n.isString = function(e) {
return "string" == typeof e || e instanceof String;
};
n.isNumber = function(e) {
return "number" == typeof e || e instanceof Number;
};
n.isNativeBlob = function(e) {
return i() && e instanceof Blob;
};
n.isNativeBlobDefined = i;
}, {} ],
127: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./constants"), i = e("./object");
n.makeNormalUrl = function(e) {
return r.domainBase + r.apiBaseUrl + e;
};
n.makeDownloadUrl = function(e) {
return r.downloadBase + r.apiBaseUrl + e;
};
n.makeUploadUrl = function(e) {
return r.domainBase + r.apiUploadBaseUrl + e;
};
n.makeQueryString = function(e) {
var t = encodeURIComponent, n = "?";
i.forEach(e, function(e, r) {
var i = t(e) + "=" + t(r);
n = n + i + "&";
});
return n = n.slice(0, -1);
};
}, {
"./constants": 109,
"./object": 116
} ],
128: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
!function(e) {
e[e.NO_ERROR = 0] = "NO_ERROR";
e[e.NETWORK_ERROR = 1] = "NETWORK_ERROR";
e[e.ABORT = 2] = "ABORT";
}(n.ErrorCode || (n.ErrorCode = {}));
}, {} ],
129: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./error"), i = e("./object"), o = e("./promise_external"), a = e("./type"), s = e("./xhrio"), u = function() {
function e() {
var e = this;
this.sent_ = !1;
this.xhr_ = new XMLHttpRequest();
this.errorCode_ = s.ErrorCode.NO_ERROR;
this.sendPromise_ = o.make(function(t, n) {
e.xhr_.addEventListener("abort", function(n) {
e.errorCode_ = s.ErrorCode.ABORT;
t(e);
});
e.xhr_.addEventListener("error", function(n) {
e.errorCode_ = s.ErrorCode.NETWORK_ERROR;
t(e);
});
e.xhr_.addEventListener("load", function(n) {
t(e);
});
});
}
e.prototype.send = function(e, t, n, o) {
var s = this;
if (this.sent_) throw r.internalError("cannot .send() more than once");
this.sent_ = !0;
this.xhr_.open(t, e, !0);
if (a.isDef(o)) {
var u = o;
i.forEach(u, function(e, t) {
s.xhr_.setRequestHeader(e, t.toString());
});
}
a.isDef(n) ? this.xhr_.send(n) : this.xhr_.send();
return this.sendPromise_;
};
e.prototype.getErrorCode = function() {
if (!this.sent_) throw r.internalError("cannot .getErrorCode() before sending");
return this.errorCode_;
};
e.prototype.getStatus = function() {
if (!this.sent_) throw r.internalError("cannot .getStatus() before sending");
try {
return this.xhr_.status;
} catch (e) {
return -1;
}
};
e.prototype.getResponseText = function() {
if (!this.sent_) throw r.internalError("cannot .getResponseText() before sending");
return this.xhr_.responseText;
};
e.prototype.abort = function() {
this.xhr_.abort();
};
e.prototype.getResponseHeader = function(e) {
return this.xhr_.getResponseHeader(e);
};
e.prototype.addUploadProgressListener = function(e) {
a.isDef(this.xhr_.upload) && this.xhr_.upload.addEventListener("progress", e);
};
e.prototype.removeUploadProgressListener = function(e) {
a.isDef(this.xhr_.upload) && this.xhr_.upload.removeEventListener("progress", e);
};
return e;
}();
n.NetworkXhrIo = u;
}, {
"./error": 110,
"./object": 116,
"./promise_external": 119,
"./type": 126,
"./xhrio": 128
} ],
130: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./xhrio_network"), i = function() {
function e() {}
e.prototype.createXhrIo = function() {
return new r.NetworkXhrIo();
};
return e;
}();
n.XhrIoPool = i;
}, {
"./xhrio_network": 129
} ],
131: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./implementation/args"), i = e("./implementation/blob"), o = e("./implementation/error"), a = e("./implementation/location"), s = e("./implementation/metadata"), u = e("./implementation/object"), c = e("./implementation/path"), l = e("./implementation/requests"), h = e("./implementation/string"), f = e("./implementation/string"), p = e("./implementation/type"), d = e("./task"), v = function() {
function e(e, t) {
this.authWrapper = e;
t instanceof a.Location ? this.location = t : this.location = a.Location.makeFromUrl(t);
}
e.prototype.toString = function() {
r.validate("toString", [], arguments);
return "gs://" + this.location.bucket + "/" + this.location.path;
};
e.prototype.newRef = function(t, n) {
return new e(t, n);
};
e.prototype.mappings = function() {
return s.getMappings();
};
e.prototype.child = function(e) {
r.validate("child", [ r.stringSpec() ], arguments);
var t = c.child(this.location.path, e), n = new a.Location(this.location.bucket, t);
return this.newRef(this.authWrapper, n);
};
Object.defineProperty(e.prototype, "parent", {
get: function() {
var e = c.parent(this.location.path);
if (null === e) return null;
var t = new a.Location(this.location.bucket, e);
return this.newRef(this.authWrapper, t);
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "root", {
get: function() {
var e = new a.Location(this.location.bucket, "");
return this.newRef(this.authWrapper, e);
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "bucket", {
get: function() {
return this.location.bucket;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "fullPath", {
get: function() {
return this.location.path;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "name", {
get: function() {
return c.lastComponent(this.location.path);
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "storage", {
get: function() {
return this.authWrapper.service();
},
enumerable: !0,
configurable: !0
});
e.prototype.put = function(e, t) {
void 0 === t && (t = null);
r.validate("put", [ r.uploadDataSpec(), r.metadataSpec(!0) ], arguments);
this.throwIfRoot_("put");
return new d.UploadTask(this, this.authWrapper, this.location, this.mappings(), new i.FbsBlob(e), t);
};
e.prototype.putString = function(e, t, n) {
void 0 === t && (t = f.StringFormat.RAW);
r.validate("putString", [ r.stringSpec(), r.stringSpec(h.formatValidator, !0), r.metadataSpec(!0) ], arguments);
this.throwIfRoot_("putString");
var o = h.dataFromString(t, e), a = u.clone(n);
!p.isDef(a.contentType) && p.isDef(o.contentType) && (a.contentType = o.contentType);
return new d.UploadTask(this, this.authWrapper, this.location, this.mappings(), new i.FbsBlob(o.data, !0), a);
};
e.prototype.delete = function() {
r.validate("delete", [], arguments);
this.throwIfRoot_("delete");
var e = this;
return this.authWrapper.getAuthToken().then(function(t) {
var n = l.deleteObject(e.authWrapper, e.location);
return e.authWrapper.makeRequest(n, t).getPromise();
});
};
e.prototype.getMetadata = function() {
r.validate("getMetadata", [], arguments);
this.throwIfRoot_("getMetadata");
var e = this;
return this.authWrapper.getAuthToken().then(function(t) {
var n = l.getMetadata(e.authWrapper, e.location, e.mappings());
return e.authWrapper.makeRequest(n, t).getPromise();
});
};
e.prototype.updateMetadata = function(e) {
r.validate("updateMetadata", [ r.metadataSpec() ], arguments);
this.throwIfRoot_("updateMetadata");
var t = this;
return this.authWrapper.getAuthToken().then(function(n) {
var r = l.updateMetadata(t.authWrapper, t.location, e, t.mappings());
return t.authWrapper.makeRequest(r, n).getPromise();
});
};
e.prototype.getDownloadURL = function() {
r.validate("getDownloadURL", [], arguments);
this.throwIfRoot_("getDownloadURL");
return this.getMetadata().then(function(e) {
var t = e.downloadURLs[0];
if (p.isDef(t)) return t;
throw o.noDownloadURL();
});
};
e.prototype.throwIfRoot_ = function(e) {
if ("" === this.location.path) throw o.invalidRootOperation(e);
};
return e;
}();
n.Reference = v;
}, {
"./implementation/args": 103,
"./implementation/blob": 108,
"./implementation/error": 110,
"./implementation/location": 114,
"./implementation/metadata": 115,
"./implementation/object": 116,
"./implementation/path": 118,
"./implementation/requests": 123,
"./implementation/string": 124,
"./implementation/type": 126,
"./task": 133
} ],
132: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./implementation/args"), i = e("./implementation/authwrapper"), o = e("./implementation/location"), a = e("./implementation/promise_external"), s = e("./implementation/request"), u = e("./reference"), c = function() {
function e(e, t, n) {
this.bucket_ = null;
this.authWrapper_ = new i.AuthWrapper(e, function(e, t) {
return new u.Reference(e, t);
}, s.makeRequest, this, t);
this.app_ = e;
if (null != n) this.bucket_ = o.Location.makeFromBucketSpec(n); else {
var r = this.authWrapper_.bucket();
null != r && (this.bucket_ = new o.Location(r, ""));
}
this.internals_ = new l(this);
}
e.prototype.ref = function(e) {
r.validate("ref", [ r.stringSpec(function(e) {
if (/^[A-Za-z]+:\/\//.test(e)) throw "Expected child path but got a URL, use refFromURL instead.";
}, !0) ], arguments);
if (null == this.bucket_) throw new Error("No Storage Bucket defined in Firebase Options.");
var t = new u.Reference(this.authWrapper_, this.bucket_);
return null != e ? t.child(e) : t;
};
e.prototype.refFromURL = function(e) {
r.validate("refFromURL", [ r.stringSpec(function(e) {
if (!/^[A-Za-z]+:\/\//.test(e)) throw "Expected full URL but got a child path, use ref instead.";
try {
o.Location.makeFromUrl(e);
} catch (e) {
throw "Expected valid full URL but got an invalid one.";
}
}, !1) ], arguments);
return new u.Reference(this.authWrapper_, e);
};
Object.defineProperty(e.prototype, "maxUploadRetryTime", {
get: function() {
return this.authWrapper_.maxUploadRetryTime();
},
enumerable: !0,
configurable: !0
});
e.prototype.setMaxUploadRetryTime = function(e) {
r.validate("setMaxUploadRetryTime", [ r.nonNegativeNumberSpec() ], arguments);
this.authWrapper_.setMaxUploadRetryTime(e);
};
Object.defineProperty(e.prototype, "maxOperationRetryTime", {
get: function() {
return this.authWrapper_.maxOperationRetryTime();
},
enumerable: !0,
configurable: !0
});
e.prototype.setMaxOperationRetryTime = function(e) {
r.validate("setMaxOperationRetryTime", [ r.nonNegativeNumberSpec() ], arguments);
this.authWrapper_.setMaxOperationRetryTime(e);
};
Object.defineProperty(e.prototype, "app", {
get: function() {
return this.app_;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(e.prototype, "INTERNAL", {
get: function() {
return this.internals_;
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.Service = c;
var l = function() {
function e(e) {
this.service_ = e;
}
e.prototype.delete = function() {
this.service_.authWrapper_.deleteApp();
return a.resolve(void 0);
};
return e;
}();
n.ServiceInternals = l;
}, {
"./implementation/args": 103,
"./implementation/authwrapper": 106,
"./implementation/location": 114,
"./implementation/promise_external": 119,
"./implementation/request": 120,
"./reference": 131
} ],
133: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./implementation/taskenums"), i = e("./implementation/observer"), o = e("./implementation/taskenums"), a = e("./tasksnapshot"), s = e("./implementation/args"), u = e("./implementation/array"), c = e("./implementation/async"), l = e("./implementation/error"), h = e("./implementation/promise_external"), f = e("./implementation/requests"), p = e("./implementation/taskenums"), d = e("./implementation/type"), v = function() {
function e(e, t, n, i, o, a) {
void 0 === a && (a = null);
var s = this;
this.transferred_ = 0;
this.needToFetchStatus_ = !1;
this.needToFetchMetadata_ = !1;
this.observers_ = [];
this.error_ = null;
this.uploadUrl_ = null;
this.request_ = null;
this.chunkMultiplier_ = 1;
this.resolve_ = null;
this.reject_ = null;
this.ref_ = e;
this.authWrapper_ = t;
this.location_ = n;
this.blob_ = o;
this.metadata_ = a;
this.mappings_ = i;
this.resumable_ = this.shouldDoResumable_(this.blob_);
this.state_ = r.InternalTaskState.RUNNING;
this.errorHandler_ = function(e) {
s.request_ = null;
s.chunkMultiplier_ = 1;
if (e.codeEquals(l.Code.CANCELED)) {
s.needToFetchStatus_ = !0;
s.completeTransitions_();
} else {
s.error_ = e;
s.transition_(r.InternalTaskState.ERROR);
}
};
this.metadataErrorHandler_ = function(e) {
s.request_ = null;
if (e.codeEquals(l.Code.CANCELED)) s.completeTransitions_(); else {
s.error_ = e;
s.transition_(r.InternalTaskState.ERROR);
}
};
this.promise_ = h.make(function(e, t) {
s.resolve_ = e;
s.reject_ = t;
s.start_();
});
this.promise_.then(null, function() {});
}
e.prototype.makeProgressCallback_ = function() {
var e = this, t = this.transferred_;
return function(n, r) {
e.updateProgress_(t + n);
};
};
e.prototype.shouldDoResumable_ = function(e) {
return e.size() > 262144;
};
e.prototype.start_ = function() {
this.state_ === r.InternalTaskState.RUNNING && null === this.request_ && (this.resumable_ ? null === this.uploadUrl_ ? this.createResumable_() : this.needToFetchStatus_ ? this.fetchStatus_() : this.needToFetchMetadata_ ? this.fetchMetadata_() : this.continueUpload_() : this.oneShotUpload_());
};
e.prototype.resolveToken_ = function(e) {
var t = this;
this.authWrapper_.getAuthToken().then(function(n) {
switch (t.state_) {
case r.InternalTaskState.RUNNING:
e(n);
break;

case r.InternalTaskState.CANCELING:
t.transition_(r.InternalTaskState.CANCELED);
break;

case r.InternalTaskState.PAUSING:
t.transition_(r.InternalTaskState.PAUSED);
}
});
};
e.prototype.createResumable_ = function() {
var e = this;
this.resolveToken_(function(t) {
var n = f.createResumableUpload(e.authWrapper_, e.location_, e.mappings_, e.blob_, e.metadata_), r = e.authWrapper_.makeRequest(n, t);
e.request_ = r;
r.getPromise().then(function(t) {
e.request_ = null;
e.uploadUrl_ = t;
e.needToFetchStatus_ = !1;
e.completeTransitions_();
}, e.errorHandler_);
});
};
e.prototype.fetchStatus_ = function() {
var e = this, t = this.uploadUrl_;
this.resolveToken_(function(n) {
var r = f.getResumableUploadStatus(e.authWrapper_, e.location_, t, e.blob_), i = e.authWrapper_.makeRequest(r, n);
e.request_ = i;
i.getPromise().then(function(t) {
t = t;
e.request_ = null;
e.updateProgress_(t.current);
e.needToFetchStatus_ = !1;
t.finalized && (e.needToFetchMetadata_ = !0);
e.completeTransitions_();
}, e.errorHandler_);
});
};
e.prototype.continueUpload_ = function() {
var e = this, t = f.resumableUploadChunkSize * this.chunkMultiplier_, n = new f.ResumableUploadStatus(this.transferred_, this.blob_.size()), i = this.uploadUrl_;
this.resolveToken_(function(o) {
var a;
try {
a = f.continueResumableUpload(e.location_, e.authWrapper_, i, e.blob_, t, e.mappings_, n, e.makeProgressCallback_());
} catch (t) {
e.error_ = t;
e.transition_(r.InternalTaskState.ERROR);
return;
}
var s = e.authWrapper_.makeRequest(a, o);
e.request_ = s;
s.getPromise().then(function(t) {
e.increaseMultiplier_();
e.request_ = null;
e.updateProgress_(t.current);
if (t.finalized) {
e.metadata_ = t.metadata;
e.transition_(r.InternalTaskState.SUCCESS);
} else e.completeTransitions_();
}, e.errorHandler_);
});
};
e.prototype.increaseMultiplier_ = function() {
f.resumableUploadChunkSize * this.chunkMultiplier_ < 33554432 && (this.chunkMultiplier_ *= 2);
};
e.prototype.fetchMetadata_ = function() {
var e = this;
this.resolveToken_(function(t) {
var n = f.getMetadata(e.authWrapper_, e.location_, e.mappings_), i = e.authWrapper_.makeRequest(n, t);
e.request_ = i;
i.getPromise().then(function(t) {
e.request_ = null;
e.metadata_ = t;
e.transition_(r.InternalTaskState.SUCCESS);
}, e.metadataErrorHandler_);
});
};
e.prototype.oneShotUpload_ = function() {
var e = this;
this.resolveToken_(function(t) {
var n = f.multipartUpload(e.authWrapper_, e.location_, e.mappings_, e.blob_, e.metadata_), i = e.authWrapper_.makeRequest(n, t);
e.request_ = i;
i.getPromise().then(function(t) {
e.request_ = null;
e.metadata_ = t;
e.updateProgress_(e.blob_.size());
e.transition_(r.InternalTaskState.SUCCESS);
}, e.errorHandler_);
});
};
e.prototype.updateProgress_ = function(e) {
var t = this.transferred_;
this.transferred_ = e;
this.transferred_ !== t && this.notifyObservers_();
};
e.prototype.transition_ = function(e) {
if (this.state_ !== e) switch (e) {
case r.InternalTaskState.CANCELING:
case r.InternalTaskState.PAUSING:
this.state_ = e;
null !== this.request_ && this.request_.cancel();
break;

case r.InternalTaskState.RUNNING:
var t = this.state_ === r.InternalTaskState.PAUSED;
this.state_ = e;
if (t) {
this.notifyObservers_();
this.start_();
}
break;

case r.InternalTaskState.PAUSED:
this.state_ = e;
this.notifyObservers_();
break;

case r.InternalTaskState.CANCELED:
this.error_ = l.canceled();
this.state_ = e;
this.notifyObservers_();
break;

case r.InternalTaskState.ERROR:
case r.InternalTaskState.SUCCESS:
this.state_ = e;
this.notifyObservers_();
}
};
e.prototype.completeTransitions_ = function() {
switch (this.state_) {
case r.InternalTaskState.PAUSING:
this.transition_(r.InternalTaskState.PAUSED);
break;

case r.InternalTaskState.CANCELING:
this.transition_(r.InternalTaskState.CANCELED);
break;

case r.InternalTaskState.RUNNING:
this.start_();
}
};
Object.defineProperty(e.prototype, "snapshot", {
get: function() {
var e = p.taskStateFromInternalTaskState(this.state_);
return new a.UploadTaskSnapshot(this.transferred_, this.blob_.size(), e, this.metadata_, this, this.ref_);
},
enumerable: !0,
configurable: !0
});
e.prototype.on = function(e, t, n, r) {
function a(e) {
try {
l(e);
return;
} catch (e) {}
try {
h(e);
if (!(d.isJustDef(e.next) || d.isJustDef(e.error) || d.isJustDef(e.complete))) throw "";
return;
} catch (e) {
throw c;
}
}
function u(e) {
return function(t, n, o) {
null !== e && s.validate("on", e, arguments);
var a = new i.Observer(t, n, r);
p.addObserver_(a);
return function() {
p.removeObserver_(a);
};
};
}
void 0 === t && (t = void 0);
void 0 === n && (n = void 0);
void 0 === r && (r = void 0);
var c = "Expected a function or an Object with one of `next`, `error`, `complete` properties.", l = s.nullFunctionSpec(!0).validator, h = s.looseObjectSpec(null, !0).validator, f = [ s.stringSpec(function(t) {
if (e !== o.TaskEvent.STATE_CHANGED) throw "Expected one of the event types: [" + o.TaskEvent.STATE_CHANGED + "].";
}), s.looseObjectSpec(a, !0), s.nullFunctionSpec(!0), s.nullFunctionSpec(!0) ];
s.validate("on", f, arguments);
var p = this, v = [ s.looseObjectSpec(function(e) {
if (null === e) throw c;
a(e);
}), s.nullFunctionSpec(!0), s.nullFunctionSpec(!0) ];
return !(d.isJustDef(t) || d.isJustDef(n) || d.isJustDef(r)) ? u(v) : u(null)(t, n, r);
};
e.prototype.then = function(e, t) {
return this.promise_.then(e, t);
};
e.prototype.catch = function(e) {
return this.then(null, e);
};
e.prototype.addObserver_ = function(e) {
this.observers_.push(e);
this.notifyObserver_(e);
};
e.prototype.removeObserver_ = function(e) {
u.remove(this.observers_, e);
};
e.prototype.notifyObservers_ = function() {
var e = this;
this.finishPromise_();
u.clone(this.observers_).forEach(function(t) {
e.notifyObserver_(t);
});
};
e.prototype.finishPromise_ = function() {
if (null !== this.resolve_) {
var e = !0;
switch (p.taskStateFromInternalTaskState(this.state_)) {
case o.TaskState.SUCCESS:
c.async(this.resolve_.bind(null, this.snapshot))();
break;

case o.TaskState.CANCELED:
case o.TaskState.ERROR:
var t = this.reject_;
c.async(t.bind(null, this.error_))();
break;

default:
e = !1;
}
if (e) {
this.resolve_ = null;
this.reject_ = null;
}
}
};
e.prototype.notifyObserver_ = function(e) {
switch (p.taskStateFromInternalTaskState(this.state_)) {
case o.TaskState.RUNNING:
case o.TaskState.PAUSED:
null !== e.next && c.async(e.next.bind(e, this.snapshot))();
break;

case o.TaskState.SUCCESS:
null !== e.complete && c.async(e.complete.bind(e))();
break;

case o.TaskState.CANCELED:
case o.TaskState.ERROR:
null !== e.error && c.async(e.error.bind(e, this.error_))();
break;

default:
null !== e.error && c.async(e.error.bind(e, this.error_))();
}
};
e.prototype.resume = function() {
s.validate("resume", [], arguments);
var e = this.state_ === r.InternalTaskState.PAUSED || this.state_ === r.InternalTaskState.PAUSING;
e && this.transition_(r.InternalTaskState.RUNNING);
return e;
};
e.prototype.pause = function() {
s.validate("pause", [], arguments);
var e = this.state_ === r.InternalTaskState.RUNNING;
e && this.transition_(r.InternalTaskState.PAUSING);
return e;
};
e.prototype.cancel = function() {
s.validate("cancel", [], arguments);
var e = this.state_ === r.InternalTaskState.RUNNING || this.state_ === r.InternalTaskState.PAUSING;
e && this.transition_(r.InternalTaskState.CANCELING);
return e;
};
return e;
}();
n.UploadTask = v;
}, {
"./implementation/args": 103,
"./implementation/array": 104,
"./implementation/async": 105,
"./implementation/error": 110,
"./implementation/observer": 117,
"./implementation/promise_external": 119,
"./implementation/requests": 123,
"./implementation/taskenums": 125,
"./implementation/type": 126,
"./tasksnapshot": 134
} ],
134: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e(e, t, n, r, i, o) {
this.bytesTransferred = e;
this.totalBytes = t;
this.state = n;
this.metadata = r;
this.task = i;
this.ref = o;
}
Object.defineProperty(e.prototype, "downloadURL", {
get: function() {
if (null !== this.metadata) {
var e = this.metadata.downloadURLs;
return null != e && null != e[0] ? e[0] : null;
}
return null;
},
enumerable: !0,
configurable: !0
});
return e;
}();
n.UploadTaskSnapshot = r;
}, {} ],
135: [ function(e, t, n) {
"use strict";
function r(e) {
for (var t in e) n.hasOwnProperty(t) || (n[t] = e[t]);
}
Object.defineProperty(n, "__esModule", {
value: !0
});
r(e("./src/assert"));
r(e("./src/crypt"));
r(e("./src/constants"));
r(e("./src/deepCopy"));
r(e("./src/deferred"));
r(e("./src/environment"));
r(e("./src/errors"));
r(e("./src/json"));
r(e("./src/jwt"));
r(e("./src/obj"));
r(e("./src/query"));
r(e("./src/sha1"));
r(e("./src/subscribe"));
r(e("./src/validation"));
r(e("./src/utf8"));
}, {
"./src/assert": 136,
"./src/constants": 137,
"./src/crypt": 138,
"./src/deepCopy": 139,
"./src/deferred": 140,
"./src/environment": 141,
"./src/errors": 142,
"./src/json": 144,
"./src/jwt": 145,
"./src/obj": 146,
"./src/query": 147,
"./src/sha1": 148,
"./src/subscribe": 149,
"./src/utf8": 150,
"./src/validation": 151
} ],
136: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./constants");
n.assert = function(e, t) {
if (!e) throw n.assertionError(t);
};
n.assertionError = function(e) {
return new Error("Firebase Database (" + r.CONSTANTS.SDK_VERSION + ") INTERNAL ASSERT FAILED: " + e);
};
}, {
"./constants": 137
} ],
137: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.CONSTANTS = {
NODE_CLIENT: !1,
NODE_ADMIN: !1,
SDK_VERSION: "${JSCORE_VERSION}"
};
}, {} ],
138: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function(e) {
for (var t = [], n = 0, r = 0; r < e.length; r++) {
var i = e.charCodeAt(r);
if (i < 128) t[n++] = i; else if (i < 2048) {
t[n++] = i >> 6 | 192;
t[n++] = 63 & i | 128;
} else if (55296 == (64512 & i) && r + 1 < e.length && 56320 == (64512 & e.charCodeAt(r + 1))) {
i = 65536 + ((1023 & i) << 10) + (1023 & e.charCodeAt(++r));
t[n++] = i >> 18 | 240;
t[n++] = i >> 12 & 63 | 128;
t[n++] = i >> 6 & 63 | 128;
t[n++] = 63 & i | 128;
} else {
t[n++] = i >> 12 | 224;
t[n++] = i >> 6 & 63 | 128;
t[n++] = 63 & i | 128;
}
}
return t;
}, i = function(e) {
for (var t = [], n = 0, r = 0; n < e.length; ) {
var i = e[n++];
if (i < 128) t[r++] = String.fromCharCode(i); else if (i > 191 && i < 224) {
a = e[n++];
t[r++] = String.fromCharCode((31 & i) << 6 | 63 & a);
} else if (i > 239 && i < 365) {
var o = ((7 & i) << 18 | (63 & (a = e[n++])) << 12 | (63 & (s = e[n++])) << 6 | 63 & e[n++]) - 65536;
t[r++] = String.fromCharCode(55296 + (o >> 10));
t[r++] = String.fromCharCode(56320 + (1023 & o));
} else {
var a = e[n++], s = e[n++];
t[r++] = String.fromCharCode((15 & i) << 12 | (63 & a) << 6 | 63 & s);
}
}
return t.join("");
};
n.base64 = {
byteToCharMap_: null,
charToByteMap_: null,
byteToCharMapWebSafe_: null,
charToByteMapWebSafe_: null,
ENCODED_VALS_BASE: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
get ENCODED_VALS() {
return this.ENCODED_VALS_BASE + "+/=";
},
get ENCODED_VALS_WEBSAFE() {
return this.ENCODED_VALS_BASE + "-_.";
},
HAS_NATIVE_SUPPORT: "function" == typeof atob,
encodeByteArray: function(e, t) {
if (!Array.isArray(e)) throw Error("encodeByteArray takes an array as a parameter");
this.init_();
for (var n = t ? this.byteToCharMapWebSafe_ : this.byteToCharMap_, r = [], i = 0; i < e.length; i += 3) {
var o = e[i], a = i + 1 < e.length, s = a ? e[i + 1] : 0, u = i + 2 < e.length, c = u ? e[i + 2] : 0, l = o >> 2, h = (3 & o) << 4 | s >> 4, f = (15 & s) << 2 | c >> 6, p = 63 & c;
if (!u) {
p = 64;
a || (f = 64);
}
r.push(n[l], n[h], n[f], n[p]);
}
return r.join("");
},
encodeString: function(e, t) {
return this.HAS_NATIVE_SUPPORT && !t ? btoa(e) : this.encodeByteArray(r(e), t);
},
decodeString: function(e, t) {
return this.HAS_NATIVE_SUPPORT && !t ? atob(e) : i(this.decodeStringToByteArray(e, t));
},
decodeStringToByteArray: function(e, t) {
this.init_();
for (var n = t ? this.charToByteMapWebSafe_ : this.charToByteMap_, r = [], i = 0; i < e.length; ) {
var o = n[e.charAt(i++)], a = i < e.length ? n[e.charAt(i)] : 0, s = ++i < e.length ? n[e.charAt(i)] : 64, u = ++i < e.length ? n[e.charAt(i)] : 64;
++i;
if (null == o || null == a || null == s || null == u) throw Error();
var c = o << 2 | a >> 4;
r.push(c);
if (64 != s) {
var l = a << 4 & 240 | s >> 2;
r.push(l);
if (64 != u) {
var h = s << 6 & 192 | u;
r.push(h);
}
}
}
return r;
},
init_: function() {
if (!this.byteToCharMap_) {
this.byteToCharMap_ = {};
this.charToByteMap_ = {};
this.byteToCharMapWebSafe_ = {};
this.charToByteMapWebSafe_ = {};
for (var e = 0; e < this.ENCODED_VALS.length; e++) {
this.byteToCharMap_[e] = this.ENCODED_VALS.charAt(e);
this.charToByteMap_[this.byteToCharMap_[e]] = e;
this.byteToCharMapWebSafe_[e] = this.ENCODED_VALS_WEBSAFE.charAt(e);
this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[e]] = e;
if (e >= this.ENCODED_VALS_BASE.length) {
this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(e)] = e;
this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(e)] = e;
}
}
}
}
};
n.base64Encode = function(e) {
var t = r(e);
return n.base64.encodeByteArray(t, !0);
};
n.base64Decode = function(e) {
try {
return n.base64.decodeString(e, !0);
} catch (e) {
console.error("base64Decode failed: ", e);
}
return null;
};
}, {} ],
139: [ function(e, t, n) {
"use strict";
function r(e, t) {
if (!(t instanceof Object)) return t;
switch (t.constructor) {
case Date:
var n = t;
return new Date(n.getTime());

case Object:
void 0 === e && (e = {});
break;

case Array:
e = [];
break;

default:
return t;
}
for (var i in t) t.hasOwnProperty(i) && (e[i] = r(e[i], t[i]));
return e;
}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.deepCopy = function(e) {
return r(void 0, e);
};
n.deepExtend = r;
n.patchProperty = function(e, t, n) {
e[t] = n;
};
}, {} ],
140: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {
var e = this;
this.promise = new Promise(function(t, n) {
e.resolve = t;
e.reject = n;
});
}
e.prototype.wrapCallback = function(e) {
var t = this;
return function(n, r) {
n ? t.reject(n) : t.resolve(r);
if ("function" == typeof e) {
t.promise.catch(function() {});
1 === e.length ? e(n) : e(n, r);
}
};
};
return e;
}();
n.Deferred = r;
}, {} ],
141: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./constants");
n.getUA = function() {
return "undefined" != typeof navigator && "string" == typeof navigator.userAgent ? navigator.userAgent : "";
};
n.isMobileCordova = function() {
return "undefined" != typeof window && !!(window.cordova || window.phonegap || window.PhoneGap) && /ios|iphone|ipod|ipad|android|blackberry|iemobile/i.test(n.getUA());
};
n.isReactNative = function() {
return "object" == typeof navigator && "ReactNative" === navigator.product;
};
n.isNodeSdk = function() {
return !0 === r.CONSTANTS.NODE_CLIENT || !0 === r.CONSTANTS.NODE_ADMIN;
};
}, {
"./constants": 137
} ],
142: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = "FirebaseError", i = Error.captureStackTrace;
n.patchCapture = function(e) {
var t = i;
i = e;
return t;
};
var o = function() {
return function(e, t) {
this.code = e;
this.message = t;
if (i) i(this, a.prototype.create); else {
var n = Error.apply(this, arguments);
this.name = r;
Object.defineProperty(this, "stack", {
get: function() {
return n.stack;
}
});
}
};
}();
n.FirebaseError = o;
o.prototype = Object.create(Error.prototype);
o.prototype.constructor = o;
o.prototype.name = r;
var a = function() {
function e(e, t, n) {
this.service = e;
this.serviceName = t;
this.errors = n;
this.pattern = /\{\$([^}]+)}/g;
}
e.prototype.create = function(e, t) {
void 0 === t && (t = {});
var n, r = this.errors[e], i = this.service + "/" + e;
n = void 0 === r ? "Error" : r.replace(this.pattern, function(e, n) {
var r = t[n];
return void 0 !== r ? r.toString() : "<" + n + "?>";
});
n = this.serviceName + ": " + n + " (" + i + ").";
var a = new o(i, n);
for (var s in t) t.hasOwnProperty(s) && "_" !== s.slice(-1) && (a[s] = t[s]);
return a;
};
return e;
}();
n.ErrorFactory = a;
}, {} ],
143: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
return function() {
this.blockSize = -1;
};
}();
n.Hash = r;
}, {} ],
144: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.jsonEval = function(e) {
return JSON.parse(e);
};
n.stringify = function(e) {
return JSON.stringify(e);
};
}, {} ],
145: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./crypt"), i = e("./json");
n.decode = function(e) {
var t = {}, n = {}, o = {}, a = "";
try {
var s = e.split(".");
t = i.jsonEval(r.base64Decode(s[0]) || "");
n = i.jsonEval(r.base64Decode(s[1]) || "");
a = s[2];
o = n.d || {};
delete n.d;
} catch (e) {}
return {
header: t,
claims: n,
data: o,
signature: a
};
};
n.isValidTimestamp = function(e) {
var t, r, i = n.decode(e).claims, o = Math.floor(new Date().getTime() / 1e3);
if ("object" == typeof i) {
i.hasOwnProperty("nbf") ? t = i.nbf : i.hasOwnProperty("iat") && (t = i.iat);
r = i.hasOwnProperty("exp") ? i.exp : t + 86400;
}
return o && t && r && o >= t && o <= r;
};
n.issuedAtTime = function(e) {
var t = n.decode(e).claims;
return "object" == typeof t && t.hasOwnProperty("iat") ? t.iat : null;
};
n.isValidFormat = function(e) {
var t = n.decode(e), r = t.claims;
return !!t.signature && !!r && "object" == typeof r && r.hasOwnProperty("iat");
};
n.isAdmin = function(e) {
var t = n.decode(e).claims;
return "object" == typeof t && !0 === t.admin;
};
}, {
"./crypt": 138,
"./json": 144
} ],
146: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
n.contains = function(e, t) {
return Object.prototype.hasOwnProperty.call(e, t);
};
n.safeGet = function(e, t) {
if (Object.prototype.hasOwnProperty.call(e, t)) return e[t];
};
n.forEach = function(e, t) {
for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && t(n, e[n]);
};
n.extend = function(e, t) {
n.forEach(t, function(t, n) {
e[t] = n;
});
return e;
};
n.clone = function(e) {
return n.extend({}, e);
};
n.isNonNullObject = function(e) {
return "object" == typeof e && null !== e;
};
n.isEmpty = function(e) {
for (var t in e) return !1;
return !0;
};
n.getCount = function(e) {
var t = 0;
for (var n in e) t++;
return t;
};
n.map = function(e, t, n) {
var r = {};
for (var i in e) r[i] = t.call(n, e[i], i, e);
return r;
};
n.findKey = function(e, t, n) {
for (var r in e) if (t.call(n, e[r], r, e)) return r;
};
n.findValue = function(e, t, r) {
var i = n.findKey(e, t, r);
return i && e[i];
};
n.getAnyKey = function(e) {
for (var t in e) return t;
};
n.getValues = function(e) {
var t = [], n = 0;
for (var r in e) t[n++] = e[r];
return t;
};
n.every = function(e, t) {
for (var n in e) if (Object.prototype.hasOwnProperty.call(e, n) && !t(n, e[n])) return !1;
return !0;
};
}, {} ],
147: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./obj");
n.querystring = function(e) {
var t = [];
r.forEach(e, function(e, n) {
Array.isArray(n) ? n.forEach(function(n) {
t.push(encodeURIComponent(e) + "=" + encodeURIComponent(n));
}) : t.push(encodeURIComponent(e) + "=" + encodeURIComponent(n));
});
return t.length ? "&" + t.join("&") : "";
};
n.querystringDecode = function(e) {
var t = {};
e.replace(/^\?/, "").split("&").forEach(function(e) {
if (e) {
var n = e.split("=");
t[n[0]] = n[1];
}
});
return t;
};
}, {
"./obj": 146
} ],
148: [ function(e, t, n) {
"use strict";
var r = this && this.__extends || function() {
var e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
};
return function(t, n) {
function r() {
this.constructor = t;
}
e(t, n);
t.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
}();
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = function(e) {
function t() {
var t = e.call(this) || this;
t.chain_ = [];
t.buf_ = [];
t.W_ = [];
t.pad_ = [];
t.inbuf_ = 0;
t.total_ = 0;
t.blockSize = 64;
t.pad_[0] = 128;
for (var n = 1; n < t.blockSize; ++n) t.pad_[n] = 0;
t.reset();
return t;
}
r(t, e);
t.prototype.reset = function() {
this.chain_[0] = 1732584193;
this.chain_[1] = 4023233417;
this.chain_[2] = 2562383102;
this.chain_[3] = 271733878;
this.chain_[4] = 3285377520;
this.inbuf_ = 0;
this.total_ = 0;
};
t.prototype.compress_ = function(e, t) {
t || (t = 0);
var n = this.W_;
if ("string" == typeof e) for (l = 0; l < 16; l++) {
n[l] = e.charCodeAt(t) << 24 | e.charCodeAt(t + 1) << 16 | e.charCodeAt(t + 2) << 8 | e.charCodeAt(t + 3);
t += 4;
} else for (l = 0; l < 16; l++) {
n[l] = e[t] << 24 | e[t + 1] << 16 | e[t + 2] << 8 | e[t + 3];
t += 4;
}
for (l = 16; l < 80; l++) {
h = n[l - 3] ^ n[l - 8] ^ n[l - 14] ^ n[l - 16];
n[l] = 4294967295 & (h << 1 | h >>> 31);
}
for (var r, i, o = this.chain_[0], a = this.chain_[1], s = this.chain_[2], u = this.chain_[3], c = this.chain_[4], l = 0; l < 80; l++) {
if (l < 40) if (l < 20) {
r = u ^ a & (s ^ u);
i = 1518500249;
} else {
r = a ^ s ^ u;
i = 1859775393;
} else if (l < 60) {
r = a & s | u & (a | s);
i = 2400959708;
} else {
r = a ^ s ^ u;
i = 3395469782;
}
var h = (o << 5 | o >>> 27) + r + c + i + n[l] & 4294967295;
c = u;
u = s;
s = 4294967295 & (a << 30 | a >>> 2);
a = o;
o = h;
}
this.chain_[0] = this.chain_[0] + o & 4294967295;
this.chain_[1] = this.chain_[1] + a & 4294967295;
this.chain_[2] = this.chain_[2] + s & 4294967295;
this.chain_[3] = this.chain_[3] + u & 4294967295;
this.chain_[4] = this.chain_[4] + c & 4294967295;
};
t.prototype.update = function(e, t) {
if (null != e) {
void 0 === t && (t = e.length);
for (var n = t - this.blockSize, r = 0, i = this.buf_, o = this.inbuf_; r < t; ) {
if (0 == o) for (;r <= n; ) {
this.compress_(e, r);
r += this.blockSize;
}
if ("string" == typeof e) for (;r < t; ) {
i[o] = e.charCodeAt(r);
++r;
if (++o == this.blockSize) {
this.compress_(i);
o = 0;
break;
}
} else for (;r < t; ) {
i[o] = e[r];
++r;
if (++o == this.blockSize) {
this.compress_(i);
o = 0;
break;
}
}
}
this.inbuf_ = o;
this.total_ += t;
}
};
t.prototype.digest = function() {
var e = [], t = 8 * this.total_;
this.inbuf_ < 56 ? this.update(this.pad_, 56 - this.inbuf_) : this.update(this.pad_, this.blockSize - (this.inbuf_ - 56));
for (r = this.blockSize - 1; r >= 56; r--) {
this.buf_[r] = 255 & t;
t /= 256;
}
this.compress_(this.buf_);
for (var n = 0, r = 0; r < 5; r++) for (var i = 24; i >= 0; i -= 8) {
e[n] = this.chain_[r] >> i & 255;
++n;
}
return e;
};
return t;
}(e("./hash").Hash);
n.Sha1 = i;
}, {
"./hash": 143
} ],
149: [ function(e, t, n) {
"use strict";
function r(e, t) {
if ("object" != typeof e || null === e) return !1;
for (var n = 0, r = t; n < r.length; n++) {
var i = r[n];
if (i in e && "function" == typeof e[i]) return !0;
}
return !1;
}
function i() {}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.createSubscribe = function(e, t) {
var n = new o(e, t);
return n.subscribe.bind(n);
};
var o = function() {
function e(e, t) {
var n = this;
this.observers = [];
this.unsubscribes = [];
this.observerCount = 0;
this.task = Promise.resolve();
this.finalized = !1;
this.onNoObservers = t;
this.task.then(function() {
e(n);
}).catch(function(e) {
n.error(e);
});
}
e.prototype.next = function(e) {
this.forEachObserver(function(t) {
t.next(e);
});
};
e.prototype.error = function(e) {
this.forEachObserver(function(t) {
t.error(e);
});
this.close(e);
};
e.prototype.complete = function() {
this.forEachObserver(function(e) {
e.complete();
});
this.close();
};
e.prototype.subscribe = function(e, t, n) {
var o, a = this;
if (void 0 === e && void 0 === t && void 0 === n) throw new Error("Missing Observer.");
void 0 === (o = r(e, [ "next", "error", "complete" ]) ? e : {
next: e,
error: t,
complete: n
}).next && (o.next = i);
void 0 === o.error && (o.error = i);
void 0 === o.complete && (o.complete = i);
var s = this.unsubscribeOne.bind(this, this.observers.length);
this.finalized && this.task.then(function() {
try {
a.finalError ? o.error(a.finalError) : o.complete();
} catch (e) {}
});
this.observers.push(o);
return s;
};
e.prototype.unsubscribeOne = function(e) {
if (void 0 !== this.observers && void 0 !== this.observers[e]) {
delete this.observers[e];
this.observerCount -= 1;
0 === this.observerCount && void 0 !== this.onNoObservers && this.onNoObservers(this);
}
};
e.prototype.forEachObserver = function(e) {
if (!this.finalized) for (var t = 0; t < this.observers.length; t++) this.sendOne(t, e);
};
e.prototype.sendOne = function(e, t) {
var n = this;
this.task.then(function() {
if (void 0 !== n.observers && void 0 !== n.observers[e]) try {
t(n.observers[e]);
} catch (e) {
"undefined" != typeof console && console.error && console.error(e);
}
});
};
e.prototype.close = function(e) {
var t = this;
if (!this.finalized) {
this.finalized = !0;
void 0 !== e && (this.finalError = e);
this.task.then(function() {
t.observers = void 0;
t.onNoObservers = void 0;
});
}
};
return e;
}();
n.async = function(e, t) {
return function() {
for (var n = [], r = 0; r < arguments.length; r++) n[r] = arguments[r];
Promise.resolve(!0).then(function() {
e.apply(void 0, n);
}).catch(function(e) {
t && t(e);
});
};
};
}, {} ],
150: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./assert");
n.stringToByteArray = function(e) {
for (var t = [], n = 0, i = 0; i < e.length; i++) {
var o = e.charCodeAt(i);
if (o >= 55296 && o <= 56319) {
var a = o - 55296;
i++;
r.assert(i < e.length, "Surrogate pair missing trail surrogate.");
o = 65536 + (a << 10) + (e.charCodeAt(i) - 56320);
}
if (o < 128) t[n++] = o; else if (o < 2048) {
t[n++] = o >> 6 | 192;
t[n++] = 63 & o | 128;
} else if (o < 65536) {
t[n++] = o >> 12 | 224;
t[n++] = o >> 6 & 63 | 128;
t[n++] = 63 & o | 128;
} else {
t[n++] = o >> 18 | 240;
t[n++] = o >> 12 & 63 | 128;
t[n++] = o >> 6 & 63 | 128;
t[n++] = 63 & o | 128;
}
}
return t;
};
n.stringLength = function(e) {
for (var t = 0, n = 0; n < e.length; n++) {
var r = e.charCodeAt(n);
if (r < 128) t++; else if (r < 2048) t += 2; else if (r >= 55296 && r <= 56319) {
t += 4;
n++;
} else t += 3;
}
return t;
};
}, {
"./assert": 136
} ],
151: [ function(e, t, n) {
"use strict";
function r(e, t, n) {
var r = "";
switch (t) {
case 1:
r = n ? "first" : "First";
break;

case 2:
r = n ? "second" : "Second";
break;

case 3:
r = n ? "third" : "Third";
break;

case 4:
r = n ? "fourth" : "Fourth";
break;

default:
throw new Error("errorPrefix called with argumentNumber > 4.  Need to update it?");
}
var i = e + " failed: ";
return i += r + " argument ";
}
Object.defineProperty(n, "__esModule", {
value: !0
});
n.validateArgCount = function(e, t, n, r) {
var i;
r < t ? i = "at least " + t : r > n && (i = 0 === n ? "none" : "no more than " + n);
if (i) {
var o = e + " failed: Was called with " + r + (1 === r ? " argument." : " arguments.") + " Expects " + i + ".";
throw new Error(o);
}
};
n.errorPrefix = r;
n.validateNamespace = function(e, t, n, i) {
if ((!i || n) && "string" != typeof n) throw new Error(r(e, t, i) + "must be a valid firebase namespace.");
};
n.validateCallback = function(e, t, n, i) {
if ((!i || n) && "function" != typeof n) throw new Error(r(e, t, i) + "must be a valid function.");
};
n.validateContextObject = function(e, t, n, i) {
if ((!i || n) && ("object" != typeof n || null === n)) throw new Error(r(e, t, i) + "must be a valid context object.");
};
}, {} ],
152: [ function(e, t, n) {
e("@firebase/polyfill");
t.exports = e("@firebase/app").default;
}, {
"@firebase/app": 2,
"@firebase/polyfill": 98
} ],
153: [ function(e, t, n) {
e("@firebase/auth");
}, {
"@firebase/auth": 4
} ],
154: [ function(e, t, n) {
t.exports = e("@firebase/database");
}, {
"@firebase/database": 5
} ],
155: [ function(e, t, n) {
var r = e("./app");
e("./auth");
e("./database");
e("./messaging");
e("./storage");
t.exports = r;
}, {
"./app": 152,
"./auth": 153,
"./database": 154,
"./messaging": 156,
"./storage": 157
} ],
156: [ function(e, t, n) {
e("@firebase/messaging");
}, {
"@firebase/messaging": 87
} ],
157: [ function(e, t, n) {
e("@firebase/storage");
}, {
"@firebase/storage": 102
} ],
158: [ function(e, t, n) {
!function(e) {
function n() {}
function r(e, t) {
return function() {
e.apply(t, arguments);
};
}
function i(e) {
if (!(this instanceof i)) throw new TypeError("Promises must be constructed via new");
if ("function" != typeof e) throw new TypeError("not a function");
this._state = 0;
this._handled = !1;
this._value = void 0;
this._deferreds = [];
l(e, this);
}
function o(e, t) {
for (;3 === e._state; ) e = e._value;
if (0 !== e._state) {
e._handled = !0;
i._immediateFn(function() {
var n = 1 === e._state ? t.onFulfilled : t.onRejected;
if (null !== n) {
var r;
try {
r = n(e._value);
} catch (e) {
s(t.promise, e);
return;
}
a(t.promise, r);
} else (1 === e._state ? a : s)(t.promise, e._value);
});
} else e._deferreds.push(t);
}
function a(e, t) {
try {
if (t === e) throw new TypeError("A promise cannot be resolved with itself.");
if (t && ("object" == typeof t || "function" == typeof t)) {
var n = t.then;
if (t instanceof i) {
e._state = 3;
e._value = t;
u(e);
return;
}
if ("function" == typeof n) {
l(r(n, t), e);
return;
}
}
e._state = 1;
e._value = t;
u(e);
} catch (t) {
s(e, t);
}
}
function s(e, t) {
e._state = 2;
e._value = t;
u(e);
}
function u(e) {
2 === e._state && 0 === e._deferreds.length && i._immediateFn(function() {
e._handled || i._unhandledRejectionFn(e._value);
});
for (var t = 0, n = e._deferreds.length; t < n; t++) o(e, e._deferreds[t]);
e._deferreds = null;
}
function c(e, t, n) {
this.onFulfilled = "function" == typeof e ? e : null;
this.onRejected = "function" == typeof t ? t : null;
this.promise = n;
}
function l(e, t) {
var n = !1;
try {
e(function(e) {
if (!n) {
n = !0;
a(t, e);
}
}, function(e) {
if (!n) {
n = !0;
s(t, e);
}
});
} catch (e) {
if (n) return;
n = !0;
s(t, e);
}
}
var h = setTimeout;
i.prototype.catch = function(e) {
return this.then(null, e);
};
i.prototype.then = function(e, t) {
var r = new this.constructor(n);
o(this, new c(e, t, r));
return r;
};
i.all = function(e) {
return new i(function(t, n) {
function r(e, a) {
try {
if (a && ("object" == typeof a || "function" == typeof a)) {
var s = a.then;
if ("function" == typeof s) {
s.call(a, function(t) {
r(e, t);
}, n);
return;
}
}
i[e] = a;
0 == --o && t(i);
} catch (e) {
n(e);
}
}
if (!e || "undefined" == typeof e.length) throw new TypeError("Promise.all accepts an array");
var i = Array.prototype.slice.call(e);
if (0 === i.length) return t([]);
for (var o = i.length, a = 0; a < i.length; a++) r(a, i[a]);
});
};
i.resolve = function(e) {
return e && "object" == typeof e && e.constructor === i ? e : new i(function(t) {
t(e);
});
};
i.reject = function(e) {
return new i(function(t, n) {
n(e);
});
};
i.race = function(e) {
return new i(function(t, n) {
for (var r = 0, i = e.length; r < i; r++) e[r].then(t, n);
});
};
i._immediateFn = "function" == typeof setImmediate && function(e) {
setImmediate(e);
} || function(e) {
h(e, 0);
};
i._unhandledRejectionFn = function(e) {
"undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e);
};
i._setImmediateFn = function(e) {
i._immediateFn = e;
};
i._setUnhandledRejectionFn = function(e) {
i._unhandledRejectionFn = e;
};
"undefined" != typeof t && t.exports ? t.exports = i : e.Promise || (e.Promise = i);
}(this);
}, {} ],
Constants: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7f3a9xXN3pEYa9Po9eR2cjE", "Constants");
window.Constants = {};
Constants.FIREBASE_CONFIG = {
apiKey: "AIzaSyCIaJG1iHYFU_bZsCvw9T-hPg7UmQk1hE8",
authDomain: "dancing-lighting.firebaseapp.com",
databaseURL: "https://dancing-lighting.firebaseio.com",
projectId: "dancing-lighting",
storageBucket: "",
messagingSenderId: "909429222302"
};
cc._RF.pop();
}, {} ],
GamePlayController: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2b832LNeA9DQabUiylJWArA", "GamePlayController");
e("firebase");
cc.Class({
extends: cc.Component,
properties: {
planetPrefab: cc.Prefab,
planetWithGlowPrefab: cc.Prefab,
linePrefab: cc.Prefab,
circlePrefab: cc.Prefab,
labelPrefab: cc.Prefab,
loopBackgroundPrefab: cc.Prefab,
planets: null,
lightCircleTimestamps: cc.Array,
planetParentNode: cc.Node,
glowFrame: cc.SpriteFrame,
soundPath: cc.AudioClip,
handNode: cc.Node,
isReady: !1,
levelEnded: !1,
levelStarted: !1,
songId: null,
currentIdx: 0,
currentScore: 0,
timeLeftToStart: 0,
time: 0,
basePlanet: cc.Node,
countdownLabel: cc.Label,
scoreLabel: cc.Label,
startNodes: cc.Node,
backgroundParentNode: cc.Node,
_activatedNodes: cc.Array,
_isTheFirstMatchDone: !1,
_winSize: null
},
onLoad: function() {
this._winSize = cc.director.getWinSize();
},
start: function() {
this.lightCircleTimestamps = [];
this._activatedNodes = [];
this.planets = [];
this.prevPlanet = this.basePlanet;
this.createPlanets();
this.createBackgrounds();
this.playStartNodeActions();
this.node.on(cc.Node.EventType.TOUCH_START, function() {
this.levelStarted || this.startGame();
if (this.levelEnded) this.resetGame(); else if (this.isReady) if (0 != this._activatedNodes.length) {
var e = this._activatedNodes[0];
this._activatedNodes.splice(0, 1);
e.getComponent(cc.Sprite).spriteFrame = this.glowFrame;
var t = o * VELOCITY * .2;
if (e.y < i + t && e.y > i - t) {
this.showResult("Perfect");
this.currentScore += 3;
} else {
this.showResult("Great");
this.currentScore++;
}
this.scoreLabel.string = this.currentScore;
var n = cc.v2(e.x - this.prevPlanet.x, e.y - this.prevPlanet.y), r = 180 * -cc.pToAngle(n) / Math.PI, a = cc.pLength(n);
this.createLightLine(this.prevPlanet, r, a);
this.prevPlanet = e;
this._isTheFirstMatchDone || (this._isTheFirstMatchDone = !0);
} else this.gameOver();
}.bind(this), this.node);
},
createLightLine: function(e, t, n) {
var r = cc.instantiate(this.linePrefab), i = n / r.width;
r.anchorX = 0;
r.scaleX = 0;
r.rotation = t;
e.addChild(r, -2);
var o = .2 / i;
r.runAction(cc.scaleTo(o, i, 1));
},
createPlanets: function() {
for (var e = 0; e < r.length; e++) {
var t = r[e], n = 320 * Math.random() + 100, o = i + VELOCITY * t, a = cc.instantiate(this.planetPrefab);
a.setPosition(n, o);
a.setTag(t);
this.planetParentNode.addChild(a);
this.planets.push(a);
}
},
createBackgrounds: function() {
var e = Math.ceil((i + VELOCITY * r[r.length - 1]) / this._winSize.height);
cc.log(e);
for (var t = 0; t <= e; t++) {
var n = cc.instantiate(this.loopBackgroundPrefab);
n.y = this._winSize.height * (t + 1);
this.backgroundParentNode.addChild(n);
}
},
update: function(e) {
var t = this.planetParentNode.getChildren(), n = this.backgroundParentNode.getChildren();
if (this.isReady) {
this.checkGameOver();
for (var r = i + o * VELOCITY, a = 0; a < t.length; a++) {
(a > 0 || this._isTheFirstMatchDone) && (t[a].y -= VELOCITY * e);
n[a] && (n[a].y -= VELOCITY * e);
var s = this.planets[a];
if (s && s.y <= r) {
this.planets.splice(a, 1);
this.activateNode(s);
}
}
0 == this.planets.length && 0 == this._activatedNodes.length && this.clearLevel();
}
},
resetGame: function() {
cc.audioEngine.stopAll();
cc.director.loadScene("GamePlayScene");
},
gameOver: function() {
cc.audioEngine.stopAll();
this.countdownLabel.string = "YOU LOSE!\nTRY AGAIN!";
this.isReady = !1;
this.levelEnded = !0;
this.countdownLabel.unscheduleAllCallbacks();
},
clearLevel: function() {
this.isReady = !1;
this.countdownLabel.string = "YOU WIN!\nTAP TO REPLAY";
this.levelEnded = !0;
},
startCountdownLabel: function() {
this.timeLeftToStart = 3;
this.countdownLabel.schedule(this.setCountdownLabelText.bind(this), 1, 2);
},
setCountdownLabelText: function() {
cc.log("setCountdownLabelText()");
this.timeLeftToStart <= 1 ? this.countdownLabel.string = "" : this.countdownLabel.string = --this.timeLeftToStart;
},
activateNode: function(e) {
var t = cc.instantiate(this.circlePrefab);
t.scale = 0;
t.setTag(a);
e.addChild(t, -2);
t.runAction(cc.sequence(cc.scaleTo(o, 1), cc.scaleTo(o, 0)));
this._activatedNodes.push(e);
},
showResult: function(e) {
var t = cc.instantiate(this.labelPrefab);
t.setPosition(0, -200);
t.getComponent(cc.Label).string = e;
this.node.addChild(t);
t.scale = 0;
var n = 1;
e.indexOf("Perfect") > -1 ? n = 2 : t.color = cc.color(255, 255, 255, 255);
t.runAction(cc.spawn(cc.sequence(cc.scaleTo(.25, n), cc.fadeTo(.25, 0)), cc.moveBy(.5, 0, 30)));
},
startGame: function() {
this.stopStartNodeActions();
this.levelStarted = !0;
this.startNodes.active = !1;
cc.loader.loadRes("Sounds/swan-lake", cc.AudioClip, function(e, t) {
this.countdownLabel.string = 3;
this.startCountdownLabel();
this.node.runAction(cc.sequence(cc.delayTime(3), cc.callFunc(function() {
this.isReady = !0;
this.songId = cc.audioEngine.play(t);
}.bind(this))));
}.bind(this));
},
playStartNodeActions: function() {
this.handNode.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.5, .95), cc.scaleTo(.5, 1.05))));
this.basePlanet.runAction(cc.repeatForever(cc.sequence(cc.fadeTo(.5, 180), cc.fadeTo(.5, 255))));
},
stopStartNodeActions: function() {
this.handNode.stopAllActions();
this.basePlanet.stopAllActions();
},
checkGameOver: function() {
var e = this._activatedNodes[0];
e && 0 == e.getChildByTag(a).getNumberOfRunningActions() && this.gameOver();
}
});
var r = [ 3.286, 4.723, 5.078, 5.44, 5.794, 6.148, 7.222, 7.589, 8.651, 9.004, 10.081, 10.432, 10.793, 11.136, 11.52, 11.903, 13.687, 14.037, 14.414, 14.737, 16.148, 16.5, 16.863, 17.24, 17.59, 18.64, 19.003, 20.075, 20.452, 21.515, 21.851, 22.228, 22.578, 22.928, 23.305, 25.445, 26.155, 26.863, 27.579, 28.29, 28.647, 29.005, 30.083, 30.445, 31.161, 31.507, 31.869, 32.94, 33.294, 34.018, 34.364, 34.719, 35.789, 36.159, 36.505, 36.876, 37.238, 37.576, 38.301, 39.014, 39.718, 40.081, 40.437, 41.507, 41.867, 42.577, 42.937, 43.296, 44.364, 44.72, 45.433, 45.79, 46.146, 47.223, 47.583, 48.298, 48.644, 49.004, 50.074, 50.43, 51.51, 51.86, 53.296, 53.653, 54.006, 54.369, 54.719, 55.792, 56.146, 57.219, 57.585, 58.656, 59.009, 59.366, 59.719, 60.075, 60.439 ], i = 200, o = .5, a = 100001;
window.VELOCITY = 400;
console.log("03011802");
console.log(i);
cc._RF.pop();
}, {
firebase: 155
} ],
PlanetController: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "29c12qgz/NKH4J7vfwFFop7", "PlanetController");
cc.Class({
extends: cc.Component,
properties: {
movingTime: cc.float
},
start: function() {},
update: function(e) {}
});
cc._RF.pop();
}, {} ]
}, {}, [ "Constants", "GamePlayController", "PlanetController" ]);